/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import org.genesys2.server.model.impl.Article;
import org.genesys2.server.service.ContentService;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/content")
public class ArticleController extends BaseController {

	@Autowired
	private ContentService contentService;

	@RequestMapping
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	public String list(ModelMap model, @RequestParam(value = "page", defaultValue = "1") int page) {
		model.addAttribute("pagedData", contentService.listArticles(new PageRequest(page - 1, 50, new Sort("slug"))));
		return "/content/index";
	}

	@RequestMapping("{url:.+}")
	public String view(ModelMap model, @PathVariable(value = "url") String slug) {
		_logger.debug("Viewing article " + slug);

		final Article article = contentService.getGlobalArticle(slug, getLocale());
		if (article == null) {
			if (hasRole("ADMINISTRATOR")) {
				return "redirect:/content/" + slug + "/edit";
			}
			throw new ResourceNotFoundException();
		}
		model.addAttribute("title", article.getTitle());
		model.addAttribute("article", article);

		return "/content/article";
	}

	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	@RequestMapping("{url}/edit")
	public String edit(ModelMap model, @PathVariable(value = "url") String slug) {
		_logger.debug("Editing article " + slug);

		Article article = contentService.getGlobalArticle(slug, getLocale(), false);
		if (article == null) {
			article = new Article();
			article.setSlug(slug);
			article.setLang(getLocale().getLanguage());
		}
		model.addAttribute("article", article);

		return "/content/article-edit";
	}

	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	@RequestMapping(value = "/save-article", method = { RequestMethod.POST })
	public String createNewGlobalArticle(ModelMap model, @RequestParam("slug") String slug, @RequestParam("title") String title,
			@RequestParam("body") String body) {

		contentService.createGlobalArticle(slug, getLocale(), title, body);

		return "redirect:/content/" + slug;
	}

	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	@RequestMapping(value = "/save-article", params = { "id" }, method = { RequestMethod.POST })
	public String saveExistingGlobalArticle(ModelMap model, @RequestParam("id") long id, @RequestParam("slug") String slug,
			@RequestParam("title") String title, @RequestParam("body") String body) {

		contentService.updateArticle(id, slug, title, body);

		return "redirect:/content/" + slug;
	}

	@RequestMapping(value = "/blurp/update-blurp", method = { RequestMethod.POST })
	public String updateBlurp(ModelMap model, @RequestParam("id") long id, @RequestParam(required = false, value = "title") String title,
			@RequestParam("body") String body) {

		contentService.updateArticle(id, null, title, body);

		return "redirect:/";
	}

	@RequestMapping(value = "/blurp/create-blurp", method = { RequestMethod.POST })
	public String createBlurp(ModelMap model, @RequestParam("clazz") String clazz, @RequestParam("entityId") long entityId,
			@RequestParam(required = false, value = "title") String title, @RequestParam("body") String body) throws ClassNotFoundException {

		contentService.updateArticle(Class.forName(clazz), entityId, "blurp", title, body, getLocale());
		return "redirect:/";
	}
}
