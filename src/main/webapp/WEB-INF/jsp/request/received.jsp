<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="request.page.title" /></title>
</head>
<body>
	<h1>
		<spring:message code="request.page.title" />
	</h1>

	<%@include file="/WEB-INF/jsp/content/include/blurp-display.jsp"%>

</body>
</html>