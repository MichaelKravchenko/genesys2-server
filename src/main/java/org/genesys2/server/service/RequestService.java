/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service;

import java.util.Set;

import org.genesys2.server.model.genesys.MaterialRequest;
import org.genesys2.server.model.genesys.MaterialSubRequest;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.service.TokenVerificationService.NoSuchVerificationTokenException;
import org.genesys2.server.service.impl.EasySMTAException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface RequestService {

	/**
	 * Creates a {@link MaterialRequest} and sends a validation email to user
	 * 
	 * @param requestInfo
	 * @param accessionIds
	 * @return
	 * @throws RequestException
	 */
	MaterialRequest initiateRequest(RequestInfo requestInfo, Set<Long> accessionIds) throws RequestException;

	/**
	 * Validation request attempt by user
	 * 
	 * @param tokenUuid
	 * @param key
	 * @return
	 * @throws RequestException
	 *             On request processing exception (e.g. missing PID data)
	 * @throws NoSuchVerificationTokenException
	 *             If verification token is invalid
	 * @throws NoPidException 
	 * @throws EasySMTAException 
	 */
	MaterialRequest validateClientRequest(String tokenUuid, String key) throws RequestException, NoSuchVerificationTokenException, NoPidException, EasySMTAException;

	/**
	 * Relay sub-request to holding institute
	 * 
	 * @param materialSubRequest
	 */
	void relayRequest(MaterialSubRequest materialSubRequest);

	/**
	 * Attempt to validate receipt of subrequest
	 */
	MaterialSubRequest validateReceipt(String tokenUuid, String key) throws NoSuchVerificationTokenException;

	static class RequestException extends Exception {
		/**
		 *
		 */
		private static final long serialVersionUID = -2916706231454838785L;

		public RequestException(String message) {
			super(message);
		}

		public RequestException(String message, Throwable e) {
			super(message, e);
		}
	}

	static class NoPidException extends Exception {
		public NoPidException(String message) {
			super(message);
		}
	}

	public static class RequestInfo {
		private String email;
		private int purposeType;
		private boolean preacceptSMTA;
		private String notes;

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public int getPurposeType() {
			return purposeType;
		}

		public void setPurposeType(int purposeType) {
			this.purposeType = purposeType;
		}

		public boolean isPreacceptSMTA() {
			return preacceptSMTA;
		}

		public void setPreacceptSMTA(boolean preacceptSMTA) {
			this.preacceptSMTA = preacceptSMTA;
		}

		public String getNotes() {
			return notes;
		}

		public void setNotes(String notes) {
			this.notes = notes;
		}
	}

	Page<MaterialRequest> list(Pageable pageable);

	Page<MaterialSubRequest> list(FaoInstitute faoInstitute, Pageable pageable);

	MaterialRequest get(String uuid);

	MaterialSubRequest get(FaoInstitute institute, String uuid);

	MaterialRequest sendValidationEmail(MaterialRequest materialRequest);

	/**
	 * Recheck Easy-SMTA for PID
	 * 
	 * @param materialRequest
	 * @throws NoPidException
	 * @return
	 * @throws EasySMTAException 
	 */
	MaterialRequest recheckPid(MaterialRequest materialRequest) throws NoPidException, EasySMTAException;

	/**
	 * Allow admin to validate request (recheck PID, relay)
	 * 
	 * @param materialRequest
	 * @return
	 * @throws EasySMTAException 
	 * @throws NoPidException 
	 */
	MaterialRequest validateRequest(MaterialRequest materialRequest) throws NoPidException, EasySMTAException;

}
