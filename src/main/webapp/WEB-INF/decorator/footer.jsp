<%@include file="/WEB-INF/jsp/init.jsp"%>


<!-- footer start -->

<div id="footer">
  <div class="container">    
  	<div id="nav-foot" class="clearfix">
  		<ul class="nav pull-left">
    		<li><a href="<c:url value="/content/about" />"><spring:message code="menu.about" /></a></li>
			<li><a href="<c:url value="/content/contact" />"><spring:message code="menu.contact" /></a></li>
			<li><a href="<c:url value="/content/disclaimer" />"><spring:message code="menu.disclaimer" /></a></li>
			<li><a target="_blank" href="https://bitbucket.org/genesys2/genesys2-server/issues/new"><spring:message code="menu.report-an-issue" /></a></li>
			<li class="notimportant"><a target="_blank" href="https://bitbucket.org/genesys2/genesys2-server.git"><spring:message code="menu.scm" /></a></li>
			<li class="notimportant"><a target="_blank" href="https://www.transifex.com/projects/p/genesys/"><spring:message code="menu.translate" /></a></li>
		</ul>
                                                    
		<ul class="nav pull-right">
			<li><a href="<c:url value="/content/terms" />"><spring:message code="menu.terms" /></a></li>
			<li><a href="<c:url value="/content/copying" />"><spring:message code="menu.copying" /></a></li>
			<li><a href="<c:url value="/content/privacy" />"><spring:message code="menu.privacy" /></a></li>
		</ul>
  	</div>
  	<div class="pull-left" id="copyright">
  		<a href="<c:url value="/" />" class="pull-left"><img src="<c:url value="/html/images/logo_genesys_footer.png" />" alt="Genesys - Gateway to Genetic Resources" /></a>
  		<p class="pull-left"><spring:message code="footer.copyright-statement" /></p>
  	</div>
  	<div class="pull-right">
  		<div class="pull-left" style="text-align: right; color: #808080; font-family: monospace; font-size: 8px;"><spring:message code="page.rendertime" arguments="${springExecuteTime}" />
		<br /><a target="_blank" href="https://bitbucket.org/genesys2/genesys2-server/commits/${buildRevision}">${buildName}</a></div>
	</div>
  </div>
</div>

<div style="display: none" id="session_expired_popup" title="<spring:message code="session.expiry-warning-title" />" >
    <div style="margin-bottom: 1em;"><spring:message code="session.expiry-warning" /></div>
    <a class="btn btn-primary" href="${pageContext.servletContext.contextPath}"><spring:message code="session.expiry-extend" /></a>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
 <c:choose>
    	<c:when test="${requestContext.theme.name eq 'one'}">
<script type="text/javascript" src="<c:url value="/html/js/all.min.js" />"></script>
    	</c:when>
    	<c:when test="${requestContext.theme.name eq 'all-min'}">
<script type="text/javascript" src="<c:url value="/html/js/jquery.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/html/js/jquery-ui.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/html/js/bootstrap.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/html/js/leaflet.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/html/js/leaflet.locationfilter.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/html/js/genesys.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/html/js/syronex-colorpicker-min.js" />"></script>
<script type="text/javascript" src="<c:url value="/html/js/jquery.flot.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/html/js/jquery.flot.pie.min.js" />"></script>
    	</c:when>
    	<c:when test="${requestContext.theme.name eq 'all'}">
<script type="text/javascript" src="<c:url value="/html/js/jquery.js" />"></script>
<script type="text/javascript" src="<c:url value="/html/js/jquery-ui.js" />"></script>
<script type="text/javascript" src="<c:url value="/html/js/bootstrap.js" />"></script>
<script type="text/javascript" src="<c:url value="/html/js/leaflet.js" />" />"></script>
<script type="text/javascript" src="<c:url value="/html/js/leaflet.locationfilter.js" />"></script>
<script type="text/javascript" src="<c:url value="/html/js/genesys.js" />"></script>
<script type="text/javascript" src="<c:url value="/html/js/syronex-colorpicker.js" />"></script>
<script type="text/javascript" src="<c:url value="/html/js/jquery.flot.js" />"></script>
<script type="text/javascript" src="<c:url value="/html/js/jquery.flot.pie.js" />"></script>

        </c:when>
    	<c:otherwise>
<script type="text/javascript" src="<c:url value="/html/js/jquery.js" />"></script>
<script type="text/javascript" src="<c:url value="/html/js/jquery-ui.js" />"></script>
<script type="text/javascript" src="<c:url value="/html/js/bootstrap.js" />"></script>
<script type="text/javascript" src="<c:url value="/html/js/tab.js" />"></script>
<script type="text/javascript" src="<c:url value="/html/js/leaflet.js" />"></script>
<script type="text/javascript" src="<c:url value="/html/js/leaflet.locationfilter.js" />"></script>
<script type="text/javascript" src="<c:url value="/html/js/crophub.js" />"></script>
<script type="text/javascript" src="<c:url value="/html/js/syronex-colorpicker.js" />"></script>
<script type="text/javascript" src="<c:url value="/html/js/jquery.flot.js" />"></script>
<script type="text/javascript" src="<c:url value="/html/js/jquery.flot.pie.js" />"></script>
    	</c:otherwise>
    </c:choose>


<script type="text/javascript">
	L.Icon.Default.imagePath='<c:url value="/html/css/leaflet" />';
	enableSessionWarning(${pageContext.session.maxInactiveInterval});
</script>
