/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.genesys2.server.model.AclAwareModel;
import org.genesys2.server.model.BusinessModel;
import org.genesys2.server.model.EntityId;
import org.genesys2.server.servlet.controller.rest.serialization.FaoInstituteSerializer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "faoinstitute", uniqueConstraints = @UniqueConstraint(columnNames = { "code" }), indexes = { @Index(columnList = "code", name = "code_FAOINSTITUTE") })
@JsonSerialize(using = FaoInstituteSerializer.class)
public class FaoInstitute extends BusinessModel implements GeoReferencedEntity, AclAwareModel, EntityId {

	/**
	 *
	 */
	private static final long serialVersionUID = -8773002513838748431L;

	@Column(unique = true, nullable = false, length = 10)
	private String code;

	@Column(length = 300)
	private String fullName;

	@Column(length = 11)
	private String type;

	@Column(length = 300)
	private String url;

	@Column(length = 300)
	private String email;

	@Column(length = 50)
	private String acronym;

	@Column(length = 10)
	private String vCode;

    @Column(name = "codeSGSV")
    private String codeSGSV;

	private boolean current;

	@JsonIgnore
	@ManyToOne(cascade = {}, optional = true)
	@JoinColumn(name = "countryId")
	private Country country;

	@JsonIgnore
	@OneToMany(cascade = {})
	@JoinColumn(referencedColumnName = "code", name = "instCode")
	@MapKey(name = "setting")
	private Map<String, FaoInstituteSetting> settings = new HashMap<String, FaoInstituteSetting>();

	private long accessionCount;
	private boolean pgrActivity;
	private boolean maintainsCollection;
	private Double latitude;
	private Double longitude;
	private Double elevation;
	private boolean uniqueAcceNumbs = true;

    @Column(name = "allowMaterialRequests", columnDefinition = "boolean default true", nullable = false)
    private boolean allowMaterialRequests = true;

	public FaoInstitute() {
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(final String institute) {
		this.code = institute;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(final String fullName) {
		this.fullName = fullName;
	}

	public String getType() {
		return this.type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return MessageFormat.format("FaoInst id={0,number,#} code={1} fullName={2}", id, code, fullName);
	}

	public void setAcronym(final String acronym) {
		this.acronym = acronym;
	}

	public String getAcronym() {
		return acronym;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(final Country country) {
		this.country = country;
	}

	public long getAccessionCount() {
		return accessionCount;
	}

	public void setAccessionCount(long accessionCount) {
		this.accessionCount = accessionCount;
	}

	public boolean isMaintainsCollection() {
		return maintainsCollection;
	}

	public void setMaintainsCollection(boolean maintainsCollection) {
		this.maintainsCollection = maintainsCollection;
	}

	public boolean isPgrActivity() {
		return pgrActivity;
	}

	public void setPgrActivity(boolean pgrActivity) {
		this.pgrActivity = pgrActivity;
	}

	@Override
	public Double getLatitude() {
		return this.latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	@Override
	public Double getLongitude() {
		return this.longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	@Override
	public Double getElevation() {
		return this.elevation;
	};

	public void setElevation(Double elevation) {
		this.elevation = elevation;
	}

	public Map<String, FaoInstituteSetting> getSettings() {
		return settings;
	}

	public void setSettings(Map<String, FaoInstituteSetting> settings) {
		this.settings = settings;
	}

	public boolean isUniqueAcceNumbs() {
		return this.uniqueAcceNumbs;
	}

	public boolean hasUniqueAcceNumbs() {
		return this.uniqueAcceNumbs;
	}

	public void setUniqueAcceNumbs(boolean uniqueAcceNumbs) {
		this.uniqueAcceNumbs = uniqueAcceNumbs;
	}

	public String getvCode() {
		return vCode;
	}

	public void setvCode(String vCode) {
		this.vCode = vCode;
	}

	public boolean isCurrent() {
		return current;
	}

	public void setCurrent(boolean current) {
		this.current = current;
	}

    public boolean isAllowMaterialRequests() {
        return allowMaterialRequests;
    }

    public void setAllowMaterialRequests(boolean allowMaterialRequests) {
        this.allowMaterialRequests = allowMaterialRequests;
    }

    public String getCodeSGSV() {
        return codeSGSV;
    }

    public void setCodeSGSV(String codeSGSV) {
        this.codeSGSV = codeSGSV;
    }
}
