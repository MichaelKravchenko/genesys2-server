/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.genesys;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

/**
 * Class representing one coded trait descriptor value.
 *
 * @author mobreza
 */
public class TraitCode {
	final static Pattern p = Pattern.compile("([^,]+),([^;$]*)(;|$)");
	private final String code;
	private final String value;

	public TraitCode(final String code, final String value) {
		this.code = code;
		this.value = value;
	}

	public String getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return MessageFormat.format("Coded code={0} value={1}", code, value);
	}

	public static String decode(final String options, final String value) {
		if (StringUtils.isNotBlank(options)) {
			final Matcher m = p.matcher(options);
			while (m.find()) {
				if (m.group(1).equalsIgnoreCase(value)) {
					return m.group(2);
				}
			}
		}
		return value;
	}

	public static List<TraitCode> parseOptions(final String options) {
		final List<TraitCode> codes = new ArrayList<TraitCode>();
		if (StringUtils.isNotBlank(options)) {
			final Matcher m = p.matcher(options);
			while (m.find()) {
				codes.add(new TraitCode(m.group(1), m.group(2)));
			}
		}
		return codes;
	}

	public static Map<String, TraitCode> parseOptionsMap(final String options) {
		final Map<String, TraitCode> map = new HashMap<String, TraitCode>();
		for (final TraitCode code : parseOptions(options)) {
			map.put(code.getCode(), code);
		}
		return map;
	}

	public static Map<String, String> parseCodeMap(final String options) {
		final Map<String, String> map = new TreeMap<String, String>();
		for (final TraitCode code : parseOptions(options)) {
			map.put(code.getCode(), code.getValue());
		}
		return map;
	}
}
