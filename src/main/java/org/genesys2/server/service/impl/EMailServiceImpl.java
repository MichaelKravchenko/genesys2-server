/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.util.Arrays;

import javax.mail.internet.MimeMessage;

import org.genesys2.server.service.EMailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

@Service
public class EMailServiceImpl implements EMailService {

	private final Logger _logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private ThreadPoolTaskExecutor executor;

	@Value("${mail.async}")
	private boolean async;

	@Value("${mail.debug.message}")
	private String debugMessage;

	@Value("${mail.user.from}")
	private String emailFrom;

	@Override
	public void sendMail(String mailSubject, String mailBody, String... emailTo) {
		sendMail(mailSubject, mailBody, null, emailTo);
	}

	@Override
	public void sendMail(String mailSubject, String mailBody, String[] emailCc, String... emailTo) {
		sendSimpleEmail(mailSubject, mailBody, emailFrom, emailCc, emailTo);
	}

	public void sendSimpleEmail(final String subject, final String text, final String emailFrom, final String[] emailCc, final String... emailTo) {
		printDebugInfo(subject, text, emailFrom, emailTo);

		final MimeMessagePreparator preparator = new MimeMessagePreparator() {
			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "UTF-8");

				message.setFrom(emailFrom);
				message.setTo(emailTo);
				if (emailCc != null && emailCc.length > 0) {
					message.setCc(emailCc);
				}
				message.setSubject(subject);

				message.setText(text, true);
			}
		};

		doSend(preparator);
	}

	protected void doSend(final MimeMessagePreparator preparator) {
		// execute sender in separate thread
		if (async) {
			executor.submit(new Runnable() {
				@Override
				public void run() {
					try {
						mailSender.send(preparator);
					} catch (final Exception e) {
						_logger.error(e.getMessage(), e);
					}
				}
			});
		} else {
			mailSender.send(preparator);
		}
	}

	protected void printDebugInfo(String subject, String text, String emailFrom, String[] emailTo) {
		System.out.println(getDebugString(subject, text, emailFrom, emailTo));
	}

	protected String getDebugString(String subject, String text, String emailFrom, String[] emailTo) {
		return String.format(debugMessage, emailFrom, Arrays.toString(emailTo), subject, text);
	}
}
