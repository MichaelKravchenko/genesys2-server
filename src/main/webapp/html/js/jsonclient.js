function x0(path, method, object) {
	$.ajax(path, {
		type : method,
		dataType : 'json',
		contentType: 'application/json; charset=utf-8',
		data: (object==null ? null : JSON.stringify(object)),
		beforeSend : function(xhr) {
			
		},
		success : function(respObject) {
			//$("#responseBody")[0].value = JSON.stringify(respObject);
			console.log(respObject);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			//$("#responseBody")[0].value = errorThrown; 
			console.log(textStatus);
			console.log(errorThrown);
		}
	});
}

function x01(path, handler, object) {
	$.ajax(path, $.extend({}, {
		type : 'POST',
		dataType : 'json',
		contentType: 'application/json; charset=utf-8',
		data: (object==null ? null : JSON.stringify(object)),
		beforeSend : function(xhr) {
			
		},
		success : function(respObject) {
			console.log(respObject);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.log(textStatus);
			console.log(errorThrown);
		}
	}, handler));
}
