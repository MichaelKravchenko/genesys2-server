/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.model.impl.CropTaxonomy;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.FilterConstants;
import org.genesys2.server.service.GenesysFilterService;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.TraitService;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/c")
public class CropController extends BaseController {

	@Autowired
	private CropService cropService;

	@Autowired
	private GenesysService genesysService;

	@Autowired
	private TraitService traitService;

	@Autowired
	private GenesysFilterService filterService;

	@RequestMapping("/{shortName}")
	public String view(ModelMap model, @PathVariable(value = "shortName") String shortName) {
		_logger.debug("Viewing crop " + shortName);
		final Crop crop = cropService.getCrop(shortName);
		if (crop == null) {
			throw new ResourceNotFoundException();
		}
		model.addAttribute("crop", crop);
		model.addAttribute("cropTaxonomies", cropService.getCropTaxonomies(crop, new PageRequest(0, 20, new Sort("taxonomy.genus", "taxonomy.species"))));

		return "/crop/index";
	}

	@RequestMapping("/{shortName}/ajax/taxonomies")
	public String ajaxTaxonomies(ModelMap model, @PathVariable(value = "shortName") String shortName,
			@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
		final Crop crop = cropService.getCrop(shortName);
		if (crop == null || page < 1) {
			throw new ResourceNotFoundException();
		}
		final Page<CropTaxonomy> res = cropService.getCropTaxonomies(crop, new PageRequest(page - 1, 20, new Sort("taxonomy.genus", "taxonomy.species")));
		model.addAttribute("cropTaxonomies", res);
		if (res.getNumberOfElements() == 0) {
			throw new ResourceNotFoundException();
		}

		return "/crop/ajax.taxonomies";
	}

	@RequestMapping(value = "/rebuild", method = RequestMethod.POST)
	public String rebuild() {
		_logger.info("Rebuilding taxonomies");
		cropService.rebuildTaxonomies();
		return "redirect:/";
	}

	@RequestMapping("/{shortName}/data")
	public String viewData(ModelMap model, @PathVariable(value = "shortName") String shortName,
			@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
		_logger.warn("Viewing crop " + shortName);
		final Crop crop = cropService.getCrop(shortName);
		if (crop == null) {
			throw new ResourceNotFoundException();
		}

		model.addAttribute("filter", "{\"" + FilterConstants.CROPS + "\":[\"" + crop.getShortName() + "\"]}");

		return "redirect:/explore";
	}

	@RequestMapping("/{shortName}/descriptors")
	public String viewDescriptors(ModelMap model, @PathVariable(value = "shortName") String shortName,
			@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
		_logger.debug("Viewing crop " + shortName);
		final Crop crop = cropService.getCrop(shortName);
		if (crop == null) {
			throw new ResourceNotFoundException();
		}
		model.addAttribute("crop", crop);
		model.addAttribute("pagedData", traitService.listTraits(crop, new PageRequest(page - 1, 50, new Sort("title"))));
		return "/descr/index";
	}

}
