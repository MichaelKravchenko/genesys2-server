/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.genesys2.server.model.VersionedModel;

/**
 * GRIN taxonomy based on "species 2.dbf" from
 * http://www.ars-grin.gov/misc/tax/species.zip
 *
 * @author matijaobreza
 */
@Entity
public class GrinTaxonomy extends VersionedModel {

	/**
	 *
	 */
	private static final long serialVersionUID = -1294689196992826336L;

	@Column(unique = true, nullable = false)
	private Long taxno;

	@Column(nullable = true)
	private Long validTaxNo;

	@Column(length = 30)
	private String genus;
	private Character sHybrid;
	@Column(length = 30)
	private String species;
	@Column(length = 100)
	private String sAuthor;
	private Character sSpHybrid;

	@Column(length = 30)
	private String subSp;
	@Column(length = 100)
	private String subSpAuthor;
	private Character varHybrid;
	@Column(length = 30)
	private String var;
	@Column(length = 100)
	private String varAuthor;
	private Character svHybrid;
	@Column(length = 30)
	private String subVar;
	@Column(length = 100)
	private String svAuthor;
	private Character fHybrid;
	@Column(length = 30)
	private String forma;
	@Column(length = 100)
	private String fAuthor;

	@Column(length = 240)
	private String protologue;
	@Column(length = 254)
	private String taxcmt;

	@Temporal(TemporalType.DATE)
	private Date createdDate;
	@Temporal(TemporalType.DATE)
	private Date modifiedDate;
	@Temporal(TemporalType.DATE)
	private Date verifiedDate;

	public Long getTaxno() {
		return taxno;
	}

	public void setTaxno(Long taxno) {
		this.taxno = taxno;
	}

	public Long getValidTaxNo() {
		return validTaxNo;
	}

	public void setValidTaxNo(Long validTaxNo) {
		this.validTaxNo = validTaxNo;
	}

	public String getGenus() {
		return genus;
	}

	public void setGenus(String genus) {
		this.genus = genus;
	}

	public Character getsHybrid() {
		return sHybrid;
	}

	public void setsHybrid(Character sHybrid) {
		this.sHybrid = sHybrid;
	}

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public String getsAuthor() {
		return sAuthor;
	}

	public void setsAuthor(String sAuthor) {
		this.sAuthor = sAuthor;
	}

	public Character getsSpHybrid() {
		return sSpHybrid;
	}

	public void setsSpHybrid(Character sSpHybrid) {
		this.sSpHybrid = sSpHybrid;
	}

	public String getSubSp() {
		return subSp;
	}

	public void setSubSp(String subSp) {
		this.subSp = subSp;
	}

	public String getSubSpAuthor() {
		return subSpAuthor;
	}

	public void setSubSpAuthor(String subSpAuthor) {
		this.subSpAuthor = subSpAuthor;
	}

	public Character getVarHybrid() {
		return varHybrid;
	}

	public void setVarHybrid(Character varHybrid) {
		this.varHybrid = varHybrid;
	}

	public String getVar() {
		return var;
	}

	public void setVar(String var) {
		this.var = var;
	}

	public String getVarAuthor() {
		return varAuthor;
	}

	public void setVarAuthor(String varAuthor) {
		this.varAuthor = varAuthor;
	}

	public Character getSvHybrid() {
		return svHybrid;
	}

	public void setSvHybrid(Character svHybrid) {
		this.svHybrid = svHybrid;
	}

	public String getSubVar() {
		return subVar;
	}

	public void setSubVar(String subVar) {
		this.subVar = subVar;
	}

	public String getSvAuthor() {
		return svAuthor;
	}

	public void setSvAuthor(String svAuthor) {
		this.svAuthor = svAuthor;
	}

	public Character getfHybrid() {
		return fHybrid;
	}

	public void setfHybrid(Character fHybrid) {
		this.fHybrid = fHybrid;
	}

	public String getForma() {
		return forma;
	}

	public void setForma(String forma) {
		this.forma = forma;
	}

	public String getfAuthor() {
		return fAuthor;
	}

	public void setfAuthor(String fAuthor) {
		this.fAuthor = fAuthor;
	}

	public String getProtologue() {
		return protologue;
	}

	public void setProtologue(String protologue) {
		this.protologue = protologue;
	}

	public String getTaxcmt() {
		return taxcmt;
	}

	public void setTaxcmt(String taxcmt) {
		this.taxcmt = taxcmt;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Date getVerifiedDate() {
		return verifiedDate;
	}

	public void setVerifiedDate(Date verifiedDate) {
		this.verifiedDate = verifiedDate;
	}

}
