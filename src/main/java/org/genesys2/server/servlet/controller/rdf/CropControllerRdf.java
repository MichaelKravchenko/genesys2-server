/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.rdf;

import java.util.List;
import java.util.Map;

import org.bioversityinternational.model.germplasm.CCO;
import org.bioversityinternational.model.rdf.dwc.DarwinCore;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.model.impl.CropRule;
import org.genesys2.server.service.CropService;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.DC_11;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.RDFS;

@Controller
@RequestMapping(value = "/crops", method = RequestMethod.GET, headers = "accept=text/turtle", produces = RdfBaseController.RDF_MEDIATYPE_TURTLE)
public class CropControllerRdf extends RdfBaseController {

	private final String CROPS_NS = "/crops/";
	private final String SPECIES_NS = CROPS_NS + "species/";

	@Autowired
	private CropService cropService;

	/*
	 * Method generates RDF for one crop record as RDF.
	 *
	 * Crop RDF exemplar:
	 *
	 * genesys:MusaTaxon a dwc:Taxon, skos:Collection; dwc:scientificName "Musa"
	 * ; dwc:vernacularName "Musa"@en ; dwc:vernacularName "Musa"@es ;
	 * dwc:vernacularName "Муса"@ru ; dwc:vernacularName "穆萨" @zh ;
	 * dc:description "Bananas and plantains"@en ; dc:description
	 * "Los bananos y plátanos"@es ; dc:description "Бананы и бананы"@ru ;
	 * dc:description "香蕉和大蕉"@zh ; dc:hasVersion "Revision: 0.1" ; dc:creator
	 * "Bioversity International" ; skos:member cco:MusaAcuminata,
	 * cco:MusaBalbisiana .
	 */
	private void wrapCrop(Model model, Crop crop) {

		final Resource cropSubj = createSubject(model, crop, CROPS_NS + crop.getShortName(), crop.getShortName() + " Taxon");

		// FIXME That's probably not right
		cropSubj.addProperty(DarwinCore.scientificName, crop.getShortName());

		final String seeAlso = crop.getRdfUri();
		if (seeAlso != null) {
			cropSubj.addProperty(RDFS.seeAlso, model.createResource(seeAlso));
		}

		/*
		 * vernacular names & descriptions in i18n
		 */
		final Map<String, String> vernacularNameMap = crop.getLocalNameMap();

		if (vernacularNameMap != null) {
			for (final String language : vernacularNameMap.keySet()) {
				final Literal vernacularNameLiteral = model.createLiteral(vernacularNameMap.get(language), language);
				cropSubj.addLiteral(DarwinCore.vernacularName, vernacularNameLiteral);
			}
		} else {
			this._logger.warn("Empty vernacular crop name map for crop " + crop.getShortName());
		}

		final Map<String, String> vernacularDefinitionMap = crop.getLocalDefinitionMap();

		if (vernacularDefinitionMap != null) {
			for (final String language : vernacularDefinitionMap.keySet()) {
				final Literal vernacularDefinitionLiteral = model.createLiteral(vernacularDefinitionMap.get(language), language);
				cropSubj.addLiteral(DC_11.description, vernacularDefinitionLiteral);
			}
		} else {
			this._logger.warn("Empty vernacular crop description map for crop " + crop.getShortName());
		}

		/*
		 * Exemplar for CropRule species...
		 *
		 * genesys:MusaAcuminata a cco:Species; dwc:scientificName
		 * "Musa acuminata" ; dwc:genus "Musa" ; dc:description
		 * "Wild progenitor of domesticated bananas."@en ; dc:description
		 * "Progenitor salvaje de plátanos domesticados."@es ; dc:description
		 * "Дикий прародителем домашних бананов."@ru ; dc:description
		 * "香蕉驯化野生祖。"@zh ; cco:speciesIncluded "true" .
		 *
		 * Note: current GENESYS data model does NOT track the description of a
		 * species...
		 */
		final List<CropRule> rules = cropService.getCropRules(crop);
		for (final CropRule cr : rules) {
			final String species = cr.getSpecies();
			final String genus = cr.getGenus();
			final Boolean isIncluded = cr.isIncluded();

			// FIXME in CropRule the Genus is required, species is not.
			if (species != null) {
				final Resource speciesSubj = createSubject(model, crop, SPECIES_NS + canonicalID(species, true), species);
				speciesSubj.addProperty(RDF.type, CCO.Species);
				speciesSubj.addProperty(DarwinCore.scientificName, species);
				speciesSubj.addProperty(DarwinCore.genus, genus);
				speciesSubj.addProperty(CCO.speciesIncluded, isIncluded.toString());
			}
		}

	}

	@RequestMapping
	public @ResponseBody
	String dumpCrops() {

		final Model model = startModel();

		final List<Crop> crops = cropService.listCrops();
		for (final Crop crop : crops) {
			wrapCrop(model, crop);
		}

		return endModel(model);
	}

	@RequestMapping("/{shortName}")
	public @ResponseBody
	String dumpCrop(@PathVariable(value = "shortName") String shortName) {

		_logger.debug("Dump RDF data for crop: " + shortName);

		final Model model = startModel();

		final Crop crop = cropService.getCrop(shortName);
		if (crop == null) {
			throw new ResourceNotFoundException();
		}

		wrapCrop(model, crop);

		return endModel(model);
	}

}
