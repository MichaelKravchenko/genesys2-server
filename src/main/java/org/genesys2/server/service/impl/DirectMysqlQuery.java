/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.genesys.Method;
import org.genesys2.server.service.FilterConstants;
import org.genesys2.server.service.MappingService.CoordUtil;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilter;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.genesys2.server.service.impl.FilterHandler.FilterValue;
import org.genesys2.server.service.impl.FilterHandler.LiteralValueFilter;
import org.genesys2.server.service.impl.FilterHandler.MaxValueFilter;
import org.genesys2.server.service.impl.FilterHandler.MinValueFilter;
import org.genesys2.server.service.impl.FilterHandler.StartsWithFilter;
import org.genesys2.server.service.impl.FilterHandler.ValueRangeFilter;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;

public class DirectMysqlQuery {
	private static final Log LOG = LogFactory.getLog(DirectMysqlQuery.class);

	final Set<String> tables = new HashSet<String>();
	final List<Object> params = new ArrayList<Object>();
	final StringBuffer sb, whereBuffer = new StringBuffer(), sortBuffer = new StringBuffer();

	public DirectMysqlQuery(String baseTable, String baseAlias) {
		sb = new StringBuffer(300);
		sb.append(" from ").append(baseTable).append(StringUtils.SPACE).append(baseAlias);
		tables.add(baseTable);
	}

	public DirectMysqlQuery innerJoin(String table, String alias, String onExpr) {
		if (tables.contains(table)) {
			if (LOG.isDebugEnabled())
				LOG.debug("Table already inner-joined: " + table);
			return this;
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inner-joining " + table + " " + alias + " on " + onExpr);
		}
		sb.append(" inner join ").append(table).append(StringUtils.SPACE);
		tables.add(table);

		if (StringUtils.isNotBlank(alias)) {
			sb.append(alias).append(StringUtils.SPACE);
		}
		sb.append("on ").append(onExpr).append(StringUtils.SPACE);
		return this;
	}

	public DirectMysqlQuery outerJoin(String table, String alias, String onExpr) {
		if (tables.contains(table)) {
			LOG.warn("Table already joined: " + table);
			return this;
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("Leftouter-joining " + table + " " + alias + " on " + onExpr);
		}
		sb.append(" left outer join ").append(table).append(StringUtils.SPACE);
		tables.add(table);

		if (StringUtils.isNotBlank(alias)) {
			sb.append(alias).append(StringUtils.SPACE);
		}
		sb.append("on ").append(onExpr).append(StringUtils.SPACE);
		return this;
	}

	public DirectMysqlQuery jsonFilter(AppliedFilters filters, MethodResolver methodResolver) {
		return join(filters).filter(filters, methodResolver);
	}

	protected DirectMysqlQuery join(AppliedFilters filters) {
		if (hasFilter(filters, FilterConstants.CROPS) || hasFilter(filters, FilterConstants.TAXONOMY_GENUS)
				|| hasFilter(filters, FilterConstants.TAXONOMY_SPECIES) || hasFilter(filters, FilterConstants.TAXONOMY_SCINAME)) {
			innerJoin("taxonomy2", "t", "t.id=a.taxonomyId2");
			if (hasFilter(filters, FilterConstants.CROPS)) {
				innerJoin("croptaxonomy", "ct", "ct.taxonomyId=t.id");
				innerJoin("crop", null, "crop.id=ct.cropId");
			}
		}
		if (hasFilter(filters, FilterConstants.INSTITUTE_NETWORK)) {
			innerJoin("faoinstitute", "fao", "fao.id=a.instituteId");
			innerJoin("organizationinstitute", "oi", "oi.instituteId=fao.id");
			innerJoin("organization", "org", "org.id=oi.organizationId");
		}

		if (hasFilter(filters, FilterConstants.GEO_LATITUDE) || hasFilter(filters, FilterConstants.GEO_LONGITUDE)
				|| hasFilter(filters, FilterConstants.GEO_ELEVATION)) {
			innerJoin("accessiongeo", "geo", "geo.accessionId=a.id");
		}

		if (hasFilter(filters, FilterConstants.ALIAS)) {
			innerJoin("accessionalias", "accename", "accename.accessionId=a.id");
		}

		if (hasFilter(filters, FilterConstants.COLLMISSID)) {
			innerJoin("accessioncollect", "col", "col.accessionId=a.id");
		}

		if (hasFilter(filters, FilterConstants.STORAGE)) {
			innerJoin("accessionstorage", "storage", "storage.accessionId=a.id");
		}

		return this;
	}

	static boolean hasFilter(AppliedFilters filters, String filterName) {
		final AppliedFilter f = filters.get(filterName);
		return f != null && f.getValues().size() > 0;
	}

	protected DirectMysqlQuery filter(AppliedFilters filters, MethodResolver methodResolver) {
		createQuery(whereBuffer, "a.id", filters.get("id"), params);
		createQuery(whereBuffer, "a.taxGenus", filters.get("genusId"), params);
		createQuery(whereBuffer, "a.taxSpecies", filters.get("speciesId"), params);
		createQuery(whereBuffer, "a.acceNumb", filters.get(FilterConstants.ACCENUMB), params);
		createQuery(whereBuffer, "a.orgCty", filters.get(FilterConstants.ORGCTY_ISO3), params);
		createQuery(whereBuffer, "a.instCode", filters.get(FilterConstants.INSTCODE), params);
		createQuery(whereBuffer, "a.inSGSV", filters.get(FilterConstants.SGSV), params);
		createQuery(whereBuffer, "a.mlsStat", filters.get(FilterConstants.MLSSTATUS), params);
		createQuery(whereBuffer, "a.inTrust", filters.get(FilterConstants.ART15), params);
		createQuery(whereBuffer, "a.sampStat", filters.get(FilterConstants.SAMPSTAT), params);
		createQuery(whereBuffer, "a.available", filters.get(FilterConstants.AVAILABLE), params);
		createQuery(whereBuffer, "org.slug", filters.get(FilterConstants.INSTITUTE_NETWORK), params);
		createQuery(whereBuffer, "t.genus", filters.get(FilterConstants.TAXONOMY_GENUS), params);
		createQuery(whereBuffer, "t.species", filters.get(FilterConstants.TAXONOMY_SPECIES), params);
		createQuery(whereBuffer, "t.taxonName", filters.get(FilterConstants.TAXONOMY_SCINAME), params);
		createQuery(whereBuffer, "geo.longitude", filters.get(FilterConstants.GEO_LONGITUDE), params);
		createQuery(whereBuffer, "geo.latitude", filters.get(FilterConstants.GEO_LATITUDE), params);
		createQuery(whereBuffer, "geo.elevation", filters.get(FilterConstants.GEO_ELEVATION), params);
		createQuery(whereBuffer, "crop.shortName", filters.get(FilterConstants.CROPS), params);
		createQuery(whereBuffer, "accename.name", filters.get(FilterConstants.ALIAS), params);
		createQuery(whereBuffer, "col.collMissId", filters.get(FilterConstants.COLLMISSID), params);
		createQuery(whereBuffer, "storage.storage", filters.get(FilterConstants.STORAGE), params);

		for (final AppliedFilter methodFilter : filters.methodFilters()) {
			// Handle Genesys Method!
			final long methodId = Long.parseLong(methodFilter.getFilterName().substring(3));
			// Need to validate method
			final Method method = methodResolver.getMethod(methodId);
			if (method != null) {
				final String alias = "gm" + methodId;
				innerJoin("`" + methodId + "`", alias, alias + ".accessionId=a.id");
				createQuery(whereBuffer, alias + ".`" + method.getFieldName() + "`", methodFilter, params);
			} else {
				LOG.warn("No such method with id=" + methodId);
			}
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("Parameter count: " + params.size());
			LOG.debug("Count query:\n" + sb.toString());
		}

		return this;
	}

	public void filterTile(int zoom, int xtile, int ytile) {
		if (zoom >= 0) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("ZOOM=" + zoom);
			}
			final double lonW = CoordUtil.tileToLon(zoom, xtile);
			final double lonE = CoordUtil.tileToLon(zoom, xtile + 1);
			final double diffLon = lonE - lonW;
			final double latN = CoordUtil.tileToLat(zoom, ytile);
			final double latS = CoordUtil.tileToLat(zoom, ytile + 1);
			final double diffLat = latN - latS;

			if (whereBuffer.length() == 0) {
				whereBuffer.append(" where ");
			} else {
				whereBuffer.append(" and ");
			}
			whereBuffer.append(" ((geo.longitude between ? and ?) and (geo.latitude between ? and ?)) ");

			params.add(lonW - zoom * diffLon * .2);
			params.add(lonE + zoom * diffLon * .2);
			params.add(latS - zoom * diffLat * .2);
			params.add(latN + zoom * diffLat * .2);
			if (LOG.isDebugEnabled()) {
				LOG.debug(lonW + " <= lon <= " + lonE + " corr=" + diffLon * .2);
				LOG.debug(latS + " <= lat <= " + latN + " corr=" + diffLat * .2);
			}
		}
	}

	private void createQuery(StringBuffer sb, String dbName, AppliedFilter appliedFilter, List<Object> params) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Handling " + dbName);
		}

		if (appliedFilter != null && appliedFilter.getValues().size() > 0) {
			Set<FilterValue> filterValues = appliedFilter.getValues();

			if (LOG.isDebugEnabled()) {
				LOG.debug("Adding " + appliedFilter + " sz=" + filterValues.size() + " t=" + appliedFilter.getClass().getSimpleName());
			}

			if (sb.length() == 0) {
				sb.append(" where ");
			} else {
				sb.append(" and ");
			}

			// Opening
			sb.append(" ( ");

			// A filter value can be (a) explicit value or (b) an operation

			// (a) explicit values are handled by =? or by IN (?,?,..)
			int handledCount = handleExplicitValues(sb, dbName, filterValues, params);

			// do we have more?
			if (handledCount > 0 && filterValues.size() > handledCount) {
				sb.append(" OR ");
			}

			int handledCountNull = handleNullValues(sb, dbName, appliedFilter, params);
			handledCount += handledCountNull;

			// do we have more?
			if (handledCountNull > 0 && handledCount > 0 && filterValues.size() > handledCount) {
				sb.append(" OR ");
			}

			// (b) operations are expressed as {"min":12} or {"max":33} or
			// {"range":[3,10]} or {"like":"test"}
			handleOperations(sb, dbName, filterValues, params);

			// closing
			sb.append(" ) ");
		}

	}

	private int handleOperations(StringBuffer sb, String dbName, Set<FilterValue> set, List<Object> params) {

		int counter = 0;
		for (FilterValue filterValue : set) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inspecting " + dbName + " ... " + filterValue);
			}

			if (filterValue instanceof ValueRangeFilter) {
				ValueRangeFilter vrf = (ValueRangeFilter) filterValue;
				if (LOG.isDebugEnabled()) {
					LOG.debug("Adding array: " + vrf);
				}
				if (counter > 0) {
					sb.append(" or ");
				}
				counter++;
				// must be an array
				sb.append("\n ( ").append(dbName);
				sb.append(" between ? and ? ) ");
				addParam(params, vrf.getFrom());
				addParam(params, vrf.getTo());
			}

			if (filterValue instanceof MinValueFilter) {
				MinValueFilter mvf = (MinValueFilter) filterValue;
				if (LOG.isDebugEnabled()) {
					LOG.debug("Adding min number: " + mvf);
				}
				if (counter > 0) {
					sb.append(" or ");
				}
				counter++;
				// must be an number
				sb.append("\n ( ").append(dbName);
				sb.append(" >= ? ) ");
				addParam(params, mvf.getFrom());
			}

			if (filterValue instanceof MaxValueFilter) {
				MaxValueFilter mvf = (MaxValueFilter) filterValue;
				if (LOG.isDebugEnabled()) {
					LOG.debug("Adding max number: " + mvf);
				}
				if (counter > 0) {
					sb.append(" or ");
				}
				counter++;
				// must be an number
				sb.append("\n ( ").append(dbName);
				sb.append(" <= ? ) ");
				addParam(params, mvf.getTo());
			}

			if (filterValue instanceof StartsWithFilter) {
				StartsWithFilter swf = (StartsWithFilter) filterValue;
				if (LOG.isDebugEnabled()) {
					LOG.debug("Adding LIKE : " + swf);
				}
				if (counter > 0) {
					sb.append(" or ");
				}
				counter++;
				// must be an number
				sb.append("\n ( ").append(dbName);
				sb.append(" LIKE ? ) ");
				addParamLike(params, swf.getStartsWith());

			}
		}

		return counter;
	}

	private int handleNullValues(StringBuffer sb, String dbName, AppliedFilter appliedFilter, List<Object> params) {
		if (appliedFilter.getWithNull()) {
			sb.append("\n ( ").append(dbName);
			sb.append(" is NULL ) ");
			return 1;
		}

		return 0;
	}

	private int handleExplicitValues(StringBuffer sb, String dbName, Set<FilterValue> set, List<Object> params) {

		int counter = 0;
		for (FilterValue filterValue : set) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inspecting " + dbName + " ... " + filterValue);
			}
			if (filterValue instanceof LiteralValueFilter) {
				if (LOG.isDebugEnabled()) {
					LOG.debug("Adding " + filterValue);
				}
				counter++;
				addParam(params, ((LiteralValueFilter) filterValue).getValue());
			}
		}
		if (counter == 0) {
			// Nothing..
		} else if (counter == 1) {
			sb.append("\n ( ").append(dbName);
			sb.append(" = ? ) ");
		} else {
			sb.append("\n ( ").append(dbName);
			sb.append(" IN ( ?");
			for (int i = counter - 1; i > 0; i--) {
				sb.append(",?");
			}
			sb.append(" ) )");
		}

		return counter;
	}

	private void addParam(List<Object> params, Object object) {
		params.add(object);
	}

	private void addParamLike(List<Object> params, String string) {
		params.add(string + "%");
	}

	public String getCountQuery(String countWhat) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("select count(" + countWhat + ") " + sb.toString() + " " + whereBuffer.toString());
		}
		return "select count(" + countWhat + ") " + sb.toString() + " " + whereBuffer.toString();
	}

	public Object[] getParameters() {
		return params.toArray();
	}

	public DirectMysqlQuery pageable(Pageable pageable) {
		if (sortBuffer.length() != 0) {
			throw new RuntimeException("sortBuffer is not blank, invalid use of #pageable(Pageable)");
		}

		if (pageable == null) {
			return this;
		}

		if (pageable.getSort() != null) {
			sortBuffer.append("\n order by ");
			for (final Order o : pageable.getSort()) {
				if (LOG.isDebugEnabled()) {
					LOG.debug("Order: " + o);
				}
				// ClassMetadata md =
				// sessionFactory.getClassMetadata(Accession.class);
				// md.
				// EntityType<Accession> x =
				// entityManager.getMetamodel().entity(Accession.class);
				// System.err.println(x.getAttribute(o.getProperty()).getName());
				// sb.append(x.getAttribute(o.getProperty()).getName());
				sortBuffer.append("a.").append(o.getProperty());
				sortBuffer.append(" ").append(o.getDirection());
			}
		}
		sortBuffer.append(" limit ");
		if (LOG.isDebugEnabled()) {
			LOG.debug("Pageable=" + pageable.getOffset() + " " + pageable.getPageNumber());
		}
		sortBuffer.append(pageable.getOffset());
		sortBuffer.append(", ");
		sortBuffer.append(pageable.getPageSize());

		if (LOG.isDebugEnabled()) {
			LOG.debug("Filter query:\n" + sb.toString());
			LOG.debug("Parameter count: " + params.size());
			LOG.debug("Params: " + ArrayUtils.toString(params.toArray()));
		}

		return this;
	}

	public String getQuery(String what) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("select " + what + " " + sb.toString() + " " + whereBuffer.toString() + " " + sortBuffer.toString());
		}
		return "select " + what + " " + sb.toString() + " " + whereBuffer.toString() + " " + sortBuffer.toString();
	}

	public void limit(Integer limit) {
		if (sortBuffer.length() != 0) {
			throw new RuntimeException("sortBuffer is not blank, invalid use of #limit(Integer)");
		}
		if (limit != null) {
			sortBuffer.append(" limit ").append(limit);
		}
	}

	public static interface MethodResolver {
		Method getMethod(long methodId);
	}

}
