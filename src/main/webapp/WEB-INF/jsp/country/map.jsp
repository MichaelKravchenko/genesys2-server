<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="country.page.profile.title" arguments="${country.getName(pageContext.response.locale)}" argumentSeparator="|" /></title>
</head>
<body>
	<h1>
		<c:out value="${country.getName(pageContext.response.locale)}" />
		<img class="country-flag bigger" src="<c:url value="${cdnFlagsUrl}" />/${country.code3.toUpperCase()}.png" />
	</h1>

	<div class="main-col-header">
		<a href="<c:url value="/geo/${country.code3}" />"><c:out value="${country.getName(pageContext.response.locale)}" /></a>
	</div>
	
	<c:if test="${jsonInstitutes ne null}">
		<div class="row" style="">
		<div class="col-sm-12">
			<div id="map" class="gis-map gis-map-square"></div>
		</div>
		</div>

<content tag="javascript">	
		<script type="text/javascript">
		jQuery(document).ready(function() {	
			GenesysMaps.map("${pageContext.response.locale.language}", $("#map"), {
				maxZoom: 8,
				center: new GenesysMaps.LatLng(0, 0)
			}, function(el, map) {
				// markers
				var jsonInstitutes=${jsonInstitutes};
				jsonInstitutes.forEach(function(inst) {
					var marker = L.marker([inst.lat, inst.lng]).addTo(map);
					marker.bindPopup("<a href='<c:url value="/wiews/" />" + inst.code +"'>"+inst.title+"</a>");
				});
				map.fitBounds(GenesysMaps.boundingBox(jsonInstitutes));
			});
		});
		</script>
</content>
	</c:if>
</body>
</html>