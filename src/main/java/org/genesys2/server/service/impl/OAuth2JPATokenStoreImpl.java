/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.oauth.OAuthAccessToken;
import org.genesys2.server.model.oauth.OAuthRefreshToken;
import org.genesys2.server.model.oauth.OAuthToken;
import org.genesys2.server.persistence.domain.OAuthAccessTokenPersistence;
import org.genesys2.server.persistence.domain.OAuthRefreshTokenPersistence;
import org.genesys2.server.security.AuthUserDetails;
import org.genesys2.server.service.JPATokenStore;
import org.genesys2.server.service.JPATokenStoreCleanup;
import org.genesys2.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.DefaultOAuth2RefreshToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.common.util.OAuth2Utils;
import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.security.oauth2.provider.DefaultAuthorizationRequest;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.AuthenticationKeyGenerator;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * OAuth2JPATokenStoreImpl based on JdbcTokenStore
 * 
 * Original authors of JdbcTokenStore:
 * 
 * @author Ken Dombeck
 * @author Luke Taylor
 * @author Dave Syer
 * 
 *         JPA:
 * @author Matija Obreza
 */
@Service("tokenStore")
@Transactional(readOnly = false)
public class OAuth2JPATokenStoreImpl implements JPATokenStoreCleanup, JPATokenStore {
	private static final Log LOG = LogFactory.getLog(OAuth2JPATokenStoreImpl.class);

	@Autowired
	private OAuthAccessTokenPersistence accessTokenPersistence;

	@Autowired
	private OAuthRefreshTokenPersistence refreshTokenPersistence;

	@Autowired
	private UserService userService;

	private final AuthenticationKeyGenerator authenticationKeyGenerator = new AuthenticationKeyGenerator() {

		private static final String CLIENT_ID = "client_id";

		private static final String SCOPE = "scope";

		private static final String USERNAME = "username";

		private static final String REDIRECT_URI = "redirect_uri";

		@Override
		public String extractKey(OAuth2Authentication authentication) {
			final Map<String, String> values = new LinkedHashMap<String, String>();
			final AuthorizationRequest authorizationRequest = authentication.getAuthorizationRequest();
			if (!authentication.isClientOnly()) {
				values.put(USERNAME, authentication.getName());
			}
			values.put(CLIENT_ID, authorizationRequest.getClientId());
			values.put(REDIRECT_URI, authentication.getAuthorizationRequest().getRedirectUri());
			if (authorizationRequest.getScope() != null) {
				values.put(SCOPE, OAuth2Utils.formatParameterList(authorizationRequest.getScope()));
			}
			MessageDigest digest;
			try {
				digest = MessageDigest.getInstance("MD5");
			} catch (final NoSuchAlgorithmException e) {
				throw new IllegalStateException("MD5 algorithm not available.  Fatal (should be in the JDK).");
			}

			try {
				final byte[] bytes = digest.digest(values.toString().getBytes("UTF-8"));
				return String.format("%032x", new BigInteger(1, bytes));
			} catch (final UnsupportedEncodingException e) {
				throw new IllegalStateException("UTF-8 encoding not available.  Fatal (should be in the JDK).");
			}
		}
	};

	private ObjectMapper mapper = new ObjectMapper();

	/**
	 * Cleanup executed every 10 minutes
	 */
	@Override
	@Scheduled(fixedDelay = 50000)
	public void removeExpired() {
		final Date olderThan = new Date(new Date().getTime() - 600000);
		LOG.debug("Removing OAuth access tokens from before: " + olderThan);
		final int countAccessTokens = accessTokenPersistence.deleteOlderThan(olderThan);
		if (countAccessTokens > 0)
			LOG.info("Removed expired OAuth access tokens: " + countAccessTokens);

		LOG.debug("Removing OAuth refresh tokens from before: " + olderThan);
		final int countRefreshTokens = refreshTokenPersistence.deleteOlderThan(olderThan);
		if (countRefreshTokens > 0)
			LOG.info("Removed expired OAuth refresh tokens: " + countRefreshTokens);
	}

	@Override
	public Collection<OAuth2AccessToken> findTokensByClientId(String clientId) {
		LOG.debug("findTokensByClientId clientId=" + clientId);
		final List<OAuth2AccessToken> tokens = new ArrayList<OAuth2AccessToken>();
		for (final OAuthAccessToken token : accessTokenPersistence.findByClientId(clientId)) {
			if (token != null) {
				tokens.add(toAccessToken(token));
			}
		}
		return tokens;
	}

	@Override
	public Collection<OAuthRefreshToken> findRefreshTokensByClientId(String clientId) {
		return refreshTokenPersistence.findByClientId(clientId);
	}

	@Override
	public Collection<OAuth2AccessToken> findTokensByUserName(String userUuid) {
		LOG.debug("findTokensByUserName username=" + userUuid);
		final List<OAuth2AccessToken> tokens = new ArrayList<OAuth2AccessToken>();
		for (final OAuthAccessToken token : accessTokenPersistence.findByUserUuid(userUuid)) {
			if (token != null) {
				tokens.add(toAccessToken(token));
			}
		}
		return tokens;
	}

	@Override
	public OAuth2AccessToken getAccessToken(OAuth2Authentication authentication) {
		LOG.debug("getAccessToken authentication=" + authentication);
		OAuth2AccessToken accessToken = null;

		final String key = authenticationKeyGenerator.extractKey(authentication);
		try {

			final OAuthAccessToken persisted = accessTokenPersistence.findByAuthenticationId(key);
			accessToken = toAccessToken(persisted);

		} catch (final NullPointerException e) {
			if (LOG.isInfoEnabled()) {
				LOG.debug("Failed to find access token for authentication " + authentication);
			}
		} catch (final IllegalArgumentException e) {
			LOG.error("Could not extract access token for authentication " + authentication);
		}

		return accessToken;
	}

	@Override
	public OAuth2AccessToken readAccessToken(String tokenValue) {
		if (StringUtils.isBlank(tokenValue)) {
			LOG.debug("readAccessToken for blank token is ignored");
			return null;
		}

		LOG.debug("readAccessToken tokenValue=" + tokenValue);
		OAuth2AccessToken accessToken = null;

		try {
			final OAuthAccessToken persisted = accessTokenPersistence.findByValue(tokenValue);
			accessToken = toAccessToken(persisted);
		} catch (final NullPointerException e) {
			if (LOG.isInfoEnabled()) {
				LOG.info("Failed to find access token for token " + tokenValue);
			}
		} catch (final IllegalArgumentException e) {
			LOG.warn("Failed to deserialize access token for " + tokenValue);
		}

		if (accessToken != null) {
			LOG.debug("Access token obtained " + accessToken.getValue());
		}
		return accessToken;
	}

	@SuppressWarnings("unchecked")
	private OAuth2AccessToken toAccessToken(OAuthAccessToken persisted) {

		DefaultOAuth2AccessToken accessToken = new DefaultOAuth2AccessToken(persisted.getValue());
		accessToken.setExpiration(persisted.getExpiration());
		accessToken.setTokenType(persisted.getTokenType());

		if (persisted.getRefreshToken() != null)
			accessToken.setRefreshToken(toRefreshToken(refreshTokenPersistence.findByValue(persisted.getRefreshToken())));

		try {
			accessToken.setScope(mapper.readValue(persisted.getScopes(), HashSet.class));
			accessToken.setAdditionalInformation(mapper.readValue(persisted.getAdditionalInfo(), HashMap.class));
		} catch (IOException e) {
			LOG.error("Could not deserialize accessToken.scope or additionalInformation", e);
		}
		return accessToken;
	}

	private OAuth2RefreshToken toRefreshToken(OAuthRefreshToken rt) {
		if (rt == null)
			return null;

		if (rt.getExpiration() != null) {
			return new DefaultExpiringOAuth2RefreshToken(rt.getValue(), rt.getExpiration());
		} else {
			return new DefaultOAuth2RefreshToken(rt.getValue());
		}
	}

	@Override
	public OAuth2Authentication readAuthentication(OAuth2AccessToken token) {
		LOG.debug("readAuthentication token=" + token);
		return readAuthentication(token.getValue());
	}

	@Override
	public OAuth2Authentication readAuthentication(String token) {
		LOG.debug("readAuthentication " + token);
		OAuth2Authentication authentication = null;

		try {

			final OAuthAccessToken persisted = accessTokenPersistence.findByValue(token);
			authentication = createAuthentication(persisted);

		} catch (final NullPointerException e) {
			if (LOG.isInfoEnabled()) {
				LOG.info("Failed to find access token for token " + token);
			}
		} catch (final IllegalArgumentException e) {
			LOG.warn("Failed to deserialize authentication for " + token);
			removeAccessToken(token);
		} catch (IOException e) {
			LOG.warn("Failed to deserialize scopes for " + token, e);
		}

		return authentication;
	}

	/**
	 * Generate {@link Authentication} from access or refresh token data
	 * 
	 * @param persisted
	 * @return
	 * @throws IOException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 */
	private OAuth2Authentication createAuthentication(final OAuthToken persisted) throws IOException, JsonParseException, JsonMappingException {
		OAuth2Authentication authentication;
		@SuppressWarnings("unchecked")
		DefaultAuthorizationRequest authorizationRequest = new DefaultAuthorizationRequest(persisted.getClientId(), mapper.readValue(persisted.getScopes(),
				HashSet.class));
		authorizationRequest.setApproved(true);
		authorizationRequest.setRedirectUri(persisted.getRedirectUri());

		PreAuthenticatedAuthenticationToken userAuthentication = null;
		if (persisted.getUserUuid() != null) {
			UserDetails userDetails = userService.getUserDetails(persisted.getUserUuid());

			userAuthentication = new PreAuthenticatedAuthenticationToken(userDetails, null, userDetails.getAuthorities());
			userAuthentication.setAuthenticated(true);
		}

		authentication = new OAuth2Authentication(authorizationRequest, userAuthentication);
		authentication.setAuthenticated(true);
		return authentication;
	}

	@Override
	public OAuth2Authentication readAuthenticationForRefreshToken(OAuth2RefreshToken token) {
		LOG.debug("readAuthenticationForRefreshToken " + token.getValue());
		return readAuthenticationForRefreshToken(token.getValue());
	}

	public OAuth2Authentication readAuthenticationForRefreshToken(String value) {
		LOG.debug("readAuthenticationForRefreshToken value=" + value);
		OAuth2Authentication authentication = null;

		try {
			final OAuthRefreshToken persisted = refreshTokenPersistence.findByValue(value);
			authentication = createAuthentication(persisted);

		} catch (final NullPointerException e) {
			if (LOG.isInfoEnabled()) {
				LOG.info("Failed to find refresh token for token " + value);
			}
		} catch (final IllegalArgumentException e) {
			LOG.warn("Failed to deserialize refresh token for " + value);
			removeRefreshToken(value);
		} catch (final IOException e) {
			LOG.warn("Failed to deserialize refresh token for " + value);
			removeRefreshToken(value);
		}

		return authentication;
	}

	@Override
	public OAuth2RefreshToken readRefreshToken(String token) {
		if (StringUtils.isBlank(token)) {
			LOG.debug("readRefreshToken for blank token is ignored");
			return null;
		}

		OAuth2RefreshToken refreshToken = null;
		LOG.debug("readRefreshToken token=" + token);
		try {
			final OAuthRefreshToken persisted = refreshTokenPersistence.findByValue(token);
			refreshToken = persisted.toToken();
		} catch (final NullPointerException e) {
			if (LOG.isInfoEnabled()) {
				LOG.info("Failed to find refresh token for token " + token);
			}
		} catch (final IllegalArgumentException e) {
			LOG.warn("Failed to deserialize refresh token for token " + token);
			removeRefreshToken(token);
		}

		return refreshToken;
	}

	@Override
	public void removeAccessToken(OAuth2AccessToken token) {
		removeAccessToken(token.getValue());
	}

	public void removeAccessToken(String tokenValue) {
		LOG.debug("removeAccessToken token=" + tokenValue);
		accessTokenPersistence.deleteByValue(tokenValue);
	}

	@Override
	public void removeAccessTokenUsingRefreshToken(OAuth2RefreshToken refreshToken) {
		LOG.debug("removeAccessTokenUsingRefreshToken token=" + refreshToken.getValue());
		accessTokenPersistence.deleteByRefreshToken(refreshToken.getValue());
	}

	@Override
	public void removeRefreshToken(OAuth2RefreshToken token) {
		LOG.debug("removeRefreshToken token=" + token.getValue());
		removeRefreshToken(token.getValue());
	}

	public void removeRefreshToken(String value) {
		try {
			refreshTokenPersistence.deleteByValue(value);
			;
		} catch (final EmptyResultDataAccessException e) {
			LOG.warn("Could not delete token " + value);
		}
	}

	@Override
	public void storeAccessToken(OAuth2AccessToken token, OAuth2Authentication authentication) {
		if (authentication == null) {
			LOG.warn("Authentication object is null, ignoring storeAccessToken request.");
			return;
		}

		LOG.info("Storing new access token " + token.getValue());
		if (authentication != null) {
			LOG.debug("Access token authentication " + authentication.getClass() + " auth=" + authentication);
			try {
				LOG.debug(mapper.writeValueAsString(authentication));
			} catch (JsonProcessingException e) {

			}
		}
		final OAuthAccessToken persisted = new OAuthAccessToken();

		persisted.setAuthenticationId(authenticationKeyGenerator.extractKey(authentication));

		DefaultOAuth2AccessToken accessToken = (DefaultOAuth2AccessToken) token;
		persisted.setExpiration(accessToken.getExpiration());
		persisted.setTokenType(accessToken.getTokenType());
		LOG.info("accessToken value=" + accessToken.getValue());
		persisted.setValue(accessToken.getValue());

		try {
			persisted.setScopes(mapper.writeValueAsString(accessToken.getScope()));
			persisted.setAdditionalInfo(mapper.writeValueAsString(accessToken.getAdditionalInformation()));

		} catch (JsonProcessingException e) {
			LOG.error("Could not serialize accessToken", e);
			throw new RuntimeException("Serialization of OAuth2 accessToken failed");
		}

		Authentication userAuthentication = authentication.getUserAuthentication();
		if (userAuthentication != null) {
			Object userPrincipal = userAuthentication.getPrincipal();
			if (userPrincipal != null && userPrincipal instanceof AuthUserDetails) {
				AuthUserDetails authUser = (AuthUserDetails) userPrincipal;
				LOG.info("userPrincipal=" + userPrincipal.getClass() + " " + userPrincipal);
				persisted.setUserUuid(authUser.getUser().getUuid());
			}
		}

		AuthorizationRequest authorizationRequest = authentication.getAuthorizationRequest();
		persisted.setClientId(authorizationRequest.getClientId());
		persisted.setRedirectUri(authorizationRequest.getRedirectUri());
		// persisted.setAuthentication(serializeAuthentication(authentication));

		if (token.getRefreshToken() != null) {
			persisted.setRefreshToken(token.getRefreshToken().getValue());
		}

		persisted.setCreatedDate(new Date());

		accessTokenPersistence.save(persisted);
	}

	@Override
	public void storeRefreshToken(OAuth2RefreshToken refreshToken, OAuth2Authentication authentication) {
		if (authentication == null) {
			LOG.warn("Authentication object is null, ignoring storeRefreshToken request.");
			return;
		}

		LOG.info("Storing new refresh token " + refreshToken.getValue());
		if (authentication != null) {
			LOG.debug("Refresh token authentication " + authentication.getClass() + " auth=" + authentication);
			try {
				LOG.debug(mapper.writeValueAsString(authentication));
			} catch (JsonProcessingException e) {

			}
		}

		final OAuthRefreshToken persisted = new OAuthRefreshToken();

		persisted.setValue(refreshToken.getValue());
		if (refreshToken instanceof DefaultExpiringOAuth2RefreshToken) {
			DefaultExpiringOAuth2RefreshToken expRefreshToken = (DefaultExpiringOAuth2RefreshToken) refreshToken;
			persisted.setExpiration(expRefreshToken.getExpiration());
		}

		Authentication userAuthentication = authentication.getUserAuthentication();
		if (userAuthentication != null) {
			Object userPrincipal = userAuthentication.getPrincipal();
			if (userPrincipal != null && userPrincipal instanceof AuthUserDetails) {
				AuthUserDetails authUser = (AuthUserDetails) userPrincipal;
				LOG.info("userPrincipal=" + userPrincipal.getClass() + " " + userPrincipal);
				persisted.setUserUuid(authUser.getUser().getUuid());
			}
		}

		AuthorizationRequest authorizationRequest = authentication.getAuthorizationRequest();
		try {
			persisted.setScopes(mapper.writeValueAsString(authorizationRequest.getScope()));
		} catch (JsonProcessingException e) {
			LOG.error("Failed to serialize refreshToken.scope", e);
		}
		persisted.setClientId(authorizationRequest.getClientId());
		persisted.setRedirectUri(authorizationRequest.getRedirectUri());

		refreshTokenPersistence.save(persisted);
	}

	@Override
	public void removeAccessToken(long tokenId) {
		accessTokenPersistence.delete(tokenId);
	}

	@Override
	public void removeRefreshToken(long tokenId) {
		refreshTokenPersistence.delete(tokenId);
	}

	// protected String md5Digest(String value) {
	// LOG.debug("md5 of " + value);
	// if (value == null) {
	// return null;
	// }
	// MessageDigest digest;
	// try {
	// digest = MessageDigest.getInstance("MD5");
	// } catch (final NoSuchAlgorithmException e) {
	// throw new
	// IllegalStateException("MD5 algorithm not available.  Fatal (should be in the JDK).");
	// }
	//
	// try {
	// final byte[] bytes = digest.digest(value.getBytes("UTF-8"));
	// return String.format("%032x", new BigInteger(1, bytes));
	// } catch (final UnsupportedEncodingException e) {
	// throw new
	// IllegalStateException("UTF-8 encoding not available.  Fatal (should be in the JDK).");
	// }
	// }

}
