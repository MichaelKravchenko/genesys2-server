/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.genesys2.server.model.BusinessModel;

/**
 * ClassPK serves as a classifier for content elements ({@link Article}).
 *
 * @author mobreza
 *
 */
@Entity
@Table(name = "classname")
public class ClassPK extends BusinessModel {

	private static final long serialVersionUID = 838906032026597957L;

	@Column(length = 250, unique = true, nullable = false)
	private String className;

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	@Override
	public String toString() {
		return "ClassPK " + this.className + " id=" + id;
	}
}
