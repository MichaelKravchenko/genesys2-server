/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes("authorizationRequest")
public class ConfirmationController extends BaseController {

	@Autowired
	protected ClientDetailsService clientDetailsService;

	@RequestMapping("/oauth/confirm_access")
	public String getAccessConfirmation(ModelMap model) throws Exception {
		final AuthorizationRequest clientAuth = (AuthorizationRequest) model.remove("authorizationRequest");
		final ClientDetails client = clientDetailsService.loadClientByClientId(clientAuth.getClientId());
		model.put("auth_request", clientAuth);
		model.put("client", client);
		return "/oauth/confirm";
	}

	@RequestMapping(value = "/oauth/oob", params = { "code" })
	public String showCode(ModelMap model, @RequestParam(value = "code") String code) throws Exception {
		model.addAttribute("code", code);
		return "/oauth/showcode";
	}

	@RequestMapping(value = "/oauth/oob", params = { "error" })
	public String showError(ModelMap model, @RequestParam("error") String error) throws Exception {
		model.addAttribute("error", error);
		return "/oauth/showerror";
	}

	@RequestMapping(value = "/oauth/error")
	public String handleError(ModelMap model, HttpServletRequest request) throws Exception {
		Object e = request.getAttribute("error");
		if (e instanceof OAuth2Exception) {
			OAuth2Exception ex = (OAuth2Exception) e;
			model.put("error", ex.getOAuth2ErrorCode());
			model.put("errorDescription", ex.getLocalizedMessage());
		}
		model.put("message", "There was a problem with the OAuth2 protocol");
		return "/oauth/showerror";
	}
}
