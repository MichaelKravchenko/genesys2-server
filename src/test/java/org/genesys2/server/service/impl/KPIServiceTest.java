/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.genesys2.server.model.kpi.BooleanDimension;
import org.genesys2.server.model.kpi.Dimension;
import org.genesys2.server.model.kpi.DimensionKey;
import org.genesys2.server.model.kpi.Execution;
import org.genesys2.server.model.kpi.NumericListDimension;
import org.genesys2.server.model.kpi.Observation;
import org.genesys2.server.model.kpi.KPIParameter;
import org.genesys2.server.model.kpi.StringListDimension;
import org.genesys2.server.service.KPIService;
import org.genesys2.server.test.JpaDataConfig;
import org.genesys2.server.test.PropertyPlacholderInitializer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = KPIServiceTest.Config.class, initializers = PropertyPlacholderInitializer.class)
public class KPIServiceTest {
	private static Logger log = Logger.getLogger(KPIService.class);

	@Import(JpaDataConfig.class)
	@ComponentScan(basePackages = { "org.genesys2.server.persistence.domain" })
	public static class Config {

		@Bean
		public KPIService kpiService() {
			return new KPIServiceImpl();
		}
	}

	@Autowired
	private KPIService kpiService;

	@Test
	public void testzz() {
		log.info("Test 1");

		Object res = null;
		res = kpiService
				.getSingleResult(
						"select count(distinct _base) from Accession _base inner join _base.institute _ped1  where (10 member of _base.stoRage) and ( _ped1.code = ?1 )",
						"HUN003");
		printRes(res);
	}

	// @Test
	public void test1() {
		log.info("Test 1");

		Object res = null;
		res = kpiService.getSingleResult("select count(a) from Accession a where a.mlsStatus=?1", true);
		printRes(res);

		res = kpiService.getSingleResult("select count(a) from Accession a inner join a.institute i where a.mlsStatus=?1 and i.code=?2", true, "NGA039");
		printRes(res);

		List<String> instlist = new ArrayList<String>();
		instlist.add("NGA039");
		instlist.add("PHL001");
		res = kpiService.getSingleResult("select count(a) from Accession a inner join a.institute i where a.mlsStatus=?1 and i.code in ( ?2 )", true, instlist);
		printRes(res);

		res = kpiService.getSingleResult("select count(a) from Accession a where ?1 in elements(a.stoRage)", 10);
		printRes(res);
		List<Integer> storagelist = new ArrayList<Integer>();
		storagelist.add(10);
		storagelist.add(11);
		storagelist.add(30);
		res = kpiService.getSingleResult("select count(distinct a) from Accession a inner join a.stoRage st where st in ( ?1 )", storagelist);
		printRes(res);

		res = kpiService.getSingleResult(
				"select count(distinct a) from Accession a inner join a.institute _ped1  where ( _ped1.code = ?1 ) and ( a.sampleStatus = ?2 )", "NGA039", 110);
		printRes(res);

		res = kpiService
				.getSingleResult(
						"select count(distinct a) from Accession a inner join a.institute _ped1  inner join a.taxonomy.cropTaxonomies _ped3  where ( _ped1.code = ?1 ) and ( a.sampleStatus = ?2 ) and ( _ped3.crop.shortName = ?3 )",
						"NGA039", 110, "banana");
		printRes(res);
	}

	private void printRes(Object res) {
		if (res != null)
			log.info("Result=" + res + " " + res.getClass());
		else
			log.info("Result is null");
	}

	public static void main(String[] a) {
		new KPIServiceTest().test2();
	}

	@Test
	public void test2() {
		StringListDimension dimInstCode = new StringListDimension();
		dimInstCode.setTitle("Holding institute code");
		dimInstCode.setName("instCode");
		Set<String> instlist = new HashSet<String>();
		instlist.add("NGA039");
		instlist.add("PHL001");
		instlist.add("MEX002");
		instlist.add("SYR002");
		dimInstCode.setValues(instlist);

		NumericListDimension dimSampStat = new NumericListDimension();
		dimSampStat.setTitle("Biological status of accession");
		dimSampStat.setName("sampleStatus");
		Set<Number> samplist = new HashSet<Number>();
		samplist.add(100);
		samplist.add(110);
		samplist.add(120);
		dimSampStat.setValues(samplist);

		StringListDimension dimCrop = new StringListDimension();
		dimCrop.setTitle("Crop name");
		dimCrop.setName("crop");
		Set<String> croplist = new HashSet<String>();
		croplist.add("banana");
		croplist.add("wheat");
		croplist.add("maize");
		croplist.add("rice");
		dimCrop.setValues(croplist);

		KPIParameter accessionCount = new KPIParameter();
		accessionCount.setName("accession.count");
		accessionCount.setEntity("Accession");

		Execution paramExec = new Execution();
		paramExec.setName("foobar");
		paramExec.setParameter(accessionCount);
		paramExec.addDimension(dimInstCode, "institute", "code");
		paramExec.addDimension(dimSampStat, null, "sampleStatus");
		paramExec.addDimension(dimCrop, "taxonomy.cropTaxonomies", "crop.shortName");

		String paQuery = paramExec.query();
		log.info("Query = " + paQuery);

		execute(paQuery, paramExec, 0, new ArrayList<Object>());
	}

	@Test
	public void testAccessionsInMLS() {
		StringListDimension dimInstCode = new StringListDimension();
		dimInstCode.setTitle("Holding institute code");
		dimInstCode.setName("instCode");
		Set<String> instlist = new HashSet<String>();
		instlist.add("NGA039");
		instlist.add("PHL001");
		instlist.add("AUT007");
		instlist.add("MEX002");
		instlist.add("SYR002");
		dimInstCode.setValues(instlist);

		BooleanDimension dimBoolean = new BooleanDimension();
		dimBoolean.setTitle("Yes/No values");
		dimBoolean.setName("bool");

		KPIParameter accessionsInMls = new KPIParameter();
		accessionsInMls.setName("accession.mls");
		accessionsInMls.setEntity("Accession");

		Execution paramExec = new Execution();
		paramExec.setParameter(accessionsInMls);
		paramExec.addDimension(dimInstCode, "institute", "code");
		paramExec.addDimension(dimBoolean, null, "mlsStatus");

		String paQuery = paramExec.query();
		log.info("Query = " + paQuery);

		execute(paQuery, paramExec, 0, new ArrayList<Object>());
	}

	private void execute(String paQuery, Execution paramExec, int depth, List<Object> params) {
		Dimension<?> dim = paramExec.getDimension(depth);
		if (dim == null) {
			// execute
			log.info("Executing: " + paQuery + " params=" + params);
			if (kpiService != null) {
				Object res = kpiService.getSingleResult(paQuery, params.toArray());
				printRes(res, paramExec, params.toArray());
			}
		} else {
			// Recurse
			for (Object val : dim.getValues()) {
				params.add(val);
				execute(paQuery, paramExec, depth + 1, params);
				params.remove(depth);
			}
		}
	}

	private void printRes(Object res, Execution paramExec, Object[] array) {
		log.info("Reporting result\n\n");
		KPIParameter kPIParameter = paramExec.getParameter();
		if (res != null)
			log.info(kPIParameter.getName() + "=" + res + " " + res.getClass());
		else {
			log.warn(kPIParameter.getName() + " has no result (null)");
			return;
		}

		Observation observation = new Observation();
		observation.setValue((Long) res);
		for (int i = 0; i < array.length; i++) {
			DimensionKey dk = new DimensionKey();
			dk.setName(paramExec.getDimension(i).getName());
			dk.setValue(array[i].toString());
			log.info("\t\t" + dk);

			observation.getDimensions().add(dk);
		}
		log.info("OBSERVATION: " + observation);
	}
}
