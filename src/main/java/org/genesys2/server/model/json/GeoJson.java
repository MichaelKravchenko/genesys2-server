/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.json;

public class GeoJson {
	private Double latitude;
	private Double longitude;
	private Double elevation;
	private Double coordUncert;
	private String coordDatum;
	private String georefMeth;

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getElevation() {
		return elevation;
	}

	public void setElevation(Double elevation) {
		this.elevation = elevation;
	}

	public Double getCoordUncert() {
		return coordUncert;
	}

	public void setCoordUncert(Double coordUncert) {
		this.coordUncert = coordUncert;
	}

	public String getCoordDatum() {
		return coordDatum;
	}

	public void setCoordDatum(String coordDatum) {
		this.coordDatum = coordDatum;
	}

	public String getGeorefMeth() {
		return georefMeth;
	}

	public void setGeorefMeth(String georefMeth) {
		this.georefMeth = georefMeth;
	}

}
