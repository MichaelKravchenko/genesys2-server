<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="metadata.page.view.title" /></title>
</head>
<body>
	<h1>
		<c:out value="${metadata.title}" />
	</h1>
	
	<div class="main-col-header clearfix">
	<div class="nav-header pull-left">
		<div class="results"><spring:message code="paged.totalElements" arguments="${pagedData.totalElements}" /></div>
		<div class="pagination">
		<spring:message code="paged.pageOfPages" arguments="${pagedData.number+1},${pagedData.totalPages}" />
		<a class="${pagedData.number eq 0 ? 'disabled' :''}" href="?page=${pagedData.number eq 0 ? 1 : pagedData.number}"><spring:message code="pagination.previous-page" /></a> <a href="?page=${pagedData.number + 2}"><spring:message code="pagination.next-page" /></a>
		</div>
	</div>
	</div>

	<c:if test="${page eq 1}">
		<table>
			<thead>
				<tr>
					<td><spring:message code="ce.trait" /></td>
					<td><spring:message code="ce.method" /></td>
					<td><spring:message code="unit-of-measure" /></td>
					<td><spring:message code="method.fieldName" /></td>
			</thead>
			<tbody>
				<c:forEach items="${methods}" var="method">
					<tr>
						<td><a href="<c:url value="/descriptors/${method.parameter.id}" />"><c:out value="${method.parameter.title}" /></a></td>
						<td><a href="<c:url value="/descriptors/${method.parameter.id}/${method.id}" />"><c:out value="${method.method}" /></a></td>
						<td><c:out value="${method.unit}" /></td>
						<td><c:out value="${method.fieldName}" /></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		
		<p>
			<c:out value="${metadata.description}" />
		</p>
		<p>
			<c:out value="${metadata.location}" />
		</p>
		<p>
			<c:out value="${metadata.citation}" />
		</p>
		<p>
			<c:out value="${metadata.SDate}" />
			<c:out value="${metadata.EDate}" />
		</p>
		<div>
			<c:out value="${metadata.description}" escapeXml="false" />
		</div>


		<div>
			<form class="form-horizontal" method="post" action="/data/view/${metadata.id}/dwca">
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				<div class="row" style="margin-top: 2em;">
					<div class="col-sm-4">
						<button class="btn btn-primary" type="submit"><spring:message code="metadata.download-dwca" /></button>
					</div>
				</div>
			</form>
		</div>
		
		<c:if test="${faoInstitute ne null}">
		<p>
			<a class="" href="<c:url value="/wiews/${faoInstitute.code}" />"><b><c:out value="${faoInstitute.code}" /></b> <c:out value="${faoInstitute.fullName}" /></a> 
		</p>
		</c:if>
		
		<div class="audit-info">
			<c:if test="${metadata.lastModifiedBy ne null}"><spring:message code="audit.lastModifiedBy" arguments="${jspHelper.userFullName(metadata.lastModifiedBy)}" /></c:if>
			<fmt:formatDate value="${metadata.lastModifiedDate}" type="both" />
		</div>
	</c:if>


	<div class="table-responsive">
	<table class="accessions wide-table">
		<thead>
			<tr>
				<td class="idx-col"></td>
				<td />
				<td><spring:message code="accession.accessionName" /></td>
				<%-- 		<td><spring:message code="accession.origin" /></td>
		 --%>
				<td><spring:message code="accession.taxonomy" /></td>
				<%-- 			<td><spring:message code="accession.holdingInstitute" /></td>
				<td><spring:message code="accession.holdingCountry" /></td>
	 --%>
				<c:forEach items="${methods}" var="method">
					<td><c:out value="${method.fieldName}" /> <c:if test="${method.unit ne null}">[<c:out value="${method.unit}" />]</c:if></td>
				</c:forEach>



			</tr>
		</thead>
		<tbody>
			<c:forEach items="${pagedData.content}" var="accession" varStatus="status">
				<tr class="acn ${status.count % 2 == 0 ? 'even' : 'odd'}">
					<td class="idx-col">${status.count + pagedData.size * pagedData.number}</td>
					<td class="sel" x-aid="${accession.id}"></td>
					<td><a href="<c:url value="/acn/id/${accession.id}" />"><b><c:out value="${accession.accessionName}" /></b></a></td>
					<%-- 		<td><a href="<c:url value="/geo/${accession.origin}" />"><c:out value="${accession.countryOfOrigin.getName(pageContext.response.locale)}" /></a></td>
			 --%>
					<td><c:out value="${accession.taxonomy.taxonName}" /></td>
					<%-- 	<td><a href="<c:url value="/wiews/${accession.institute.code}" />"><c:out value="${accession.institute.code}" /></a></td>
					<td><a href="<c:url value="/geo/${accession.institute.country.code3}" />"><c:out value="${accession.institute.country.getName(pageContext.response.locale)}" /></a></td>
				 --%>

					<c:set value="${accessionMethods[accession.id]}" var="methodValues" />
					<c:forEach items="${methods}" var="method">
						<td><c:forEach items="${methodValues[method.id]}" var="methodValue">
							<c:out value="${method.coded ? method.decode(methodValue) : methodValue}" />
							</c:forEach></td>
					</c:forEach>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</div>
	
<content tag="javascript">	
	<script type="text/javascript">
	<%@include file="/WEB-INF/jsp/wiews/ga.jsp"%>
	</script>
</content>
</body>
</html>