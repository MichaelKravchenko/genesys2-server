/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.genesys2.server.model.kpi.BooleanDimension;
import org.genesys2.server.model.kpi.Dimension;
import org.genesys2.server.model.kpi.DimensionKey;
import org.genesys2.server.model.kpi.Execution;
import org.genesys2.server.model.kpi.ExecutionDimension;
import org.genesys2.server.model.kpi.ExecutionRun;
import org.genesys2.server.model.kpi.JpaDimension;
import org.genesys2.server.model.kpi.KPIParameter;
import org.genesys2.server.model.kpi.NumericListDimension;
import org.genesys2.server.model.kpi.Observation;
import org.genesys2.server.model.kpi.StringListDimension;
import org.genesys2.server.persistence.domain.kpi.DimensionKeyRepository;
import org.genesys2.server.service.KPIService;
import org.genesys2.server.test.JpaDataConfig;
import org.genesys2.server.test.PropertyPlacholderInitializer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = KPIEntitiesTest.Config.class, initializers = PropertyPlacholderInitializer.class)
public class KPIEntitiesTest {
	private static Logger log = Logger.getLogger(KPIEntitiesTest.class);

	@Import(JpaDataConfig.class)
	// @ComponentScan(basePackages = { "org.genesys2.server.persistence.domain"
	// })
	public static class Config {

		@Bean
		public KPIService kpiService() {
			return new KPIServiceImpl();
		}
	}

	@Autowired
	private KPIService kpiService;
	@Autowired
	private DimensionKeyRepository dimensionKeyRepository;

	@Test
	public void persistStringListDimension() {
		StringListDimension dimInstCode = new StringListDimension();
		dimInstCode.setTitle("Holding institute code");
		dimInstCode.setName("instCode");
		Set<String> list = new HashSet<String>();
		list.add("NGA039");
		list.add("PHL001");
		list.add("MEX002");
		list.add("SYR002");
		dimInstCode.setValues(list);
		log.debug(dimInstCode.getValues());
		Dimension<?> savedDim = kpiService.save(dimInstCode);
		assertTrue(savedDim == dimInstCode);
		assertTrue(savedDim.getId() != null);

		Dimension<?> loaded = kpiService.getDimension(savedDim.getId());
		assertTrue(loaded != savedDim);
		assertTrue(StringUtils.equals(loaded.getName(), dimInstCode.getName()));
		assertTrue(StringUtils.equals(loaded.getTitle(), dimInstCode.getTitle()));
		assertTrue(loaded instanceof StringListDimension);
		log.debug(loaded.getValues());
		assertTrue(loaded.getValues().containsAll(list));
		assertTrue(list.containsAll(loaded.getValues()));
	}

	@Test
	public void persistNumericListDimension() {
		NumericListDimension dimNum = new NumericListDimension();
		dimNum.setTitle("Biological status of sample");
		dimNum.setName("sampStat");
		dimNum.setClazz(Long.class);
		Set<Number> list = new HashSet<Number>();
		list.add(1l);
		list.add(2l);
		list.add(3l);
		list.add(4l);
		dimNum.setValues(list);
		log.debug(dimNum.getValues());
		Dimension<?> savedDim = kpiService.save(dimNum);
		assertTrue(savedDim == dimNum);
		assertTrue(savedDim.getId() != null);

		Dimension<?> loaded = kpiService.getDimension(savedDim.getId());
		assertTrue(loaded != savedDim);
		assertTrue(StringUtils.equals(loaded.getName(), dimNum.getName()));
		assertTrue(StringUtils.equals(loaded.getTitle(), dimNum.getTitle()));
		assertTrue(loaded instanceof NumericListDimension);
		log.debug(loaded.getValues());
		NumericListDimension nl = (NumericListDimension) loaded;
		log.debug(nl.getClazz());
		assertTrue(nl.getClazz() == Long.class);
		assertTrue(loaded.getValues().containsAll(list));
		assertTrue(list.containsAll(loaded.getValues()));
	}

	@Test
	public void persistBooleanListDimension() {
		BooleanDimension dim = new BooleanDimension();
		dim.setTitle("Yes/No dimension");
		dim.setName("boolean");
		assertTrue(dim.hasFalse());
		assertTrue(dim.hasTrue());
		log.debug(dim.getValues());
		
		dim.useFalse(false);
		assertFalse(dim.hasFalse());
		assertTrue(dim.hasTrue());
		dim.useTrue(false);
		assertFalse(dim.hasFalse());
		assertFalse(dim.hasTrue());
		dim.setMode(3);
		assertTrue(dim.hasTrue());
		assertTrue(dim.hasFalse());
		
		Dimension<?> savedDim = kpiService.save(dim);
		assertTrue(savedDim == dim);
		assertTrue(savedDim.getId() != null);
		
		// For comparison
		Set<Boolean> list = new HashSet<Boolean>();
		list.add(true);
		list.add(false);
		
		Dimension<?> loaded = kpiService.getDimension(savedDim.getId());
		assertTrue(loaded != savedDim);
		assertTrue(StringUtils.equals(loaded.getName(), dim.getName()));
		assertTrue(StringUtils.equals(loaded.getTitle(), dim.getTitle()));
		assertTrue(loaded instanceof BooleanDimension);
		log.debug(loaded.getValues());
		assertTrue(loaded.getValues().containsAll(list));
		assertTrue(list.containsAll(loaded.getValues()));
	}

	@Test
	public void persistJpaDimension() {
		JpaDimension dimJpa = new JpaDimension();
		dimJpa.setTitle("Example JPA Entity#field dimension");
		dimJpa.setName("jpadimension");
		dimJpa.setEntity("Dimension");
		dimJpa.setField("name");
		dimJpa.setCondition("version > -1");

		Dimension<?> savedDim = kpiService.save(dimJpa);
		assertTrue(savedDim == dimJpa);
		assertTrue(savedDim.getId() != null);

		Dimension<?> loaded = kpiService.getDimension(savedDim.getId());
		assertTrue(loaded != savedDim);
		assertTrue(StringUtils.equals(loaded.getName(), dimJpa.getName()));
		assertTrue(StringUtils.equals(loaded.getTitle(), dimJpa.getTitle()));
		assertTrue(loaded instanceof JpaDimension);
		JpaDimension loadedJpa = (JpaDimension) loaded;
		assertTrue(StringUtils.equals(loadedJpa.getEntity(), dimJpa.getEntity()));
		assertTrue(StringUtils.equals(loadedJpa.getField(), dimJpa.getField()));

		try {
			loadedJpa.getValues();
			fail("JpaDimension#getValues() must throw RuntimeException");
		} catch (RuntimeException e) {
		}

		Set<?> values = kpiService.getValues(loadedJpa);
		log.info(values);
		assertTrue(values.contains("jpadimension"));
	}

	@Test
	public void persistParameter() {
		KPIParameter parameter = new KPIParameter();
		parameter.setTitle("KPI Dimensions");
		parameter.setDescription("Total number of KPI Dimensions defined");
		parameter.setName("dimension.count." + System.currentTimeMillis());
		parameter.setEntity("Dimension");

		KPIParameter persisted = kpiService.save(parameter);
		assertTrue(persisted.getId() != null);

		KPIParameter loaded = kpiService.getParameter(parameter.getName());
		assertTrue(loaded != null);
		assertTrue(loaded != parameter);
		assertTrue(StringUtils.equals(loaded.getName(), parameter.getName()));
		assertTrue(StringUtils.equals(loaded.getTitle(), parameter.getTitle()));
		assertTrue(StringUtils.equals(loaded.getEntity(), parameter.getEntity()));
		assertTrue(StringUtils.equals(loaded.getDescription(), parameter.getDescription()));

		KPIParameter parameter2 = new KPIParameter();
		parameter2.setTitle("KPI Dimensions 2");
		parameter2.setDescription("Total number of KPI Dimensions defined ");
		parameter2.setName("dimension.count");
		try {
			kpiService.save(parameter2);
			fail("KPIService should not allow duplicate parameter names!");
		} catch (DataIntegrityViolationException e) {
			// All good!
		}

		// drop it
		kpiService.delete(loaded);
	}

	@Test
	public void persistExecution() {
		KPIParameter parameter = new KPIParameter();
		parameter.setTitle("KPI Dimensions");
		parameter.setDescription("Total number of KPI Dimensions defined");
		parameter.setName("dimension.count");
		parameter.setEntity("Dimension");
		kpiService.save(parameter);

		NumericListDimension dimVersion = new NumericListDimension();
		dimVersion.setClazz(Long.class);
		dimVersion.setName("version");
		dimVersion.setTitle("Version numbers");
		Set<Number> list = new HashSet<Number>();
		list.add(0l);
		list.add(1l);
		list.add(2l);
		dimVersion.setValues(list);
		for (Number n : dimVersion.getValues()) {
			assertTrue(n instanceof Long);
		}
		kpiService.save(dimVersion);

		Execution execution = new Execution();
		execution.setName("foobar");
		execution.setParameter(parameter);
		execution.addDimension(dimVersion, null, "version");

		String paQuery = execution.query();
		log.info("Query = " + paQuery);

		kpiService.save(execution);
		
		
		Execution execution2 = new Execution();
		execution2.setName("versionexec");
		execution2.setParameter(parameter);
		execution2.addDimension(dimVersion, null, "version");
		kpiService.save(execution2);

		Execution loaded = kpiService.getExecution(execution.getId());
		for (ExecutionDimension d : loaded.getExecutionDimensions()) {
			log.debug("ED = " + d);
		}
		assertEquals(loaded.getParameter().getId(), parameter.getId());
		assertTrue(loaded.getExecutionDimensions().size() == execution.getExecutionDimensions().size());

		List<Observation> observations = kpiService.execute(execution);
		for (Observation obs : observations) {
			log.info(obs);
		}

		List<Observation> observations2 = kpiService.execute(execution);
		for (Observation obs : observations2) {
			log.info(obs);
		}

		kpiService.save(execution, observations);
		kpiService.save(execution2, observations2);

		// Let's check DimensionKey
		List<DimensionKey> dks = dimensionKeyRepository.findAll();
		for (DimensionKey dk : dks) {
			log.info("DK=" + dk);
		}

		// drop it
		log.info("DELETING!!!!");
		ExecutionRun executionRun = kpiService.getLastExecutionRun(execution);
		kpiService.deleteObservations(executionRun);
		kpiService.delete(execution);

		// Let's check DimensionKey again
		dks = dimensionKeyRepository.findAll();
		for (DimensionKey dk : dks) {
			log.info("DK=" + dk);
		}

		ExecutionRun executionRun2 = kpiService.getLastExecutionRun(execution2);
		kpiService.deleteObservations(executionRun2);
		kpiService.delete(execution2);
	}
}
