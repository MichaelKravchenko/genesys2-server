/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.genesys2.server.service.HtmlSanitizer;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@Ignore
@ContextConfiguration(locations = { "classpath:spring/servlet.xml", "classpath:spring/application-context.xml" })
public class HtmlSanitizerTest {

	@Autowired
	private HtmlSanitizer htmlSanitizer;

	@Test
	public void test1() {
		assertNotNull(htmlSanitizer);

		assertNull(htmlSanitizer.sanitize(null));
		assertEquals("", htmlSanitizer.sanitize(""));
		assertEquals("genesys 1 2 3", htmlSanitizer.sanitize("genesys 1 2 3"));
		assertEquals("<p>genesys 1 2 3</p>", htmlSanitizer.sanitize("<p>genesys 1 2 3</p>"));
		assertEquals("<p>genesys 1 2 3</p>", htmlSanitizer.sanitize("<p>genesys 1 2 3<script>alert('haha!');</script></p>"));
		assertEquals("<p>genesys 1<br />2 3</p>", htmlSanitizer.sanitize("<p>genesys 1<br />2 3</p>"));
		assertEquals("<p>genesys 1 2 3</p>", htmlSanitizer.sanitize("<p>genesys 1 <span>2</span> 3</p>"));
		assertEquals("<p>genesys 1</p><p>genesys 1</p>", htmlSanitizer.sanitize("<p>genesys 1</p><p>genesys 1</p>"));

		assertEquals(
				"GENESYS is the result of collaboration between <a href=\"http://www.bioversityinternational.org/\" rel=\"nofollow\">Bioversity International</a>",
				htmlSanitizer
						.sanitize("GENESYS is the result of collaboration between <a target='_blank' href='http://www.bioversityinternational.org/'>Bioversity International</a>"));
	}
}
