/**
 * Copyright 2013 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * This superclass adds a Resource Description Framework (RDF) Uniform Resource
 * Identifier (URI) to records to link them with semantic web linked open
 * (meta-)data
 *
 * @author Richard Bruskiewich
 *
 */
@MappedSuperclass
public abstract class GlobalVersionedAuditedModel extends VersionedAuditedModel {

	/**
	 *
	 */
	private static final long serialVersionUID = 1771989294484858302L;
	@Column(length = 500)
	private String rdfUri;

	public String getRdfUri() {
		return this.rdfUri;
	}

	public void setRdfUri(final String rdfUri) {
		this.rdfUri = rdfUri;
	}

	@JsonIgnore
	public String getRdfUriId() {
		if (this.rdfUri == null) {
			return "";
		} else {
			// first, check for hash URIs
			final String[] hashpart = rdfUri.split("#");

			if (hashpart.length > 1) {
				// assumed well-formed URI.. tail part is the RDF ID
				return hashpart[1];
			} else {
				// not a hash uri... hmmm...
				// since this should be a term URI,
				// then take the tail part of the path as the RDF ID
				return rdfUri.substring(rdfUri.lastIndexOf('/') + 1);
			}
		}
	}
}
