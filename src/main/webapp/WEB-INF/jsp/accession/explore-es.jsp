<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="accession.page.data.title" /></title>
</head>
<body>
	<h1>
		<spring:message code="accession.page.data.title" />
	</h1>
    <%--Dropdown filters--%>
	<div class="main-col-header clearfix">
        
	<div class="nav-header">
		<div class="results">
			<spring:message code="accessions.number" arguments="${pagedData.totalElements}" />
		</div>
		<div class="row">
			<div class="col-sm-12 col-md-6">
				<form method="get" action="">
					<input type="hidden" name="filter" value="<c:out value="${jsonFilter}" />" />
					<div class="pagination">
						<spring:message code="paged.pageOfPages" arguments="${pagedData.number+1},${pagedData.totalPages}" />
						<a href="<spring:url value=""><spring:param name="page" value="${pagedData.number eq 0 ? 1 : pagedData.number}" /><spring:param name="filter" value="${jsonFilter}" /></spring:url>"><spring:message code="pagination.previous-page" /></a>
						<input class="form-control" style="display: inline; max-width: 5em; text-align: center" type="text" name="page" placeholder="${pagedData.number + 1}" />
						<a href="<spring:url value=""><spring:param name="page" value="${pagedData.number+2}" /><spring:param name="filter" value="${jsonFilter}" /></spring:url>"><spring:message code="pagination.next-page" /></a>
					</div>
				</form>
			</div>
			<div class="col-sm-12 col-md-6" style="text-align: right; padding-top: 12px">
				<c:if test="${pagedData.totalElements le 100000}">
				<form style="display: inline-block" method="post" action="/explore/dwca">
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					<input type="hidden" name="filter" value="<c:out value="${jsonFilter}" />" />
					<button class="btn btn-default" type="submit"><spring:message code="filter.download-dwca" /></button>
				</form>
				</c:if>
				<a class="btn btn-default" href="<c:url value="/explore/map"><c:param name="filter">${jsonFilter}</c:param></c:url>"><span class="glyphicon glyphicon-globe"></span><span style="margin-left: 0.5em;"><spring:message code="maps.view-map" /></span></a>
			</div>
		</div>
	</div>
	</div>

    <%--Filters--%>
    <div id="toggleFilters" class="applied-filters">
    	<ul class="nav nav-pills">
	    	<li class="active filter-toggler"><a><spring:message code="filters.toggle-filters" /></a></li>
	    	<li class="message right">
				<span class="${fn:length(currentFilters) gt 0 ? '' : 'hide'}"><spring:message code="filter.filters-applied" /></span>
				<span class="${fn:length(currentFilters) gt 0 ? 'hide' : ''}"><spring:message code="filter.filters-not-applied" /></span>
	    	</li>
		</ul>
    </div>
    <div id="allfilters" class="applied-filters hide">
    	<ul class="nav nav-pills">
	    	<li class="filter-toggler"><a><spring:message code="filters.toggle-filters" /></a></li>
            <li class="active dropdown form-horizontal" id="menu1">

                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <spring:message code="filter.add"/>
                    <b class="glyphicon-plus"></b>
                </a>

                <ul class="dropdown-menu">
                    <c:forEach items="${availableFilters}" var="filter">
                        <li><a href="#" i-key="${filter.key}" class="filter-enable">
                            <spring:message code="filter.${filter.key}"/></a></li>
                    </c:forEach>
                </ul>

            </li>


            <li class="dropdown form-horizontal" id="menu2" style="display: none">
                <a class="" data-toggle="modal" data-target="#myModal">
                    <spring:message code="filter.additional"/>
                    <b class="glyphicon-plus"></b>
                </a>
            </li>
			<li class="message right">
				<span class="${fn:length(currentFilters) gt 0 ? '' : 'hide'}"><spring:message code="filter.filters-applied" /></span>
				<span class="${fn:length(currentFilters) gt 0 ? 'hide' : ''}"><spring:message code="filter.filters-not-applied" /></span>
	    	</li>
        </ul>

		<%-- Only render currently present filters --%>
        <c:forEach items="${currentFilters}" var="filter">

            <c:set var="normalizedKey" value="${filter.key.replace('.', '-').replace(':', '_')}"/>

            <div class="clearfix filter-block" id="<c:out value="${normalizedKey}" />_filter" norm-key="<c:out value="${normalizedKey}" />" i-key="<c:out value="${filter.key}" />">
                <div class="col-lg-3 edit-fil">
                    <c:if test="${not filter.core}">
                        <c:out value="${filter.title}" />
                        <%-- <a href="<c:url value="/descriptors/${filter.key}" />"> --%>
                    </c:if>

                    <c:if test="${filter.core}">
						<spring:message code="filter.${filter.key}" />
                    </c:if>
                </div>
                <div class="col-lg-5 filter-new">
                    <c:choose>
                        <c:when test="${filter.filterType=='LIST'}">
                            <div class="">
                                <c:forEach items="${filter.options}" var="option">
                                    <div>
                                        <label>
                                            <input class="filter-list" id="<c:out value="${normalizedKey}${option.value}" />_input" ${fn:contains(filters[filter.key], option.value)?'checked':''} norm-key="<c:out value="${normalizedKey}" />" i-key="<c:out value="${filter.key}" />" type="checkbox" value="${option.value}" />
                                            <spring:message code="${option.name}"/>
                                        </label>
                                    </div>
                                </c:forEach>
                            </div>
                        </c:when>
                        <c:when test="${filter.filterType=='I18NLIST'}">
                            <div class="">
                                <c:forEach items="${filter.options}" var="option">
                                    <div>
                                        <label>
                                            <input class="filter-list" id="<c:out value="${normalizedKey}${option.value}" />_input" ${fn:contains(filters[filter.key], option.value)?'checked':''} norm-key="<c:out value="${normalizedKey}" />" i-key="<c:out value="${filter.key}" />" type="checkbox" value="${option.value}" />
                                            <spring:message code="${option.name}"/>
                                        </label>
                                    </div>
                                </c:forEach>
                            </div>
                        </c:when>
                        <c:when test="${filter.filterType=='AUTOCOMPLETE'}">
                            <div class="ui-front">
                                <div class="form-group input-group">
                                     <span class="input-group-btn">
                                         <input id="<c:out value="${normalizedKey}" />_input"  class="span2 form-control autocomplete-filter string-type" x-source="${filter.autocompleteUrl}" placeholder="<spring:message code="filter.autocomplete-placeholder" />" type="text"/>
                                         <button class="btn notimportant filter-auto" norm-key="<c:out value="${normalizedKey}" />" i-key="<c:out value="${filter.key}" />">+</button>
                                      </span>
                                </div>
                            </div>
                        </c:when>
                        <c:when test="${filter.dataType=='NUMERIC'}">
                            <div class="form-group input-group">
                                <span class="input-group-btn">
                                    <input id="<c:out value="${normalizedKey}" />_input_1" class="span5 form-control" type="text"/>
                                    <input id="<c:out value="${normalizedKey}" />_input_2" class="span5 form-control" type="text"/>
                                    <button class="btn notimportant filter-range" norm-key="<c:out value="${normalizedKey}" />" i-key="<c:out value="${filter.key}" />">+</button>
                                </span>
                            </div>
                        </c:when>
                        <c:when test="${filter.dataType=='BOOLEAN'}">
                            <div class="">
                                <div><label><input type="checkbox" ${fn:contains(filters[filter.key], 'true')?'checked':''} class="filter-bool"  i-key="<c:out value="${filter.key}" />" id="<c:out value="${normalizedKey}" />" value="true"><spring:message code="boolean.true"/></label></div>
                                <div><label><input type="checkbox" ${fn:contains(filters[filter.key], 'false')?'checked':''} class="filter-bool" i-key="<c:out value="${filter.key}" />" id="<c:out value="${normalizedKey}" />" value="false"><spring:message code="boolean.false"/></label></div>
                                <div><label><input type="checkbox" ${fn:contains(filters[filter.key], 'null')?'checked':''}  class="filter-bool" i-key="<c:out value="${filter.key}" />" id="<c:out value="${normalizedKey}" />" value="null"><spring:message code="boolean.null"/></label></div>
                            </div>
                        </c:when>
                        <c:when test="${filter.key=='crops'}">
                            <div class="form-group input-group">
                                <span class="input-group-btn">
                                    <select name="crops" id="cropselector" i-key="${filter.key}" class="form-control filter-crop">
                                        <option value=""></option>
                                        <c:forEach items="${crops}" var="c">
                                            <option  value="${c.shortName}" ${c.shortName== crop.shortName?'selected':''} ><c:out value="${c.getName(pageContext.response.locale)}" /></option>
                                        </c:forEach>
                                    </select>
                                </span>
                            </div>
                        </c:when>
                        <c:otherwise>
                        <div class="ui-front" >
                            <div class="form-group input-group">
                                <span class="input-group-btn">
                                    <select id="like-switcher" class="form-control select-like">
                                        <option value="like"><spring:message code="filter.string.like" /></option>
                                        <option value="equals"><spring:message code="filter.string.equals" /></option>
                                    </select>
                                    <input class="span2 form-control string-type" id="<c:out value="${normalizedKey}" />_input" type="text"/>
                                    <button class="btn notimportant filter-auto" norm-key="<c:out value="${normalizedKey}" />" i-key="<c:out value="${filter.key}" />">+</button>
                                </span>
                            </div>
                        </div>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="col-lg-9">
                	<div class="filter-values" id="<c:out value="${normalizedKey}" />_value">
	                    <c:forEach items="${filters[filter.key]}" var="value">
	                        <c:set var="string" value="${value}"/>
	                        <c:if test="${fn:contains(value, 'range')}">
	                            <c:set var="string" value="${fn:replace(value,'{range=[','Between ')}"/>
	                            <c:set var="string" value="${fn:replace(string,',',' and ')}"/>
	                            <c:set var="string" value="${fn:replace(string,']}','')}"/>
                                <c:set var="value" value="${fn:replace(value,'{','{\"')}"/>
                                <c:set var="value" value="${fn:replace(value,'=','\":')}"/>
	                        </c:if>
	                        <c:if test="${fn:contains(value, 'min')}">
	                            <c:set var="string" value="${fn:replace(value,'{min=','More than ')}"/>
	                            <c:set var="string" value="${fn:replace(string,'}','')}"/>
                                <c:set var="value" value="${fn:replace(value,'{','{\"')}"/>
                                <c:set var="value" value="${fn:replace(value,'=','\":')}"/>
	                        </c:if>
	                        <c:if test="${fn:contains(value, 'max')}">
	                            <c:set var="string" value="${fn:replace(value,'{max=','Less than ')}"/>
	                            <c:set var="string" value="${fn:replace(string,'}','')}"/>
                                <c:set var="value" value="${fn:replace(value,'{','{\"')}"/>
                                <c:set var="value" value="${fn:replace(value,'=','\":')}"/>
	                        </c:if>
                            <c:if test="${fn:contains(value, 'like')}">
                                <c:set var="string" value="${fn:replace(value,'{like=','Like ')}"/>
                                <c:set var="string" value="${fn:replace(string,'}','')}"/>
                                <c:set var="value" value="${fn:replace(value,'{','{\"')}"/>
                                <c:set var="value" value="${fn:replace(value,'=','\":\"')}"/>
                                <c:set var="value" value="${fn:replace(value,'}','\"}')}"/>
                            </c:if>

	                        <c:if test="${string==null}">
	                        	<c:set var="string" value="null" />
	                        	<c:set var="value" value="null" />
	                        </c:if>
	                        <div class="filtval complex" x-key="<c:out value="${normalizedKey}" /><c:out value="${value}"/>" i-key="<c:out value="${filter.key}" />"><c:out value="${string}" /></div>
	                        <c:remove var="string" />
	                    </c:forEach>
	                </div>
                    <div style="margin-top: 3px" class="filter-apply-btn">
	                    <button class="btn btn-primary apply"><spring:message code="filter.apply"/></button>
	                    <button class="btn btn-default remove-filter"><spring:message code="filter.remove"/></button>
	                </div>
                </div>
            </div>
        </c:forEach>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content"></div>
        </div>
    </div>
    <!--End modal-->
    <%--Accessions--%>
	<table class="accessions">
		<thead>
			<tr>
				<td class="idx-col"></td>
				<td />
				<td><spring:message code="accession.accessionName" /></td>
				<td><spring:message code="accession.taxonomy" /></td>
				<td class="notimportant"><spring:message code="accession.origin" /></td>
				<td class="notimportant"><spring:message code="accession.sampleStatus" /></td>
				<td class="notimportant"><spring:message code="accession.holdingInstitute" /></td>
				<%-- 				<td><spring:message code="accession.holdingCountry" /></td>
 --%>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${pagedData.content}" var="accession" varStatus="status">
				<tr class="acn ${status.count % 2 == 0 ? 'even' : 'odd'}">
					<td class="idx-col">${status.count + pagedData.size * pagedData.number}</td>
					<td class="sel" x-aid="${accession.id}"></td>
					<td><a href="<c:url value="/acn/id/${accession.id}" />"><b><c:out value="${accession.acceNumb}" /></b></a></td>
					<%-- <td><a href="<c:url value="/acn/t/${accession.taxonomy.genus}/${accession.taxonomy.species}" />"><c:out value="${accession.taxonomy.sciName}" /></a></td> --%>
					<td><c:out value="${accession.taxonomy.sciName}" /></td>
					<%-- <td class="notimportant"><a href="<c:url value="/geo/${accession.orgCty.iso3}" />"><c:out value="${accession.orgCty.name}" /></a></td> --%>
					<td class="notimportant"><c:out value="${jspHelper.getCountry(accession.orgCty.iso3).getName(pageContext.request.locale)}" /></td>
					<td class="notimportant"><spring:message code="accession.sampleStatus.${accession.sampStat}" /></td>
					<td class="notimportant"><a href="<c:url value="/wiews/${accession.institute.code}" />"><c:out value="${accession.institute.code}" /></a></td>
					<%-- 			<td><a href="<c:url value="/geo/${accession.institute.country.iso3}" />"><c:out value="${accession.institute.country.name}" /></a></td>
		 --%>
				</tr>
			</c:forEach>
		</tbody>
	</table>

    <content tag="javascript">
        <script type="text/javascript">
            var jsonData = ${jsonFilter};

            var page=${pagedData.number};

            $(document).ready(function () {

                if (jsonData.crops !=null && jsonData.crops.length > 0) {
                    $("#menu2").show();
                }

                if (page == 0) {
                    $("#allfilters").removeClass("hide");
                    $("#toggleFilters").addClass("hide");
                } else {
                    $("#allfilters").addClass("hide");
                    $("#toggleFilters").removeClass("hide");
                }

                $(".filter-toggler").on("click", function (ev) {
                    ev.preventDefault();
                    $("#allfilters").toggleClass("hide");
                    $("#toggleFilters").toggleClass("hide");
                });


                $("#menu2").on("click", function () {
                    $.ajax("/modal", {
                        type: 'GET',
                        data: "shortName=" + jsonData.crops,
                        success: function (data) {
                            $(".modal-content").empty();
                            $(".modal-content").append(data);
                            for (var x in jsonData) {
                                console.log(".modal-body input[value='" + x + "']");
                                $(".modal-body .filter-block label > input[value='" + x + "']").prop("checked", true);
                            }
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                });


                $("body").on("click", ".additional", function () {
                    var id = $(this).attr("id");
                    var filterId = "#" + id.replace("_id", "_filter");
                    var filter = id.replace("gm_", "gm:").replace("_id", "");

                    if ($(this).is(":checked")) {
                        GenesysFilterUtil.appendFilter(filter, filterId,jsonData);
                    } else {
                        delete jsonData[filter];
                        $(filterId).remove();
                    }
                });

                $("body").on("keypress", ".string-type", function (e) {
                  if(e.keyCode==13){
                      var btn=$(this).parent().find("button");
                     console.log(btn)
                      var selectedValue = $(this).parent().find("#like-switcher option:selected").val();

                      if (selectedValue == "like") {
                          GenesysFilter.filterLike(btn, jsonData);
                      } else {
                          GenesysFilter.filterAutocomplete(btn, jsonData);
                      }
                  }
                });

                $("body").on("click", ".filter-list", function () {
                    GenesysFilter.filterList($(this), jsonData);
                });

                $("body").on("click", ".filter-auto", function () {
                    var selectedValue = $(this).parent().find("#like-switcher option:selected").val();
                    if (selectedValue == "like") {
                        GenesysFilter.filterLike($(this), jsonData);
                    } else  {
                        GenesysFilter.filterAutocomplete($(this), jsonData);
                    }
                });

                $("body").on("click", ".filter-range", function () {
                    GenesysFilter.filterRange($(this), jsonData);
                });

                $("body").on("click", ".filter-bool", function () {
                    GenesysFilter.filterBoolean($(this), jsonData);
                });

                $("body").on("click", ".filter-crop", function () {
                    GenesysFilter.filterCrop($(this), jsonData);
                });

                $("body").on("click", ".filter-enable", function () {
                    var key=$(this).attr("i-key");
                    var normKey = GenesysFilter.normKey(key);
                    var filterId = "#" + normKey + "_filter";
                    GenesysFilterUtil.appendFilter(key,filterId,jsonData);
                });

                $("body").on("click", ".apply", function () {
                    GenesysFilterUtil.submitJson('/explore-es', jsonData);
                });
                
                $("body").on("click", ".remove-filter", function () {
                    var key = $(this).parents(".filter-block").attr("i-key");
                    delete jsonData[key];
                    GenesysFilterUtil.submitJson('/explore-es', jsonData);
                });

                $("body").on("click", ".edit-fil", function () {
                	GenesysFilterUtil.showFilter($(this).parents(".filter-block"));
                });

                $("body").on("click", ".filtval", function (event) {
                    event.preventDefault();

                    var key = $(this).attr("i-key");
                    var normKey = GenesysFilter.normKey(key);
                    var value = $(this).attr("x-key").replace(normKey, "");

//                    if (value.indexOf("{") > -1) {
//                        var jsonVal = value.replace("=", ":").replace(" ", "");
//                        value = GenesysFilterUtil.setCharAt(jsonVal, jsonVal.indexOf("{"), '{"');
//                        value = GenesysFilterUtil.setCharAt(value, jsonVal.indexOf(":") + 1, '":');
//                    }
                    
                    if (value=="null") value=null;
                    GenesysFilterUtil.removeValue(value, key, jsonData);

                    if (! $(this).parents(".filter-block").hasClass("filter-edit")) {
                        GenesysFilterUtil.submitJson('/explore-es', jsonData);
                    }

                    $(this).remove();
                    $('input[i-key=' + normKey + value + ']').prop('checked', false);
                });

                GenesysFilterUtil.registerAutocomplete("#allfilters",jsonData);
            });
        </script>
    </content>
</body>
</html>