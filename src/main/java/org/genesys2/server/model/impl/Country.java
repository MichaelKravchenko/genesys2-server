/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.genesys2.server.model.BusinessModel;
import org.genesys2.server.servlet.controller.rest.serialization.CountrySerializer;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "country")
@JsonSerialize(using = CountrySerializer.class)
public class Country extends BusinessModel {

	private static final long serialVersionUID = -1688723909298769804L;

	@Column(nullable = false, unique = true, length = 3)
	private String code3;

	@Column(unique = false, length = 2)
	private String code2;
	private boolean current;

	@Column(unique = false, nullable = false, length = 200)
	private String name;

	@Column(length = 3)
	private String codeNum;
	private Long refnameId;

	/**
	 * Localized names
	 */
	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String nameL;

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String wikiLink;

	@Transient
	private JsonNode nameJ;

	@ManyToOne(cascade = {}, optional = true)
	@JoinColumn(name = "replacedBy")
	private Country replacedBy;

	public Country() {
	}

	public String getCode3() {
		return code3;
	}

	public void setCode3(final String code3) {
		this.code3 = code3;
	}

	public String getCode2() {
		return this.code2;
	}

	public void setCode2(final String code2) {
		this.code2 = code2;
	}

	public boolean isCurrent() {
		return this.current;
	}

	public void setCurrent(final boolean current) {
		this.current = current;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setCodeNum(final String isoNum) {
		this.codeNum = isoNum;
	}

	public String getCodeNum() {
		return codeNum;
	}

	public void setRefnameId(final Long refnameId) {
		this.refnameId = refnameId;
	}

	public Long getRefnameId() {
		return refnameId;
	}

	public String getNameL() {
		return nameL;
	}

	public void setNameL(String nameL) {
		this.nameL = nameL;
	}

	public String getName(Locale locale) {
		return getNameLocal(locale);
	}

	public void setName(Locale locale, String name) {
		// TODO Fix this
		final ObjectMapper mapper = new ObjectMapper();
		try {
			this.nameJ = mapper.readTree(nameL);
			// ObjectNode newLang =
			// mapper.createObjectNode().put(locale.getLanguage(), name);
			// System.err.println(newLang);
		} catch (final IOException e) {
		}
	}

	private synchronized String getNameLocal(Locale locale) {
		if (this.nameJ == null && this.nameL != null) {
			final ObjectMapper mapper = new ObjectMapper();
			try {
				this.nameJ = mapper.readTree(nameL);
			} catch (final IOException e) {
				e.printStackTrace();
			}
		}

		return this.nameJ != null && this.nameJ.has(locale.getLanguage()) ? this.nameJ.get(locale.getLanguage()).textValue() : this.name;
	}

	@Override
	public String toString() {
		return MessageFormat.format("Country id={0} name={1} current={2}", id, name, current);
	}

	public String getWikiLink() {
		return wikiLink;
	}

	public void setWikiLink(String wikiLink) {
		this.wikiLink = wikiLink;
	}

	public Country getReplacedBy() {
		return replacedBy;
	}

	public void setReplacedBy(Country replacedBy) {
		this.replacedBy = replacedBy;
	}

	public static void sort(List<Country> all, final Locale locale) {
		Collections.sort(all, new Comparator<Country>() {
			@Override
			public int compare(Country o1, Country o2) {
				return o1.getName(locale).compareTo(o2.getName(locale));
			}
		});
	}
}
