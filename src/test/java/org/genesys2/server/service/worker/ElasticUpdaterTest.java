/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.worker;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.genesys2.server.aspect.AsAdminAspect;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.service.AclService;
import org.genesys2.server.service.BatchRESTService;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.ElasticService;
import org.genesys2.server.service.GenesysFilterService;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.GeoService;
import org.genesys2.server.service.HtmlSanitizer;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.OrganizationService;
import org.genesys2.server.service.TaxonomyService;
import org.genesys2.server.service.UserService;
import org.genesys2.server.service.impl.AclServiceImpl;
import org.genesys2.server.service.impl.BatchRESTServiceImpl;
import org.genesys2.server.service.impl.ContentServiceImpl;
import org.genesys2.server.service.impl.CropServiceImpl;
import org.genesys2.server.service.impl.ElasticsearchSearchServiceImpl;
import org.genesys2.server.service.impl.GenesysFilterServiceImpl;
import org.genesys2.server.service.impl.GenesysServiceImpl;
import org.genesys2.server.service.impl.GeoServiceImpl;
import org.genesys2.server.service.impl.InstituteServiceImpl;
import org.genesys2.server.service.impl.OWASPSanitizer;
import org.genesys2.server.service.impl.OrganizationServiceImpl;
import org.genesys2.server.service.impl.TaxonomyServiceImpl;
import org.genesys2.server.service.impl.UserServiceImpl;
import org.genesys2.server.test.JpaDataConfig;
import org.genesys2.server.test.PropertyPlacholderInitializer;
import org.genesys2.spring.config.ElasticsearchConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.support.NoOpCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ui.velocity.VelocityEngineFactoryBean;

import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ElasticUpdaterTest.Config.class, initializers = PropertyPlacholderInitializer.class)
public class ElasticUpdaterTest {

	private static final Log LOG = LogFactory.getLog(ElasticUpdaterTest.class);

	@EnableAspectJAutoProxy
	@Import({ JpaDataConfig.class, ElasticsearchConfig.class })
	@ComponentScan(basePackages = { "org.genesys2.server.persistence.domain" })
	public static class Config {
		@Bean
		public AsAdminAspect asAdminAspect() {
			return new AsAdminAspect();
		}

		@Bean
		public UserService userService() {
			return new UserServiceImpl();
		}

		@Bean
		public ObjectMapper objectMapper() {
			return new ObjectMapper();
		}

		@Bean
		public AclService aclService() {
			return new AclServiceImpl();
		}

		@Bean
		public TaxonomyService taxonomyService() {
			return new TaxonomyServiceImpl();
		}

		@Bean
		public BatchRESTService batchRESTService() {
			return new BatchRESTServiceImpl();
		}

		@Bean
		public GenesysService genesysService() {
			return new GenesysServiceImpl();
		}

		@Bean
		public GenesysFilterService genesysFilterService() {
			return new GenesysFilterServiceImpl();
		}

		@Bean
		public CacheManager cacheManager() {
			return new NoOpCacheManager();
		}

		@Bean
		public HtmlSanitizer htmlSanitizer() {
			return new OWASPSanitizer();
		}

		@Bean
		public GeoService geoService() {
			return new GeoServiceImpl();
		}

		@Bean
		public ContentService contentService() {
			return new ContentServiceImpl();
		}

		@Bean
		public VelocityEngine velocityEngine() throws VelocityException, IOException {
			final VelocityEngineFactoryBean vf = new VelocityEngineFactoryBean();
			return vf.createVelocityEngine();
		}

		@Bean
		public OrganizationService organizationService() {
			return new OrganizationServiceImpl();
		}

		@Bean
		public InstituteService instituteService() {
			return new InstituteServiceImpl();
		}

		@Bean
		public ElasticService elasticService() {
			return new ElasticsearchSearchServiceImpl();
		}

		@Bean
		public ElasticUpdaterProcessor elasticUpdaterProcessor() {
			return new ElasticUpdaterProcessor();
		}

		@Bean
		public ElasticUpdater elasticUpdater() {
			return new ElasticUpdater();
		}

		@Bean
		public TaskExecutor taskExecutor() {
			return new ThreadPoolTaskExecutor();
		}

		@Bean
		public CropService cropService() {
			return new CropServiceImpl();
		}

		@Bean
		public ElasticUpdaterAspect elasticUpdaterAspect() {
			return new ElasticUpdaterAspect();
		}
	}

	@Autowired
	private ElasticUpdater elasticUpdater;

	@Autowired
	private GenesysService genesysService;

	@Test
	public void testDo() throws Exception {
		elasticUpdater.update(Accession.class, 1l);
		LOG.info("Done.");
	}

	@Test
	public void testAspect() throws InterruptedException {
		Accession a = new Accession();
		a.setAccessionName("A-1");
		a.setInstituteCode("INS000");
		genesysService.saveAccession(a);
		LOG.info("Accession saved");
		Thread.sleep(1000);
	}

	@Test
	public void testAspect2() throws InterruptedException {
		Accession[] a = new Accession[100];
		for (int i = 0; i < 100; i++) {
			a[i] = new Accession();
			a[i].setAccessionName("A-" + i);
			a[i].setInstituteCode("INS000");
		}
		genesysService.saveAccession(a);
		LOG.info("Accessions saved");
		Thread.sleep(5000);
	}
}
