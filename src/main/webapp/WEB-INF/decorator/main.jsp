<%@include file="/WEB-INF/jsp/init.jsp"%>

<!DOCTYPE html>

<html lang="${pageContext.response.locale.language}" dir="${pageContext.response.locale.language=='fa' || pageContext.response.locale.language=='ar' ? 'rtl' : 'ltr'}"
	xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
	xmlns:schema="http://schema.org/"
	xmlns:dwc="http://rs.tdwg.org/dwc/terms/"
	xmlns:germplasm="http://purl.org/germplasm/germplasmTerm#"
	xmlns:germplasmType="http://purl.org/germplasm/germplasmType#"
>
<head >
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- CSRF protection-->
    <meta name="_csrf" content="${_csrf.token}"/>
    <!-- default header name is X-CSRF-TOKEN -->
    <meta name="_csrf_header" content="${_csrf.headerName}"/>

	<link rel="shortcut icon" href="<c:url value="/genesys.png" />" />

    <title><sitemesh:write property="title" /></title>

    <!-- Custom styles for this template -->
    <%@include file="css.jsp" %>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

	<sitemesh:write property="head" />
</head>

<body>
	<div class="site-wrapper">
	<security:authentication var="user" property="principal" />

	<%@include file="header.jsp" %>
	<%@include file="menu.jsp" %>
	
	<div id="content" class="clearfix">
		<div class="container">
			<div id="dialog"></div>
			<div class="main-col clearfix" typeof="<sitemesh:write property="body.typeof" />">
				<sitemesh:write property="body" />
			</div>
		</div>
	</div>
	</div>
		
	<%@include file="footer.jsp" %>
	<sitemesh:write property="page.javascript" />
	<%@include file="ga.jsp" %>
</body>
</html>
