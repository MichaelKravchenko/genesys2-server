<!DOCTYPE html>

<%@include file="init.jsp"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<html>
<head>
<title><spring:message code="page.home.title" /></title>
</head>
<body>
	<div class="row">
	<div class="col-md-2" id="left-col">
	
		<div class="content-block" id="crop-list-dropdown">
   			<h2><spring:message code="crop.croplist" /></h2>
			<sec:authorize access="hasRole('ADMINISTRATOR')">
				<form method="post" action="<c:url value="/c/rebuild" />">
					<input type="submit" class="btn form-control" value="Rebuild" />
                    <!-- CSRF protection -->
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				</form>
			</sec:authorize>
			<div class="dropdown">
				<input class="form-control autocomplete-genus" placeholder="<spring:message code="autocomplete.genus" />" x-source="<c:url value="/explore/ac/genus" />" />
            </div> 
          </div>
          
	<c:if test="${cropList ne null and cropList.size() gt 0}">
          <div class="content-block" id="crop-list">
            <ul class="nav">
            	<li class="all-crops"><a class="show" href="<c:url value="/explore/" />"><spring:message code="crop.all-crops" /></a></li>
            	
            	<c:forEach items="${cropList}" var="crop" varStatus="status">
					<li><a class="show" href="/explore/c/${crop.shortName}"><c:out value="${crop.getName(pageContext.response.locale)}" /></a></li>
				</c:forEach>
			</ul>
          </div>
	</c:if>
	</div>


<!-- left column end / middle columns start -->
    
    <div class="col-md-7" id="middle-col">
    
    <c:if test="${welcomeBlurp ne null}">
	 	<h2>${welcomeBlurp.title}</h2>
	    <div class="welcome-blurp">
	    <c:set var="blurp" value="${welcomeBlurp}" />
		<%@include file="/WEB-INF/jsp/content/include/blurp-display.jsp"%>
	    <c:remove var="blurp" />
	    </div>
    </c:if>
    
    <h2><spring:message code="activity.recent-activity" /></h2>
         
          <div class="tab-content">
            <div class="tab-pane active" id="news-feed">
			<security:authorize access="hasRole('ADMINISTRATOR')">
				<a href="<c:url value="/content/activitypost/new" />" class="pull-right close" style=""> <spring:message code="activitypost.add-new-post" />
				</a>
			</security:authorize>
              <h1>Recent Activity</h1>
              
              <div class="all-posts">
              
            <c:forEach items="${lastNews}" var="activityPost" varStatus="status">
				<%@include file="/WEB-INF/jsp/content/include/activitypost.jsp"%>
			</c:forEach>
              
              </div>
            </div>
          </div>
    </div>
  
	<!-- middle column end / right columns start -->
	<div class="col-md-3" id="right-col">
	  <div class="content-block" id="stats">
	    <h2>Stats</h2>
	    
	    <div class="stats-map">
	      <div class="all-stats">
	        <div class="one-stat"><a href="<c:url value="/geo/" />"><spring:message code="stats.number-of-countries" arguments="${numberOfCountries}" /></a></div>
	        <div class="one-stat"><a href="<c:url value="/wiews/active" />"><spring:message code="stats.number-of-institutes" arguments="${numberOfInstitutes}" /></a></div>
	        <div class="one-stat"><a href="<c:url value="/explore" />"><spring:message code="stats.number-of-accessions" arguments="${numberOfAccessions}" /></a></div>
	      </div>
	    </div>
	  </div>
	  
	  
		<c:if test="${sideBlurp ne null}">
		<div class="content-block">
			<h2>${sideBlurp.title}</h2>
			<div class="blurp">
			<c:set var="blurp" value="${sideBlurp}" />
			<%@include file="/WEB-INF/jsp/content/include/blurp-display.jsp"%>
			<c:remove var="blurp" />
			</div>
		</div>
		</c:if>
		
		<div class="content-block">
			<h2><spring:message code="maps.accession-map" /></h2>
			<div class="blurp">
			<div id="globalmap" class="gis-map"></div>
			</div>
		</div>
	</div>
</div>
<content tag="javascript">
<script type="text/javascript">
  $( document ).ready(function() {
    $('#news a').click(function (e) {
      e.preventDefault();
      $(this).tab('show');
    });
    
    $(".autocomplete-genus").each(function() {
		var t=$(this); 
		t.autocomplete({ delay: 200, minLength: 3, source: t.attr('x-source'),  
			messages: { noResults: '', results: function() {} },
			select: function(event, ui) { document.location.pathname='/acn/t/'+ui.item.value; } });
	});
	
	$("#globalmap").on("click", function(e) {
		e.preventDefault();
		window.location='<c:url value="/explore/map" />';
	});
	var map = L.map('globalmap', {minZoom:0, maxZoom:0, dragging:false, zoomControl:false, keyboard:false}).setView([20,0], 0);
	L.tileLayer('https://otile{s}-s.mqcdn.com/tiles/1.0.0/sat/{z}/{x}/{y}.png', {
	    attribution: "MapQuest",
	    styleId: 22677,
	    noWrap: true,
	    subdomains: ['1','2','3','4'],
	    opacity: 0.6
	}).addTo(map);
	L.tileLayer("{s}/explore/tile/{z}/{x}/{y}?filter={}", {
	    attribution: "<a href='${props.baseUrl}'>Genesys</a>",
	    styleId: 22677,
	    noWrap: true,
	    subdomains: [${props.tileserverCdn}]
	}).addTo(map);
  });
</script>
</content>

</body>
</html>