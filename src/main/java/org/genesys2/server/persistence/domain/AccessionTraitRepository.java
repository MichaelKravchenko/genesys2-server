/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain;

import java.util.List;

import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.AccessionTrait;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface AccessionTraitRepository extends JpaRepository<AccessionTrait, Long> {

	List<AccessionTrait> findByAccession(Accession accession);

	@Query("select distinct at.metadataId from AccessionTrait at where at.accession = ?1")
	List<Long> listMetadataIds(Accession accession);

	@Query("select distinct at.methodId from AccessionTrait at where at.accession = ?1")
	List<Long> listMethodIds(Accession accession);

	@Query(value = "select distinct a from AccessionTrait at inner join at.accession a where at.metadataId = ?1", countQuery = "select count(ma.accessionId) from MetadataAccession ma where ma.metadata.id = ?1")
	Page<Accession> listMetadataAccessions(long metadataId, Pageable pageable);

	List<AccessionTrait> findByMetadataIdAndAccession(Long id, Accession accession);

	@Query("select count(id) from AccessionTrait at where at.metadataId = ?1 and at.accession = ?2")
	long countByMetadataAndAccessionId(long metadataId, Accession accession);

	@Modifying
	@Query("delete from AccessionTrait where accession = ?1 and metadataId= ?2 and methodId = ?3")
	void deleteByAccessionAndMetadataIdAndMethodId(Accession accession, long metadataId, long methodId);

}
