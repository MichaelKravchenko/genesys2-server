/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.genesys2.server.model.BusinessModel;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "dataset")
public class Dataset extends BusinessModel {
	private static final long serialVersionUID = 4562288873039057125L;

	@ManyToOne(cascade = {}, optional = true)
	private License license;

	@Column(nullable = false, length = 500)
	private String name;

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String description;

	@Column(length = 500)
	private String source;

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String headers;

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String mapping;

	private Date uploadDate;

	@OrderBy("orderIndex")
	@OneToMany(cascade = {}, mappedBy = "dataset")
	private List<DatasetDescriptor> datasetDescriptors;

	public License getLicense() {
		return license;
	}

	public void setLicense(final License license) {
		this.license = license;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(final Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	public String getSource() {
		return source;
	}

	public void setSource(final String source) {
		this.source = source;
	}

	/**
	 * Gets list of headers in source file
	 *
	 * @return
	 */
	public String getHeaders() {
		return headers;
	}

	public void setHeaders(final String headers) {
		this.headers = headers;
	}

	/**
	 * Data mapping info
	 *
	 * @return
	 */
	public String getMapping() {
		return mapping;
	}

	public void setMapping(final String mapping) {
		this.mapping = mapping;
	}

	public List<DatasetDescriptor> getDatasetDescriptors() {
		return datasetDescriptors;
	}

	public void setDatasetDescriptors(List<DatasetDescriptor> datasetDescriptors) {
		this.datasetDescriptors = datasetDescriptors;
	}

	@Override
	public String toString() {
		return MessageFormat.format("Dataset id={0,number,#} date={1,date,dd/MM/yyyy} name={2}", id, uploadDate, name);
	}
}
