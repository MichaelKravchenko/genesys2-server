<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="metadata.page.title" /></title>
</head>
<body>
	<h1>
		<c:out value="${trait.title}" />
	</h1>
	<div>
		<spring:message code="filter.crop" />: <b><a href="<c:url value="/c/${trait.crop.shortName}/descriptors" />">${trait.crop.getName(pageContext.response.locale)}</a></b>
	</div>
	<table>
			<thead>
			<tr>
				<td><spring:message code="ce.trait" /></td>
				<td><spring:message code="ce.method" /></td>
				<td><spring:message code="ce.sameAs" /></td>
				<td><spring:message code="method.fieldName" /></td>
				<td><spring:message code="unit-of-measure" /></td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><a href="<c:url value="/descriptors/${trait.id}" />"><c:out value="${method.parameter.getTitle(pageContext.response.locale)}" /></a></td>
				<td><a href="<c:url value="/descriptors/${trait.id}/${method.id}" />"><c:out value="${method.getMethod(pageContext.response.locale)}" /></a></td>
				<td>
				<c:if test="${method.rdfUri ne null}">
				   <a href="<c:url value="${method.rdfUri}" />">
				      <c:out value="${method.getRdfUriId()}" />
				   </a>
				</c:if>
				</td>
				<td><c:out value="${method.fieldName}" /></td>
				<td><c:out value="${method.unit}" /></td>
			</tr>
		</tbody>
	</table>
	
	<c:if test="${method.coded}">
		<h3><spring:message code="method.coding-table" /></h3>
		<ul class="funny-list">
		<c:forEach items="${codeMap.keySet()}" var="key" varStatus="status">
			<li class="${status.count % 2 == 0 ? 'even' : 'odd'}">
				<span class="method-code"><c:out value="${key}" /></span>
				<a title="<spring:message code="view.accessions" />" href="<c:url value="/explore"><c:param name="crop" value="${trait.crop.shortName}" /><c:param name="filter">{"gm:${method.id}":["${key}"]}</c:param></c:url>">
					<c:out value="${codeMap[key]}" />
				</a>
				count=${codeStatistics[key]}
			</li>
		</c:forEach>
		</ul>
	</c:if>

	<h3><spring:message code="accession.metadatas" /></h3>

	<ul class="funny-list">
		<c:forEach items="${metadatas}" var="metadata" varStatus="status">
			<li class="clearfix ${status.count % 2 == 0 ? 'even' : 'odd'}">
				<a class="show pull-left" href="/data/view/${metadata.id}"><c:out value="${metadata.title}" /></a>
				<div class="pull-right">
					<c:out value="${metadata.instituteCode}" />
				</div></li>
		</c:forEach>
	</ul>
	<style>
		span.method-code { font-weight: bold; font-size: 110%; margin-left: 2em; float: right; } 
	</style>
</body>
</html>