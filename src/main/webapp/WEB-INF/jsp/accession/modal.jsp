<%@ page contentType="charset=UTF-8" pageEncoding="UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<div class="modal-header">
    <h4 class="modal-title" id="myModalLabel"><spring:message code="filter.additional"/></h4>
</div>
    <div class="modal-body" style="overflow: auto;height: 200px">
    <c:forEach items="${categories}" var="category">
        <c:if test="${descriptors[category].size() gt 0}">
            <div class="row">
            	<h2><c:out value="${crop.getName(pageContext.request.locale)}:" /> <c:out value="${category.getName(pageContext.request.locale)}" /></h2>
            </div>
            <c:forEach items="${descriptors[category]}" var="descriptor">
                <c:if test="${methods[descriptor.id].size() gt 0}">
                    <div class="row filter-block">
                        <div class="col-lg-3">
                            <div dir="ltr"><c:out value="${descriptor.title}" /></div>
                        </div>
                        <div class="col-lg-9">
                            <c:forEach items="${methods[descriptor.id]}" var="method">
                                <c:set value="gm:${method.id}" var="gm_value"/>
                                <div><label><input class="additional"  ${fn:contains(jsonPick, gm_value)?'checked':''} id="gm_${method.id}_id" type="checkbox" style="margin-right: 1em;"  value="gm:${method.id}" /><span dir="ltr"><c:out value="${method.method}" /></span></label></div>
                            </c:forEach>
                        </div>
                    </div>
                </c:if>
            </c:forEach>
        </c:if>
    </c:forEach>
</div>
    <div class="modal-footer">
    <button type="button" class="btn btn-primary" data-dismiss="modal"><spring:message code="filter.apply"/></button>
</div>