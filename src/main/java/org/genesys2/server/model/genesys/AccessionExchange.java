/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.genesys;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import org.apache.commons.lang.StringUtils;
import org.genesys2.server.model.BusinessModel;

/**
 * AllAcqExchange generated by hbm2java
 */
@Entity
@Table(name = "accessionexchange")
public class AccessionExchange extends BusinessModel implements AccessionRelated {

	/**
	 *
	 */
	private static final long serialVersionUID = -2509737429801931061L;

	@Version
	private long version = 0;

	@OneToOne(optional = false, fetch = FetchType.LAZY, cascade = {})
	@JoinColumn(name = "accessionId", unique = true, nullable = false, updatable = false)
	private Accession accession;

	@Column(name = "donorInst", length = 8)
	private String donorInstitute;

	@Column(name = "donorName", length = 200)
	private String donorName;

	@Column(name = "acceNumb", length = 200)
	private String accNumbDonor;

	public AccessionExchange() {
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	@Override
	public Accession getAccession() {
		return accession;
	}

	public void setAccession(Accession accession) {
		this.accession = accession;
	}

	public String getDonorInstitute() {
		return this.donorInstitute;
	}

	public void setDonorInstitute(final String donorInstitute) {
		this.donorInstitute = donorInstitute;
	}

	public String getAccNumbDonor() {
		return this.accNumbDonor;
	}

	public void setAccNumbDonor(final String accNumbDonor) {
		this.accNumbDonor = accNumbDonor;
	}

	public String getDonorName() {
		return donorName;
	}

	public void setDonorName(String donorName) {
		this.donorName = donorName;
	}

	public boolean isEmpty() {
		if (StringUtils.isNotBlank(accNumbDonor))
			return false;
		if (StringUtils.isNotBlank(donorName))
			return false;
		if (StringUtils.isNotBlank(donorInstitute))
			return false;

		return true;
	}
}
