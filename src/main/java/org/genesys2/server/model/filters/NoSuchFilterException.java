package org.genesys2.server.model.filters;

public class NoSuchFilterException extends Exception {

	public NoSuchFilterException(String message) {
		super(message);
	}

}
