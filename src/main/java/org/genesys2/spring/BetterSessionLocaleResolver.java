/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

/*
 * Copyright 2002-2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.spring;

import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.i18n.SessionLocaleResolver;

/**
 * Implementation of LocaleResolver that uses a locale attribute in the user's
 * session in case of a custom setting, with a fallback to the specified default
 * locale or the request's accept-header locale.
 *
 * <p>
 * This is most appropriate if the application needs user sessions anyway, that
 * is, when the HttpSession does not have to be created for the locale.
 *
 * <p>
 * Custom controllers can override the user's locale by calling
 * {@code setLocale}, e.g. responding to a locale change request.
 *
 * @author Juergen Hoeller
 * @author Matija Obreza
 *
 * @see #setDefaultLocale
 * @see #setLocale
 */
public class BetterSessionLocaleResolver extends SessionLocaleResolver {

	private Set<String> supportedLocales = new HashSet<String>();

	public void setSupportedLocales(Set<String> supportedLocales) {
		this.supportedLocales = supportedLocales;
	}

	public Set<String> getSupportedLocales() {
		return supportedLocales;
	}

	/**
	 * Determine the default locale for the given request, Called if no locale
	 * session attribute has been found.
	 * <p>
	 * This implementation returns the request's accept-header locale if listed
	 * in the {@link #supportedLocales} , else falls back to the specified
	 * default locale.
	 *
	 * @param request
	 *            the request to resolve the locale for
	 * @return the default locale (never {@code null})
	 * @see #setDefaultLocale
	 * @see javax.servlet.http.HttpServletRequest#getLocale()
	 */
	@Override
	protected Locale determineDefaultLocale(HttpServletRequest request) {
		Locale defaultLocale = request.getLocale();

		if (defaultLocale != null && !supportedLocales.contains(defaultLocale.getLanguage())) {
			// reset request locale if not on the list of supported locales
			defaultLocale = null;
		}

		if (defaultLocale == null) {
			defaultLocale = getDefaultLocale();
		}
		return defaultLocale;
	}

}
