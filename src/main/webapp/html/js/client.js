baseUrl = "http://localhost:8080";
apiUrl = baseUrl + "/api/v0";

function x1(apiUrl, path, method) {
	$.ajax(apiUrl + path, {
		type : method,
		dataType : 'json',
		beforeSend : function(xhr) {
			if (accessToken!=null && accessToken!='') {
				xhr.setRequestHeader("Authorization", "Bearer " + accessToken)
			}
		},
		success : function(respObject) {
			$("#responseBody")[0].value = JSON.stringify(respObject);
		},
		error : function(jqXHR, textStatus, errorThrown) {
			$("#responseBody")[0].value = errorThrown;
			console.log(textStatus);
		}
	});
}

function x0(apiUrl, path, method, object) {
	$.ajax(apiUrl + path, {
		type : method,
		dataType : 'json',
		contentType: 'application/json; charset=utf-8',
		data: (object==null ? null : JSON.stringify(object)),
		beforeSend : function(xhr) {
			
		},
		success : function(respObject) {
			$("#responseBody")[0].value = JSON.stringify(respObject);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			$("#responseBody")[0].value = errorThrown; 
			console.log(textStatus);
			console.log(errorThrown);
		}
	});
}

$(document).ready(function() {
	if ($("#accessToken")[0]) {
		accessToken=$("#accessToken")[0].value;
		$("#accessToken").on("change", function(e) {
			accessToken = this.value;
		});
	}
	$("#responseBody")[0].value=null;
	$("button.rest-api").on("click", function(e) {
		x1(apiUrl, $(this).attr('x-url'), 'GET');
	});
	$("button.json-api").on("click", function(e) {
		x0("/json/v0", $(this).attr('x-url'), 'GET');
	});
	$("form.json-api button").on("click", function(e) {
		e.preventDefault();
		// Update textareas
		if (tinyMCE) tinyMCE.triggerSave();
		x0("/json/v0", $(this).attr('x-url'), $(this).attr('x-method'), $(this.form).serializeObject());
	});
	$("#clearResponse").on("click", function() {
		$("#responseBody")[0].value=null;
	});
	
	// Add rule line
	$(".addLine").on("click", function(e) {
		var template=$(this).parents(".form-group").children(".form-group")[0];
		var copy=template.cloneNode(true);
		$(copy).appendTo(template.parentElement);
		// renumber
		$(template.parentElement).children(".form-group").each(function(i, e) {
			$(e).find("[name]").each(function(j, inp) {
				inp.name=inp.name.replace(/\[\d+\]/, "["+i+"]");
			});
		});
	});
	$(document).on("click", ".removeLine", function(e) {
		var group=$(this).parents(".form-group")[0];
		var container=group.parentElement;
		if ($(container).children(".form-group").length==1) {
			// last element, don't remove
			return;
		}
		group.remove();
		// renumber names
		$(container).children(".form-group").each(function(i, e) {
			$(e).find("[name]").each(function(j, inp) {
				inp.name=inp.name.replace(/\[\d+\]/, "["+i+"]");
			});
		});
	});
	
	tinyMCE.init({
		selector : ".html-editor",
		menubar : false,
		statusbar : false,
		height : 200,
		plugins: "link autolink",
		directionality: document.dir
	});
});