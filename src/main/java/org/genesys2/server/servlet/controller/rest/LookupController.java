/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.rest;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.genesys2.server.model.impl.Country;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.GeoService;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.TaxonomyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@PreAuthorize("isAuthenticated()")
@RequestMapping(value = { "/api/v0/lookup", "/json/v0/lookup" })
public class LookupController extends RestController {

	@Autowired
	GenesysService genesysService;

	@Autowired
	InstituteService instituteService;

	@Autowired
	GeoService geoService;

	@Autowired
	TaxonomyService taxonomyService;

	@RequestMapping(value = "/orgCty", method = { RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	Map<String, String> get(@RequestParam(value = "lang", required = false, defaultValue = "") String lang) {
		final Map<String, String> orgCty = new TreeMap<String, String>();
		List<Country> list = null;
		if (StringUtils.isNotBlank(lang)) {
			final Locale locale = new Locale(lang);
			list = geoService.listAll(locale);
			for (final Country c : list) {
				orgCty.put(c.getCode3(), c.getName(locale));
			}
		} else {
			list = geoService.listAll();
			for (final Country c : list) {
				orgCty.put(c.getCode3(), c.getName());
			}
		}

		return orgCty;
	}
}
