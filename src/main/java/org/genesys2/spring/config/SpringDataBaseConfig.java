/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.spring.config;

import java.util.Properties;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableJpaRepositories(basePackages = { "org.genesys2.server.persistence.domain", "org.genesys2.server.persistence.acl" }, entityManagerFactoryRef = "entityManagerFactory", transactionManagerRef = "transactionManager", repositoryImplementationPostfix = "CustomImpl")
// @EnableJpaAuditing(auditorAwareRef = "auditorAware")
@EnableTransactionManagement
@Configuration
@ImportResource("classpath:/spring/spring-db.xml")
public class SpringDataBaseConfig {

	@Value("${db.url}")
	private String dbUrl;

	@Value("${db.driverClassName}")
	private String dbDriverClassName;

	@Value("${db.username}")
	private String dbUsername;

	@Value("${db.password}")
	private String dbPassword;

	@Value("${db.showSql}")
	private boolean dbShowSql;

	@Value("${db.dialect}")
	private String dbDialect;

	@Value("${db.hbm2ddl}")
	private String dbHbm2ddl;

	@Bean
	public DataSource dataSource() {
		final DataSource dataSource = new DataSource();
		dataSource.setUrl(dbUrl);
		dataSource.setDriverClassName(dbDriverClassName);
		dataSource.setUsername(dbUsername);
		dataSource.setPassword(dbPassword);
		dataSource.setValidationQuery("SELECT 1");
		dataSource.setTestWhileIdle(true);
		dataSource.setTestOnBorrow(true);

		return dataSource;
	}

	@Bean(name = "entityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		final LocalContainerEntityManagerFactoryBean entityManager = new LocalContainerEntityManagerFactoryBean();
		entityManager.setDataSource(dataSource());
		entityManager.setPersistenceUnitName("spring-jpa");
		entityManager.setJpaVendorAdapter(hibernateJpaVendorAdapter());
		entityManager.setJpaProperties(jpaProperties());
		entityManager.setPackagesToScan("org.genesys2.server.model.acl", "org.genesys2.server.model.impl", "org.genesys2.server.model.genesys",
				"org.genesys2.server.model.oauth", "org.genesys2.server.model.kpi");

		return entityManager;
	}

	@Bean
	public JpaTransactionManager transactionManager() {
		final JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setDataSource(dataSource());
		transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
		return transactionManager;
	}

	private Properties jpaProperties() {
		final Properties properties = new Properties();
		properties.setProperty("hibernate.dialect", dbDialect);
		properties.setProperty("hibernate.connection.charSet", "utf8");
		properties.setProperty("hibernate.connection.autocommit", "false");
		properties.setProperty("hibernate.hbm2ddl.auto", dbHbm2ddl);
		return properties;
	}

	private HibernateJpaVendorAdapter hibernateJpaVendorAdapter() {
		final HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
		jpaVendorAdapter.setShowSql(dbShowSql);
		return jpaVendorAdapter;
	}

}
