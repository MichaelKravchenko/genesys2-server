/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.genesys;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import org.apache.commons.lang.StringUtils;
import org.genesys2.server.model.BusinessModel;
import org.hibernate.annotations.Type;

/**
 * Collecting data
 */
@Entity
@Table(name = "accessioncollect")
public class AccessionCollect extends BusinessModel implements AccessionRelated {

	/**
	 *
	 */
	private static final long serialVersionUID = 6848317825287346724L;

	@Version
	private long version = 0;

	@OneToOne(optional = false, fetch = FetchType.LAZY, cascade = {})
	@JoinColumn(name = "accessionId", unique = true, nullable = false, updatable = false)
	private Accession accession;

	@Column(name = "collDate", length = 8)
	private String collDate;

	@Column(name = "collNumb", length = 64)
	private String collNumb;

	@Column(name = "collMissId", length = 128)
	private String collMissId;

	@Column(name = "collCode", length = 128)
	private String collCode;

	@Column(name = "collName")
	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String collName;

	@Column(name = "collInstAddress")
	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String collInstAddress;

	@Column(name = "collSite")
	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String collSite;

	@Column
	private Integer collSrc;

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	@Override
	public Accession getAccession() {
		return accession;
	}

	public void setAccession(Accession accession) {
		this.accession = accession;
	}

	public String getCollDate() {
		return collDate;
	}

	public void setCollDate(String collDate) {
		this.collDate = collDate;
	}

	public String getCollNumb() {
		return collNumb;
	}

	public void setCollNumb(String collNumb) {
		this.collNumb = collNumb;
	}

	public String getCollMissId() {
		return collMissId;
	}

	public void setCollMissId(String collMissId) {
		this.collMissId = collMissId;
	}

	public String getCollCode() {
		return collCode;
	}

	public void setCollCode(String collCode) {
		this.collCode = collCode;
	}

	public String getCollName() {
		return collName;
	}

	public void setCollName(String collName) {
		this.collName = collName;
	}

	public String getCollInstAddress() {
		return collInstAddress;
	}

	public void setCollInstAddress(String collInstAddress) {
		this.collInstAddress = collInstAddress;
	}

	public String getCollSite() {
		return collSite;
	}

	public void setCollSite(String collSite) {
		this.collSite = collSite;
	}

	public Integer getCollSrc() {
		return collSrc;
	}

	public void setCollSrc(Integer collSrc) {
		this.collSrc = collSrc;
	}

	public boolean isEmpty() {
		if (StringUtils.isNotBlank(collDate))
			return false;
		
		if (StringUtils.isNotBlank(collNumb))
			return false;

		if (StringUtils.isNotBlank(collMissId))
			return false;

		if (StringUtils.isNotBlank(collName))
			return false;

		if (StringUtils.isNotBlank(collName))
			return false;

		if (StringUtils.isNotBlank(collInstAddress))
			return false;

		if (StringUtils.isNotBlank(collSite))
			return false;

		if (collSrc != null)
			return false;
		
		return true;
	}

}
