/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.imageio.ImageIO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.service.GenesysFilterService;
import org.genesys2.server.service.MappingService;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Service
public class MappingServiceImpl implements MappingService {

	// A 3x3 kernel that blurs an image
	// Kernel kernel = new Kernel(3, 3, new float[] { 0.05f, 0.1f, 0.05f, 0.05f,
	// 0.5f, 0.05f, 0.05f, 0.01f, 0.05f });
	// Copied from wikipedia
	// private final Kernel kernel = new Kernel(7, 7, new float[] { 0.00000067f,
	// 0.00002292f, 0.00019117f, 0.00038771f, 0.00019117f, 0.00002292f,
	// 0.00000067f,
	// 0.00002292f, 0.00078634f, 0.00655965f, 0.01330373f, 0.00655965f,
	// 0.00078633f, 0.00002292f, 0.00019117f, 0.00655965f, 0.05472157f,
	// 0.11098164f,
	// 0.05472157f, 0.00655965f, 0.00019117f, 0.00038771f, 0.01330373f,
	// 0.11098164f, 0.22508352f, 0.11098164f, 0.01330373f, 0.00038771f,
	// 0.00019117f,
	// 0.00655965f, 0.05472157f, 0.11098164f, 0.05472157f, 0.00655965f,
	// 0.00019117f, 0.00002292f, 0.00078633f, 0.00655965f, 0.01330373f,
	// 0.00655965f,
	// 0.00078633f, 0.00002292f, 0.00000067f, 0.00002292f, 0.00019117f,
	// 0.00038771f, 0.00019117f, 0.00002292f, 0.00000067f });
	// private final BufferedImageOp op = new ConvolveOp(kernel);

	private static final Log LOG = LogFactory.getLog(MappingServiceImpl.class);

	@Autowired
	private GenesysFilterService filterService;

	private final ObjectMapper mapper = new ObjectMapper();

	@Override
	public String filteredKml(AppliedFilters filters) {
		LOG.debug(filters.toString());

		final StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<kml xmlns=\"http://www.opengis.net/kml/2.2\">");
		sb.append("<Document>");

		filterService.listGeo(filters, null, new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				try {
					sb.append("<Placemark>");
					sb.append("<name>").append(rs.getString("acceNumb")).append("</name>");
					sb.append("<description></description>");
					sb.append("<Point><coordinates>");
					sb.append(rs.getDouble("longitude")).append(",").append(rs.getDouble("latitude"));
					sb.append("</coordinates></Point>");
					sb.append("</Placemark>");
				} catch (final SQLException e) {
					LOG.warn(e.getMessage());
					throw e;
				}
			}
		});
		sb.append("</Document>");
		sb.append("</kml>");

		return sb.toString();
	}

	@Override
	public String filteredGeoJson(AppliedFilters filters, Integer limit) throws IOException {
		final ObjectNode geoJson = mapper.createObjectNode();
		geoJson.put("type", "FeatureCollection");
		final ArrayNode featuresArray = geoJson.arrayNode();
		geoJson.put("features", featuresArray);

		filterService.listGeo(filters, limit, new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				try {
					final ObjectNode feature = featuresArray.objectNode();
					feature.put("type", "Feature");
					feature.put("id", rs.getLong("id"));

					ObjectNode geometry;
					feature.put("geometry", geometry = feature.objectNode());
					geometry.put("type", "Point");

					ArrayNode coordArray;
					geometry.put("coordinates", coordArray = geometry.arrayNode());
					coordArray.add(rs.getDouble("longitude"));
					coordArray.add(rs.getDouble("latitude"));

					ObjectNode properties;
					feature.put("properties", properties = feature.objectNode());
					properties.put("acceNumb", rs.getString("acceNumb"));
					properties.put("instCode", rs.getString("instCode"));
					properties.put("datum", rs.getString("datum"));
					properties.put("uncertainty", rs.getString("uncertainty"));

					featuresArray.add(feature);
				} catch (final SQLException e) {
					LOG.warn(e.getMessage());
					throw e;
				}
			}
		});

		return geoJson.toString();
	}

	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@Override
	@CacheEvict(value = "tileserver", allEntries = true)
	public void clearCache() {
		LOG.warn("Cleared tiles cache");
	}

	@Override
	@Cacheable(value = "tileserver", key = "'tile-' + #zoom + '-' + #xtile + '-' + #ytile + '-' + #filters")
	public byte[] getTile(AppliedFilters filters, final int zoom, final int xtile, final int ytile) {
		final BufferedImage bi = new BufferedImage(256, 256, BufferedImage.TYPE_INT_ARGB);

		// final int pixelSize = (int) Math.round(1.0 + (1 << (zoom / 2))) / 2;
		final int[] pixelSizes = new int[] { 0, 0, 0, 1, 1, 2, 2, 2, 3, 2, 2, 3 };
		final int pixelSize = pixelSizes[zoom >= pixelSizes.length ? pixelSizes.length - 1 : zoom];
		if (LOG.isDebugEnabled()) {
			LOG.debug(filters.toString());
			LOG.debug("PIXELSIZE=" + pixelSize + " zoom=" + zoom);
		}
		// // Border
		// for (int i = 0; i < 256; i++) {
		// bi.setRGB(0, i, Color.red.getRGB());
		// bi.setRGB(i, 0, Color.red.getRGB());
		// bi.setRGB(255, i, Color.red.getRGB());
		// bi.setRGB(i, 255, Color.red.getRGB());
		// }

		Color color = new Color(Color.yellow.getRed(), Color.yellow.getGreen(), Color.yellow.getBlue(), 170);
		final int colorWithAlpha = color.getRGB();

		filterService.listGeoTile(true, filters, null, zoom, xtile, ytile, new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				try {
					// System.err.println("Adding dot " +
					// rs.getDouble("longitude") + " " +
					// rs.getDouble("latitude"));
					final int longitude = CoordUtil.lonToImg3(zoom, xtile, rs.getDouble("longitude"));
					final int latitude = CoordUtil.latToImg3(zoom, ytile, rs.getDouble("latitude"));
					// System.err.println("Dotting " + longitude + "," +
					// latitude);

					for (int i = -pixelSize / 2; i <= pixelSize / 2; i++) {
						for (int j = -pixelSize / 2; j <= pixelSize / 2; j++) {
							if (longitude + i >= 0 && latitude + j >= 0 && longitude + i < 256 && latitude + j < 256) {
								bi.setRGB(longitude + i, latitude + j, colorWithAlpha);
							}
						}
					}

				} catch (final SQLException e) {
					LOG.warn(e.getMessage());
					throw e;
				}
			}
		});

		// for (int j = -80; j < 81; j += 10) {
		// int latitude = CoordUtil.latToImg3(zoom, ytile, j);
		// if (latitude >= 0 && latitude < 256)
		// for (int i = 0; i < 256; i++) {
		// bi.setRGB(i, latitude, Color.blue.getRGB());
		// }
		// }
		//

		try {
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			// if (zoom < -20) {

			// ImageIO.write(op.filter(bi, null), "png", baos);
			// } else {
			ImageIO.write(bi, "png", baos);
			// }
			return baos.toByteArray();
		} catch (final IOException e) {
			LOG.warn(e);
			throw new RuntimeException("Could not render image", e);
		}
	}
}
