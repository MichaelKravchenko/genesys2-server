/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Matija Obreza, matija.obreza@croptrust.org
 */
@Controller
@RequestMapping("/robots.txt")
public class RobotsTxtController {

	@Value("${base.host}")
	private final String host = "localhost";

	private static final String ROBOTS_ALLOW_ALL = "User-Agent: *\nAllow: /";
	private static final String ROBOTS_DENY_ALL = "User-Agent: *\nDisallow: /";

	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody
	String robotsTxt(HttpServletResponse response, HttpServletRequest request) {
		response.setContentType("text/plain");
		String reqHost = request.getHeader("Host");
		if (StringUtils.isNotBlank(reqHost) && StringUtils.contains(reqHost, ":")) {
			reqHost = reqHost.substring(0, reqHost.indexOf(":"));
		}

		if (StringUtils.equalsIgnoreCase(this.host, reqHost)) {
			return ROBOTS_ALLOW_ALL;
		} else {
			return ROBOTS_DENY_ALL;
		}
	}
}
