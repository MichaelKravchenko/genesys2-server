/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain.kpi;

import java.util.List;

import org.genesys2.server.model.kpi.Execution;
import org.genesys2.server.model.kpi.ExecutionRun;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface ExecutionRunRepository extends JpaRepository<ExecutionRun, Long> {

	List<ExecutionRun> findByExecution(Execution execution, Pageable pageable);

	@Query("select er from ExecutionRun er where er.execution=?1 order by er.timestamp desc")
	List<ExecutionRun> findLast(Execution execution, Pageable pageable);

	@Modifying
	@Query("delete from ExecutionRun er where er.execution=?1")
	void deleteByExecution(Execution execution);

}
