<%@include file="/WEB-INF/jsp/init.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
	<c:set var="ex" value="${sessionScope['SPRING_SECURITY_LAST_EXCEPTION']}" />
	<c:if test="${ex != null}">
		<div class="error">
			<h2>Oh no!</h2>
			<p>Access could not be granted</p>
		</div>
	</c:if>

	<c:remove scope="session" var="SPRING_SECURITY_LAST_EXCEPTION" />
	<security:authentication var="user" property="principal" />

	<security:authorize access="isAuthenticated()">
		<h1><spring:message code="oauth2.confirm-request" /></h1>
		<p>
			<spring:message code="oauth2.confirm-client" arguments="${user.user.name},${client.clientId}" htmlEscape="false" />
		</p>

		<div class="row">
			<div class="col-sm-2">
				<form action="<c:url value="/oauth/authorize" />" method="post">
					<input name="user_oauth_approval" value="true" type="hidden" /> <label><input class="btn btn-primary" name="authorize" value="<spring:message code="oauth2.button-approve" />" type="submit" /></label>
                    <!-- CSRF protection -->
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				</form>
			</div>
			<div class="col-sm-2">
				<form action="<c:url value="/oauth/authorize" />" method="post">
					<input name="user_oauth_approval" value="false" type="hidden" /> <label><input class="btn btn-default" name="deny" value="<spring:message code="oauth2.button-deny" />" type="submit" /></label>
                    <!-- CSRF protection -->
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				</form>
			</div>
		</div>
	</security:authorize>
</body>
</html>
