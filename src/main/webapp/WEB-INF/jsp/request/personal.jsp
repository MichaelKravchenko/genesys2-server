<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="request.page.title" /></title>
</head>
<body>
	<h1>
		<spring:message code="request.page.title" />
	</h1>

	<%@include file="/WEB-INF/jsp/content/include/blurp-display.jsp"%>
	
	<c:if test="${error ne null}">
	    <div class="alert alert-danger">
			<c:out value="${error.message}" />
	    </div>
	</c:if>

	<c:if test="${smta ne null and smta ne true}">
	    <div class="alert alert-warning">
	        <spring:message code="request.smta-not-accepted"/>
	    </div>
	</c:if>
	
	<form method="post" action="<c:url value="/request/submit" />" class="form-horizontal">
		<div class="form-group">
			<label class="col-lg-3 control-label"><spring:message code="request.your-email" /></label>
			<div class="col-lg-9">
				<input type="text" name="email" class="span3 required email form-control" value="${requestEmail}" />
			</div>
		</div>

		<security:authorize access="isAnonymous()">
			<div class="form-group">
				<label class="col-lg-3 control-label"><spring:message code="captcha.text" /></label>
				<div class="col-lg-9">
					<%@include file="/WEB-INF/jsp/recaptcha/here.jsp" %>
				</div>
			</div>
		</security:authorize>
		
		<div class="form-group">
			<label class="col-lg-3 control-label"><spring:message code="request.accept-smta" /></label>
			<div class="col-lg-9">
				<label class="col-xs-12"><input type="radio" name="smta" value="true"> <spring:message code="request.smta-will-accept" /></label>
				<label class="col-xs-12"><input type="radio" name="smta" value="false"> <spring:message code="request.smta-will-not-accept" /></label>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-lg-3 control-label"><spring:message code="request.purpose" /></label>
			<div class="col-lg-9">
				<label class="col-xs-12"><input type="radio" name="purpose" value="1" checked> <spring:message code="request.purpose.1" /></label>
				<label class="col-xs-12"><input type="radio" name="purpose" value="0"> <spring:message code="request.purpose.0" /></label>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-lg-3 control-label"><spring:message code="request.notes" /></label>
			<div class="col-lg-9">
				<textarea name="notes" class="form-control"><c:out value="${notes}" /></textarea>
			</div>
		</div>
		
		<div class="form-actions">
			<input class="btn btn-primary" type="submit" value="<spring:message code="request.start-request" />" />
		</div>
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>

</body>
</html>