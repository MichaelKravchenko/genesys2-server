/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service;

import java.util.List;

import org.genesys2.server.model.genesys.Taxonomy2;
import org.genesys2.server.model.impl.Crop;

public interface TaxonomyService {

	List<String> autocompleteGenus(String term,Crop crop);

	List<String> autocompleteSpecies(String term, Crop crop, List<String> genus);

	List<String> autocompleteTaxonomy(String term);

	long getTaxonomy2Id(String genus);

	long getTaxonomy2Id(String genus, String species);

	Taxonomy2 ensureTaxonomy2(String genus, String species, String spAuthor, String subtaxa, String subtAuthor);

	long countTaxonomy2();

	Taxonomy2 get(Long id);

	Taxonomy2 get(String genus);

	Taxonomy2 get(String genus, String species);

}
