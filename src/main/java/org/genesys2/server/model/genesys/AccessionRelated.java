package org.genesys2.server.model.genesys;

public interface AccessionRelated {

	Accession getAccession();

}
