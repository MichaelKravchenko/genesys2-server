/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.kpi;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import net.sf.oval.constraint.NotBlank;

import org.genesys2.server.model.VersionedAuditedModel;

/**
 * Evaluates {@link KPIParameter} by {@link Dimension}s.
 * 
 * @author matijaobreza
 * 
 */
@Entity
@Table(name = "kpiexecution")
public class Execution extends VersionedAuditedModel {

	/**
	 * This specifies the "key" under which observations are filed
	 */
	@NotBlank
	@Column(length = 100, unique = true, nullable = false)
	private String name;

	@ManyToOne(cascade = {}, fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "parameterId")
	private KPIParameter parameter;

	@OneToMany(orphanRemoval = true, fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
	@JoinColumn(name = "executionId")
	private List<ExecutionDimension> dimensions = new ArrayList<ExecutionDimension>();

	@Column(length = 100)
	private String title;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setParameter(KPIParameter parameter) {
		this.parameter = parameter;
	}

	public KPIParameter getParameter() {
		return parameter;
	}

	public void addDimension(Dimension<?> dimension, String link, String field) {
		// what do we do?
		ExecutionDimension ped = new ExecutionDimension();
		ped.setDimension(dimension);
		ped.setLink(link);
		ped.setField(field);

		dimensions.add(ped);
	}

	public String query() {
		StringBuffer sb = new StringBuffer(), where = new StringBuffer();
		String alias = "_base";
		sb.append("select count(distinct ");
		sb.append(alias);
		sb.append(") from ");
		sb.append(parameter.getEntity());
		sb.append(" ").append(alias);

		int pedC = 0;
		for (ExecutionDimension ped : dimensions) {
			pedC++;
			if (ped.getLink() != null) {
				sb.append(" inner join ");
				sb.append(alias).append(".");
				sb.append(ped.getLink());
				sb.append(" _ped").append(pedC).append(" ");
			}

			if (pedC > 1)
				where.append(" and ");

			if (ped.getLink() == null) {
				where.append("( ").append(alias).append(".").append(ped.getField()).append(" = ?").append(pedC).append(" )");
			} else {
				where.append("( _ped").append(pedC).append(".").append(ped.getField()).append(" = ?").append(pedC).append(" )");
			}
		}

		if (where.length() > 0 || parameter.getCondition() != null) {
			sb.append(" where ");
			if (parameter.getCondition() != null) {
				sb.append(parameter.getCondition());
				sb.append(" and ");
			}
			sb.append(where);
		}

		return sb.toString();
	}

	public List<ExecutionDimension> getExecutionDimensions() {
		return dimensions;
	}

	public Dimension<?> getDimension(int depth) {
		if (depth >= dimensions.size())
			return null;
		return dimensions.get(depth).getDimension();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
