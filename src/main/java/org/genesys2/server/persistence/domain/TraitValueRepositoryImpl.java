/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.AccessionTrait;
import org.genesys2.server.model.genesys.ExperimentAccessionTrait;
import org.genesys2.server.model.genesys.ExperimentTrait;
import org.genesys2.server.model.genesys.Metadata;
import org.genesys2.server.model.genesys.Method;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public class TraitValueRepositoryImpl implements TraitValueRepository {
	public static final Log LOG = LogFactory.getLog(TraitValueRepository.class);
	private JdbcTemplate jdbcTemplate;
	private NamedParameterJdbcTemplate namedJdbcTemplate;

	@Autowired
	private MethodRepository methodRepository;

	@Autowired
	private MetadataMethodRepository metadataMethodRepository;

	@Autowired
	private MetadataAccessionRepository metadataAccessionRepository;

	@Autowired
	private AccessionTraitRepository accessionTraitRepository;

	@Autowired
	public void setDataSource(final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.namedJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	// /*
	// * (non-Javadoc)
	// *
	// * @see
	// * org.croptrust.genesys.data.repositories.TraitValueRepository#count(org
	// * .croptrust.genesys.data.core.entities.Method)
	// */
	// @Override
	// public int count(final Method method) {
	// LOG.trace("Counting for " + method);
	//
	// final int count = this.jdbcTemplate.queryForInt("select count(`" +
	// method.getFieldName() + "`) from `" + method.getId() + "`");
	//
	// return count;
	// }
	//
	// @Override
	// public List<?> getValues(final Method method) {
	// LOG.trace("Querying for " + method);
	//
	// final Class<?> requiredType = method.getDataType();
	// final List<?> ret = this.jdbcTemplate.queryForList("select `" +
	// method.getFieldName() + "` from `" + method.getId() + "`", requiredType);
	//
	// return ret;
	// }

	@Override
	public List<ExperimentAccessionTrait> getValues(final Metadata metadata, final Method method) {
		final long metadataId = metadata.getId();
		return this.jdbcTemplate.query(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
				final PreparedStatement pst = conn.prepareStatement("select accessionId, `" + method.getFieldName() + "` as traitvalue from `" + method.getId()
						+ "` where metadataId=?");
				pst.setLong(1, metadataId);
				return pst;
			}
		}, new RowMapper<ExperimentAccessionTrait>() {
			@Override
			public ExperimentAccessionTrait mapRow(ResultSet rs, int pos) throws SQLException {
				return new ExperimentAccessionTrait(metadataId, rs.getLong(1), rs.getObject(2));
			}
		});
	}

	@Override
	public Map<Long, List<ExperimentTrait>> getValues(List<AccessionTrait> accessionTraits) {
		if (accessionTraits == null) {
			LOG.warn("Null accessionTraits list not acceptable");
			throw new NullPointerException();
		}

		final Map<Long, List<ExperimentTrait>> methodValues = new HashMap<Long, List<ExperimentTrait>>();

		for (final AccessionTrait at : accessionTraits) {
			final Method method = methodRepository.findOne(at.getMethodId());

			final List<Map<String, Object>> rows = this.jdbcTemplate.queryForList("select metadataId, `" + method.getFieldName() + "` as traitvalue from `"
					+ method.getId() + "` where accessionId=?", new Object[] { at.getAccession().getId() });

			if (rows == null || rows.size() == 0) {
				// Skip
				continue;
			}

			final List<ExperimentTrait> ret = new ArrayList<ExperimentTrait>();

			for (final Map<String, Object> row : rows) {
				final ExperimentTrait et = new ExperimentTrait((Long) row.get("metadataId"), row.get("traitvalue"));
				ret.add(et);
			}

			methodValues.put(method.getId(), ret);
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("Done! " + methodValues);
		}
		return methodValues;
	}

	@Override
	public Map<Long, Map<Long, List<Object>>> getValues(Metadata metadata, List<Method> methods, List<Accession> accessions) {
		final Map<Long, Map<Long, List<Object>>> accessionMetadataValues = new HashMap<Long, Map<Long, List<Object>>>();

		if (accessions.size() == 0) {
			return accessionMetadataValues;
		}

		final Set<Long> accessionIds = new HashSet<Long>(accessions.size());
		for (final Accession accession : accessions) {
			// create lists
			accessionMetadataValues.put(accession.getId(), new HashMap<Long, List<Object>>());
			accessionIds.add(accession.getId());
		}

		final Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("metadataId", metadata.getId());
		paramMap.put("accessionIds", accessionIds);

		for (final Method method : methods) {
			final long methodId = method.getId();
			LOG.debug("MethodID=" + methodId);

			this.namedJdbcTemplate.query("select accessionId, `" + method.getFieldName() + "` as traitvalue from `" + method.getId()
					+ "` where metadataId = :metadataId and accessionId in ( :accessionIds )", paramMap, new RowCallbackHandler() {

				@Override
				public void processRow(ResultSet rs) throws SQLException {
					final long accessionId = rs.getLong(1);
					final Object value = rs.getObject(2);

					if (value != null) {
						final Map<Long, List<Object>> accessionValues = accessionMetadataValues.get(accessionId);
						if (accessionValues != null) {
							List<Object> x = accessionValues.get(methodId);
							if (x == null) {
								accessionValues.put(methodId, x = new ArrayList<Object>(3));
							}
							x.add(value);
						}
					}
				}
			});
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("Done! ");
		}
		return accessionMetadataValues;
	}

	@Transactional
	private void delete(final Metadata metadata, final Accession accession, Method method) {
		LOG.info("Deleting trait values in methodId=" + method.getId() + " for metadataId=" + metadata.getId() + " and accnId=" + accession.getId());
		this.jdbcTemplate.update(String.format("DELETE FROM `%s` WHERE accessionId=? AND metadataId=?", method.getId()), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement pst) throws SQLException {
				pst.setLong(1, accession.getId());
				pst.setLong(2, metadata.getId());
			}
		});
	}

	@Transactional
	private List<Object> insert(Metadata metadata, Accession accession, final Method method, final List<Object> accessionValues) {
		final long accessionId = accession.getId();
		final long metadataId = metadata.getId();

		// Make sure NULL values are skipped
		final List<Object> values = new ArrayList<Object>(accessionValues.size());
		for (final Object v : accessionValues) {
			if (v != null) {
				LOG.debug("Adding value " + v);
				values.add(v);
			} else {
				LOG.debug("Skipping value null");
			}
		}

		if (values.size() == 0) {
			// Nothing to insert
			return values;
		}

		this.jdbcTemplate.batchUpdate(
				String.format("INSERT INTO `%s` (accessionId, metadataId, `%s`) VALUES (?, ?, ?)", method.getId(), method.getFieldName()),
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement pst, int idx) throws SQLException {
						pst.setLong(1, accessionId);
						pst.setLong(2, metadataId);
						pst.setObject(3, values.get(idx));
						LOG.info("Inserting trait value in methodId=" + method.getId() + " for metadataId=" + metadataId + " and accnId=" + accessionId
								+ " val=" + values.get(idx));
					}

					@Override
					public int getBatchSize() {
						return values.size();
					}
				});

		return values;
	}

	/**
	 * Basic upsert deletes all existing data and inserts new records (IDs will
	 * go crazy)
	 *
	 * @return
	 */
	@Override
	public List<Object> upsert(Metadata metadata, Accession accession, Method method, List<Object> list) {
		delete(metadata, accession, method);
		final List<Object> x = insert(metadata, accession, method, list);

		// Need to update MetadataMethod
		if (0 == countTraitAccessions(method.getId(), metadata.getId())) {
			this.metadataMethodRepository.deleteByMetadataAndMethodId(metadata, method.getId());
		} else {
			ensureMetadataMethod(metadata, method);
		}

		// Need to update AccessionTrait
		if (0 == countTraitValues(method.getId(), metadata.getId(), accession.getId())) {
			this.accessionTraitRepository.deleteByAccessionAndMetadataIdAndMethodId(accession, metadata.getId(), method.getId());
		} else {
			ensureAccessionTrait(metadata, method, accession);
		}

		// Need to update metadataaccession
		final long mda = this.accessionTraitRepository.countByMetadataAndAccessionId(metadata.getId(), accession);
		if (0 == mda) {
			this.metadataAccessionRepository.deleteByMetadataAndAccessionId(metadata, accession.getId());
		} else {
			ensureMetadataAccession(metadata, accession);
		}

		return x;
	}

	private void ensureAccessionTrait(final Metadata metadata, final Method method, final Accession accession) {
		this.jdbcTemplate.update("INSERT IGNORE INTO accessiontrait (metadataId, methodId, accessionId) values (?, ?, ?)", new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, metadata.getId());
				ps.setLong(2, method.getId());
				ps.setLong(3, accession.getId());
			}
		});
	}

	private void ensureMetadataMethod(final Metadata metadata, final Method method) {
		this.jdbcTemplate.update("INSERT IGNORE INTO metadatamethod (metadataId, methodId) values (?, ?)", new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, metadata.getId());
				ps.setLong(2, method.getId());
			}
		});
	}

	private void ensureMetadataAccession(final Metadata metadata, final Accession accession) {
		this.jdbcTemplate.update("INSERT IGNORE INTO metadataaccession (metadataId, accessionId) values (?, ?)", new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, metadata.getId());
				ps.setLong(2, accession.getId());
			}
		});
	}

	/**
	 * Returns the number of accession trait-values of a method in a dataset
	 *
	 * @param method
	 * @param metadataId
	 * @return
	 */
	private int countTraitAccessions(long methodId, long metadataId) {
		return this.jdbcTemplate.queryForObject(String.format("SELECT COUNT(*) FROM `%s` WHERE metadataId=?", methodId), Integer.class, metadataId);
	}

	/**
	 * Returns the number of per-accession trait-values of a method in a dataset
	 *
	 * @param methodId
	 * @param metadataId
	 * @param accessionId
	 * @return
	 */
	private int countTraitValues(long methodId, long metadataId, long accessionId) {
		return this.jdbcTemplate.queryForObject(String.format("SELECT COUNT(*) FROM `%s` WHERE metadataId=? AND accessionId=?", methodId), Integer.class,
				metadataId, accessionId);
	}

	@Override
	public Map<String, Long> getStatistics(Method method) {
		final HashMap<String, Long> stats = new HashMap<String, Long>();
		this.jdbcTemplate.query(MessageFormat.format("SELECT `{0}`, COUNT(*) FROM `{1,number,#}` GROUP BY `{0}`", method.getFieldName(), method.getId()),
				new RowCallbackHandler() {
					@Override
					public void processRow(ResultSet rs) throws SQLException {
						stats.put(rs.getString(1), rs.getLong(2));
					}
				});
		return stats;
	}
}
