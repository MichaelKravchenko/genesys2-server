package org.genesys2.server.service.impl;


public class EasySMTAException extends Exception {

	public EasySMTAException(String string, Throwable e) {
		super(string, e);
	}

}
