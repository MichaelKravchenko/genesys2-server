package org.genesys2.server.service.worker;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.service.ElasticService;
import org.genesys2.server.service.worker.ElasticUpdater.ElasticNode;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hazelcast.core.HazelcastInstanceNotActiveException;
import com.hazelcast.core.IQueue;

/**
 * Processor
 * 
 * @author matijaobreza
 */
@Component
class ElasticUpdaterProcessor implements Runnable, InitializingBean, DisposableBean {
	public static final Log LOG = LogFactory.getLog(ElasticUpdaterProcessor.class);

	private static final int BATCH_SIZE = 100;

	private Thread worker;
	private boolean running;

	@Autowired
	private ElasticService elasticService;

	@Resource
	private IQueue<ElasticNode> elasticRemoveQueue;

	@Resource
	private IQueue<ElasticNode> elasticUpdateQueue;

	private long indexDelay = 5000;

	private HashMap<String, Set<Long>> buckets = new HashMap<String, Set<Long>>();

	public void setIndexDelay(long indexDelay) {
		this.indexDelay = indexDelay;
	}

	public long getIndexDelay() {
		return indexDelay;
	}

	@Override
	public void run() {
		LOG.info("Started.");
		// Set<String> updatedIndices = new HashSet<String>();

		while (running) {
			try {

				// First remove
				{
					int i = 0;
					ElasticNode toRemove = null;
					do {
						toRemove = elasticRemoveQueue.poll();
						i++;

						if (toRemove != null) {
							removeNode(toRemove);
						}

						if (i % 100 == 0) {
							LOG.info("Queue size remove=" + elasticRemoveQueue.size() + " update=" + elasticUpdateQueue.size() + " deleted=" + i);
						}

					} while (running && toRemove != null);
				}

				// Then update
				{
					int i = 0;
					ElasticNode toUpdate = null;
					do {
						toUpdate = elasticUpdateQueue.poll();
						i++;

						if (toUpdate != null) {
							updateNode(toUpdate);
						}

						if (i % 100 == 0) {
							LOG.info("Queue size remove=" + elasticRemoveQueue.size() + " update=" + elasticUpdateQueue.size() + " indexed=" + i);
						}

					} while (running && toUpdate != null);
				}

				if (!buckets.isEmpty()) {
					if (LOG.isDebugEnabled()) {
						LOG.debug("Queue size remove=" + elasticRemoveQueue.size() + " update=" + elasticUpdateQueue.size());
					}

					for (String className : buckets.keySet()) {
						Set<Long> bucket = buckets.get(className);
						elasticService.updateAll(className, bucket);
						bucket.clear();
					}
					buckets.clear();
				}

			} catch (HazelcastInstanceNotActiveException e) {
				LOG.warn("Hazelcast not active.");
			}

			try {
				// LOG.info("ES updater sleeping");
				Thread.sleep(5000);
			} catch (InterruptedException e) {
			}
		}
		LOG.info("Finished");
	}

	private void updateNode(ElasticNode toUpdate) {
		if (toUpdate == null)
			return;

		String className = toUpdate.getClassName();
		Set<Long> bucket = buckets.get(className);
		if (bucket == null) {
			buckets.put(className, bucket = new HashSet<Long>());
		}
		bucket.add(toUpdate.getId());
		if (bucket.size() >= BATCH_SIZE) {
			elasticService.updateAll(className, bucket);
			bucket.clear();
		}
	}

	private void removeNode(ElasticNode toRemove) {
		if (toRemove == null)
			return;

		elasticService.remove(toRemove.getClassName(), toRemove.getId());
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.running = true;
		this.worker = new Thread(this, "es-processor");
		this.worker.start();
	}

	@Override
	public void destroy() throws Exception {
		LOG.info("Stopping worker");
		this.running = false;
		this.worker.interrupt();
	}

}