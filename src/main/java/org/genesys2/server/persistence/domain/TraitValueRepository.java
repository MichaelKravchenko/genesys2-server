/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain;

import java.util.List;
import java.util.Map;

import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.AccessionTrait;
import org.genesys2.server.model.genesys.ExperimentAccessionTrait;
import org.genesys2.server.model.genesys.ExperimentTrait;
import org.genesys2.server.model.genesys.Metadata;
import org.genesys2.server.model.genesys.Method;

public interface TraitValueRepository {

	// int count(Method method);
	//
	// List<ExperimentTrait> getValues(Accession accession, Method method);

	List<ExperimentAccessionTrait> getValues(Metadata metadata, Method method);

	Map<Long, List<ExperimentTrait>> getValues(List<AccessionTrait> accessionTraits);

	Map<Long, Map<Long, List<Object>>> getValues(Metadata metadata, List<Method> methods, List<Accession> accessions);

	/**
	 * @return list of non-null trait values
	 */
	List<Object> upsert(Metadata metadata, Accession accession, Method method, List<Object> list);

	Map<String, Long> getStatistics(Method method);

}
