/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.genesys;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.genesys2.server.model.VersionedAuditedModel;
import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.FaoInstitute;

@Entity
@Table(name = "accession")
// TODO FIXME Reenable
// @Table(name = "accession", uniqueConstraints = {
// @UniqueConstraint(columnNames = { "instCode", "acceNumb", "taxGenus" }) })
public class Accession extends VersionedAuditedModel {
	private static final long serialVersionUID = -7630113633534038876L;

	@Column(length = 36)
	private String uuid;

	@Column(name = "instCode", length = 10, nullable = false)
	private String instituteCode;

	@ManyToOne(cascade = {}, optional = true)
	@JoinColumn(name = "instituteId")
	private FaoInstitute institute;

	@Column(name = "acceNumb", nullable = false, length = 128)
	private String accessionName;

	@ManyToOne(cascade = {}, optional = true)
	@JoinColumn(name = "taxonomyId")
	private Taxonomy taxonomy1;

	@ManyToOne(cascade = {}, optional = true)
	@JoinColumn(name = "taxonomyId2")
	private Taxonomy2 taxonomy;

	@Column(name = "acqSrc", length = 3)
	private String acquisitionSource;

	@Column(name = "acqDate", length = 8)
	private String acquisitionDate;

	@Column(name = "orgCty", length = 3)
	private String origin;

	@ManyToOne(cascade = {}, optional = true)
	@JoinColumn(name = "orgCtyId", nullable = true)
	private Country countryOfOrigin;

	@Column(name = "duplSite", length = 32)
	private String duplSite;

	@Column(name = "sampStat", length = 3)
	private Integer sampleStatus;

	@Column(name = "storage", length = 12)
	private String storage;

	@Column(name = "inSGSV")
	private Boolean inSvalbard;

	@Column(name = "inTrust")
	private Boolean inTrust;

	@Column(name = "available")
	private Boolean availability;

	@Column(name = "mlsStat")
	private Boolean mlsStatus;

	@Column(name = "taxGenus", nullable = true)
	private Long taxGenus;

	@Column(name = "taxSpecies", nullable = true)
	private Long taxSpecies;

	@Column(name = "storage", nullable = false)
	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name = "accessionstorage", joinColumns = @JoinColumn(name = "accessionId"))
	@OrderBy("storage")
	private List<Integer> stoRage = new ArrayList<Integer>();

	public Accession() {
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public FaoInstitute getInstitute() {
		return this.institute;
	}

	public void setInstitute(final FaoInstitute institute) {
		this.institute = institute;
	}

	public String getInstituteCode() {
		return instituteCode;
	}

	public void setInstituteCode(final String instituteCode) {
		this.instituteCode = instituteCode;
	}

	public String getAccessionName() {
		return this.accessionName;
	}

	public void setAccessionName(final String accessionName) {
		this.accessionName = accessionName;
	}

	public Taxonomy getTaxonomy1() {
		return this.taxonomy1;
	}

	public Taxonomy2 getTaxonomy() {
		return this.taxonomy;
	}

	public String getAcquisitionSource() {
		return this.acquisitionSource;
	}

	public void setAcquisitionSource(final String acquisitionSource) {
		this.acquisitionSource = acquisitionSource;
	}

	public String getAcquisitionDate() {
		return this.acquisitionDate;
	}

	public void setAcquisitionDate(final String acquisitionDate) {
		this.acquisitionDate = acquisitionDate;
	}

	public String getOrigin() {
		return this.origin;
	}

	public void setOrigin(final String origin) {
		this.origin = origin;
	}

	public Country getCountryOfOrigin() {
		return countryOfOrigin;
	}

	public void setCountryOfOrigin(Country countryOfOrigin) {
		this.countryOfOrigin = countryOfOrigin;
	}

	public String getDuplSite() {
		return this.duplSite;
	}

	public void setDuplSite(final String duplSite) {
		this.duplSite = duplSite;
	}

	public Integer getSampleStatus() {
		return this.sampleStatus;
	}

	public void setSampleStatus(final Integer sampleStatus) {
		this.sampleStatus = sampleStatus;
	}

	public String getStorage() {
		return this.storage;
	}

	public void setStorage(final String storage) {
		this.storage = storage;
	}

	public Boolean getInSvalbard() {
		return this.inSvalbard;
	}

	public void setInSvalbard(final Boolean inSvalbard) {
		this.inSvalbard = inSvalbard;
	}

	public Boolean getInTrust() {
		return this.inTrust;
	}

	public void setInTrust(final Boolean inTrust) {
		this.inTrust = inTrust;
	}

	public Boolean getAvailability() {
		return this.availability;
	}

	public void setAvailability(final Boolean availability) {
		this.availability = availability;
	}

	public Boolean getMlsStatus() {
		return this.mlsStatus;
	}

	public void setMlsStatus(final Boolean mlsStatus) {
		this.mlsStatus = mlsStatus;
	}

	public long getTaxGenus() {
		return taxGenus == null ? -1 : taxGenus.longValue();
	}

	// TODO Must be long
	public void setTaxGenus(Long taxGenus) {
		this.taxGenus = taxGenus;
	}

	public Long getTaxSpecies() {
		return taxSpecies;
	}

	public void setTaxSpecies(Long taxSpecies) {
		this.taxSpecies = taxSpecies;
	}

	public void setTaxonomy(Taxonomy2 taxonomy2) {
		this.taxonomy = taxonomy2;
	}

	public void setTaxonomy1(Taxonomy taxonomy1) {
		this.taxonomy1 = taxonomy1;
	}

	public List<Integer> getStoRage() {
		return stoRage;
	}

	public void setStoRage(List<Integer> stoRage) {
		this.stoRage = stoRage;
	}

	@Override
	public String toString() {
		return MessageFormat.format("Accession id={0,number,#} A={3} inst={1} genus={2}", id, instituteCode, taxGenus, accessionName);
	}
}
