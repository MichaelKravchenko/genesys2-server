/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.oauth.OAuthAccessToken;
import org.genesys2.server.model.oauth.OAuthClientDetails;
import org.genesys2.server.model.oauth.OAuthClientType;
import org.genesys2.server.model.oauth.OAuthRefreshToken;
import org.genesys2.server.persistence.domain.OAuthAccessTokenPersistence;
import org.genesys2.server.persistence.domain.OAuthClientDetailsPersistence;
import org.genesys2.server.persistence.domain.OAuthRefreshTokenPersistence;
import org.genesys2.server.service.OAuth2ClientDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.common.exceptions.InvalidClientException;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.NoSuchClientException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("clientDetails")
@Transactional
public class OAuth2ClientDetailsServiceImpl implements OAuth2ClientDetailsService {
	private static final Log logger = LogFactory.getLog(OAuth2ClientDetailsServiceImpl.class);

	@Autowired
	private OAuthClientDetailsPersistence clientDetailsPersistence;

	@Autowired
	private OAuthAccessTokenPersistence accessTokenPersistence;

	@Value("${base.host}")
	private String hostname;

	@Autowired
	private OAuthRefreshTokenPersistence refreshTokenPersistence;

	public OAuth2ClientDetailsServiceImpl() {
	}

	@Override
	public Collection<OAuthAccessToken> findTokensByClientId(String clientId) {
		return accessTokenPersistence.findByClientId(clientId);
	}

	@Override
	public Collection<OAuthRefreshToken> findRefreshTokensClientId(String clientId) {
		return refreshTokenPersistence.findByClientId(clientId);
	}

	@Override
	public Collection<OAuthAccessToken> findTokensByUserUuid(String uuid) {
		return accessTokenPersistence.findByUserUuid(uuid);
	}

	@Override
	@PostAuthorize("hasRole('ADMINISTRATOR') or hasPermission(returnObject, 'READ')")
	public OAuthClientDetails getClientDetails(long clientId) {
		return clientDetailsPersistence.findOne(clientId);
	}

	@Override
	public ClientDetails loadClientByClientId(String clientId) throws InvalidClientException {
		if (logger.isDebugEnabled())
			logger.debug("loadClientByClientId: " + clientId);

		ClientDetails details;
		try {
			if (StringUtils.isBlank(clientId))
				throw new NoSuchClientException("Blank client_id provided");
			details = clientDetailsPersistence.findByClientId(clientId);
		} catch (final EmptyResultDataAccessException e) {
			throw new NoSuchClientException("No client with requested id: " + clientId);
		}

		if (details == null)
			throw new NoSuchClientException("No such client: " + clientId);

		return details;
	}

	@PreAuthorize("hasAnyRole('VETTEDUSER','ADMINISTRATOR')")
	@Override
	public OAuthClientDetails addClientDetails(String title, String description, String redirectUri, Integer accessTokenValiditySeconds,
			Integer refreshTokenValiditySeconds, OAuthClientType clientType) {

		String clientId = RandomStringUtils.randomAlphanumeric(5) + "." + RandomStringUtils.randomAlphanumeric(20) + "@" + hostname;
		String clientSecret = RandomStringUtils.randomAlphanumeric(32);

		final OAuthClientDetails clientDetails = new OAuthClientDetails();
		clientDetails.setTitle(title);
		clientDetails.setDescription(StringUtils.defaultIfBlank(description, null));
		clientDetails.setClientId(clientId);
		clientDetails.setClientSecret(StringUtils.defaultIfBlank(clientSecret, null));
		clientDetails.setAccessTokenValiditySeconds(accessTokenValiditySeconds);
		clientDetails.setRefreshTokenValiditySeconds(refreshTokenValiditySeconds);
		clientDetails.setRedirectUris(StringUtils.defaultIfBlank(redirectUri, null));
		// clientDetails.setClientType(clientType);
		clientDetails.setScope("read,write");
		clientDetails.setAuthorizedGrantTypes("authorization_code,refresh_token");
		clientDetails.setAuthorities("USER");
		return clientDetailsPersistence.save(clientDetails);
	}

	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#clientDetails, 'WRITE')")
	@Override
	public OAuthClientDetails update(OAuthClientDetails clientDetails, String title, String description, String clientSecret, String redirectUris,
			Integer accessTokenValiditySeconds, Integer refreshTokenValiditySeconds) {
		clientDetails.setTitle(title);
		clientDetails.setDescription(description);
		clientDetails.setClientSecret(StringUtils.defaultIfEmpty(clientSecret, null));
		clientDetails.setRedirectUris(redirectUris);
		clientDetails.setAccessTokenValiditySeconds(accessTokenValiditySeconds);
		clientDetails.setRefreshTokenValiditySeconds(refreshTokenValiditySeconds);

		return clientDetailsPersistence.save(clientDetails);
	}

	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#clientDetails, 'DELETE')")
	@Override
	public void removeClient(OAuthClientDetails clientDetails) {
		String clientId = clientDetails.getClientId();

		logger.info("Deleting all accessTokens for " + clientId);
		accessTokenPersistence.deleteByClientId(clientId);
		logger.info("Deleting all refreshTokens for " + clientId);
		refreshTokenPersistence.deleteByClientId(clientId);

		logger.info("Deleting clientDetails " + clientId);
		clientDetailsPersistence.delete(clientDetails);
	}

	@Override
	public List<OAuthClientDetails> listClientDetails() {
		return clientDetailsPersistence.findAll();
	}

}
