/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.genesys2.server.aspect.AsAdminAspect;
import org.genesys2.server.model.AclAwareModel;
import org.genesys2.server.model.acl.AclClass;
import org.genesys2.server.model.acl.AclEntry;
import org.genesys2.server.model.acl.AclObjectIdentity;
import org.genesys2.server.model.acl.AclSid;
import org.genesys2.server.persistence.acl.AclClassPersistence;
import org.genesys2.server.persistence.acl.AclEntryPersistence;
import org.genesys2.server.persistence.acl.AclObjectIdentityPersistence;
import org.genesys2.server.persistence.acl.AclSidPersistence;
import org.genesys2.server.security.AuthUserDetails;
import org.genesys2.server.service.AclService;
import org.genesys2.server.service.UserService;
import org.genesys2.spring.SecurityContextUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.acls.domain.BasePermission;
import org.springframework.security.acls.model.Permission;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * TODO Add support for cleaning up after objects are removed
 */
@Service
@Transactional
public class AclServiceImpl implements AclService {

	private static final Logger LOG = LoggerFactory.getLogger(AclServiceImpl.class);

	private static Permission[] basePermissions;

	@Autowired
	private UserService userService;

	@Autowired
	private AclClassPersistence aclClassPersistence;

	@Autowired
	private AclEntryPersistence aclEntryPersistence;

	@Autowired
	private AclObjectIdentityPersistence aclObjectIdentityPersistence;

	@Autowired
	private AclSidPersistence aclSidPersistence;

	@Autowired
	private AsAdminAspect asAdminAspect;

	@Autowired
	private CacheManager cacheManager;

	static {
		basePermissions = new Permission[] { BasePermission.CREATE, BasePermission.READ, BasePermission.WRITE, BasePermission.DELETE,
				BasePermission.ADMINISTRATION };
	}

	@Override
	@Transactional(readOnly = true)
	public Permission[] getAvailablePermissions(String className) {
		// Do not remove parameter. We may change available permissions based on
		// parameter type!
		return basePermissions;
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#objectIdIdentity, #className, 'ADMINISTRATION')")
	public boolean addPermissions(long objectIdIdentity, String className, String uuid, boolean principal, Map<Integer, Boolean> permissions) {
		final AclSid sid = ensureSid(uuid, principal);
		final AclObjectIdentity oid = ensureObjectIdentity(className, objectIdIdentity);

		addPermissions(sid, oid, permissions);
		return true;
	}

	@Override
	public synchronized void addCreatorPermissions(AclAwareModel target) {
		if (target == null) {
			LOG.warn("No target specified for ACL permissions, bailing out!");
			return;
		}
		// assume that auth user has already AclSid implemented
		final AuthUserDetails authUser = SecurityContextUtil.getAuthUser();
		if (authUser == null) {
			LOG.warn("No user in security context, not doing ACL");
			return;
		}

		final String uuid = authUser.getUsername();

		// it's ok if it is null
		// it can be pre-authorized Admin
		final AclSid aclSid = ensureSid(uuid, true);

		final AclClass aclClass = ensureAclClass(target.getClass().getName());

		// create object identity
		final AclObjectIdentity objectIdentity = new AclObjectIdentity();
		objectIdentity.setObjectIdIdentity(target.getId());
		objectIdentity.setAclClass(aclClass);
		objectIdentity.setOwnerSid(aclSid);
		objectIdentity.setParentObject(null);
		objectIdentity.setEntriesInheriting(false);

		// save object identity
		aclObjectIdentityPersistence.save(objectIdentity);

		final Map<Integer, Boolean> permissionsMap = new HashMap<>();
		for (final Permission permission : basePermissions) {
			permissionsMap.put(permission.getMask(), true);
		}

		addPermissions(aclSid, objectIdentity, permissionsMap);
	}

	private AclClass ensureAclClass(String className) {
		AclClass aclClass = aclClassPersistence.findByAclClass(className);

		if (aclClass == null) {
			LOG.warn("Missing AclClass...");
			aclClass = new AclClass();
			aclClass.setAclClass(className);
			aclClassPersistence.save(aclClass);
		}

		return aclClass;
	}

	private AclSid ensureSid(String uuid, boolean principal) {
		AclSid aclSid = aclSidPersistence.findBySidAndPrincipal(uuid, principal);

		if (aclSid == null) {
			// create Acl Sid
			aclSid = new AclSid();
			aclSid.setPrincipal(principal);
			aclSid.setSid(uuid);

			// save it into db
			aclSidPersistence.save(aclSid);
			LOG.warn("New SID " + aclSid);
		}

		return aclSid;
	}

	private void addPermissions(AclSid ownerSid, AclObjectIdentity objectIdentity, Map<Integer, Boolean> permissions) {
		// create Acl Entry
		for (final Permission permission : basePermissions) {
			int mask = permission.getMask();
			final AclEntry aclEntry = new AclEntry();
			aclEntry.setAclObjectIdentity(objectIdentity);
			aclEntry.setAclSid(ownerSid);
			aclEntry.setAceOrder(getAceOrder(objectIdentity.getId()));
			aclEntry.setGranting(permissions.get(mask));
			aclEntry.setAuditSuccess(true);
			aclEntry.setAuditFailure(true);
			// set full access for own organization
			aclEntry.setMask(mask);

			// save ACL
			aclEntryPersistence.save(aclEntry);
		}
		cacheManager.getCache("aclcache").clear();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Long> listIdentitiesForSid(Class<? extends AclAwareModel> clazz, AuthUserDetails authUser, Permission permission) {
		return aclEntryPersistence.findObjectIdentitiesBySidAndAclClassAndMask(authUser.getUsername(), clazz.getName(), permission.getMask());
	}

	/**
	 * Generates next ace_order value (to avoid DuplicateIndex exception :
	 * acl_object_identity + ace_order is unique index)
	 * 
	 * @param aclObjectEntityId
	 *            - id of acl_object_identity table
	 * @return - ace_order value
	 */
	private Long getAceOrder(long aclObjectEntityId) {
		final Long maxAceOrder = aclEntryPersistence.getMaxAceOrderForObjectEntity(aclObjectEntityId);
		return maxAceOrder != null ? maxAceOrder + 1 : 1;
	}

	@Override
	@Transactional(readOnly = true)
	public AclObjectIdentity getObjectIdentity(long id) {
		return aclObjectIdentityPersistence.findOne(id);
	}

	@Override
	@Transactional
	public AclObjectIdentity ensureObjectIdentity(String className, long objectIdIdentity) {
		AclObjectIdentity aoi = aclObjectIdentityPersistence.findByObjectIdIdentityAndClassName(objectIdIdentity, className);
		if (aoi == null) {
			aoi = new AclObjectIdentity();
			aoi.setObjectIdIdentity(objectIdIdentity);
			aoi.setAclClass(ensureAclClass(className));
			// System user UUID
			final String uuid = asAdminAspect.getSystemAdminAccount().getName();
			final AclSid ownerSid = ensureSid(uuid, true);
			aoi.setOwnerSid(ownerSid);
			aclObjectIdentityPersistence.save(aoi);
		}
		return aoi;
	}

	@Override
	@Transactional(readOnly = true)
	public AclObjectIdentity getObjectIdentity(String className, long id) {
		return aclObjectIdentityPersistence.findByObjectIdIdentityAndClassName(id, className);
	}

	@Override
	@Transactional(readOnly = true)
	public AclObjectIdentity getObjectIdentity(AclAwareModel entity) {
		return aclObjectIdentityPersistence.findByObjectIdIdentityAndClassName(entity.getId(), entity.getClass().getName());
	}

	@Override
	@Transactional(readOnly = true)
	public List<AclEntry> getAclEntries(AclObjectIdentity objectIdentity) {
		return aclEntryPersistence.findByObjectIdentity(objectIdentity);
	}

	@Override
	@Transactional(readOnly = true)
	public List<AclEntry> getAclEntries(AclAwareModel entity) {
		return aclEntryPersistence.findByObjectIdentity(getObjectIdentity(entity));
	}

	@Override
	@Transactional(readOnly = true)
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#id, #className, 'ADMINISTRATION')")
	public List<AclSid> getSids(long id, String className) {
		return aclEntryPersistence.getSids(id, className);
	}

	@Override
	@Transactional(readOnly = true)
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#entity, 'ADMINISTRATION')")
	public List<AclSid> getSids(AclAwareModel entity) {
		return aclEntryPersistence.getSids(entity.getId(), entity.getClass().getName());
	}

	@Override
	@Transactional(readOnly = true)
	public List<AclSid> getAllSids() {
		return aclSidPersistence.findAll();
	}

	@Transactional(readOnly = true)
	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#id, #className, 'ADMINISTRATION')")
	public Map<String, Map<Integer, Boolean>> getPermissions(long id, String className) {
		final Map<String, Map<Integer, Boolean>> perm = new HashMap<>();

		final List<AclEntry> aclEntries = getAclEntries(getObjectIdentity(className, id));
		for (final AclEntry aclEntry : aclEntries) {
			Map<Integer, Boolean> granted = perm.get(aclEntry.getAclSid().getSid());
			if (granted == null) {
				perm.put(aclEntry.getAclSid().getSid(), granted = new HashMap<>());
			}
			granted.put((int) aclEntry.getMask(), aclEntry.isGranting());
		}

		return perm;
	}

	@Transactional(readOnly = true)
	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#entity, 'ADMINISTRATION')")
	public Map<String, Map<Integer, Boolean>> getPermissions(AclAwareModel entity) {
		return getPermissions(entity.getId(), entity.getClass().getName());
	}

	@Override
	public void updatePermission(AclObjectIdentity entity, String sid, Map<Integer, Boolean> permissionMap) {
		boolean oneGranting = false;
		final List<AclEntry> aclEntries = aclEntryPersistence.findBySidAndAclClass(sid, entity.getAclClass().getAclClass());
		for (final AclEntry aclEntry : aclEntries) {
			aclEntry.setGranting(permissionMap.get((int) aclEntry.getMask()));
			oneGranting |= aclEntry.isGranting();
		}
		if (oneGranting) {
			LOG.info("Saving " + aclEntries);
			aclEntryPersistence.save(aclEntries);
		} else {
			LOG.info("Deleting " + aclEntries);
			aclEntryPersistence.delete(aclEntries);
		}
		cacheManager.getCache("aclcache").clear();
	}
}
