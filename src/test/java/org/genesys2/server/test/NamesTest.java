/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class NamesTest {
	@Test
	public void testCleanup() {
		final List<Object[]> names = new ArrayList<Object[]>();
		names.add(new Object[] { 1l, null, "Qing ke dou", 0, null });
		names.add(new Object[] { 2l, null, "Qing ke dou", 5, null });
		names.add(new Object[] { 3l, null, "ZDD05417", 5, null });
		names.add(new Object[] { 4l, null, "ZDD05417", 5, null });
		names.add(new Object[] { 5l, "BLAHBLA", "ZDD05417", 5, null });

		cleanup(names);
	}

	private void cleanup(List<Object[]> names) {
		final Set<Long> toRemove = new HashSet<Long>();
		for (int i = 0; i < names.size() - 1; i++) {
			final Object[] name1 = names.get(i);
			if (toRemove.contains(name1[0])) {
				continue;
			}
			System.err.println("Base " + i + " " + ArrayUtils.toString(name1));
			for (int j = i + 1; j < names.size(); j++) {
				System.err.println("Inspecting " + j);
				final Object[] name2 = names.get(j);
				if (toRemove.contains(name2[0])) {
					continue;
				}
				final int res = whatToKeep(name1, name2);
				if (res == -1) {
					System.err.println("Would remove " + i + " " + ArrayUtils.toString(name1));
					toRemove.add((long) name1[0]);
				} else if (res == 1) {
					System.err.println("Would remove " + j + " " + ArrayUtils.toString(name2));
					toRemove.add((long) name2[0]);
				}
			}
		}
	}

	private int whatToKeep(Object[] name1, Object[] name2) {
		if (StringUtils.equals((String) name1[2], (String) name2[2])) {
			final float score1 = score(name1), score2 = score(name2);
			if (score1 < score2) {
				return -1;
			} else {
				return 1;
			}
		} else {
			return 0;
		}
	}

	private float score(Object[] name1) {
		float score = 1.0f;
		if (name1[1] != null) {
			score += 2;
			if ((int) name1[3] == 5) {
				score *= 2;
			}
		} else {
			if ((int) name1[3] == 0) {
				score += 1;
			}
		}
		return score;
	}
}
