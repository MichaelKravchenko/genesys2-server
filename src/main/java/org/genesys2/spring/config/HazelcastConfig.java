/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.spring.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.hazelcast.config.MapConfig.EvictionPolicy;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ILock;
import com.hazelcast.core.ManagedContext;
import com.hazelcast.spring.context.SpringManagedContext;

@Configuration
@EnableCaching
@Import({ HazelcastConfigDev.class, HazelcastConfigAWS.class })
public abstract class HazelcastConfig {
	@Value("${hazelcast.port}")
	protected int hazelPort;
	@Value("${hazelcast.instanceName}")
	protected String instanceName = "genesys";
	@Value("${hazelcast.password}")
	protected String password;
	@Value("${hazelcast.name}")
	protected String name;

	@Value("${cache.tileserver.max-size}")
	protected int tileserverMaxSize;
	@Value("${cache.tileserver.max-idle-seconds}")
	protected int tileserverMaxIdle;
	@Value("${cache.tileserver.time-to-live-seconds}")
	protected int tileserverTTL;
	@Value("${cache.tileserver.eviction-policy}")
	protected EvictionPolicy tileserverEvictionPolicy;

	@Bean
	public ManagedContext managedContext() {
		return new SpringManagedContext();
	}

	@Bean
	public ILock taxonomyUpdateLock(HazelcastInstance hazelcast) {
		ILock lock = hazelcast.getLock("taxonomyUpdateLock");
		return lock;
	}

}
