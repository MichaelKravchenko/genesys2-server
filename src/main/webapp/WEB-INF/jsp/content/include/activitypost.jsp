<%@include file="/WEB-INF/jsp/init.jsp"%>

<div class="post type-article">

  <div class="post-icon"></div>
  
  <div class="post-head clearfix">
    <div class="user-icon"><img src="<c:url value="/html/images/icon_user_genesys.png" />" alt="" /></div>
    <div class="post-head-content"><c:out value="${activityPost.title}" escapeXml="false" /></div>
  </div>
  
<c:if test="${activityPost.body ne null and activityPost.body.length() gt 0}">
	<div class="post-inner clearfix">
    	<div class="post-content">
			<c:out value="${activityPost.body}" escapeXml="false" />
		</div>
	</div>
</c:if>
  
  <div class="post-actions">
<security:authorize access="hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')">
		<a href="<c:url value="/content/activitypost/${activityPost.id}/edit" />"><spring:message code="edit" /></a>
    &bull;
</security:authorize>
<%--
    <a href="#">Like</a>
    &bull;
    <a href="#">Comment</a>
    &bull;
    <a href="#">Share</a>
    &bull;
    <a href="#" class="comments-num">12</a>
    &bull;
--%>
		<%-- <spring:message code="audit.createdBy" arguments="${activityPost.createdBy.name}" /> --%>
		<c:if test="${activityPost.lastModifiedBy ne null}"><spring:message code="audit.lastModifiedBy" arguments="${jspHelper.userFullName(activityPost.lastModifiedBy)}" /></c:if>
		<fmt:formatDate value="${activityPost.postDate.time}" />
  </div>
</div>
