/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import org.genesys2.server.exception.UserException;
import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.GeoService;
import org.genesys2.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JspHelper {
	@Autowired
	private UserService userService;
	@Autowired
	private GeoService geoService;
	@Autowired
	private CropService cropService;

	public String userFullName(Long userId) {
		if (userId == null) {
			return null;
		}
		try {
			final User user = userService.getUserById(userId);
			return user.getName();
		} catch (final UserException e) {
			return null;
		}
	}

	public User userByUuid(String uuid) {
		if (uuid == null) {
			return null;
		}
		return userService.getUserByUuid(uuid);
	}

	public Country getCountry(String iso3) {
		return geoService.getCountry(iso3);
	}
	
	public Crop getCrop(String shortName) {
		return cropService.getCrop(shortName);
	}
}
