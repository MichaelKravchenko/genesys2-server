/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain;

import java.util.Collection;
import java.util.List;

import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.FaoInstitute;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface FaoInstituteRepository extends JpaRepository<FaoInstitute, Long> {

	FaoInstitute findByCode(String code);

	@Query("select distinct(type) from FaoInstitute")
	List<String> findTypes();

	@Query("from FaoInstitute fi where fi.country = ?1 and fi.current=true and fi.pgrActivity=true")
	List<FaoInstitute> listByCountry(Country country, Sort sort);

	@Query("from FaoInstitute fi where fi.country = ?1 and fi.accessionCount > 0")
	List<FaoInstitute> findByCountryActive(Country country, Sort sort);

	@Query("from FaoInstitute fi where fi.accessionCount > 0")
	Page<FaoInstitute> listAllActive(Pageable pageable);

	@Query("select count(fi) from FaoInstitute fi where fi.accessionCount > 0")
	long countActive();

	@Query("from FaoInstitute fi where lower(fi.code) in ( ?1 )")
	List<FaoInstitute> findAllByCodes(Collection<String> wiewsCodes);

	@Query("from FaoInstitute fi where fi.pgrActivity=true")
	Page<FaoInstitute> listInstitutes(Pageable pageable);

	@Query("from FaoInstitute fi where fi.id in ( ?1 )")
	List<FaoInstitute> findByIds(List<Long> oids, Sort sort);

	@Modifying
	@Query("update FaoInstitute fi set accessionCount=(select count(a) from Accession a where a.institute = :institute ) where fi=:institute")
	void updateInstituteAccessionCount(@Param("institute") FaoInstitute institute);

	@Query("select distinct fi from FaoInstitute fi where fi.code like ?1 or fi.fullName like ?1 or fi.acronym like ?1 order by fi.accessionCount desc")
	List<FaoInstitute> autocomplete(String string, Pageable pageable);

}
