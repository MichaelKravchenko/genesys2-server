/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain;

import java.util.List;

import org.genesys2.server.model.genesys.Taxonomy;
import org.genesys2.server.model.genesys.Taxonomy2;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface TaxonomyRepository extends JpaRepository<Taxonomy, Long> {
	Taxonomy getByGenusAndSpecies(String genus, String species);

	List<Taxonomy> findByGenus(String genus);

	Taxonomy getByTaxonName(String fullTaxa);

	@Query("select distinct t.genus from Taxonomy t where t.genus like ?1")
	List<String> autocompleteGenus(String term, Pageable page);

	@Query("select distinct t.taxonName from Taxonomy t where t.taxonName like ?1")
	List<String> autocompleteTaxonomy(String term, Pageable page);

	@Modifying
	@Query("update Accession a set a.taxonomy=?2, a.taxGenus=?3, a.taxSpecies=?4 where a.taxonomy1 = ?1")
	int upgrade(Taxonomy t1, Taxonomy2 t2, Long taxGenus, Long taxSpecies);
}
