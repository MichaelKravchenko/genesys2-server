/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.RandomStringUtils;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.service.UserService;
import org.genesys2.server.servlet.util.GoogleOAuthUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.social.google.api.Google;
import org.springframework.social.google.api.impl.GoogleTemplate;
import org.springframework.social.google.api.userinfo.GoogleUserInfo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class GoogleSocialController extends BaseController {

	@Autowired
	private UserService userService;

	private final AuthenticationSuccessHandler authSuccessHandler = new SavedRequestAwareAuthenticationSuccessHandler();
	private final AuthenticationFailureHandler authFailureHandler = new SimpleUrlAuthenticationFailureHandler();

	@Autowired
	private GoogleOAuthUtil googleOAuthUtil;

	@RequestMapping("/google/login")
	public void redirectToGoogle(HttpServletResponse response) throws IOException {
		response.sendRedirect(googleOAuthUtil.getAuthenticationUrl());
	}

	@RequestMapping(GoogleOAuthUtil.LOCAL_GOOGLEAUTH_PATH)
	public void googleAuth(Model model, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		String accessToken = null;
		try {
			accessToken = googleOAuthUtil.exchangeForAccessToken(request);
		} catch (IOException e) {
			_logger.warn(e.getMessage(), e);
		}

		if (accessToken == null) {
			model.addAttribute("error", true);
			authFailureHandler.onAuthenticationFailure(request, response, new BadCredentialsException("Could not authenticate you with Google+"));
			return;
		}

		final Google google = new GoogleTemplate(accessToken);
		final GoogleUserInfo userInfo = google.userOperations().getUserInfo();

		if (!userService.exists(userInfo.getEmail())) {
			final String pwd = RandomStringUtils.randomAlphanumeric(20);
			final User user = userService.createAccount(userInfo.getEmail(), pwd, userInfo.getName());
			userService.userEmailValidated(user.getUuid());
		}

		final Authentication authentication = googleOAuthUtil.googleAuthentication(userInfo);

		// Redirect to URL in session
		authSuccessHandler.onAuthenticationSuccess(request, response, authentication);
	}

}
