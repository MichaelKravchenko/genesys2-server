<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title>${title}</title>
</head>
<body>
	<c:if test="${title ne ''}">
		<h1>
			<c:out value="${title}" />
		</h1>
	</c:if>
	
	<c:if test="${article.lang != pageContext.response.locale.language}">
		<%@include file="/WEB-INF/jsp/not-translated.jsp" %>
	</c:if>
	
	<div class="article" dir="${article.lang=='fa' || article.lang=='ar' ? 'rtl' : 'ltr'}">
		<security:authorize access="hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')">
			<a href="<c:url value="/content/${article.slug}/edit" />" class="close">
				<spring:message code="edit" />
			</a>
			<a href="<c:url value="/content/${article.slug}/edit#raw" />" class="close">
				<spring:message code="edit" />
			</a>
		</security:authorize>

		<c:out value="${article.body}" escapeXml="false" />
	</div>

	<div class="clearfix pull-right">
		<fmt:formatDate value="${article.postDate.time}" />
	</div>
	
</body>
</html>