<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
    <title><spring:message code="oauth-client.page.list.title"/></title>
</head>
<body>
<h1>
    <spring:message code="oauth-client.info"/>
</h1>

<security:authorize access="hasRole('ADMINISTRATOR') or hasPermission(#clientDetails, 'ADMINISTRATION')">
	<a href="<c:url value="/acl/${clientDetails.class.name}/${clientDetails.id}/permissions"><c:param name="back">/management/${clientDetails.clientId}/</c:param></c:url>" class="close"> <spring:message code="edit-acl" /></a>
	<a href="<c:url value="/management/${clientDetails.id}/edit" />" class="close">
		<spring:message code="edit" />
	</a>
</security:authorize>


<div class="row">
    <label class="col-lg-2 control-label"><spring:message code="oauth-client.title"/></label>
    <div class="col-lg-10"><c:out value="${clientDetails.title}" /></div>
</div>
<div class="row">
    <label class="col-lg-2 control-label"><spring:message code="oauth-client.description"/></label>
    <div class="col-lg-10"><c:out value="${clientDetails.description}" /></div>
</div>
<div class="row">
    <label class="col-lg-2 control-label"><spring:message code="oauth-client.id"/></label>
    <div class="col-lg-10">${clientDetails.clientId}</div>
</div>
<div class="row">
    <label class="col-lg-2 control-label"><spring:message code="oauth-client.secret"/></label>
    <div class="col-lg-10">${clientDetails.clientSecret}</div>
</div>

<security:authorize access="hasRole('ADMINISTRATOR')">
    <h3><spring:message code="client.details.token.list"/></h3>

	<a href="<c:url value="/management/${clientDetails.clientId}/removeall"/> "><spring:message
                code="oauth-client.remove.all"/></a>
    <table class="accessions">
        <tbody>
        <c:forEach items="${accessTokens}" var="accessToken">
            <tr class="">
				<td><fmt:formatDate value="${accessToken.expiration}" type="both" /></td>
                <td><c:out value="${jspHelper.userByUuid(accessToken.userUuid).email}"/></td>
                <td><c:out value="${accessToken.redirectUri}"/></td>
                <td>
                    <a href="<c:url value="/management/${clientDetails.clientId}/${accessToken.id}/remove"/> "><spring:message
                            code="oauth-client.remove"/></a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    
    <h3><spring:message code="client.details.refresh-token.list"/></h3>

    <a href="<c:url value="/management/${clientDetails.clientId}/removeall-rt"/> "><spring:message
            code="oauth-client.remove.all"/></a>
    <table class="accessions">
        <tbody>
        <c:forEach items="${refreshTokens}" var="refreshToken">
            <tr class="">
                <td><fmt:formatDate value="${refreshToken.expiration}" type="both" /></td>
                <td><c:out value="${jspHelper.userByUuid(refreshToken.userUuid).email}"/></td>
                <td><c:out value="${refreshToken.redirectUri}"/></td>
                <td>
                    <a href="<c:url value="/management/${clientDetails.clientId}/${refreshToken.id}/remove-rt"/> "><spring:message
                            code="oauth-client.remove"/></a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</security:authorize>


</body>
</html>
