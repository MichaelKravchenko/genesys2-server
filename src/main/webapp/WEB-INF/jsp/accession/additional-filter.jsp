<%@ page contentType="charset=UTF-8" pageEncoding="UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<c:forEach items="${additionalFilters}" var="filter">

    <c:set var="normalizedKey" value="${filter.key.replace(':', '_').replace('.','-')}"/>

    <div class="clearfix filter-block" id="<c:out value="${normalizedKey}" />_filter" norm-key="<c:out value="${normalizedKey}" />" i-key="<c:out value="${filter.key}" />">

		<div class="col-lg-3 edit-fil">
            <c:if test="${not filter.core}">
                <a href="<c:url value="/descriptors/${filter.key}" />"><c:out value="${filter.title}" /></a>
            </c:if>

            <c:if test="${filter.core}">
                <spring:message code="filter.${filter.key}" />
            </c:if>
        </div>
		<div class="col-lg-5 filter-new">
            <c:choose>
            	<c:when test="${filter.key=='crops'}">
					<div class="form-group input-group">
                        <span class="input-group-btn">
                            <select name="crops" id="cropselector" i-key="<c:out value="${filter.key}" />" class="form-control filter-crop">
                                <option value=""></option>
                                <c:forEach items="${crops}" var="c">
                                    <option  value="${c.shortName}" ${c.shortName== crop.shortName?'selected':''} ><c:out value="${c.getName(pageContext.response.locale)}" /></option>
                                </c:forEach>
                            </select>
                        </span>
                    </div>
                </c:when>
				<c:when test="${filter.filterType=='LIST'}">
                    <div class="">
                        <c:forEach items="${filter.options}" var="option">
                            <div>
                                <label>
                                    <input class="filter-list" id="<c:out value="${normalizedKey}${option.value}" />_input" ${fn:contains(filters[filter.key], option.value)?'checked':''} norm-key="<c:out value="${normalizedKey}" />" i-key="<c:out value="${filter.key}" />" type="checkbox" value="${option.value}" />
                                    <spring:message code="${option.name}"/>
                                </label>
                            </div>
                        </c:forEach>
                    </div>
                </c:when>
                <c:when test="${filter.filterType=='I18NLIST'}">
                    <div class="">
                        <c:forEach items="${filter.options}" var="option">
                            <div>
                                <label>
                                    <input class="filter-list" id="<c:out value="${normalizedKey}${option.value}" />_input" ${fn:contains(filters[filter.key], option.value)?'checked':''} norm-key="<c:out value="${normalizedKey}" />" i-key="<c:out value="${filter.key}" />" type="checkbox" value="${option.value}" />
                                    <spring:message code="${option.name}"/>
                                </label>
                            </div>
                        </c:forEach>
                    </div>
                </c:when>
                <c:when test="${filter.filterType=='AUTOCOMPLETE'}">
                    <div class="ui-front">
                        <div class="form-group input-group">
                             <span class="input-group-btn">
                                 <input id="${normalizedKey}_input"  class="span2 form-control autocomplete-filter string-type" x-source="${filter.autocompleteUrl}" placeholder="<spring:message code="filter.autocomplete-placeholder" />" type="text"/>
                                 <button class="btn notimportant filter-auto" norm-key="<c:out value="${normalizedKey}" />" i-key="<c:out value="${filter.key}" />">+</button>
                              </span>
                        </div>
                    </div>
                </c:when>
                <c:when test="${filter.dataType=='NUMERIC'}">
                    <div class="form-group input-group">
                        <span class="input-group-btn">
                            <input id="${normalizedKey}_input_1" class="span5 form-control" type="text"/>
                            <input id="${normalizedKey}_input_2" class="span5 form-control" type="text"/>
                            <button class="btn notimportant filter-range" norm-key="${normalizedKey}" i-key="<c:out value="${filter.key}" />">+</button>
                        </span>
                    </div>
                </c:when>
                <c:when test="${filter.dataType=='BOOLEAN'}">
                    <div class="">
                        <div><label><input type="checkbox" ${fn:contains(filters[filter.key], 'true')?'checked':''} class="filter-bool"  i-key="<c:out value="${filter.key}" />" id="<c:out value="${normalizedKey}" />" value="true"><spring:message code="boolean.true"/></label></div>
                        <div><label><input type="checkbox" ${fn:contains(filters[filter.key], 'false')?'checked':''} class="filter-bool" i-key="<c:out value="${filter.key}" />" id="<c:out value="${normalizedKey}" />" value="false"><spring:message code="boolean.false"/></label></div>
                        <div><label><input type="checkbox" ${fn:contains(filters[filter.key], 'null')?'checked':''}  class="filter-bool" i-key="<c:out value="${filter.key}" />" id="<c:out value="${normalizedKey}" />" value="null"><spring:message code="boolean.null"/></label></div>
                    </div>
                </c:when>
                <c:otherwise>
					<div class="ui-front">
                        <div class="form-group input-group">
                            <span class="input-group-btn">
                                <select id="like-switcher" class="form-control select-like">
                                    <option value="like"><spring:message code="filter.string.like" /></option>
                                    <option value="equals"><spring:message code="filter.string.equals" /></option>
                                </select>
                                <input class="span2 form-control string-type" id="${normalizedKey}_input" type="text"/>
                                <button class="btn notimportant filter-auto" norm-key="${normalizedKey}" i-key="<c:out value="${filter.key}" />">+</button>
                            </span>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
        <div class="col-lg-9">
        	<div class="filter-values" id="<c:out value="${normalizedKey}" />_value">
        	</div>
        	<div style="margin-top: 3px" class="filter-apply-btn">
                <button class="btn btn-primary apply"><spring:message code="filter.apply"/></button>
                <button class="btn btn-default remove-filter"><spring:message code="filter.remove"/></button>
            </div>
        </div>
    </div>
</c:forEach>
