/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import java.text.MessageFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.genesys2.server.model.BusinessModel;
import org.hibernate.annotations.Type;

/**
 * A Descriptor represents a dimension of a sparse vector.
 *
 * @author mobreza
 */
@Entity
@Table(name = "descriptor")
public class Descriptor extends BusinessModel {
	private static final long serialVersionUID = 3832200593904442940L;

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String title;

	@Column(nullable = false, length = 200)
	private String code;

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String description;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return MessageFormat.format("Descriptor id={0,number,#} name={1}", id, code);
	}
}
