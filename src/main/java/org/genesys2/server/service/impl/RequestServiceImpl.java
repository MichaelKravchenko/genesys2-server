/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.MaterialRequest;
import org.genesys2.server.model.genesys.MaterialSubRequest;
import org.genesys2.server.model.impl.Article;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.model.impl.FaoInstituteSetting;
import org.genesys2.server.model.impl.VerificationToken;
import org.genesys2.server.persistence.domain.MaterialRequestRepository;
import org.genesys2.server.persistence.domain.MaterialSubRequestRepository;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.EMailService;
import org.genesys2.server.service.EasySMTA;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.RequestService;
import org.genesys2.server.service.TokenVerificationService;
import org.genesys2.server.service.TokenVerificationService.NoSuchVerificationTokenException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
@Transactional(readOnly = true)
public class RequestServiceImpl implements RequestService {
	private static final String REQUEST_TOKENTYPE = "confirm-request";
	private static final String RECEIPT_TOKENTYPE = "confirm-receipt";

	private static final Log LOG = LogFactory.getLog(RequestServiceImpl.class);

	@Autowired
	private EasySMTA pidChecker;

	@Autowired
	private TokenVerificationService tokenVerificationService;

	@Autowired
	private EMailService emailService;

	@Autowired
	private ContentService contentService;

	@Autowired
	private GenesysService genesysService;

	@Autowired
	private InstituteService instituteService;

	@Autowired
	private MaterialRequestRepository requestRepository;

	@Autowired
	private MaterialSubRequestRepository subRequestRepository;

	@Value("${base.url}")
	private String baseUrl;

	@Value("${mail.requests.to}")
	private String requestsEmail;

	private final ObjectMapper mapper = new ObjectMapper();

	@Override
	@Transactional
	public MaterialRequest initiateRequest(RequestInfo requestInfo, Set<Long> accessionIds) throws RequestException {
		final Set<Long> availableAccessionIds = genesysService.filterAvailableForDistribution(accessionIds);
		final Locale locale = LocaleContextHolder.getLocale();
		System.err.println("Current locale: " + locale);
		if (availableAccessionIds == null || availableAccessionIds.size() == 0) {
			throw new RequestException("None of the selected accessions are available for distribution");
		}

		// Check Easy-SMTA for PID
		EasySMTA.EasySMTAUserData pid;
		try {
			pid = pidChecker.getUserData(requestInfo.getEmail());
		} catch (EasySMTAException e) {
			throw new RequestException(e.getMessage(), e);
		}

		final MaterialRequest request = createRequest(requestInfo, pid, availableAccessionIds);

		return sendValidationEmail(request);
	}

	// Send email to the user with verification
	@Override
	@Transactional
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public MaterialRequest sendValidationEmail(MaterialRequest materialRequest) {
		RequestBody rb = null;
		try {
			rb = mapper.readValue(materialRequest.getBody(), RequestBody.class);
		} catch (final IOException e) {
			// FIXME Some other exception?
			throw new RuntimeException("Could not handle request JSON", e);
		}

		Article article;
		if (StringUtils.isBlank(materialRequest.getPid())) {
			LOG.warn("No such user in ITPGRFA system");
			article = contentService.getGlobalArticle("smtp.material-confirm-no-pid", Locale.ENGLISH);
		} else {
			article = contentService.getGlobalArticle("smtp.material-confirm", Locale.ENGLISH);
		}

		// Generate verification token+key
		final VerificationToken verificationToken = tokenVerificationService.generateToken(REQUEST_TOKENTYPE, materialRequest.getUuid());

		final Page<Accession> accessions = genesysService.listAccessions(rb.accessionIds, new PageRequest(0, Integer.MAX_VALUE));

		// Create the root hash
		final Map<String, Object> root = new HashMap<String, Object>();
		root.put("baseUrl", baseUrl);
		root.put("verificationToken", verificationToken);
		root.put("pid", rb.pid);
		root.put("accessions", accessions.getContent());

		final String mailBody = contentService.processTemplate(article.getBody(), root);
		final String mailSubject = "[" + materialRequest.getUuid() + "] " + article.getTitle();
		LOG.info(">>>" + mailBody);

		// send to user
		emailService.sendMail(mailSubject, mailBody, materialRequest.getEmail());

		materialRequest.setLastReminderDate(new Date());
		return requestRepository.save(materialRequest);
	}

	MaterialRequest createRequest(RequestInfo requestInfo, EasySMTA.EasySMTAUserData pid, Set<Long> accessionIds) throws RequestException {
		final Set<Long> availableAccessionIds = genesysService.filterAvailableForDistribution(accessionIds);

		if (availableAccessionIds == null || availableAccessionIds.size() == 0) {
			throw new RequestException("None of the selected accessions are available for distribution");
		}

		MaterialRequest request = new MaterialRequest();
		request.setEmail(requestInfo.getEmail());

		if (pid != null) {
			request.setPid(pid.getPid());
		}

		final RequestBody rb = new RequestBody(requestInfo, pid, availableAccessionIds);
		request.setBody(serialize(rb));

		request = requestRepository.save(request);
		LOG.info("Persisted new material request: " + request);
		return request;
	}

	private String serialize(RequestBody rb) {
		String x = null;
		try {
			x = mapper.writeValueAsString(rb);
			System.err.println("JSON: " + x);
		} catch (final JsonProcessingException e) {
			e.printStackTrace();
		}
		return x;
	}

	// Rollback for any exception
	@Override
	@Transactional
	public MaterialRequest validateClientRequest(String tokenUuid, String key) throws NoSuchVerificationTokenException, NoPidException, EasySMTAException {
		final VerificationToken consumedToken = tokenVerificationService.consumeToken(REQUEST_TOKENTYPE, tokenUuid, key);

		final MaterialRequest materialRequest = requestRepository.findByUuid(consumedToken.getData());

		validateRequest(materialRequest);

		if (StringUtils.isNotBlank(materialRequest.getPid())) {
			// If PID is registered, break the request up
			breakup(materialRequest);
			relayRequests(materialRequest);
			return materialRequest;
		} else {
			// Notify user
			throw new NoPidException("Not registered with PID server");
		}
	}

	@Transactional
	private void relayRequests(MaterialRequest materialRequest) {
		final List<MaterialSubRequest> subRequests = subRequestRepository.findBySourceRequest(materialRequest);
		if (subRequests.size() == 0) {
			LOG.info("Nothing to relay.");
			return;
		}

		LOG.info("Material subrequests " + subRequests.size());
		materialRequest.setState(MaterialRequest.DISPATCHED);
		requestRepository.save(materialRequest);

		for (final MaterialSubRequest msr : subRequests) {
			relayRequest(msr);
		}

	}

	@Override
	@Transactional
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public MaterialRequest recheckPid(MaterialRequest materialRequest) throws NoPidException, EasySMTAException {
		// re-test email for PID
		EasySMTA.EasySMTAUserData pid = pidChecker.getUserData(materialRequest.getEmail());

		if (pid == null) {
			throw new NoPidException("Email not registered with PID server");
		}
		materialRequest.setPid(pid.getPid());
		requestRepository.save(materialRequest);
		try {
			final RequestBody rb = mapper.readValue(materialRequest.getBody(), RequestBody.class);
			rb.pid = pid;
			materialRequest.setBody(serialize(rb));

			// Need to update all subrequests
			for (final MaterialSubRequest subrequest : materialRequest.getSubRequests()) {
				final RequestBody rbs = mapper.readValue(subrequest.getBody(), RequestBody.class);
				rbs.pid = pid;
				subrequest.setBody(serialize(rbs));
				LOG.info("Updating subrequest: " + subrequest);
				subRequestRepository.save(subrequest);
			}

			return requestRepository.save(materialRequest);

		} catch (final IOException e) {
			// FIXME Some other exception?
			throw new RuntimeException("Could not handle request JSON", e);
		}
	}

	/**
	 * Mark request as validated if PID checks out.
	 *
	 * @param materialRequest
	 * @return
	 * @throws EasySMTAException 
	 * @throws NoPidException 
	 */
	@Override
	@Transactional
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public MaterialRequest validateRequest(MaterialRequest materialRequest) throws NoPidException, EasySMTAException {
		// Client email is confirmed
		materialRequest.setState(MaterialRequest.VALIDATED);
		materialRequest = requestRepository.save(materialRequest);

		recheckPid(materialRequest);

		breakup(materialRequest);
		relayRequests(materialRequest);

		return materialRequest;
	}

	/**
	 * Create requests to holding institutes if no subrequests exist for this
	 * request
	 *
	 * @param materialRequest
	 * @return
	 * @throws NoPidException 
	 */
	private List<MaterialSubRequest> breakup(MaterialRequest materialRequest) throws NoPidException {
		if (StringUtils.isBlank(materialRequest.getPid())) {
			LOG.warn("Material request has no PID, will not break it up.");
			throw new NoPidException("Not breaking up request without PID.");
		}

		final List<MaterialSubRequest> existingRequests = subRequestRepository.findBySourceRequest(materialRequest);

		if (existingRequests.size() > 0) {
			// check for subrequests
			LOG.warn("Subrequests exists, will not recreate them.");
			return existingRequests;
		}

		RequestBody rb = null;
		try {
			rb = mapper.readValue(materialRequest.getBody(), RequestBody.class);
		} catch (final IOException e) {
			// FIXME Some other exception?
			throw new RuntimeException("Could not handle request JSON", e);
		}

		final ArrayList<MaterialSubRequest> subrequests = new ArrayList<MaterialSubRequest>();
		final List<FaoInstitute> holdingInstitutes = genesysService.findHoldingInstitutes(rb.accessionIds);

		for (final FaoInstitute holdingInstitute : holdingInstitutes) {
			final MaterialSubRequest subRequest = new MaterialSubRequest();
			subRequest.setInstCode(holdingInstitute.getCode());
			subRequest.setSourceRequest(materialRequest);

			final RequestBody srb = new RequestBody();
			srb.pid = rb.pid;
			srb.requestInfo = rb.requestInfo;
			srb.accessionIds = genesysService.listAccessions(holdingInstitute, rb.accessionIds);

			if (srb.accessionIds.size() == 0) {
				LOG.warn("No accessions for this institute??");
			}

			subRequest.setBody(serialize(srb));
			subrequests.add(subRequest);
		}

		return subRequestRepository.save(subrequests);
	}

	/**
	 * Relay sub-request to holding institute
	 */
	@Override
	@Transactional
	public void relayRequest(MaterialSubRequest materialSubRequest) {
		LOG.info("Relaying request " + materialSubRequest);

		final VerificationToken verificationToken = tokenVerificationService.generateToken(RECEIPT_TOKENTYPE, materialSubRequest.getUuid());

		// Recipient
		String recipient = null;
		final Locale recipientLocale = Locale.ENGLISH;

		final FaoInstitute institute = instituteService.getInstitute(materialSubRequest.getInstCode());
		final FaoInstituteSetting instMailToSetting = institute.getSettings().get("requests.mailto");
		if (instMailToSetting != null) {
			recipient = StringUtils.defaultIfBlank(instMailToSetting.getValue(), null);
		}

		String[] emailCc = null;
		if (recipient != null) {
			emailCc = new String[] { requestsEmail };
			// TODO If such user is registered, use to user's selected locale
		} else {
			LOG.info("Using default email to relay request for institute " + materialSubRequest.getInstCode());
			recipient = requestsEmail;
		}

		materialSubRequest.setInstEmail(recipient);
		subRequestRepository.save(materialSubRequest);

		final Article article = contentService.getGlobalArticle("smtp.material-request", recipientLocale);

		RequestBody rb;
		try {
			rb = mapper.readValue(materialSubRequest.getBody(), RequestBody.class);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}

		final Page<Accession> accessions = genesysService.listAccessions(rb.accessionIds, new PageRequest(0, Integer.MAX_VALUE));

		// Create the root hash
		final Map<String, Object> root = new HashMap<String, Object>();
		root.put("baseUrl", baseUrl);
		root.put("verificationToken", verificationToken);
		root.put("pid", rb.pid);
		root.put("requestInfo", rb.requestInfo);
		root.put("accessions", accessions.getContent());

		final String mailBody = contentService.processTemplate(article.getBody(), root);
		final String mailSubject = "[" + materialSubRequest.getInstCode() + "] " + "[" + materialSubRequest.getUuid() + "] " + article.getTitle();
		LOG.info(">>>" + mailBody);

		// send to recipient
		emailService.sendMail(mailSubject, mailBody, emailCc, materialSubRequest.getInstEmail());

		materialSubRequest.setLastReminderDate(new Date());
		subRequestRepository.save(materialSubRequest);
	}

	@Override
	public MaterialSubRequest validateReceipt(String tokenUuid, String key) throws NoSuchVerificationTokenException {
		final VerificationToken consumedToken = tokenVerificationService.consumeToken(RECEIPT_TOKENTYPE, tokenUuid, key);

		final MaterialSubRequest materialSubRequest = subRequestRepository.findByUuid(consumedToken.getData());
		materialSubRequest.setState(MaterialSubRequest.CONFIRMED);
		subRequestRepository.save(materialSubRequest);
		return materialSubRequest;
	}

	public static final class RequestBody {

		public RequestInfo requestInfo;
		public Set<Long> accessionIds;
		public EasySMTA.EasySMTAUserData pid;

		public RequestBody() {
		}

		public RequestBody(RequestInfo requestInfo, EasySMTA.EasySMTAUserData pid, Set<Long> accessionIds2) {
			this.requestInfo = requestInfo;
			this.pid = pid;
			this.accessionIds = new HashSet<Long>(accessionIds2);
		}
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public Page<MaterialRequest> list(Pageable pageable) {
		return requestRepository.findAll(pageable);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#faoInstitute, 'ADMINISTRATION')")
	public Page<MaterialSubRequest> list(FaoInstitute faoInstitute, Pageable pageable) {
		return subRequestRepository.findAllByInstCode(faoInstitute.getCode(), pageable);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public MaterialRequest get(String uuid) {
		return requestRepository.findByUuid(uuid);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#faoInstitute, 'ADMINISTRATION')")
	public MaterialSubRequest get(FaoInstitute institute, String uuid) {
		return subRequestRepository.findByInstCodeAndUuid(institute.getCode(), uuid);
	}
}
