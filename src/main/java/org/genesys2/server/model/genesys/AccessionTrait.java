/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.genesys;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.genesys2.server.model.BusinessModel;

@Entity
@Table(name = "accessiontrait", uniqueConstraints = { @UniqueConstraint(name = "UQ_accessiontrait_all", columnNames = { "metadataId", "accessionId", "methodId" }) })
public class AccessionTrait extends BusinessModel {
	private static final long serialVersionUID = -240056837800843686L;

	@ManyToOne(cascade = {}, optional = false)
	@JoinColumn(name = "accessionId")
	private Accession accession;

	@Column(name = "metadataId")
	private long metadataId;

	@Column(name = "methodId")
	private long methodId;

	public Accession getAccession() {
		return accession;
	}

	public void setAccession(Accession accession) {
		this.accession = accession;
	}

	public long getMetadataId() {
		return metadataId;
	}

	public void setMetadataId(long metadataId) {
		this.metadataId = metadataId;
	}

	public long getMethodId() {
		return methodId;
	}

	public void setMethodId(long methodId) {
		this.methodId = methodId;
	}
}
