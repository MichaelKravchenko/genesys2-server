/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.genesys;

import java.text.MessageFormat;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.math.RandomUtils;
import org.genesys2.server.model.VersionedAuditedModel;
import org.genesys2.server.servlet.controller.rest.serialization.MaterialSubRequestSerializer;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * {@link MaterialRequest} is broken down into individual requests to
 * institutes.
 *
 * @author matijaobreza
 *
 */
@Entity
@Table(name = "requestsub")
@JsonSerialize(using = MaterialSubRequestSerializer.class)
public class MaterialSubRequest extends VersionedAuditedModel {
	/**
	 *
	 */
	private static final long serialVersionUID = 718290325614791811L;
	public static final int NOTCONFIRMED = 0;
	public static final int CONFIRMED = 1;

	@Column
	private int state = NOTCONFIRMED;

	@Column(length = 36, unique = true)
	private String uuid;

	@Column(length = 8, nullable = false)
	private String instCode;

	@Column(length = 200, nullable = true)
	private String instEmail;

	@Column(length = 100000)
	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String body;

	@ManyToOne(cascade = {}, optional = false)
	private MaterialRequest sourceRequest;

	@Temporal(TemporalType.TIMESTAMP)
	private Date lastReminderDate;

	@PrePersist
	void prepersist() {
		if (this.uuid == null) {
			this.uuid = UUID.nameUUIDFromBytes(
					("genesys:request:" + sourceRequest.getUuid() + ":" + instCode + ":" + System.currentTimeMillis() + ":" + RandomUtils.nextInt(100))
							.getBytes()).toString();
		}
	}

	public MaterialSubRequest() {
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getInstCode() {
		return instCode;
	}

	public void setInstCode(String instCode) {
		this.instCode = instCode;
	}

	public String getInstEmail() {
		return instEmail;
	}

	public void setInstEmail(String instEmail) {
		this.instEmail = instEmail;
	}

	public MaterialRequest getSourceRequest() {
		return sourceRequest;
	}

	public void setSourceRequest(MaterialRequest sourceRequest) {
		this.sourceRequest = sourceRequest;
	}

	public Date getLastReminderDate() {
		return lastReminderDate;
	}

	public void setLastReminderDate(Date lastReminderDate) {
		this.lastReminderDate = lastReminderDate;
	}

	@Override
	public String toString() {
		return MessageFormat.format("SubRequest uuid={0} inst={1} body={2}", uuid, instCode, body);
	}
}
