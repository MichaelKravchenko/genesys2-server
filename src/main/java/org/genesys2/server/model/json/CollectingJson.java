/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.json;

public class CollectingJson {
	private String collDate;
	private String collSite;
	private String collNumb;
	private Integer collSrc;
	private String collCode;
	private String collName;
	private String collInstAddress;
	private String collMissId;

	public String getCollDate() {
		return collDate;
	}

	public void setCollDate(String collDate) {
		this.collDate = collDate;
	}

	public String getCollSite() {
		return collSite;
	}

	public void setCollSite(String collSite) {
		this.collSite = collSite;
	}

	public String getCollNumb() {
		return collNumb;
	}

	public void setCollNumb(String collNumb) {
		this.collNumb = collNumb;
	}

	public Integer getCollSrc() {
		return collSrc;
	}

	public void setCollSrc(Integer integer) {
		this.collSrc = integer;
	}

	public String getCollCode() {
		return collCode;
	}

	public void setCollCode(String collCode) {
		this.collCode = collCode;
	}

	public String getCollName() {
		return collName;
	}

	public void setCollName(String collName) {
		this.collName = collName;
	}

	public String getCollInstAddress() {
		return collInstAddress;
	}

	public void setCollInstAddress(String collInstAddress) {
		this.collInstAddress = collInstAddress;
	}

	public String getCollMissId() {
		return collMissId;
	}

	public void setCollMissId(String collMissId) {
		this.collMissId = collMissId;
	}

}
