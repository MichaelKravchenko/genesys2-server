/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.Metadata;
import org.genesys2.server.model.impl.FaoInstitute;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;

public interface DatasetService {

	@PreAuthorize("isAuthenticated()")
	List<Metadata> listMyMetadata();

	@PreAuthorize("isAuthenticated()")
	Metadata addDataset(FaoInstitute institute, String title, String description);

	Metadata getDataset(long metadataId);

	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#metadata, 'WRITE')")
	void touch(@Param("metadata") Metadata metadata);

	void upsertAccessionData(Metadata metadata, Accession accession, Map<Long, List<Object>> methodValues);

	/**
	 * Writes a Darwin Core Archive to the specified output stream
	 *
	 * @param metadata
	 * @param outputStream
	 * @throws IOException
	 */
	void writeDataset(Metadata metadata, OutputStream outputStream) throws IOException;
}
