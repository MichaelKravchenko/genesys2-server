/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.geo.sources.CountryInfo;
import org.genesys2.geo.sources.DavrosCountrySource;
import org.genesys2.geo.sources.GeoNamesCountrySource;
import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.model.impl.ITPGRFAStatus;
import org.genesys2.server.persistence.domain.CountryRepository;
import org.genesys2.server.persistence.domain.ITPGRFAStatusRepository;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.GeoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Service
@Transactional(readOnly = true)
public class GeoServiceImpl implements GeoService {
	public static final Log LOG = LogFactory.getLog(GeoServiceImpl.class);

	@Autowired
	CountryRepository countryRepository;

	@Autowired
	ContentService contentService;

	@Autowired
	ITPGRFAStatusRepository itpgrfaRepository;

	@Override
	public List<Country> listAll() {
		return countryRepository.findAll(new Sort("name", "current"));
	}

	@Override
	public List<Country> listAll(final Locale locale) {
		final List<Country> all = listAll();
		Collections.sort(all, new Comparator<Country>() {
			@Override
			public int compare(Country o1, Country o2) {
				return o1.getName(locale).compareTo(o2.getName(locale));
			}
		});
		return all;
	}

	@Override
	public List<Country> listActive(final Locale locale) {
		final List<Country> countries = countryRepository.findByCurrent(true);
		Country.sort(countries, locale);
		return countries;
	}

	@Override
	public long countActive() {
		return countryRepository.countByCurrent(true);
	}

	@Override
	public List<Country> listITPGRFA(Locale locale) {
		final List<Country> countries = countryRepository.findITPGRFA();
		Country.sort(countries, locale);
		return countries;
	}

	@Override
	public Country getCountry(String isoCode) {
		Country country = null;
		if (isoCode.length() == 3) {
			country = countryRepository.findByCode3(isoCode);
		} else if (isoCode.length() == 2) {
			country = countryRepository.findByCode2(isoCode);
		}

		return country;
	}

	@Override
	public Country findCountry(String countryString) {
		Country country = getCountry(countryString);

		// Let's try the name
		if (country == null) {
			country = countryRepository.findByName(countryString);
		}

		// Let's try translations
		if (country == null) {
			country = findCountryByName(countryString);
		}

		return country;
	}

	/**
	 * Check if we have a country that has
	 * 
	 * @param name
	 *            in i18n
	 * @param countryString
	 * @return
	 */
	private Country findCountryByName(String name) {
		final ObjectMapper mapper = new ObjectMapper();
		final List<Country> countries = countryRepository.findWithI18N("%" + name.trim() + "%");
		LOG.debug("Found " + countries.size() + " that have " + name);
		for (final Country c : countries) {
			try {
				final JsonNode nameJ = mapper.readTree(c.getNameL());
				final Iterator<JsonNode> it = nameJ.elements();
				while (it.hasNext()) {
					final JsonNode el = it.next();
					if (name.equalsIgnoreCase(el.textValue())) {
						LOG.debug("Found match for " + name + " in: " + c.getName());
						return c;
					}
				}
			} catch (final IOException e) {
				LOG.error(e);
			}
		}
		return null;
	}

	/**
	 * Get current country based on ISO3 code. Follow replacedBy where possible.
	 * 
	 * @param code3
	 * @return
	 */
	@Override
	public Country getCurrentCountry(String code3) {
		if (code3 == null) {
			return null;
		}

		Country country = getCountry(code3);

		if (country != null && country.getReplacedBy() != null) {
			// Loop detection
			final Set<Long> seenCountryId = new HashSet<Long>();
			while (!seenCountryId.contains(country.getId()) && country.getReplacedBy() != null) {
				LOG.info("Country " + country.getCode3() + " replaced by " + country.getReplacedBy());

				// Put countryId to seen list
				seenCountryId.add(country.getId());

				// Update reference
				country = country.getReplacedBy();
			}
		}

		return country;
	}

	@Override
	public Country getCountryByRefnameId(long refnameId) {
		return countryRepository.findByRefnameId(refnameId);
	}

	@Override
	@Transactional(readOnly = false)
	public void updateCountryData() throws IOException {
		// update current countries
		updateGeoNamesCountries();

		// update from Davros, it has info on inactive country codes
		updateDavrosCountries();

		LOG.info("Country data up to date");
	}

	private void updateDavrosCountries() throws IOException {
		final List<CountryInfo> countries = DavrosCountrySource.fetchCountryData();

		if (LOG.isDebugEnabled()) {
			LOG.debug("Got " + countries.size() + " countries from remote source.");
		}

		// check against repository
		for (final CountryInfo countryInfo : countries) {

			// Country country =
			// countryRepository.findByName(countryInfo.getName());
			final Country country = countryRepository.findByCode3(countryInfo.getIso3());

			if (country == null) {
				LOG.info("Country " + countryInfo.getIso3() + " is not registered: " + countryInfo);

				if (countryInfo.isActive()) {
					LOG.warn("Country is marked as active. Should not be.");
				}

				final Country newCountry = new Country();
				newCountry.setCode2(countryInfo.getIso());
				newCountry.setCode3(countryInfo.getIso3());
				newCountry.setCodeNum(countryInfo.getIsoNum());
				newCountry.setCurrent(countryInfo.isActive());
				newCountry.setName(countryInfo.getName());
				countryRepository.save(newCountry);

				LOG.info("Added country " + newCountry);

			} else {
				LOG.debug("Exists " + country);

				// if iso2 is blank
				if (StringUtils.isBlank(country.getCode2()) && StringUtils.isNotBlank(countryInfo.getIso())) {
					LOG.info("Updating country iso2 code");
					country.setCode2(countryInfo.getIso());
					countryRepository.save(country);
				}

				// if iso-numeric is blank
				if (StringUtils.isBlank(country.getCodeNum()) && StringUtils.isNotBlank(countryInfo.getIsoNum())) {
					LOG.info("Updating country iso-numeric code");
					country.setCodeNum(countryInfo.getIsoNum());
					countryRepository.save(country);
				}
				/*
				 * // if all fields match if
				 * (country.getCode2().equals(countryInfo.getIso()) &&
				 * country.getCodeNum() != null &&
				 * country.getCodeNum().equals(countryInfo.getIsoNum())) { if
				 * (country.isCurrent() != countryInfo.isActive()) {
				 * LOG.warn("Country " + country + " is listed as active=" +
				 * countryInfo.isActive() + " on remote site");
				 * country.setCurrent(countryInfo.isActive());
				 * countryRepository.save(country); } }
				 */
			}
		}
	}

	private void updateGeoNamesCountries() throws IOException {
		final List<CountryInfo> countries = GeoNamesCountrySource.fetchCountryData();

		if (LOG.isDebugEnabled()) {
			LOG.debug("Got " + countries.size() + " countries from remote source.");
		}

		// deactivate all
		countryRepository.deactivateAll();

		// check against repository
		for (final CountryInfo countryInfo : countries) {
			final Country country = countryRepository.findByCode3(countryInfo.getIso3());

			if (country == null) {
				LOG.info("Country " + countryInfo + " is not registered");

				final Country newCountry = new Country();
				newCountry.setCode2(countryInfo.getIso());
				newCountry.setCode3(countryInfo.getIso3());
				newCountry.setCodeNum(countryInfo.getIsoNum());
				newCountry.setCurrent(countryInfo.isActive());
				newCountry.setName(countryInfo.getName());
				newCountry.setRefnameId(countryInfo.getRefnameId());
				countryRepository.save(newCountry);

				LOG.info("Added country " + newCountry);

			} else {
				LOG.debug("Exists " + country);
				country.setCurrent(true);
				country.setCode2(countryInfo.getIso());
				country.setCodeNum(countryInfo.getIsoNum());
				country.setName(countryInfo.getName());
				country.setRefnameId(countryInfo.getRefnameId());
				countryRepository.save(country);

				/*
				 * // update refname id if (country.getRefnameId() == null &&
				 * countryInfo.getRefnameId() != null) {
				 * country.setRefnameId(countryInfo.getRefnameId());
				 * countryRepository.save(country); }
				 * 
				 * // if country name is not the same if
				 * (StringUtils.isNotBlank(countryInfo.getName()) &&
				 * !countryInfo.getName().equals(country.getName())) {
				 * 
				 * LOG.info("Updating country name from: " + country.getName() +
				 * " to: " + countryInfo.getName());
				 * country.setName(countryInfo.getName());
				 * countryRepository.save(country); }
				 */
			}
		}
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@Transactional(readOnly = false)
	public void updateBlurp(Country country, String blurp, Locale locale) {
		contentService.updateArticle(country, "blurp", null, blurp, locale);
	}

	@Override
	public List<Long> listCountryRefnameIds() {
		return countryRepository.listRefnameIds();
	}

	@Override
	// TODO Where do we autorize access?
	// @PreAuthorize("hasRole('ADMINISTRATOR')")
	@Transactional(readOnly = false)
	public void updateCountryNames(String isoCode3, String jsonTranslations) {
		Country country = countryRepository.findByCode3(isoCode3);
		country.setNameL(jsonTranslations);
		country = countryRepository.save(country);
		LOG.info("Updated translations of " + country + " i18n=" + country.getNameL());
	}

	@Override
	@Transactional(readOnly = false)
	public Country updateCountryWiki(String isoCode3, String wiki) {
		Country country = countryRepository.findByCode3(isoCode3);
		LOG.info("Loaded " + country + " i18n=" + country.getNameL());
		country.setWikiLink(wiki);
		country = countryRepository.save(country);
		LOG.info("Updated wiki link of " + country + " i18n=" + country.getNameL());
		return country;
	}

	@Override
	public ArrayNode toJson(List<FaoInstitute> members) {
		// Generate JSON
		final ObjectMapper mapper = new ObjectMapper();
		final ArrayNode jsonArray = mapper.createArrayNode();
		for (final FaoInstitute inst : members) {
			if (inst.getLatitude() != null) {
				final ObjectNode instNode = mapper.createObjectNode();
				instNode.put("lat", inst.getLatitude());
				instNode.put("lng", inst.getLongitude());
				instNode.put("elevation", inst.getElevation());
				instNode.put("title", inst.getFullName());
				instNode.put("code", inst.getCode());
				jsonArray.add(instNode);
			}
		}
		return jsonArray;
	}

	@Override
	public ITPGRFAStatus getITPGRFAStatus(Country country) {
		return itpgrfaRepository.findByCountry(country);
	}

	@Override
	@Transactional(readOnly = false)
	public ITPGRFAStatus updateITPGRFA(Country country, String contractingParty, String membership, String membershipBy, String nameOfNFP) {
		if (country == null) {
			LOG.warn("Country is null, not updating ITPGRFA");
			return null;
		}

		ITPGRFAStatus itpgrfaStatus = itpgrfaRepository.findByCountry(country);
		if (itpgrfaStatus == null) {
			LOG.info("New ITPGRFA entry for " + country.getName());
			itpgrfaStatus = new ITPGRFAStatus();
			itpgrfaStatus.setCountry(country);
		} else {
			LOG.info("Updating ITPGRFA entry for " + country.getName());
		}
		itpgrfaStatus.setContractingParty(contractingParty);
		itpgrfaStatus.setMembership(membership);
		itpgrfaStatus.setMembershipBy(membershipBy);
		itpgrfaStatus.setNameOfNFP(nameOfNFP);

		return itpgrfaRepository.save(itpgrfaStatus);
	}

	@Override
	public String filteredKml(String jsonFilter) {
		return null;
	}

	@Override
	public List<Country> autocomplete(String term) {
		return countryRepository.autocomplete("%" + term + "%", new PageRequest(0, 10));
	}
}
