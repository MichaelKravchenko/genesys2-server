/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.genesys2.server.model.BusinessModel;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Crop rules establish which taxonomies are part of a {@link Crop}.
 * 
 * @author mobreza
 */
@Entity
@Table(name = "croprule", uniqueConstraints = { @UniqueConstraint(columnNames = { "cropId", "genus", "species", "subtaxa" }) })
public class CropRule extends BusinessModel {
	private static final long serialVersionUID = -2336100072991067193L;

	@Column
	private boolean included;

	@Column(nullable = false, length = 100)
	private String genus;

	@Column(nullable = true, length = 100)
	private String species;

	@Column(nullable = true, length = 100)
	private String subtaxa;

	@JsonIgnore
	@ManyToOne(cascade = {}, fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "cropId")
	private Crop crop;

	public boolean isIncluded() {
		return included;
	}

	public void setIncluded(boolean included) {
		this.included = included;
	}

	public String getGenus() {
		return genus;
	}

	public void setGenus(String genus) {
		this.genus = genus;
	}

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public String getSubtaxa() {
		return subtaxa;
	}

	public void setSubtaxa(String subtaxa) {
		this.subtaxa = subtaxa;
	}

	public Crop getCrop() {
		return crop;
	}

	public void setCrop(Crop crop) {
		this.crop = crop;
	}

	@Override
	public String toString() {
		return "CropRule included=" + included + " genus=" + genus + " species=" + species + " crop=" + crop.getShortName();
	}

	public boolean matches(String genus, String species, String spAuthor, String subtaxa, String subtAuthor) {
		if (this.genus != null && !this.genus.equalsIgnoreCase(genus))
			return false;
		if (this.species != null && !this.species.equalsIgnoreCase(species))
			return false;
		if (this.subtaxa != null && !this.subtaxa.equalsIgnoreCase(subtaxa))
			return false;

		return true;
	}
}
