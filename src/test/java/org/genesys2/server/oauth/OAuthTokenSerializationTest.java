/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.oauth;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.SetUtils;
import org.junit.Test;
import org.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.DefaultOAuth2RefreshToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class OAuthTokenSerializationTest {
	ObjectMapper mapper = new ObjectMapper();

	@Test
	public void testSerializeRefreshToken() throws JsonProcessingException {
		OAuth2RefreshToken refreshToken = new DefaultExpiringOAuth2RefreshToken("token1", new Date(System.currentTimeMillis() + 100));
		System.err.println(mapper.writeValueAsString(refreshToken));

		refreshToken = new DefaultOAuth2RefreshToken("token1");
		System.err.println(mapper.writeValueAsString(refreshToken));
	}

	@Test
	public void testSerializeAccessToken() throws IOException {
		DefaultOAuth2AccessToken accessToken = new DefaultOAuth2AccessToken("accessToken1");
		accessToken.setExpiration(new Date(System.currentTimeMillis() + 5000));
		OAuth2RefreshToken refreshToken = new DefaultExpiringOAuth2RefreshToken("token1", new Date(System.currentTimeMillis() + 10000));
		accessToken.setRefreshToken(refreshToken);
		Set<String> scope = new HashSet<String>();
		scope.add("read");
		scope.add("write");
		accessToken.setScope(scope);
		accessToken.setTokenType("tokenType");
		Map<String, Object> additionalInformation = new HashMap<String, Object>();
		additionalInformation.put("testStr", "string");
		additionalInformation.put("testInt", 1);
		additionalInformation.put("testDbl", 1.1d);
		accessToken.setAdditionalInformation(additionalInformation);
		String accessTokenStr = mapper.writeValueAsString(accessToken);
		System.err.println(accessTokenStr);

		DefaultOAuth2AccessToken tokenRead = mapper.readValue(accessTokenStr, DefaultOAuth2AccessToken.class);
		assertTrue(tokenRead.getTokenType().equals(accessToken.getTokenType()));
		// roughly the same
		assertTrue(tokenRead.getExpiration().getTime() / 10000 == (accessToken.getExpiration().getTime() / 10000));
		assertTrue(tokenRead.getValue().equals(accessToken.getValue()));

		assertTrue(tokenRead.getScope().size() == accessToken.getScope().size());
		for (String s : tokenRead.getScope()) {
			assertTrue(accessToken.getScope().contains(s));
		}

		assertTrue(tokenRead.getAdditionalInformation().size() == accessToken.getAdditionalInformation().size());
		for (String aikey : tokenRead.getAdditionalInformation().keySet()) {
			assertTrue(accessToken.getAdditionalInformation().containsKey(aikey));
			Object a = tokenRead.getAdditionalInformation().get(aikey);
			Object b = accessToken.getAdditionalInformation().get(aikey);
			assertTrue((a == null && b == null) || (a != null && b != null));
			assertTrue(a.toString().equals(b.toString()));
		}

		System.err.println(mapper.writeValueAsString(tokenRead));
	}

	@Test
	public void testDeserializeRefreshToken() throws IOException {

	}
}
