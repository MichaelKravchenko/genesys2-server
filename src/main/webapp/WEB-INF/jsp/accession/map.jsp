<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="maps.accession-map" /></title>
</head>
<body>
	<h1>
		<spring:message code="maps.accession-map" />
	</h1>

	<div class="main-col-header clearfix">
	<div class="nav-header">
		<div class="pull-right">
			<a class="btn btn-default" href="<c:url value="/explore" />" id="selectArea"><spring:message code="view.accessions" /></a>
			<form style="display: inline-block" method="post" action="/explore/kml">
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				<input type="hidden" name="filter" value="<c:out value="${jsonFilter}" />" />
				<button class="btn btn-default" type="submit"><spring:message code="download.kml" /></button>
			</form>
		</div>
		<div class="results">
			<a class="btn btn-default" href="javascript: window.history.go(-1);"><spring:message code="navigate.back" /></a>
		</div>
	</div>
	</div>

    <div class="applied-filters">
        <ul class="nav nav-pills ">


            <li class="active dropdown form-horizontal pull-right" >

                <a id="get-filters" href="#"><spring:message code="savedmaps.list"/></a>

                <ul id="enabled-filters" class="dropdown-menu"></ul>

            </li>

            <li style="margin-right: 5px" class="active form-horizontal pull-right" data-toggle="modal" data-target="#modal-dialog">
                <a href="#"><spring:message code="savedmaps.save"/></a>
            </li>
        </ul>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modal-dialog" tabindex="-1" role="dialog" aria-labelledby="modal-label" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="modal-label"><spring:message code="savedmaps.save"/></h4>
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control" placeholder="<spring:message code="filter.enter.title"/>" id="filter-title">
                    <div href="#" id="color" ></div>
                </div>
                <div class="modal-footer">
                    <button type="button"  class="btn btn-default" data-dismiss="modal"><spring:message code="cancel"/></button>
                    <button id="save-filter" type="button"  class="btn btn-primary" data-dismiss="modal"><spring:message code="save"/></button>
                </div>
            </div>
        </div>
    </div>
    <%--End modal--%>

    <div class="row">
		<div class="col-sm-12">
			<div id="map" class="gis-map gis-map-square"></div>
		</div>
	</div>


<content tag="javascript">
		<script type="text/javascript">
		jQuery(document).ready(function() {

            var color=null;
            $('#color').colorPicker({
                click: function(data){
                    $('#output').html(data);
                    color=data;
                }
            });

            $("#save-filter").on("click",  function(event) {
                event.preventDefault();

                var title = $("#filter-title").val();
                var filter=${jsonFilter};

                $.ajax({
                    url : "/savedmaps/save",
                    type : "post",
                    dataType : "json",
                    contentType: 'application/json; charset=utf-8',
                    data : JSON.stringify({
                        title : title,
                        filter : JSON.stringify(filter),
                        color:color
                    }),
                    success : function(data) {
                    },
                    error : function(error) {
                        console.log(error)
                    }
                });
            });

            $("#get-filters").on("click",  function(event) {
                event.preventDefault();
                $.ajax({
                    url: "/savedmaps/get",
                    type: "get",
                    dataType: "json",
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        $.each(data, function (idx, filter) {

                            var li = "<li><label  class='saved-filter' x-fil='"+filter.filter+"' x-color='" + filter.color + "'><input style='margin-right: 10px;margin-left: 5px' type='checkbox'>" + filter.title + "</label></li>";

                            if ($("#enabled-filters").is(":visible")) {
                                $("#enabled-filters").append(li);
                            } else {
                                $("#enabled-filters").empty();
                            }
                        });
                    },
                    error : function(error) {
                        console.log(error)
                    }
                });
                $("#enabled-filters").toggle();
            });

			var map = L.map('map').setView([20,0], 2);
			L.tileLayer('https://otile{s}-s.mqcdn.com/tiles/1.0.0/sat/{z}/{x}/{y}.png', {
			    attribution: "MapQuest",
			    styleId: 22677,
			    subdomains: ['1','2','3','4'],
			    opacity: 0.6
			}).addTo(map);
			L.tileLayer("{s}/explore/tile/{z}/{x}/{y}?filter=" + '${jsonFilter}', {
			    attribution: "<a href='${props.baseUrl}'>Genesys</a>",
			    styleId: 22677,
			    subdomains: [${props.tileserverCdn}]
			}).addTo(map);

            $("#selectArea").hide();
            var filterJson =${jsonFilter};
            var filterJsonObj = {};
            if (typeof filterJson !== 'undefined') {
                filterJsonObj = JSON.parse(JSON.stringify(filterJson));

            }


            var layers={};
            $("body").on("click", ".saved-filter", function (e) {
                var title = $(this).text();
                var filter = $(this).attr("x-fil");
                var tilesColor = $(this).attr("x-color").substring(1);
				var savedFilterObj=JSON.parse(filter);

                if ($(this).find("input:checkbox").is(":checked")) {
                	if (layers[title] == null) {
	                    layers[title] = L.tileLayer("{s}/explore/tile/{z}/{x}/{y}?filter=" + filter + "&color=" + tilesColor, {
	                        attribution: "<a href='${props.baseUrl}'>Genesys</a>",
	                        styleId: 22677,
	                        subdomains: [${props.tileserverCdn}]
	                    }).addTo(map);
					}
                } else {
                	if (layers[title] != null) {
	                    map.removeLayer(layers[title]);
	                    layers[title]=null;
					}
                }

            });

            var locationFilter = new L.LocationFilter({adjustButton: false, bounds: map.getBounds().pad(-0.1) }).addTo(map);
			locationFilter.on("change", function (e) {
			    // Do something when the bounds change.
			    // Bounds are available in `e.bounds`.
				var bounds=locationFilter.getBounds();
				filterJson["geo.latitude"]=[{range:[bounds.getSouth(),bounds.getNorth()]}];
				filterJson["geo.longitude"]=[{range:[bounds.getWest(),bounds.getEast()]}];
			});

			map.on("viewreset", function() {
				if (locationFilter.isEnabled())
					return;
				var mapBounds=map.getBounds().pad(-0.1);
				locationFilter.setBounds(mapBounds);
			});
			locationFilter.on("enabled", function () {
			    // Do something when enabled.
				var bounds=locationFilter.getBounds();
				filterJson["geo.latitude"]=[{range:[bounds.getSouth(),bounds.getNorth()]}];
				filterJson["geo.longitude"]=[{range:[bounds.getWest(),bounds.getEast()]}];
			    $("#selectArea").show();
			});
			
			locationFilter.on("disabled", function () {
			    // Do something when disabled.
			    $("#selectArea").hide();
			});
			
			$("#selectArea").on("click", function(e) {
				this.href=this.href+'?filter='+JSON.stringify(filterJson);
			});
			
			var loadDetailsTimeout = null;
			var clickMarker=null;
			map.on("click", function(e) {
				if (clickMarker!=null) {
					map.removeLayer(clickMarker);
					clickMarker=null;
				}
				if (map.getZoom()>4) {
					if (loadDetailsTimeout!=null) {
						clearTimeout(loadDetailsTimeout);
					}
					
					var point=this.latLngToLayerPoint(e.latlng);
					point.x-=5; point.y+=5;
					var sw=this.layerPointToLatLng(point);
					point.x+=10; point.y-=10;
					var ne=this.layerPointToLatLng(point);
					loadDetailsTimeout=setTimeout(function() {

						var filterBounds=filterJsonObj;
                        console.log(filterBounds)
						filterBounds.latitude=[{range:[sw.lat, ne.lat]}];
						filterBounds.longitude=[{range:[sw.lng, ne.lng]}];
						//console.log(JSON.stringify(filterBounds));
						$.ajax("<c:url value="/explore/geoJson"><c:param name="limit" value="11" /></c:url>&filter="+JSON.stringify(filterBounds), {
							type : "GET",
							dataType : 'json',
							success : function(respObject) {
								if (respObject.features == null || respObject.features.length == 0)
									return;
								
								var c="";
								respObject.features.forEach(function(a, b) {
									if (b<10)
										c+="<a href='<c:url value="/acn/id/" />" + a.id + "'>" + a.properties.acceNumb + " " + a.properties.instCode + "</a><br />";
									else
										c+="...";
								});
								clickMarker=L.rectangle([sw, ne], {stroke: false, fill: true}).addTo(map);
								clickMarker.bindPopup(c).openPopup();
							},
							error: function(jqXHR, textStatus, errorThrown) {
								console.log(textStatus);
								console.log(errorThrown);
							}
						});
						
					}, 200);
				}
			});
			map.on("dblclick", function(e) {
				if (loadDetailsTimeout!=null) {
					//console.log("cleared" + loadDetailsTimeout);
					clearTimeout(loadDetailsTimeout);
					loadDetailsTimeout=null;
				}
			});
		});
		</script>
</content>
</body>
</html>