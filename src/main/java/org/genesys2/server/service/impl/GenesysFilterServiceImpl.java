/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.Method;
import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.persistence.domain.AccessionRepository;
import org.genesys2.server.persistence.domain.MethodRepository;
import org.genesys2.server.persistence.domain.TraitValueRepository;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.FilterConstants;
import org.genesys2.server.service.GenesysFilterService;
import org.genesys2.server.service.GeoService;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.TaxonomyService;
import org.genesys2.server.service.TraitService;
import org.genesys2.server.service.impl.DirectMysqlQuery.MethodResolver;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilter;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.genesys2.server.service.impl.FilterHandler.FilterValue;
import org.genesys2.server.service.impl.FilterHandler.LiteralValueFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.ArgumentPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class GenesysFilterServiceImpl implements GenesysFilterService {

	private static final Log LOG = LogFactory.getLog(GenesysFilterServiceImpl.class);

	@Autowired
	private TraitValueRepository traitValueRepository;

	@Autowired
	private MethodRepository methodRepository;

	@Autowired
	private AccessionRepository accessionRepository;

	@Autowired
	private TraitService traitService;

	@Autowired
	private GeoService geoService;

	// @PersistenceContext
	// private EntityManager entityManager;

	private JdbcTemplate jdbcTemplate;

	@Autowired
	private InstituteService instituteService;

	@Autowired
	private TaxonomyService taxonomyService;

	@Autowired
	private CropService cropService;

	@Autowired
	public void setDataSource(final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public Page<Accession> listAccessions(AppliedFilters filters, Pageable pageable) {
		if (LOG.isDebugEnabled()) {
			for (AppliedFilter filter : filters) {
				LOG.debug("Looking at " + filter.toString());
			}
		}

		final DirectMysqlQuery directQuery = new DirectMysqlQuery("accession", "a");
		directQuery.jsonFilter(filters, new MethodResolver() {
			@Override
			public Method getMethod(long methodId) {
				return methodRepository.findOne(methodId);
			}
		});
		directQuery.pageable(pageable);

		final Long totalCount = this.jdbcTemplate.queryForObject(directQuery.getCountQuery("a.id"), directQuery.getParameters(), Long.class);
		LOG.info("Total count: " + totalCount);

		if (totalCount > 0 && pageable.getPageNumber() * pageable.getPageSize() <= totalCount) {
			final List<Long> results = this.jdbcTemplate.queryForList(directQuery.getQuery("a.id"), directQuery.getParameters(), Long.class);
			LOG.info("Getting accessions " + results.size());

			return new PageImpl<Accession>(results.size() == 0 ? new ArrayList<Accession>() : accessionRepository.listById(results,
			// TODO Consider processing the pageable.getSort
					new Sort("accessionName")),
			// -- TODO
					pageable, totalCount);
		} else {
			return new PageImpl<Accession>(new ArrayList<Accession>(), pageable, totalCount);
		}
	}

	/**
	 * Filtering autocompleter
	 */
	@Override
	public List<LabelValue<String>> autocomplete(String filter, String ac, AppliedFilters filters) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Autocomplete " + filter + " ac=" + ac);
		}

		Crop crop = null;

		{
			String shortName = filters.getFirstLiteralValue(FilterConstants.CROPS, String.class);
			if (shortName != null)
				crop = cropService.getCrop((String) shortName);
		}

		final List<LabelValue<String>> completed = new ArrayList<LabelValue<String>>();
		if ("instCode".equalsIgnoreCase(filter)) {
			final List<FaoInstitute> faoInst = instituteService.autocomplete(ac);
			for (final FaoInstitute inst : faoInst) {
				completed.add(new LabelValue<String>(inst.getCode(), inst.getCode() + ", " + inst.getFullName()));
			}
		} else if ("country".equalsIgnoreCase(filter)) {
			final List<Country> countries = geoService.autocomplete(ac);
			for (final Country c : countries) {
				completed.add(new LabelValue<String>(c.getCode3(), c.getCode3() + ", " + c.getName()));
			}
		} else if ("genus".equalsIgnoreCase(filter)) {
			final List<String> genera = taxonomyService.autocompleteGenus(ac, crop);
			for (final String value : genera) {
				completed.add(new LabelValue<String>(value, value));
			}
		} else if ("species".equalsIgnoreCase(filter)) {
			List<String> genus = new ArrayList<>();

			AppliedFilter genusFilter = filters.get(FilterConstants.TAXONOMY_GENUS);
			if (genusFilter != null) {
				Set<FilterValue> elements = genusFilter.getValues();
				for (FilterValue fv : elements)
					genus.add((String) ((LiteralValueFilter) fv).getValue());
			}

			final List<String> species = taxonomyService.autocompleteSpecies(ac, crop, genus);
			for (final String value : species) {
				completed.add(new LabelValue<String>(value, value));
			}
		} else if ("taxonomy".equalsIgnoreCase(filter)) {
			final List<String> taxa = taxonomyService.autocompleteTaxonomy(ac);
			for (final String taxonomy : taxa) {
				completed.add(new LabelValue<String>(taxonomy, taxonomy));
			}
		} else {
			throw new RuntimeException("No autocompleter for " + filter);
		}
		return completed;
	}

	public static class LabelValue<T> {
		private final T value;
		private final String label;

		public LabelValue(T value, String label) {
			this.value = value;
			this.label = label;
		}

		public T getValue() {
			return value;
		}

		public String getLabel() {
			return label;
		}
	}

	@Override
	public void listGeo(AppliedFilters filters, Integer limit, RowCallbackHandler rowHandler) {
		listGeoTile(false, filters, limit, -1, 0, 0, rowHandler);
	}

	@Override
	public void listGeoTile(final boolean distinct, AppliedFilters filters, Integer limit, int zoom, int xtile, int ytile, RowCallbackHandler rowHandler) {
		if (LOG.isDebugEnabled()) {
			for (AppliedFilter filter : filters) {
				LOG.debug("Looking at " + filter.toString());
			}
		}

		final DirectMysqlQuery directQuery = new DirectMysqlQuery("accessiongeo", "geo");
		directQuery.innerJoin("accession", "a", "a.id=geo.accessionId");
		directQuery.join(filters);
		directQuery.filterTile(zoom, xtile, ytile);
		directQuery.filter(filters, new MethodResolver() {
			@Override
			public Method getMethod(long methodId) {
				return methodRepository.findOne(methodId);
			}
		});
		directQuery.limit(limit);

		this.jdbcTemplate.query(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				final PreparedStatement stmt = con.prepareStatement(directQuery.getQuery(distinct ? "distinct geo.longitude, geo.latitude"
						: "a.id, a.acceNumb, a.instCode, geo.longitude, geo.latitude, geo.datum, geo.uncertainty "));
				// Set mysql JConnector to stream results
				stmt.setFetchSize(Integer.MIN_VALUE);
				new ArgumentPreparedStatementSetter(directQuery.getParameters()).setValues(stmt);
				return stmt;
			}
		}, rowHandler);
	};
}
