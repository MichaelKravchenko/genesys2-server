<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="oauth-client.page.profile.title" arguments="${item.clientId}" argumentSeparator="|" /></title>
</head>
<body>
	<h1>
		<c:out value="${item.clientId}" />
	</h1>

	<form class="" action="<c:url value="/admin/oauth/update" />" method="post">
		<div class="form-group">
			<label for="blurp-body" class="control-label"><spring:message code="blurp.blurp-body" /></label>
			<div class="controls">
				<textarea id="blurp-body" name="blurp" class="span9 required html-editor">
					<c:out value="${blurp.body}" />
				</textarea>
			</div>
		</div>

		<input type="submit" value="<spring:message code="blurp.update-blurp"/>" class="btn btn-primary" />
		<a href="<c:url value="/geo/${country.code3}" />" class="btn btn-default"> <spring:message code="cancel" />
		</a>
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>

<content tag="javascript">	
	<script type="text/javascript" src="/html/js/tinymce/tinymce.min.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			tinyMCE.init({
				selector : "#blurp-body.html-editor",
				menubar : false,
				statusbar : false,
				height : 200,
				plugins: "link autolink",
				directionality: document.dir
			});
		});
	</script>
</content>
</body>
</html>