<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="accession.page.profile.title" arguments="${accession.accessionName}" argumentSeparator="|" /></title>
</head>
<body typeof="germplasm:GermplasmAccession">
	<h1>
		<span property="dwc:catalogNumber"><c:out value="${accession.accessionName}" /></span>
		<small property="dwc:institutionCode" datatype="germplasmType:wiewsInstituteID"><c:out value="${accession.instituteCode}" /></small>
	</h1>

	<div class="main-col-header acn">
		<div class="sel" x-aid="${accession.id}">
			<a class="add" href=""><spring:message code="selection.add" arguments="${accession.accessionName}" /></a> <a class="remove" href=""><spring:message code="selection.remove"
					arguments="${accession.accessionName}" /></a>
		</div>
	</div>

	<c:if test="${accession.inTrust eq true}">
		<div class="alert alert-info">
			<spring:message code="accession.inTrust.true" />
		</div>
	</c:if>

	<c:if test="${accession.inSvalbard eq true}">
		<div class="alert alert-info">
			<spring:message code="accession.inSvalbard.true" />
		</div>
	</c:if>

	<c:if test="${accession.mlsStatus eq true}">
		<div class="alert alert-info">
			<spring:message code="accession.mlsStatus.true" />
		</div>
	</c:if>
	
	<c:if test="${accession.availability eq false}">
		<div class="alert alert-warning">
			<spring:message code="accession.not-available-for-distribution" />
		</div>
	</c:if>
	
	<c:if test="${accession.availability eq true}">
		<div class="alert alert-info">
			<spring:message code="accession.available-for-distribution" />
		</div>
	</c:if>

	<div class="pseudo-right-col col-md-3">
		<c:if test="${accessionGeo.latitude ne null and accessionGeo.longitude ne null}">
		
		<div class="crop-location" itemscope itemtype="http://schema.org/GeoCoordinates">
		 <h3><spring:message code="accession.collecting.site" /></h3>
	        <div id="map" class="map-container"></div>
	        
	        <table class="map-data">  
	        <tr>
	            <td><spring:message code="filter.geo.latitude" />:</td>
	            <td><c:out value="${accessionGeo.latitude}" /></td>
	          </tr><tr>
	            <td><spring:message code="filter.geo.longitude" />:</td>
	            <td><c:out value="${accessionGeo.longitude}" /></td>
	          </tr><tr>
	            <td><spring:message code="accession.elevation" />:</td>
	            <td><c:out value="${accessionGeo.elevation}" /></td>
	          </tr>
	         </table>
	      </div>
		</c:if>
	</div>
	
	<div class="crop-details">
			<div class="row">
				<div class="col-xs-4"><spring:message code="accession.holdingInstitute" /></div>
				<div class="col-xs-8"><a property="dwc:institutionCode" href="<c:url value="/wiews/${accession.instituteCode}" />"> <c:out value="${accession.institute.fullName}" />
				</a></div>
			</div>
			<div class="row">
				<div class="col-xs-4"><spring:message code="accession.holdingCountry" /></div>
				<div class="col-xs-8"><a href="<c:url value="/geo/${accession.institute.country.code3}" />"><c:out value="${accession.institute.country.getName(pageContext.response.locale)}" /></a></div>
			</div>
			<div class="row">
				<div class="col-xs-4"><spring:message code="accession.accessionName" /></div>
				<div class="col-xs-8"><c:out value="${accession.accessionName}" /></div>
			</div>
			<c:if test="${accession.uuid ne null}">
			<div class="row">
				<div class="col-xs-4"><spring:message code="accession.uuid" /></div>
				<div class="col-xs-8"><b><c:out value="${accession.uuid}" /></b></div>
			</div>
			</c:if>

			<c:if test="${crops ne null}">
				<div class="row">
					<div class="col-xs-4"><spring:message code="accession.crop" /></div>
					<div class="col-xs-8"><c:forEach items="${crops}" var="crop">
							<a href="<c:url value="/c/${crop.shortName}" />"><c:out value="${crop.getName(pageContext.response.locale)}" /></a>
						</c:forEach></div>
				</div>
			</c:if>

			<c:if test="${accession.countryOfOrigin ne null}">
				<div class="row">
					<div class="col-xs-4"><spring:message code="accession.origin" /></div>
					<div class="col-xs-8"><img src="<c:url value="${cdnFlagsUrl}" />/${accession.origin.toUpperCase()}.png" /> <a href="<c:url value="/geo/${accession.origin}" />"> <c:out value="${accession.countryOfOrigin.getName(pageContext.response.locale)}" />
					</a></div>
				</div>
			</c:if>

			<div class="row">
				<div class="col-xs-4"><spring:message code="taxonomy.genus" /></div>
				<div class="col-xs-8" property="dwc:genus"><a href="<c:url value="/acn/t/${accession.taxonomy.genus}" />"><c:out value="${accession.taxonomy.genus}" /></a></div>
			</div>
			<div class="row">
				<div class="col-xs-4"><spring:message code="taxonomy.species" /></div>
				<div class="col-xs-8"><a href="<c:url value="/acn/t/${accession.taxonomy.genus}/${accession.taxonomy.species}" />"><c:out value="${accession.taxonomy.genus} ${accession.taxonomy.species}" /></a>
					<br />
					<a href="<c:url value="/wiews/${accession.institute.code}/t/${accession.taxonomy.genus}/${accession.taxonomy.species}" />"><spring:message code="accession.taxonomy-at-institute" arguments="${accession.taxonomy.genus} ${accession.taxonomy.species}|||${accession.institute.code}" argumentSeparator="|||" /></a>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-4"><spring:message code="taxonomy.taxonName" /></div>
				<div class="col-xs-8"><c:out value="${accession.taxonomy.taxonName}" /></div>
			</div>

			<div class="row">
				<div class="col-xs-4"><spring:message code="accession.sampleStatus" /></div>
				<div class="col-xs-8" property="germplasm:biologicalStatus"><spring:message code="accession.sampleStatus.${accession.sampleStatus}" /></div>
			</div>

			<div class="row">
				<div class="col-xs-4"><spring:message code="accession.storage" /></div>
				<div class="col-xs-8">
					<c:forEach items="${accession.storage.split('[;,]')}" var="storage">
						<div><spring:message code="accession.storage.${storage}" /></div>
					</c:forEach>
<%--				
					<c:forEach items="${accession.stoRage}" var="storage">
						<div><spring:message code="accession.storage.${storage}" /></div>
					</c:forEach>
--%>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-4"><spring:message code="accession.availability" /></div>
				<div class="col-xs-8"><spring:message code="accession.availability.${accession.availability}" /></div>
			</div>
			
			<div class="row">
				<div class="col-xs-4"><spring:message code="accession.otherNames" /></div>
				<div class="col-xs-8">
				<c:forEach items="${accessionAliases}" var="accessionAlias">
					<div>
						<c:out value="${accessionAlias.name}" />
						<c:if test="${accessionAlias.instCode != ''}">
							<a href="<c:url value="/wiews/${accessionAlias.instCode}" />">
							${accessionAlias.instCode}
							</a>
						</c:if>
						<c:if test="${accessionAlias.usedBy != ''}">
							${accessionAlias.usedBy}
						</c:if>
						<c:if test="${accessionAlias.lang != ''}">
							<c:out value="${accessionAlias.lang}" />
						</c:if>
						<spring:message code="accession.aliasType.${accessionAlias.aliasType}" />
					</div>			
				</c:forEach>
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs-4"><spring:message code="accession.duplSite" /></div>
				<div class="col-xs-8"><c:forEach items="${accession.duplSite.split('[;,]')}" var="duplSite">
					<div><spring:message code="${duplSite}" /></div>
				</c:forEach></div>
			</div>
			

			<c:if test="${accessionExchange ne null}">
				<div class="row">
					<div class="col-xs-4"><spring:message code="accession.donor.institute" /></div>
					<div class="col-xs-8">
						<c:if test="${accessionExchange.donorInstitute eq null}">
						${accessionExchange.donorName}
						</c:if>
						${accessionExchange.donorInstitute}
					</div>
				</div>
				<div class="row">
					<div class="col-xs-4"><spring:message code="accession.donor.accessionNumber" /></div>
					<div class="col-xs-8">${accessionExchange.accNumbDonor}</div>
				</div>
			</c:if>

			<c:if test="${accessionCollect ne null}">
				<h4><spring:message code="accession.collecting" /></h4>
				
				<div class="row">
					<div class="col-xs-4"><spring:message code="accession.collecting.institute" /></div>
					<div class="col-xs-8">${accessionCollect.collCode}</div>
				</div>
				<c:if test="${accessionCollect.collName ne null}">
				<div class="row">
					<div class="col-xs-4"><spring:message code="accession.collecting.institute" /></div>
					<div class="col-xs-8">${accessionCollect.collName}
					${accessionCollect.collInstAddress}</div>
				</div>
				</c:if>
				<div class="row">
					<div class="col-xs-4"><spring:message code="accession.collecting.mission" /></div>
					<div class="col-xs-8"><c:out value="${accessionCollect.collMissId}" /></div>
<%--				<div class="col-xs-8"><a href="<c:url value="/collectingmission"><c:param name="collMissId" value="${accessionCollect.collMissId}" /></c:url>"><c:out value="${accessionCollect.collMissId}" /></a></div> --%>
				</div>
				<div class="row">
					<div class="col-xs-4"><spring:message code="accession.collecting.date" /></div>
					<div class="col-xs-8">${accessionCollect.collDate}</div>
				</div>
				<div class="row">
					<div class="col-xs-4"><spring:message code="accession.collecting.number" /></div>
					<div class="col-xs-8">${accessionCollect.collNumb}</div>
				</div>
				<div class="row">
					<div class="col-xs-4"><spring:message code="accession.collecting.site" /></div>
					<div class="col-xs-8">${accessionCollect.collSite}</div>
				</div>
				<div class="row">
					<div class="col-xs-4"><spring:message code="accession.collecting.source" /></div>
					<div class="col-xs-8"><spring:message code="accession.collectingSource.${accessionCollect.collSrc}" /></div>
				</div>
			</c:if>

			<c:if test="${accessionBreeding ne null}">
				<h4><spring:message code="accession.breeding" /></h4>

				<div class="row">
					<div class="col-xs-4"><spring:message code="accession.breederCode" /></div>
					<div class="col-xs-8">${accessionBreeding.breederCode}</div>
				</div>
				<div class="row">
					<div class="col-xs-4"><spring:message code="accession.pedigree" /></div>
					<div class="col-xs-8">${accessionBreeding.pedigree}</div>
				</div>
			</c:if>


			<c:if test="${accessionGeo ne null}">
				<h4><spring:message code="accession.geo" /></h4>

				<c:if test="${accessionCollect.collSite ne null}">
				<div class="row">
					<div class="col-xs-4"><spring:message code="accession.collecting.site" /></div>
					<div class="col-xs-8">${accessionCollect.collSite}</div>
				</div>
				</c:if>
				<c:if test="${accessionGeo.latitude ne null}">
				<div class="row">
					<div class="col-xs-4"><spring:message code="accession.geolocation" /></div>
					<div class="col-xs-8">${accessionGeo.latitude}, ${accessionGeo.longitude}</div>
				</div>
				</c:if>
				<c:if test="${accessionGeo.elevation ne null}">
				<div class="row">
					<div class="col-xs-4"><spring:message code="accession.elevation" /></div>
					<div class="col-xs-8">${accessionGeo.elevation}<span class="uom">m</span></div>
				</div>
				</c:if>
				<div class="row">
					<div class="col-xs-4"><spring:message code="accession.geo.datum" /></div>
					<div class="col-xs-8">${accessionGeo.datum}</div>
				</div>
				<div class="row">
					<div class="col-xs-4"><spring:message code="accession.geo.method" /></div>
					<div class="col-xs-8">${accessionGeo.method}</div>
				</div>
				<c:if test="${accessionGeo.uncertainty ne null}">
				<div class="row">
					<div class="col-xs-4"><spring:message code="accession.geo.uncertainty" /></div>
					<div class="col-xs-8">${accessionGeo.uncertainty}<span class="uom">m</span></div>
				</div>
				</c:if>
			</c:if>


			<c:if test="${svalbardData ne null}">
				<h4><spring:message code="accession.svalbard-data" /></h4>

				<div class="row">
					<div class="col-xs-4"><spring:message code="accession.svalbard-data.taxonomy" /></div>
					<div class="col-xs-8">${svalbardData.taxonomy}</div>
				</div>

				<div class="row">
					<div class="col-xs-4"><spring:message code="accession.svalbard-data.depositDate" /></div>
					<div class="col-xs-8">${svalbardData.depositDate}</div>
				</div>
				<div class="row">
					<div class="col-xs-4"><spring:message code="accession.svalbard-data.boxNumber" /></div>
					<div class="col-xs-8">${svalbardData.boxNumber}</div>
				</div>
				<div class="row">
					<div class="col-xs-4"><spring:message code="accession.svalbard-data.quantity" /></div>
					<div class="col-xs-8">${svalbardData.quantity}</div>
				</div>

			</c:if>

	
			<c:if test="${fn:length(accessionRemarks) gt 0}">		
				<h4><spring:message code="accession.remarks" /></h4>
			</c:if>
			<c:forEach items="${accessionRemarks}" var="accessionRemark">
				<div class="row">
					<div class="col-xs-4"><c:out value="${accessionRemark.fieldName}" /></div>
					<div class="col-xs-8"><c:out value="${accessionRemark.remark}" /></div>
				</div>
			</c:forEach>
	</div>


	<c:if test="${methods.size() gt 0}">

		<div class="crop-details">
			<h4>
				<spring:message code="accession.methods" />
			</h4>
			<c:forEach items="${methods}" var="method" varStatus="status">
				<div class="row targeted" id="method${method.id}">
					<div class="col-xs-6 col-sm-4"><c:out value="${method.parameter.title}" /></div>
					<div class="col-xs-6 col-sm-4"><c:forEach items="${methodValues[method.id]}" var="val">
							<div>
								<c:out value="${method.decode(val.value)}" />
								<span class="uom"><c:out value="${method.unit}" /></span> <sup><a href="#metadata-${val.experimentId}"><c:out value="${val.experimentId}" /></a></sup>
							</div>
						</c:forEach></div>
					<div class="col-xs-12 col-sm-4"><c:out value="${method.method}" /></div>
				</div>
			</c:forEach>
		</div>
	</c:if>

	<c:if test="${metadatas.size() gt 0}">
		<h4>
			<spring:message code="accession.metadatas" />
		</h4>
		<ul class="funny-list">
			<c:forEach items="${metadatas}" var="metadata" varStatus="status">
				<li class="clearfix targeted ${status.count % 2 == 0 ? 'even' : 'odd'}" id="metadata-${metadata.id}">
					<div class="show pull-left">
						<sup><c:out value="${metadata.id}" /></sup> <a href="<c:url value="/data/view/${metadata.id}" />"><c:out value="${metadata.title}" /></a>
					</div>
					<div class="pull-right">
						<c:out value="${metadata.instituteCode}" />
					</div>
					<div class="pull-right">
						<c:out value="${metadata.description}" />
					</div>
				</li>
			</c:forEach>
		</ul>
	</c:if>
	
	
	
	<div class="audit-info">
		<c:if test="${accession.lastModifiedBy ne null}"><spring:message code="audit.lastModifiedBy" arguments="${jspHelper.userFullName(accession.lastModifiedBy)}" /></c:if>
		<fmt:formatDate value="${accession.lastModifiedDate}" type="both" />
	</div>

	<content tag="javascript">
	<c:if test="${accessionGeo ne null and accessionGeo.latitude ne null}">
		<script type="text/javascript">
				jQuery(document).ready(function() {
					var map = L.map('map').setView([${accessionGeo.latitude}, ${accessionGeo.longitude}], 2);
					L.tileLayer('https://otile{s}-s.mqcdn.com/tiles/1.0.0/sat/{z}/{x}/{y}.png', {
					    attribution: "MapQuest",
					    styleId: 22677,
					    subdomains: ['1','2','3','4'],
					    opacity: 0.6
					}).addTo(map);
					var marker = L.marker([${accessionGeo.latitude}, ${accessionGeo.longitude}]).addTo(map);
				});
		</script>
	</c:if>

	<script type="text/javascript">
		<%@include file="/WEB-INF/jsp/wiews/ga.jsp"%>
		_pageDim = { institute: '${accession.instituteCode}', genus: '${accession.taxonomy.genus}' };
	</script>
	</content>
</body>
</html>