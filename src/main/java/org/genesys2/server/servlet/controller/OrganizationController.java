/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.model.impl.Organization;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.FilterConstants;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.GeoService;
import org.genesys2.server.service.OrganizationService;
import org.genesys2.server.service.TaxonomyService;
import org.genesys2.server.service.impl.FilterHandler;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilter;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/org")
public class OrganizationController extends BaseController {

	@Autowired
	private OrganizationService organizationService;

	@Autowired
	private GenesysService genesysService;

	@Autowired
	private TaxonomyService taxonomyService;

	@Autowired
	private ContentService contentService;

	@Autowired
	private GeoService geoService;

	@RequestMapping("/")
	public String view(ModelMap model, @RequestParam(value = "page", required = false, defaultValue = "1") int page) {
		model.addAttribute("pagedData", organizationService.list(new PageRequest(page - 1, 50, new Sort("title"))));
		return "/organization/index";
	}

	@RequestMapping("/{slug}")
	public String view(ModelMap model, @PathVariable(value = "slug") String slug) {
		_logger.debug("Viewing organization " + slug);
		final Organization organization = organizationService.getOrganization(slug);
		if (organization == null) {
			throw new ResourceNotFoundException();
		}
		final List<FaoInstitute> members = organizationService.getMembers(organization);

		_logger.debug("Has: " + members.size());

		// Sort members by country
		final Locale locale = getLocale();

		Collections.sort(members, new Comparator<FaoInstitute>() {
			@Override
			public int compare(FaoInstitute o1, FaoInstitute o2) {
				return o1.getCountry().getName(locale).compareTo(o2.getCountry().getName(locale));
			}
		});

		model.addAttribute("organization", organization);
		model.addAttribute("members", members);

		model.addAttribute("blurp", contentService.getArticle(organization, "blurp", getLocale()));

		return "/organization/details";
	}

	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@RequestMapping("/{slug}/edit")
	public String edit(ModelMap model, @PathVariable(value = "slug") String slug) {
		try {
			view(model, slug);
		} catch (final ResourceNotFoundException e) {
			final Organization organization = new Organization();
			organization.setSlug(slug);
			model.addAttribute("organization", organization);
		}
		return "/organization/edit";
	}

	/**
	 * View map of member institutes
	 * 
	 * @param model
	 * @param slug
	 * @return
	 */
	@RequestMapping("/{slug}/map")
	public String map(ModelMap model, @PathVariable(value = "slug") String slug) {
		view(model, slug);

		@SuppressWarnings("unchecked")
		final List<FaoInstitute> members = (List<FaoInstitute>) model.get("members");

		model.addAttribute("jsonInstitutes", geoService.toJson(members).toString());

		return "/organization/map";
	}

	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@RequestMapping(value = "/{slug}/update")
	public String update(ModelMap model, @PathVariable(value = "slug") String slug, @RequestParam(value = "slug") String newSlug,
			@RequestParam("title") String title, @RequestParam("blurp") String blurp) {
		_logger.debug("Updating organization " + slug);

		Organization organization = organizationService.getOrganization(slug);

		if (organization == null) {
			organization = organizationService.create(newSlug, title);
		} else {
			organization = organizationService.update(organization.getId(), newSlug, title);
		}

		organizationService.updateBlurp(organization, blurp, getLocale());

		return "redirect:/org/" + organization.getSlug();
	}

	@RequestMapping("/{orgSlug}/data")
	public String viewData(ModelMap model, @PathVariable(value = "orgSlug") String orgSlug,
			@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
		_logger.debug("Viewing organization " + orgSlug);
		final Organization organization = organizationService.getOrganization(orgSlug);
		if (organization == null) {
			throw new ResourceNotFoundException();
		}

		model.addAttribute("filter", "{\"" + FilterConstants.INSTITUTE_NETWORK + "\":[\"" + organization.getSlug() + "\"]}");
		model.addAttribute("page", page);
		return "redirect:/explore";
	}

	@RequestMapping("/{orgSlug}/overview")
	public String overview(HttpServletRequest request, @PathVariable(value = "orgSlug") String orgSlug) throws UnsupportedEncodingException {
		final Organization organization = organizationService.getOrganization(orgSlug);
		if (organization == null) {
			throw new ResourceNotFoundException();
		}

		AppliedFilters appliedFilters = new AppliedFilters();
		appliedFilters.add(new AppliedFilter().setFilterName(FilterConstants.INSTITUTE_NETWORK).addFilterValue(
				new FilterHandler.LiteralValueFilter(organization.getSlug())));

		return "forward:/explore/overview?filter=" + URLEncoder.encode(appliedFilters.toString(), "UTF8");
	}
}
