<%@include file="/WEB-INF/jsp/init.jsp" %>

<header id="header">
    <div class="container">
        <div class="clearfix">
            <div class="pull-left">
        <a href="<c:url value="/" />"><img src="<c:url value="/html/images/logo_genesys.png" />" alt="Genesys - Gateway to Genetic Resources" title="Genesys - Gateway to Genetic Resources" /></a>
            </div>

      <form class="navbar-form navbar-left" role="search" id="search" method="get" action="<c:url value="/acn/search" />">
                <div class="form-group">
          <input type="text" class="form-control" name="q" placeholder="<spring:message code="search.input.placeholder" />">
                </div>
        <button type="submit" class="btn"><img src="<c:url value="/html/images/icon_search.png" />" alt="" /></button>
            </form>

            <ul class="nav navbar-nav navbar-right">
                <security:authorize access="isAnonymous()">
                    <li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><spring:message code="page.login" /></a>
                        <ul class="dropdown-menu pull-left">
                            <li>
                                <form role="form" method="post" action="/login-attempt">
                                    <div class="form-group">
                                        <label for="username"><spring:message code="login.username"/>:</label>
                  						<input type="email" class="form-control" id="username" name="j_username" placeholder="<spring:message code="login.username"/>" />
                                    </div>
                                    <div class="form-group">
                                        <label for="password"><spring:message code="login.password"/></label>
                 						<input type="password" class="form-control" id="password" name="j_password" placeholder="<spring:message code="login.password"/>" />
                                    </div>
                                    <div class="checkbox">
                                        <label>
                  							<input type="checkbox" name="_spring_security_remember_me"  id="_spring_security_remember_me"/>
                                            <spring:message code="login.remember-me"/>
                                        </label>
                                    </div>
                                    <button type="submit" class="btn btn-green"><spring:message
                                            code="login.login-button"/></button>
                                    <span class="or">-</span>
	                            	<a href="<c:url value="/google/login" />" class="btn btn-default google-signin"><spring:message code="login.with-google-plus"/></a>
					                <a href="<c:url value="/registration" />" class="btn btn-default"><spring:message code="login.register-now"/></a>
                                    <!-- CSRF protection -->
                                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                </form>
                            </li>
                        </ul>
                    </li>
                </security:authorize>
                <security:authorize access="isAuthenticated()">
                    <li class="dropdown">
         				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><spring:message code="user.pulldown.heading" arguments="${user.user.name}" /></a>
                        <ul class="dropdown-menu pull-left">
                            <security:authorize access="hasRole('ADMINISTRATOR')">
							<li><a href="<c:url value="/admin/" />"><spring:message code="user.pulldown.administration" /></a></li>
							<li><a href="<c:url value="/profile/list" />"><spring:message code="user.pulldown.users" /></a></li>
							<li><a href="<c:url value="/team" />"><spring:message code="user.pulldown.teams" /></a></li>
							<li><a href="<c:url value="/management/" />"><spring:message code="user.pulldown.oauth-clients" /></a></li>
                            </security:authorize>
                            <li><a href="<c:url value="/profile/${user.username}" />"><spring:message code="user.pulldown.profile"/></a></li>
                            <li><a id="logout" href="<c:url value="/logout" />"><spring:message code="user.pulldown.logout"/></a>
                            </li>
                        </ul>
                    </li>
                </security:authorize>

                <li class="dropdown" id="lang">
         		  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><c:out value="${pageContext.response.locale.displayLanguage}" /> <b class="caret"></b></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="?lang=en">English</a></li>
                        <li><a href="?lang=ar">Arabic</a></li>
                        <li><a href="?lang=de">German</a></li>
                        <li><a href="?lang=es">Spanish</a></li>
                        <li><a href="?lang=fa">Persian</a></li>
                        <li><a href="?lang=fr">French</a></li>
                        <li><a href="?lang=pt">Portugese</a></li>
                        <li><a href="?lang=ru">Russian</a></li>
                        <li><a href="?lang=zh">Chinese</a></li>
                        <%-- Only show fully translated languages --%>
                        <security:authorize access="isAuthenticated()">
                            <li><a href="?lang=sl">Slovene</a></li>
                        </security:authorize>
						<li><a target="_blank" href="https://www.transifex.com/projects/p/genesys/">Translate Genesys</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</header>

<!-- Mobile Header -->
<div class="mobile-header">
    <div class="header-top">
      <div class="container">
        <a href="#" class="mobile-menu-show"><img src="<c:url value="/html/images/icon_mobile_menu.png" />" alt="" /></a>
        <a href="#" class="mobile-menu-hide"><img src="<c:url value="/html/images/icon_mobile_menu_back.png" />" alt="" /></a>
        
        <ul class="nav navbar-nav navbar-right">
              <security:authorize access="isAnonymous()">
                    <li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><spring:message code="page.login" /></a>
                        <ul class="dropdown-menu pull-left">
                            <li>
                                <form role="form" method="post" action="/login-attempt">
                                    <div class="form-group">
                                        <label for="username"><spring:message code="login.username"/>:</label>
                  						<input type="email" class="form-control" id="username" name="j_username" placeholder="<spring:message code="login.username"/>" />
                                    </div>
                                    <div class="form-group">
                                        <label for="password"><spring:message code="login.password"/></label>
                 						<input type="password" class="form-control" id="password" name="j_password" placeholder="<spring:message code="login.password"/>" />
                                    </div>
                                    <div class="checkbox">
                                        <label>
                  							<input type="checkbox" name="_spring_security_remember_me"  id="_spring_security_remember_me"/>
                                            <spring:message code="login.remember-me"/>
                                        </label>
                                    </div>
                                    <button type="submit" class="btn btn-green"><spring:message
                                            code="login.login-button"/></button>
                                    <span class="or">-</span>
	                            	<a href="<c:url value="/google/login" />" class="btn btn-default google-signin"><spring:message code="login.with-google-plus"/></a>
					                <a href="<c:url value="/registration" />" class="btn btn-default"><spring:message code="login.register-now"/></a>

                                    <!-- CSRF protection -->
                                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                </form>
                            </li>
                        </ul>
                    </li>
                </security:authorize>
                <security:authorize access="isAuthenticated()">
                    <li class="dropdown">
         				<a href="<c:url value="/profile" />" class="dropdown-toggle" data-toggle="dropdown"><spring:message code="user.pulldown.heading" arguments="${user.user.name}" /></a>
                        <ul class="dropdown-menu pull-left">
                            <security:authorize access="hasRole('ADMINISTRATOR')">
							<li><a href="<c:url value="/admin/" />"><spring:message code="user.pulldown.administration" /></a></li>
							<li><a href="<c:url value="/profile/list" />"><spring:message code="user.pulldown.users" /></a></li>
							<li><a href="<c:url value="/team" />"><spring:message code="user.pulldown.teams" /></a></li>
                            </security:authorize>
                            <li><a href="<c:url value="/profile/${user.username}" />"><spring:message code="user.pulldown.profile"/></a></li>
                            <li><a id="logout" href="<c:url value="/logout" />"><spring:message code="user.pulldown.logout"/></a></li>
                        </ul>
                    </li>
                </security:authorize>

          <li class="dropdown" id="lang">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><c:out value="${pageContext.response.locale.displayLanguage}" /> <b class="caret"></b></a>
             <ul class="dropdown-menu" role="menu">
                <li><a href="?lang=en">English</a></li>
                <li><a href="?lang=ar">Arabic</a></li>
                <li><a href="?lang=de">German</a></li>
                <li><a href="?lang=es">Spanish</a></li>
                <li><a href="?lang=fa">Persian</a></li>
                <li><a href="?lang=fr">French</a></li>
                <li><a href="?lang=pt">Portugese</a></li>
                <li><a href="?lang=ru">Russian</a></li>
                <li><a href="?lang=zh">Chinese</a></li>
                <%-- Only show fully translated languages --%>
                <security:authorize access="isAuthenticated()">
                    <li><a href="?lang=sl">Slovene</a></li>
                </security:authorize>
				<li><a target="_blank" href="https://www.transifex.com/projects/p/genesys/">Translate Genesys</a></li>
           	</ul>
          </li>
        </ul>
      </div>
    </div> 
    
    <div class="logo-container">
      <div class="container">
        <a href="<c:url value="/" />"><img src="<c:url value="/html/images/logo_genesys.png" />" alt="Genesys - Gateway to Genetic Resources" title="Genesys - Gateway to Genetic Resources" /></a>
      </div>
    </div>
    
    <form role="search" id="search" action="<c:url value="/acn/search" />">
      <div class="container">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search Genesys ..." />
        </div>
        <button type="submit" class="btn"><img src="<c:url value="/html/images/icon_search.png" />" alt="" /></button>
      </div>
    </form>

</div> <!-- Mobile Header end -->
