<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="faoInstitutes.page.profile.title" arguments="${faoInstitute.fullName}" argumentSeparator="|" /></title>
</head>
<body typeof="schema:Organization">
	<h1>
		<img class="country-flag bigger" src="<c:url value="${cdnFlagsUrl}" />/${faoInstitute.country.code3.toUpperCase()}.png" />
		<span property="schema:Organization#name"><c:out value="${faoInstitute.fullName}" /></span>
		<small><c:out value="${faoInstitute.code}" /></small>
	</h1>

	<c:if test="${not faoInstitute.current}">
		<div class="alert alert-warning">
			<spring:message code="faoInstitute.institute-not-current" />
			<a href="<c:url value="/wiews/${faoInstitute.vCode}" />"><spring:message code="faoInstitute.view-current-institute" arguments="${faoInstitute.vCode}" /></a>
		</div>
	</c:if>

	<c:if test="${faoInstitute.current and countByInstitute eq 0}">
		<div class="alert alert-info">
			<spring:message code="faoInstitute.no-accessions-registered" />
		</div>
	</c:if>

	<div class="jumbotron">
		<spring:message code="faoInstitutes.stat.accessionCount" arguments="${countByInstitute}" />
		<c:if test="${countByInstitute gt 0}">
			<a href="<c:url value="/wiews/${faoInstitute.code}/data" />"><spring:message code="view.accessions" /></a>
			<a href="<c:url value="/wiews/${faoInstitute.code}/overview" />"><spring:message code="data-overview.short" /></a>
		</c:if>
		<br />
		<spring:message code="faoInstitutes.stat.datasetCount" arguments="${datasetCount}" />
		<c:if test="${datasetCount gt 0}">
			<a href="<c:url value="/wiews/${faoInstitute.code}/datasets" />"><spring:message code="view.datasets" /></a>
		</c:if>
	</div>
	
	<div class="">
		<security:authorize access="hasRole('ADMINISTRATOR') or hasPermission(#faoInstitute, 'ADMINISTRATION')">
			<a href="<c:url value="/acl/${faoInstitute.class.name}/${faoInstitute.id}/permissions"><c:param name="back">/wiews/${faoInstitute.code}</c:param></c:url>" class="close"> <spring:message code="edit-acl" /></a>
			<a href="<c:url value="/wiews/${faoInstitute.code}/edit" />" class="close">
				<spring:message code="edit" />
			</a>
		</security:authorize>

		<span property="schema:Organization#description">
		<%@include file="/WEB-INF/jsp/content/include/blurp-display.jsp" %>
		</span>
				
		<div class="row" style="">
		<div class="col-sm-4" property="schema:Organization#location">
			<spring:message code="faoInstitute.country" />:
			<%-- <img src="<c:url value="${cdnFlagsUrl}" />/${faoInstitute.country.code3.toUpperCase()}.png" /> --%>
			<a href="<c:url value="/geo/${faoInstitute.country.code3}" />">
				<span typeof="schema:Country"><span property="schema:Country#name">
					<c:out value="${faoInstitute.country.getName(pageContext.response.locale)}" />
				</span></span>
			</a>
		</div>
		<%-- <div class="col-sm-4">
			<spring:message code="faoInstitute.code" />:
			<c:out value="${faoInstitute.code}" />
		</div>
		<div class="col-sm-4">
			<spring:message code="faoInstitute.acronym" />:
			<c:out value="${faoInstitute.acronym}" />
		</div> --%>
		</div>
		
		<div class="row" style="">
		<%-- <div class="col-sm-4">
			<spring:message code="faoInstitute.email" />:
			<c:out value="${faoInstitute.email}" />
		</div> --%>
<%-- 		<p>
			<c:out value="${faoInstitute.type}" />
		</p>
 --%>
		<div class="col-sm-12">
			<spring:message code="faoInstitute.url" />:
			<a href="<c:out value="${faoInstitute.url}" />"><span property="schema:Organization#sameAs"><c:out value="${faoInstitute.url}" /></span></a>
		</div>
		</div>
		
		<c:if test="${organizations.size() gt 0}">
		<div class="row" style="">
		<div class="col-sm-12">
			<spring:message code="faoInstitute.member-of-organizations-and-networks" />
			<c:forEach items="${organizations}" var="organization">
				<a href="<c:url value="/org/${organization.slug}" />"><c:out value="${organization.title}" /></a>
			</c:forEach>
		</div>
		</div>
		</c:if>
	</div>

	<c:if test="${faoInstitute.latitude ne null}">
		<div class="row" style="">
		<div class="col-sm-12">
			<div id="map" class="gis-map"></div>
			<span property="schema:Organization#location"><span typeof="schema:Place"><span property="schema:Place#geo"><span typeof="schema:GeoCoordinates">
				<span property="schema:GeoCoordinates#latitude">${faoInstitute.latitude}</span>, <span property="schema:GeoCoordinates#longitude">${faoInstitute.longitude}</span>
			</span></span></span></span>
		</div>
		</div>
	</c:if>


	<%-- <h3>
		<spring:message code="faoInstitute.statistics" />
	</h3> --%>


	<div class="row" style="margin-top: 2em;">
		<%-- <c:if test="${statisticsCrop ne null}">
		<div class="col-sm-4">
			<h4><spring:message code="faoInstitute.stat-by-crop" arguments="${statisticsCrop.numberOfElements}" /></h4>
			<ul class="funny-list statistics">
				<c:forEach items="${statisticsCrop.content}" var="stat" varStatus="status">
					<li class="clearfix ${status.count % 2 == 0 ? 'even' : 'odd'}"><span class="stats-number"><fmt:formatNumber value="${stat[1]}" /></span> <a href="<c:url value="/wiews/${faoInstitute.code}/" />"><c:out value="${stat[0].getName(pageContext.response.locale)}" /></a></li>
				</c:forEach>
			</ul>
		</div>
		</c:if> --%>
		
		<div class="col-sm-6">
			<h4><spring:message code="faoInstitute.stat-by-genus" arguments="${statisticsGenus.numberOfElements}" /></h4>
			
			<div class="chart chart-pie">
				<div id="chartStatsByGenus" style="height:300px;"></div>
			</div>
			
			<ul class="funny-list statistics">
				<c:forEach items="${statisticsGenus.content}" var="stat" varStatus="status">
					<li class="clearfix ${status.count % 2 == 0 ? 'even' : 'odd'}"><span class="stats-number"><fmt:formatNumber value="${stat[1]}" /></span> <a href="<c:url value="/wiews/${faoInstitute.code.toLowerCase()}/t/${stat[0]}" />"><c:out value="${stat[0]}" /></a></li>
				</c:forEach>
			</ul>
		</div>

		<div class="col-sm-6">
			<h4><spring:message code="faoInstitute.stat-by-species" arguments="${statisticsTaxonomy.numberOfElements}" /></h4>

			<div class="chart chart-pie">
				<div id="chartStatsBySpecies" style="height:300px"></div>
			</div>

			<ul class="funny-list statistics">
				<c:forEach items="${statisticsTaxonomy.content}" var="stat" varStatus="status">
					<li class="clearfix ${status.count % 2 == 0 ? 'even' : 'odd'}"><span class="stats-number"><fmt:formatNumber value="${stat[1]}" /></span> <a href="<c:url value="/wiews/${faoInstitute.code}/t/${stat[0].genus}/${stat[0].species}" />"><c:out value="${stat[0].taxonName}" /></a></li>
				</c:forEach>
			</ul>
		</div>
				
	</div>


	<c:if test="${countByInstitute gt 0}">
		<form class="form-horizontal" method="post" action="/wiews/${faoInstitute.code}/dwca">
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		<div class="row" style="margin-top: 2em;">
			<div class="col-sm-4">
				<button class="btn btn-primary" type="submit"><spring:message code="metadata.download-dwca" /></button>
			</div>
		</div>
		</form>
	</c:if>

<content tag="javascript">
		<c:if test="${faoInstitute.latitude ne null}">
		<script type="text/javascript">
			jQuery(document).ready(function() {
				var map=GenesysMaps.map("${pageContext.response.locale.language}", $("#map"), {
					minZoom: 4,
					maxZoom: 6, /* WIEWS does not provide enough detail */
					center: new GenesysMaps.LatLng(${faoInstitute.latitude}, ${faoInstitute.longitude}), 
					markerTitle: "<spring:escapeBody javaScriptEscape="true">${faoInstitute.fullName}</spring:escapeBody>",
					scrollWheelZoom: false,
					touchZoom: false,
					dragging: false,
					doubleClickZoom: false,
					boxZoom: false
				});
			});
		</script>
		</c:if>
		<script type="text/javascript">
			<%@include file="/WEB-INF/jsp/wiews/ga.jsp"%>
			_pageDim = { institute: '${faoInstitute.code}' };
		</script>
    <script>
        jQuery(document).ready(function () {
            GenesysChart.chart("#chartStatsByGenus", "/wiews/${faoInstitute.code}/stat-genus", null, null, function(genus) { window.location=window.location.pathname + "/t/" + genus; });
            GenesysChart.chart("#chartStatsBySpecies", "/wiews/${faoInstitute.code}/stat-species", null, function(taxonomy) { return taxonomy.taxonName; }, function(taxonomy) { window.location=window.location.pathname + "/t/" + taxonomy.genus + "/" + taxonomy.species; });
        });
    </script>
</content>
</body>
</html>