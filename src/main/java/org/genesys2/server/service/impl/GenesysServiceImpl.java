/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.elastic.AccessionDetails;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.AccessionAlias;
import org.genesys2.server.model.genesys.AccessionAlias.AliasType;
import org.genesys2.server.model.genesys.AccessionBreeding;
import org.genesys2.server.model.genesys.AccessionCollect;
import org.genesys2.server.model.genesys.AccessionExchange;
import org.genesys2.server.model.genesys.AccessionGeo;
import org.genesys2.server.model.genesys.AccessionRemark;
import org.genesys2.server.model.genesys.AccessionTrait;
import org.genesys2.server.model.genesys.AllAccnames;
import org.genesys2.server.model.genesys.ExperimentAccessionTrait;
import org.genesys2.server.model.genesys.ExperimentTrait;
import org.genesys2.server.model.genesys.Metadata;
import org.genesys2.server.model.genesys.Method;
import org.genesys2.server.model.genesys.SvalbardData;
import org.genesys2.server.model.genesys.Taxonomy2;
import org.genesys2.server.model.genesys.TraitCode;
import org.genesys2.server.model.impl.AccessionIdentifier3;
import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.model.impl.Organization;
import org.genesys2.server.persistence.domain.AccessionAliasRepository;
import org.genesys2.server.persistence.domain.AccessionBreedingRepository;
import org.genesys2.server.persistence.domain.AccessionCollectRepository;
import org.genesys2.server.persistence.domain.AccessionExchangeRepository;
import org.genesys2.server.persistence.domain.AccessionGeoRepository;
import org.genesys2.server.persistence.domain.AccessionNameRepository;
import org.genesys2.server.persistence.domain.AccessionRemarkRepository;
import org.genesys2.server.persistence.domain.AccessionRepository;
import org.genesys2.server.persistence.domain.AccessionTraitRepository;
import org.genesys2.server.persistence.domain.CropTaxonomyRepository;
import org.genesys2.server.persistence.domain.FaoInstituteRepository;
import org.genesys2.server.persistence.domain.GenesysLowlevelRepository;
import org.genesys2.server.persistence.domain.MetadataMethodRepository;
import org.genesys2.server.persistence.domain.MetadataRepository;
import org.genesys2.server.persistence.domain.MethodRepository;
import org.genesys2.server.persistence.domain.OrganizationRepository;
import org.genesys2.server.persistence.domain.SvalbardRepository;
import org.genesys2.server.persistence.domain.TraitValueRepository;
import org.genesys2.server.security.AuthUserDetails;
import org.genesys2.server.service.AclService;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.DatasetService;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.HtmlSanitizer;
import org.genesys2.server.service.TaxonomyService;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.genesys2.spring.SecurityContextUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.acls.domain.BasePermission;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.bytecode.opencsv.CSVWriter;
import au.com.bytecode.opencsv.ResultSetHelper;
import au.com.bytecode.opencsv.ResultSetHelperService;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service
@Transactional(readOnly = true)
public class GenesysServiceImpl implements GenesysService, DatasetService {

	public static final Log LOG = LogFactory.getLog(GenesysServiceImpl.class);

	// Services
	@Autowired
	private TaxonomyService taxonomyService;
	@Autowired
	private AclService aclService;
	@Autowired
	private HtmlSanitizer htmlSanitizer;

	// Repositories
	@Autowired
	private AccessionRepository accessionRepository;
	@Autowired
	private AccessionBreedingRepository accessionBreedingRepository;
	@Autowired
	private AccessionCollectRepository accessionCollectRepository;
	@Autowired
	private AccessionNameRepository accessionNamesRepository;
	@Autowired
	private AccessionExchangeRepository accessionExchangeRepository;
	@Autowired
	private AccessionGeoRepository accessionGeoRepository;
	@Autowired
	private AccessionTraitRepository accessionTraitRepository;
	@Autowired
	private MetadataRepository metadataRepository;
	@Autowired
	private MetadataMethodRepository metadataMethodRepository;
	@Autowired
	private TraitValueRepository traitValueRepository;
	@Autowired
	private MethodRepository methodRepository;
	@Autowired
	private CropTaxonomyRepository cropTaxonomyRepository;
	@Autowired
	private OrganizationRepository organizationRepository;
	@Autowired
	private FaoInstituteRepository instituteRepository;
	@Autowired
	private GenesysLowlevelRepository genesysLowlevelRepository;
	@Autowired
	private SvalbardRepository svalbardRepository;
	@Autowired
	private AccessionAliasRepository accessionAliasRepository;
	@Autowired
	private AccessionRemarkRepository accessionRemarkRepository;

	private final ObjectMapper mapper = new ObjectMapper();

	@Autowired
	private CropService cropService;

	@Override
	public long countAll() {
		return accessionRepository.count();
	}

	@Override
	public long countByInstitute(FaoInstitute institute) {
		return accessionRepository.countByInstitute(institute);
	}

	@Override
	public long countByOrigin(String isoCode3) {
		return accessionRepository.countByOrigin(isoCode3);
	}

	@Override
	public long countByLocation(String isoCode3) {
		return accessionRepository.countByLocation(isoCode3);
	}

	@Override
	public Page<Accession> listAccessionsByOrigin(Country country, Pageable pageable) {
		return accessionRepository.findByOrigin(country.getCode3(), pageable);
	}

	@Override
	public Page<Accession> listAccessionsByInstitute(FaoInstitute faoInstitute, Pageable pageable) {
		return accessionRepository.findByInstitute(faoInstitute, pageable);
	}

	@Override
	public List<FaoInstitute> findHoldingInstitutes(Set<Long> accessionIds) {
		return accessionRepository.findDistinctInstitutesFor(accessionIds);
	}

	@Override
	public Set<Long> listAccessions(FaoInstitute institute, Set<Long> accessionIds) {
		return accessionRepository.findByInstituteAndIds(institute, accessionIds);
	}

	@Override
	public Page<Accession> listAccessions(Collection<Long> accessionIds, Pageable pageable) {
		if (accessionIds == null || accessionIds.size() == 0) {
			return null;
		}
		return accessionRepository.findById(accessionIds, pageable);
	}

	@Override
	public List<Accession> listAccessionsSGSV(List<? extends AccessionIdentifier3> accns) {
		final List<Accession> result = new ArrayList<Accession>(accns.size());
		for (final AccessionIdentifier3 aid3 : accns) {

			Accession accn = null;

			try {
				accn = accessionRepository.findOneSGSV(aid3.getHoldingInstitute(), aid3.getAccessionName(), aid3.getGenus());
			} catch (IncorrectResultSizeDataAccessException e) {
				LOG.warn("Duplicate entry for " + aid3);
			}

			if (accn != null) {
				result.add(accn);
			} else {

				try {
					Accession accnByAlias = accessionAliasRepository.findAccession(aid3.getHoldingInstitute(), aid3.getAccessionName(),
							AccessionAlias.AliasType.OTHERNUMB.getId());

					if (accnByAlias != null) {
						LOG.info("Found accession by alias " + accnByAlias);
						// Genus must match
						if (StringUtils.equalsIgnoreCase(aid3.getGenus(), accnByAlias.getTaxonomy().getGenus()))
							result.add(accnByAlias);
						else {
							LOG.info("... but genus doesn't match");
							result.add(null);
						}
					} else {
						result.add(null);

						if (LOG.isDebugEnabled()) {
							// Only log full miss
							LOG.debug("No accession " + aid3);
						}
					}
				} catch (IncorrectResultSizeDataAccessException e) {
					LOG.warn("Multple accessions with alias " + aid3);
					result.add(null);
				}
			}
		}
		return result;
	}

	@Override
	public Accession getAccession(AccessionIdentifier3 aid3) throws NonUniqueAccessionException {
		try {
			Accession accession = accessionRepository.findOne(aid3.getHoldingInstitute(), aid3.getAccessionName(), aid3.getGenus());
			if (accession != null)
				accession.getStoRage().size();
			return accession;
		} catch (IncorrectResultSizeDataAccessException e) {
			LOG.error("Duplicate accession name instCode=" + aid3.getHoldingInstitute() + " acceNumb=" + aid3.getAccessionName() + " genus=" + aid3.getGenus());
			throw new NonUniqueAccessionException(aid3.getHoldingInstitute(), aid3.getAccessionName(), aid3.getGenus());
		}
	}

	@Override
	public Accession getAccession(String instCode, String acceNumb) throws NonUniqueAccessionException {
		try {
			Accession accession = accessionRepository.findByInstituteCodeAndAccessionName(instCode, acceNumb);
			if (accession != null)
				accession.getStoRage().size();
			return accession;
		} catch (IncorrectResultSizeDataAccessException e) {
			LOG.error("Duplicate accession name instCode=" + instCode + " acceNumb=" + acceNumb);
			throw new NonUniqueAccessionException(instCode, acceNumb);
		}
	}

	@Override
	public Accession getAccession(String instCode, String acceNumb, String genus) throws NonUniqueAccessionException {
		if (genus == null) {
			throw new NullPointerException("Genus is required to load accession by instCode, acceNumb and genus");
		}
		try {
			Accession accession = accessionRepository.findOne(instCode, acceNumb, genus);
			if (accession != null)
				accession.getStoRage().size();
			return accession;
		} catch (IncorrectResultSizeDataAccessException e) {
			LOG.error("Duplicate accession name instCode=" + instCode + " acceNumb=" + acceNumb);
			throw new NonUniqueAccessionException(instCode, acceNumb);
		}
	}

	@Override
	public Accession getAccession(long accessionId) {
		Accession accession = accessionRepository.findOne(accessionId);
		if (accession != null) {
			accession.getStoRage().size();
			if (accession.getCountryOfOrigin() != null)
				accession.getCountryOfOrigin().getId();
		}
		return accession;
	}

	@Override
	public AccessionDetails getAccessionDetails(long accessionId) {
		Accession accession = getAccession(accessionId);

		AccessionDetails ad = AccessionDetails.from(accession);
		ad.networks(organizationRepository.getOrganizations(accession.getInstitute()));
		ad.aliases(listAccessionAliases(accession));
		ad.exch(listAccessionExchange(accession));
		ad.collect(listAccessionCollect(accession));
		ad.breeding(listAccessionBreeding(accession));
		ad.geo(listAccessionGeo(accession));
		ad.svalbard(getSvalbardData(accession));
		ad.remarks(listAccessionRemarks(accession));
		// ad.traits(listMethods(accession),
		// getAccessionTraitValues(accession));
		ad.crops(cropService.getCrops(accession.getTaxonomy()));
		return ad;
	}

	@Override
	public Set<AccessionDetails> getAccessionDetails(Collection<Accession> accessions) {
		Set<AccessionDetails> set = new HashSet<AccessionDetails>(accessions.size());
		for (Accession accn : accessions) {
			set.add(getAccessionDetails(accn.getId()));
		}
		return set;
	}

	@Override
	public List<Accession> listAccessions(FaoInstitute faoInstitute, String accessionName) {
		return accessionRepository.findByInstituteAndAccessionName(faoInstitute, accessionName);
	}

	@Override
	public AccessionBreeding listAccessionBreeding(Accession accession) {
		if (accession == null || accession.getId() == null) {
			return null;
		}
		return accessionBreedingRepository.findByAccession(accession);
	}

	@Override
	public AccessionGeo listAccessionGeo(Accession accession) {
		if (accession == null || accession.getId() == null) {
			return null;
		}
		return accessionGeoRepository.findByAccession(accession);
	}

	@Override
	public List<AccessionGeo> listAccessionsGeo(Set<Long> copy) {
		if (copy == null || copy.isEmpty()) {
			return null;
		}
		return accessionGeoRepository.findForAccessions(copy);
	}

	@Override
	public SvalbardData getSvalbardData(Accession accession) {
		if (accession == null) {
			return null;
		}
		return svalbardRepository.findOne(accession.getId());
	}

	@Override
	public AccessionCollect listAccessionCollect(Accession accession) {
		if (accession == null || accession.getId() == null) {
			return null;
		}
		return accessionCollectRepository.findByAccession(accession);
	}

	@Override
	public AccessionExchange listAccessionExchange(Accession accession) {
		if (accession == null || accession.getId() == null) {
			return null;
		}
		return accessionExchangeRepository.findByAccession(accession);
	}

	@Override
	public List<AccessionRemark> listAccessionRemarks(Accession accession) {
		if (accession == null || accession.getId() == null) {
			return null;
		}
		return accessionRemarkRepository.findByAccession(accession);
	}

	@Override
	public AllAccnames listAccessionNames(Accession accession) {
		return accessionNamesRepository.findByAccession(accession);
	}

	@Override
	public List<AccessionAlias> listAccessionAliases(Accession accession) {
		return accessionAliasRepository.findByAccession(accession);
	}

	@Override
	public List<Metadata> listMetadata(Accession accession) {
		final List<Long> x = accessionTraitRepository.listMetadataIds(accession);
		return x.size() == 0 ? null : metadataRepository.findByIds(x);
	}

	@Override
	public Page<Metadata> listMetadata(Pageable pageable) {
		return metadataRepository.findAll(pageable);
	}

	@Override
	public Page<Metadata> listDatasets(FaoInstitute faoInstitute, Pageable pageable) {
		return metadataRepository.findByInstituteCode(faoInstitute.getCode(), pageable);
	}

	@Override
	public long countDatasets(FaoInstitute faoInstitute) {
		return metadataRepository.countByInstituteCode(faoInstitute.getCode());
	}

	@Override
	public Metadata getMetadata(long metadataId) {
		return metadataRepository.findOne(metadataId);
	}

	@Override
	public List<Method> listMethods(Metadata metadata) {
		final List<Long> x = metadataMethodRepository.listMetadataMethods(metadata);
		return x.size() == 0 ? null : methodRepository.findByIds(x);
	}

	@Override
	public Page<Accession> listMetadataAccessions(long metadataId, Pageable pageable) {
		return accessionTraitRepository.listMetadataAccessions(metadataId, pageable);
	}

	@Override
	public Map<Long, List<ExperimentTrait>> getAccessionTraitValues(Accession accession) {
		LOG.debug("Getting trait values for accession: " + accession);
		final List<AccessionTrait> x = accessionTraitRepository.findByAccession(accession);
		return x.size() == 0 ? null : traitValueRepository.getValues(x);
	}

	@Override
	public Map<Long, Map<Long, List<Object>>> getMetadataTraitValues(Metadata metadata, List<Accession> accessions) {
		final List<Method> methods = methodRepository.findByIds(metadataMethodRepository.listMetadataMethods(metadata));
		return traitValueRepository.getValues(metadata, methods, accessions);
	}

	@Override
	public Page<Object[]> statisticsGenusByInstitute(FaoInstitute institute, Pageable pageable) {
		return accessionRepository.statisticsGenusInInstitute(institute, pageable);
	}

	@Override
	public Page<Object[]> statisticsSpeciesByInstitute(FaoInstitute institute, Pageable pageable) {
		final Page<Object[]> page = accessionRepository.statisticsSpeciesInInstitute(institute, pageable);
		for (final Object[] r : page.getContent()) {
			if (r[0] != null) {
				r[0] = taxonomyService.get((Long) r[0]);
			}
		}
		return page;
	}

	@Override
	public Page<Accession> listAccessionsByInstituteAndSpecies(FaoInstitute institute, long taxSpecies, Pageable pageable) {
		return accessionRepository.findByInstituteAndTaxSpecies(institute, taxSpecies, pageable);
	}

	@Override
	public Page<Accession> listAccessionsByTaxSpecies(long taxSpecies, Pageable pageable) {
		return accessionRepository.findByTaxSpecies(taxSpecies, pageable);
	}

	@Override
	public Page<Accession> listAccessionsByTaxGenus(long taxGenusId, Pageable pageable) {
		return accessionRepository.findByTaxGenus(taxGenusId, pageable);
	}

	@Override
	public List<Long> listAccessionsIds(Pageable pageable) {
		return accessionRepository.listAccessionsIds(pageable);
	}

	@Override
	public Page<Accession> listAccessionsByCrop(Crop crop, Pageable pageable) {
		final List<Taxonomy2> taxonomies = cropTaxonomyRepository.findTaxonomiesByCrop(crop);
		if (taxonomies == null || taxonomies.size() == 0) {
			return null;
		}
		return accessionRepository.findByTaxonomy(taxonomies, pageable);
	}

	@Override
	public Page<Accession> listAccessionsByOrganization(Organization organization, Pageable pageable) {
		final List<FaoInstitute> members = organizationRepository.findInstitutesByOrganization(organization);
		if (members == null || members.size() == 0) {
			return new PageImpl<Accession>(ListUtils.EMPTY_LIST);
		}
		return accessionRepository.findByInstitute(members, pageable);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@Transactional(readOnly = false)
	public void updateAccessionCountryRefs() {
		genesysLowlevelRepository.updateCountryRefs();
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@Transactional(readOnly = false)
	public void updateAccessionInstitueRefs() {
		genesysLowlevelRepository.updateFaoInstituteRefs();
	}

	@Override
	@Transactional(readOnly = false)
	public void updateAccessionCount(FaoInstitute institute) {
		instituteRepository.updateInstituteAccessionCount(institute);
	}

	@Override
	@Transactional(readOnly = false)
	public void setInSvalbard(List<Accession> matching) {
		if (matching.size() > 0) {
			accessionRepository.setInSvalbard(matching);
		}
	}

	/**
	 * @deprecated Should be removed
	 */
	@Deprecated
	@Override
	@Transactional(readOnly = false)
	public void addAccessions(List<Accession> accessions) {
		accessionRepository.save(accessions);
	}

	@Override
	@Transactional(readOnly = false)
	public List<Accession> saveAccessions(FaoInstitute institute, List<Accession> accessions) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Saving " + accessions.size() + " accessions");
		}
		accessionRepository.save(accessions);
		updateAccessionCount(institute);
		return accessions;
	}

	@Override
	@Transactional(readOnly = false)
	public List<Accession> saveAccession(Accession... accession) {
		List<Accession> list = new ArrayList<Accession>();
		for (final Accession a : accession) {
			if (LOG.isDebugEnabled())
				LOG.debug("Updating " + a);
			accessionRepository.save(a);
			list.add(a);
		}
		return list;
	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#institute, 'DELETE') or hasPermission(#institute, 'MANAGE')")
	public List<Accession> removeAccessions(FaoInstitute institute, List<Accession> toDelete) {
		if (toDelete.size() > 0) {
			final Set<Long> accessionIds = new HashSet<Long>();

			for (final Accession accn : toDelete) {
				if (institute.getId().equals(accn.getInstitute().getId()))
					accessionIds.add(accn.getId());
				else
					throw new RuntimeException("Accession " + accn.getAccessionName() + " does not belong to " + institute.getCode());
			}

			accessionAliasRepository.deleteForAccessions(accessionIds);
			accessionBreedingRepository.deleteForAccessions(accessionIds);

			accessionCollectRepository.deleteForAccessions(accessionIds);
			accessionExchangeRepository.deleteForAccessions(accessionIds);
			accessionGeoRepository.deleteForAccessions(accessionIds);
			accessionNamesRepository.deleteForAccessions(accessionIds);

			svalbardRepository.deleteForAccessions(accessionIds);
			accessionRepository.delete(toDelete);

			updateAccessionCount(institute);
		}

		return toDelete;
	}

	@Override
	// Worker threads don't carry this information
	// @PreAuthorize("hasRole('ADMINISTRATOR')")
	@Transactional(readOnly = false)
	public List<SvalbardData> saveSvalbards(List<SvalbardData> svalbards) {
		svalbardRepository.save(svalbards);
		return svalbards;
	}

	@Override
	@Transactional(readOnly = false)
	public List<AccessionCollect> saveCollecting(List<AccessionCollect> all) {
		accessionCollectRepository.save(all);
		return all;
	}

	@Override
	@Transactional(readOnly = false)
	public List<AccessionCollect> removeCollecting(List<AccessionCollect> all) {
		accessionCollectRepository.delete(all);
		return all;
	}

	@Override
	@Transactional(readOnly = false)
	public List<AccessionGeo> saveGeo(List<AccessionGeo> all) {
		accessionGeoRepository.save(all);
		return all;
	}

	@Override
	@Transactional(readOnly = false)
	public List<AccessionGeo> removeGeo(List<AccessionGeo> all) {
		accessionGeoRepository.delete(all);
		return all;
	}

	@Override
	@Transactional(readOnly = false)
	public List<AccessionBreeding> saveBreeding(List<AccessionBreeding> all) {
		accessionBreedingRepository.save(all);
		return all;
	}

	@Override
	@Transactional(readOnly = false)
	public List<AccessionBreeding> removeBreeding(List<AccessionBreeding> all) {
		accessionBreedingRepository.delete(all);
		return all;
	}

	@Override
	@Transactional(readOnly = false)
	public List<AccessionExchange> saveExchange(List<AccessionExchange> all) {
		accessionExchangeRepository.save(all);
		return all;
	}

	@Override
	@Transactional(readOnly = false)
	public List<AccessionExchange> removeExchange(List<AccessionExchange> all) {
		accessionExchangeRepository.delete(all);
		return all;
	}

	@Override
	@Transactional(readOnly = false)
	public List<AccessionRemark> saveRemarks(List<AccessionRemark> toSaveRemarks) {
		accessionRemarkRepository.save(toSaveRemarks);
		return toSaveRemarks;
	}

	@Override
	@Transactional(readOnly = false)
	public List<AccessionRemark> removeRemarks(List<AccessionRemark> toRemoveRemarks) {
		accessionRemarkRepository.delete(toRemoveRemarks);
		return toRemoveRemarks;
	}

	@Override
	public long countAvailableForDistribution(Set<Long> accessionIds) {
		if (accessionIds == null || accessionIds.size() == 0) {
			return 0;
		}
		return accessionRepository.countAvailableForDistribution(accessionIds);
	}

	@Override
	public Set<Long> filterAvailableForDistribution(Set<Long> accessionIds) {
		if (accessionIds == null || accessionIds.size() == 0) {
			return Collections.emptySet();
		}
		return accessionRepository.filterAvailableForDistribution(accessionIds);
	}

	/**
	 * Returns datasets to which current user has 'WRITE'
	 */
	@Override
	@PreAuthorize("isAuthenticated()")
	public List<Metadata> listMyMetadata() {
		final AuthUserDetails user = SecurityContextUtil.getAuthUser();
		final List<Long> oids = aclService.listIdentitiesForSid(Metadata.class, user, BasePermission.WRITE);
		LOG.info("Got " + oids.size() + " elements for " + user.getUsername());
		if (oids.size() == 0) {
			return null;
		}

		return metadataRepository.findByIds(oids);
	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#institute, 'WRITE') or hasPermission(#institute, 'CREATE')")
	public Metadata addDataset(FaoInstitute institute, String title, String description) {
		final Metadata metadata = new Metadata();
		metadata.setInstituteCode(institute.getCode());
		metadata.setTitle(title);
		metadata.setDescription(htmlSanitizer.sanitize(description));
		return metadataRepository.save(metadata);
	}

	@Override
	public Metadata getDataset(long id) {
		return metadataRepository.findOne(id);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#metadata, 'WRITE')")
	public void touch(@Param("metadata") Metadata metadata) {

	}

	/**
	 * @param metadata
	 * @param accession
	 * @param methodValues
	 */
	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#metadata, 'WRITE')")
	@Transactional(readOnly = false)
	public void upsertAccessionData(Metadata metadata, Accession accession, Map<Long, List<Object>> methodValues) {
		// // Load all existing records for metadata+accession
		// List<AccessionTrait> existingEntries =
		// accessionTraitRepository.findByMetadataIdAndAccession(metadata.getId(),
		// accession);
		//
		// // Remove entries missing from map
		// Collection<AccessionTrait> forRemoval = new
		// ArrayList<AccessionTrait>(methodValues.size());
		// Collection<AccessionTrait> forAddition = new
		// ArrayList<AccessionTrait>(methodValues.size());
		//
		// for (AccessionTrait accessionTrait : existingEntries) {
		// LOG.debug("Existing trait " + accessionTrait);
		// if (methodValues.containsKey(accessionTrait.getMethodId())) {
		// // keep it
		// } else {
		// LOG.info("New data does not have methodId=" +
		// accessionTrait.getMethodId());
		// forRemoval.add(accessionTrait);
		// }
		// }
		// // Add new entries from map
		// for (final long methodId : methodValues.keySet()) {
		// if (null == CollectionUtils.find(existingEntries, new Predicate() {
		// @Override
		// public boolean evaluate(Object object) {
		// return ((AccessionTrait) object).getMethodId() == methodId;
		// }
		// })) {
		// // Create new AccessionTrait
		// AccessionTrait at = new AccessionTrait();
		// at.setAccession(accession);
		// at.setMetadataId(metadata.getId());
		// at.setMethodId(methodId);
		// forAddition.add(at);
		// }
		// }
		// accessionTraitRepository.save(forAddition);
		// accessionTraitRepository.delete(forRemoval);

		LOG.info("Upserting accession data " + accession);
		final Set<Long> methodsWithValues = new HashSet<Long>();

		// stick them to database
		for (final long methodId : methodValues.keySet()) {
			final Method method = methodRepository.findOne(methodId);
			// traitValueRepository.delete(metadata, accession, method);
			// traitValueRepository.insert(metadata, accession, method,
			// methodValues.get(methodId));
			LOG.info("Upserting accession data for method " + method.getId() + " accn=" + accession);
			final List<Object> withValues = traitValueRepository.upsert(metadata, accession, method, methodValues.get(methodId));
			if (withValues.size() > 0) {
				methodsWithValues.add(method.getId());
			}
		}
	}

	@Override
	public int countAccessions(AppliedFilters filters) {
		return genesysLowlevelRepository.countAccessions(filters);
	}

	@Override
	// TODO FIXME Need proper term URLs
	public void writeAccessions(AppliedFilters filters, OutputStream outputStream) throws IOException {
		// UTF8 is used for encoding entry names
		final ZipOutputStream zos = new ZipOutputStream(outputStream);
		zos.setComment("Genesys Accessions filter=" + filters);

		// Filter information
		final ZipEntry readmeEntry = new ZipEntry("README.txt");
		readmeEntry.setComment("Extra iformation");
		readmeEntry.setTime(System.currentTimeMillis());
		zos.putNextEntry(readmeEntry);
		writeREADME(filters, zos);
		zos.closeEntry();

		// Accessions
		final ZipEntry coreEntry = new ZipEntry("core.csv");
		coreEntry.setComment("Accession information");
		coreEntry.setTime(System.currentTimeMillis());
		zos.putNextEntry(coreEntry);
		writeAccessionsCore(filters, zos);
		zos.closeEntry();

		// AccessionGeo
		ZipEntry entry = new ZipEntry("names.csv");
		entry.setComment("Accession Names");
		entry.setTime(System.currentTimeMillis());
		zos.putNextEntry(entry);
		writeAccessionsNames(filters, zos);
		zos.closeEntry();

		// AccessionGeo
		entry = new ZipEntry("geo.csv");
		entry.setComment("GIS information");
		entry.setTime(System.currentTimeMillis());
		zos.putNextEntry(entry);
		writeAccessionsGeo(filters, zos);
		zos.closeEntry();

		// AccessionCollect
		entry = new ZipEntry("coll.csv");
		entry.setComment("Collecting information");
		entry.setTime(System.currentTimeMillis());
		zos.putNextEntry(entry);
		writeAccessionsColl(filters, zos);
		zos.closeEntry();

		final ZipEntry metaEntry = new ZipEntry("meta.xml");
		metaEntry.setComment("Darwin Core Archive metadata");
		metaEntry.setTime(System.currentTimeMillis());
		zos.putNextEntry(metaEntry);
		final BufferedWriter osw = new BufferedWriter(new OutputStreamWriter(zos));
		osw.write("<?xml version='1.0' encoding='utf-8'?>\n");
		osw.write("<archive xmlns=\"http://rs.tdwg.org/dwc/text/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://rs.tdwg.org/dwc/text/ http://rs.tdwg.org/dwc/text/tdwg_dwc_text.xsd\">\n");
		osw.write("<core encoding=\"UTF-8\" fieldsTerminatedBy=\",\" linesTerminatedBy=\"\\n\" fieldsEnclosedBy=\"&quot;\" ignoreHeaderLines=\"0\">\n");
		osw.write("\t<files><location>core.csv</location></files>\n");
		osw.write("\t<id index=\"0\" />\n");
		osw.write("\t<field index=\"1\" term=\"UUID\"/>\n");
		osw.write("\t<field index=\"2\" term=\"http://purl.org/germplasm/germplasmType#wiewsInstituteID\"/>\n");
		osw.write("\t<field index=\"3\" term=\"http://purl.org/germplasm/germplasmTerm#germplasmIdentifier\"/>\n");
		osw.write("\t<field index=\"4\" term=\"http://rs.tdwg.org/dwc/terms/genus\"/>\n");
		osw.write("\t<field index=\"5\" term=\"http://rs.tdwg.org/dwc/terms/species\"/>\n");
		osw.write("\t<field index=\"6\" term=\"orgCty\"/>\n");
		osw.write("\t<field index=\"7\" term=\"acqSrc\"/>\n");
		osw.write("\t<field index=\"8\" term=\"acqDate\"/>\n");
		osw.write("\t<field index=\"9\" term=\"mlsStat\"/>\n");
		osw.write("\t<field index=\"10\" term=\"available\"/>\n");
		osw.write("\t<field index=\"11\" term=\"storage\"/>\n");
		osw.write("\t<field index=\"12\" term=\"sampStat\"/>\n");
		osw.write("\t<field index=\"13\" term=\"dublInst\"/>\n");
		osw.write("\t<field index=\"14\" term=\"createdBy\"/>\n");
		osw.write("\t<field index=\"15\" term=\"createdDate\"/>\n");
		osw.write("\t<field index=\"16\" term=\"lastModifiedBy\"/>\n");
		osw.write("\t<field index=\"17\" term=\"lastModifiedDate\"/>\n");
		osw.write("</core>\n");

		osw.write("<extension encoding=\"UTF-8\" fieldsTerminatedBy=\",\" linesTerminatedBy=\"\\n\" fieldsEnclosedBy=\"&quot;\" ignoreHeaderLines=\"0\">\n");
		osw.write("\t<files><location>");
		osw.write("names.csv");
		osw.write("</location></files>\n");
		osw.write("\t<coreid index=\"0\" />\n");
		osw.write("\t<field index=\"1\" term=\"instCode\"/>\n");
		osw.write("\t<field index=\"2\" term=\"name\"/>\n");
		osw.write("\t<field index=\"3\" term=\"aliasType\"/>\n");
		osw.write("\t<field index=\"4\" term=\"lang\"/>\n");
		osw.write("\t<field index=\"5\" term=\"version\"/>\n");
		osw.write("</extension>\n");

		osw.write("<extension encoding=\"UTF-8\" fieldsTerminatedBy=\",\" linesTerminatedBy=\"\\n\" fieldsEnclosedBy=\"&quot;\" ignoreHeaderLines=\"0\">\n");
		osw.write("\t<files><location>");
		osw.write("geo.csv");
		osw.write("</location></files>\n");
		osw.write("\t<coreid index=\"0\" />\n");
		osw.write("\t<field index=\"1\" term=\"latitude\"/>\n");
		osw.write("\t<field index=\"2\" term=\"longitude\"/>\n");
		osw.write("\t<field index=\"3\" term=\"elevation\"/>\n");
		osw.write("\t<field index=\"4\" term=\"datum\"/>\n");
		osw.write("\t<field index=\"5\" term=\"uncertainty\"/>\n");
		osw.write("\t<field index=\"6\" term=\"method\"/>\n");
		osw.write("\t<field index=\"7\" term=\"version\"/>\n");
		osw.write("</extension>\n");

		osw.write("<extension encoding=\"UTF-8\" fieldsTerminatedBy=\",\" linesTerminatedBy=\"\\n\" fieldsEnclosedBy=\"&quot;\" ignoreHeaderLines=\"0\">\n");
		osw.write("\t<files><location>");
		osw.write("coll.csv");
		osw.write("</location></files>\n");
		osw.write("\t<coreid index=\"0\" />\n");
		osw.write("\t<field index=\"1\" term=\"collMissId\"/>\n");
		osw.write("\t<field index=\"2\" term=\"collNumb\"/>\n");
		osw.write("\t<field index=\"3\" term=\"collDate\"/>\n");
		osw.write("\t<field index=\"4\" term=\"collSrc\"/>\n");
		osw.write("\t<field index=\"5\" term=\"collSite\"/>\n");
		osw.write("\t<field index=\"6\" term=\"collCode\"/>\n");
		osw.write("\t<field index=\"7\" term=\"collName\"/>\n");
		osw.write("\t<field index=\"8\" term=\"collInstAddress\"/>\n");
		osw.write("\t<field index=\"9\" term=\"version\"/>\n");
		osw.write("</extension>\n");

		osw.write("</archive>\n");
		osw.flush();
		zos.closeEntry();

		zos.finish();
	}

	private void writeREADME(AppliedFilters filters, ZipOutputStream zos) throws IOException {
		final BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(zos));
		bw.write("Date:\t");
		bw.write(new Date().toString());
		bw.write("\n");

		bw.write("Filter:\t");
		bw.write(filters.toString());
		bw.write("\n");

		bw.write("URL:\thttps://www.genesys-pgr.org/explore?filter=");
		bw.write(filters.toString());
		bw.write("\n");

		bw.write("Attribution:\t");
		bw.write("https://www.genesys-pgr.org/content/terms");
		bw.write("\n");
		bw.flush();
	}

	private void writeAccessionsCore(final AppliedFilters filters, ZipOutputStream zos) throws IOException {
		@SuppressWarnings("resource")
		final CSVWriter csv = new CSVWriter(new BufferedWriter(new OutputStreamWriter(zos)), ',', '"', '\\', "\n");
		csv.writeNext(new String[] { "genesysId", "uuid", "instCode", "acceNumb", "genus", "species", "fullTaxa", "orgCty", "acqSrc", "acqDate", "mlsStat",
				"available", "storage", "sampStat", "duplSite", "createdBy", "createdDate", "lastModifiedBy", "lastModifiedDate" });

		final ResultSetHelper csvResultsetHelper = new ResultSetHelperService();

		// Write accession information
		genesysLowlevelRepository.listAccessions(filters, new RowCallbackHandler() {
			int i = 0;

			@Override
			public void processRow(ResultSet rs) throws SQLException {
				try {
					csv.writeNext(csvResultsetHelper.getColumnValues(rs));
				} catch (final IOException e) {
					LOG.error("Reading resultset for DWCA", e);
				}
				if (i++ % 5000 == 0) {
					LOG.info("Writing accessions DWCA " + filters.toString() + " " + i);
				}
			}
		});

		csv.flush();
	}

	private void writeAccessionsGeo(final AppliedFilters filters, ZipOutputStream zos) throws IOException {
		@SuppressWarnings("resource")
		final CSVWriter csv = new CSVWriter(new BufferedWriter(new OutputStreamWriter(zos)), ',', '"', '\\', "\n");
		csv.writeNext(new String[] { "genesysId", "latitude", "longitude", "elevation", "datum", "uncertainty", "method", "version" });

		final ResultSetHelper csvResultsetHelper = new ResultSetHelperService();

		// Write accession information
		genesysLowlevelRepository.listAccessionsGeo(filters, new RowCallbackHandler() {
			int i = 0;

			@Override
			public void processRow(ResultSet rs) throws SQLException {
				try {
					csv.writeNext(csvResultsetHelper.getColumnValues(rs));
				} catch (final IOException e) {
					LOG.error("Reading resultset for DWCA", e);
				}
				if (i++ % 5000 == 0) {
					LOG.info("Writing geo DWCA " + filters.toString() + " " + i);
				}
			}
		});

		csv.flush();
	}

	private void writeAccessionsNames(final AppliedFilters filters, ZipOutputStream zos) throws IOException {
		@SuppressWarnings("resource")
		final CSVWriter csv = new CSVWriter(new BufferedWriter(new OutputStreamWriter(zos)), ',', '"', '\\', "\n");
		csv.writeNext(new String[] { "genesysId", "instCode", "name", "aliasType", "lang", "version" });

		final ResultSetHelper csvResultsetHelper = new ResultSetHelperService();

		// Write accession information
		genesysLowlevelRepository.listAccessionsAlias(filters, new RowCallbackHandler() {
			int i = 0;

			@Override
			public void processRow(ResultSet rs) throws SQLException {
				try {
					final String[] r = csvResultsetHelper.getColumnValues(rs);
					if (r[3] != null) {
						try {
							final AliasType aliasType = AliasType.getType(Integer.parseInt(r[3]));
							r[3] = aliasType.name();
						} catch (final NumberFormatException e) {
							LOG.warn(e.getMessage());
						}
					}
					csv.writeNext(r);
				} catch (final IOException e) {
					LOG.error("Reading resultset for DWCA aliases ", e);
				}
				if (i++ % 5000 == 0) {
					LOG.info("Writing alias DWCA " + filters.toString() + " " + i);
				}
			}
		});

		csv.flush();
	}

	private void writeAccessionsColl(final AppliedFilters filters, ZipOutputStream zos) throws IOException {
		@SuppressWarnings("resource")
		final CSVWriter csv = new CSVWriter(new BufferedWriter(new OutputStreamWriter(zos)), ',', '"', '\\', "\n");
		csv.writeNext(new String[] { "genesysId", "collMissId", "collNumb", "collDate", "collSrc", "collSite", "collCode", "collName", "collInstAddress",
				"version" });

		final ResultSetHelper csvResultsetHelper = new ResultSetHelperService();

		// Write accession information
		genesysLowlevelRepository.listAccessionsColl(filters, new RowCallbackHandler() {
			int i = 0;

			@Override
			public void processRow(ResultSet rs) throws SQLException {
				try {
					csv.writeNext(csvResultsetHelper.getColumnValues(rs));
				} catch (final IOException e) {
					LOG.error("Reading resultset for DWCA", e);
				}
				if (i++ % 5000 == 0) {
					LOG.info("Writing collecting DWCA " + filters.toString() + " " + i);
				}
			}
		});

		csv.flush();
	}

	@Override
	public void writeDataset(Metadata metadata, OutputStream outputStream) throws IOException {
		// Methods: A CSV is generated for each method
		final List<Method> metadataMethods = listMethods(metadata);

		CSVWriter csv = null;

		// UTF8 is used for encoding entry names
		final ZipOutputStream zos = new ZipOutputStream(outputStream);
		zos.setComment("Genesys Dataset " + metadata.getUuid());

		final ZipEntry coreEntry = new ZipEntry("core.csv");
		coreEntry.setComment("Accession information");
		coreEntry.setTime(System.currentTimeMillis());
		zos.putNextEntry(coreEntry);

		// Write accession information
		writeDatasetCore(metadata, zos);

		zos.closeEntry();

		for (final Method method : metadataMethods) {
			final ZipEntry methodEntry = new ZipEntry(String.format("%1$s.csv", method.getFieldName().toLowerCase()));
			methodEntry.setComment(method.getMethod());
			methodEntry.setTime(System.currentTimeMillis());
			zos.putNextEntry(methodEntry);

			final List<ExperimentAccessionTrait> vals = traitValueRepository.getValues(metadata, method);
			csv = new CSVWriter(new BufferedWriter(new OutputStreamWriter(zos)), ',', '"', '\\', "\n");
			csv.writeNext(new String[] { "genesysId", "datasetId", "value" });

			Map<String, String> codeMap = null;
			if (method.isCoded()) {
				codeMap = TraitCode.parseCodeMap(method.getOptions());
			}
			for (final ExperimentAccessionTrait et : vals) {
				if (et.getValue() == null) {
					continue;
				}
				if (codeMap == null) {
					csv.writeNext(new String[] { String.valueOf(et.getAccessionId()), String.valueOf(et.getExperimentId()), et.getValue().toString() });
				} else {
					csv.writeNext(new String[] { String.valueOf(et.getAccessionId()), String.valueOf(et.getExperimentId()), codeMap.get(et.getValue()) });
				}
			}

			csv.flush();
			zos.closeEntry();
		}

		final ZipEntry metaEntry = new ZipEntry("meta.xml");
		metaEntry.setComment("Darwin Core Archive metadata");
		metaEntry.setTime(System.currentTimeMillis());
		zos.putNextEntry(metaEntry);
		final BufferedWriter osw = new BufferedWriter(new OutputStreamWriter(zos));
		osw.write("<?xml version='1.0' encoding='utf-8'?>\n");
		osw.write("<archive xmlns=\"http://rs.tdwg.org/dwc/text/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://rs.tdwg.org/dwc/text/ http://rs.tdwg.org/dwc/text/tdwg_dwc_text.xsd\">\n");
		osw.write("<core encoding=\"UTF-8\" fieldsTerminatedBy=\",\" linesTerminatedBy=\"\\n\" fieldsEnclosedBy=\"&quot;\" ignoreHeaderLines=\"0\">\n");
		osw.write("\t<files><location>core.csv</location></files>\n");
		osw.write("\t<id index=\"0\" />\n");
		osw.write("\t<field index=\"1\" term=\"http://purl.org/germplasm/germplasmType#wiewsInstituteID\"/>\n");
		osw.write("\t<field index=\"2\" term=\"http://purl.org/germplasm/germplasmTerm#germplasmIdentifier\"/>\n");
		osw.write("\t<field index=\"3\" term=\"http://rs.tdwg.org/dwc/terms/genus\"/>\n");
		osw.write("</core>\n");

		for (int i = 0; i < metadataMethods.size(); i++) {
			final Method method = metadataMethods.get(i);

			osw.write("<extension encoding=\"UTF-8\" fieldsTerminatedBy=\",\" linesTerminatedBy=\"\\n\" fieldsEnclosedBy=\"&quot;\" ignoreHeaderLines=\"0\">\n");
			osw.write("\t<files><location>");
			osw.write(method.getFieldName().toLowerCase());
			osw.write(".csv</location></files>\n");
			osw.write("\t<coreid index=\"0\" />\n");
			osw.write("\t<field index=\"1\" term=\"http://rs.tdwg.org/dwc/terms/datasetID\"/>\n");
			osw.write("\t<field index=\"2\" term=\"http://www.genesys-pgr.org/descriptors//");
			osw.write(String.valueOf(method.getId()));
			osw.write("\"/>\n");
			osw.write("</extension>\n");
		}
		osw.write("</archive>\n");
		osw.flush();
		zos.closeEntry();

		zos.finish();
	}

	private void writeDatasetCore(final Metadata metadata, ZipOutputStream zos) throws IOException {
		@SuppressWarnings("resource")
		final CSVWriter csv = new CSVWriter(new BufferedWriter(new OutputStreamWriter(zos), 4096), ',', '"', '\\', "\n");
		csv.writeNext(new String[] { "genesysId", "instCode", "acceNumb", "genus" });

		final ResultSetHelper csvResultsetHelper = new ResultSetHelperService();

		// Write accession information
		genesysLowlevelRepository.listMetadataAccessions(metadata.getId(), new RowCallbackHandler() {
			int i = 0;

			@Override
			public void processRow(ResultSet rs) throws SQLException {
				try {
					csv.writeNext(csvResultsetHelper.getColumnValues(rs));
				} catch (final IOException e) {
					LOG.error("Reading resultset for DWCA", e);
				}
				if (i++ % 5000 == 0) {
					try {
						csv.flush();
					} catch (final IOException e) {
						throw new SQLException("Could not flush stream", e);
					}
					LOG.info("Writing metadata core DWCA " + metadata.getId() + " " + i);
				}
			}
		});

		csv.flush();
	}

	@Override
	@Transactional
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public void refreshMetadataMethods() {
		genesysLowlevelRepository.refreshMetadataMethods();
	}

	@Override
	@Transactional
	public List<AccessionAlias> saveAliases(List<AccessionAlias> aliases) {
		if (aliases.size() > 0) {
			accessionAliasRepository.save(aliases);
		}
		return aliases;
	}

	@Override
	@Transactional
	public List<AccessionAlias> removeAliases(List<AccessionAlias> aliases) {
		if (aliases.size() > 0) {
			accessionAliasRepository.delete(aliases);
		}
		return aliases;
	}

	@Override
	@Transactional
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public void upsertAliases(long accessionId, String acceNames, String otherIds) {
		final Accession accession = getAccession(accessionId);
		if (accession == null) {
			LOG.error("No such accession " + accessionId);
			return;
		}

		// LOG.info("Updating acceNames=" + acceNames);
		final List<AccessionAlias> aliases = new ArrayList<AccessionAlias>();
		if (StringUtils.isNotBlank(acceNames)) {
			final String[] acceName = acceNames.split(";");
			for (final String oi : acceName) {
				if (StringUtils.isBlank(oi)) {
					continue;
				}
				final AccessionAlias alias = new AccessionAlias();
				alias.setAccession(accession);
				alias.setName(oi.trim());
				alias.setAliasType(AliasType.ACCENAME);
				aliases.add(alias);
			}
		}
		ensureAliases(accession, aliases, AliasType.ACCENAME);

		// LOG.info("Updating otherIds=" + otherIds);
		aliases.clear();
		if (StringUtils.isNotBlank(otherIds)) {
			final String[] otherId = otherIds.split(";");
			for (final String oi : otherId) {
				if (StringUtils.isBlank(oi)) {
					continue;
				}
				final String[] oin = oi.split(":", 2);
				final AccessionAlias alias = new AccessionAlias();
				alias.setAccession(accession);
				if (oin.length == 1) {
					if (StringUtils.isBlank(oin[0])) {
						continue;
					}
					alias.setName(oin[0].trim());
				} else {
					if (StringUtils.isBlank(oin[1])) {
						continue;
					}
					alias.setUsedBy(StringUtils.defaultIfBlank(oin[0].trim(), null));

					if (alias.getUsedBy() != null && alias.getUsedBy().length() > 7) {
						LOG.warn("Invalid instCode: " + alias.getUsedBy() + " in=" + oi);
						continue;
					}

					alias.setName(oin[1].trim());
				}
				alias.setAliasType(AliasType.OTHERNUMB);
				aliases.add(alias);
			}
		}
		ensureAliases(accession, aliases, AliasType.OTHERNUMB);
	}

	private void ensureAliases(Accession accession, List<AccessionAlias> aliases, AliasType aliasType) {
		final List<AccessionAlias> existingAliases = accessionAliasRepository.findByAccessionAndAliasType(accession, aliasType.getId());

		// Find aliases to remove
		for (final AccessionAlias aa : existingAliases) {
			if (null == CollectionUtils.find(aliases, new Predicate<AccessionAlias>() {
				@Override
				public boolean evaluate(AccessionAlias alias) {
					return StringUtils.equals(alias.getName(), aa.getName());
				}
			})) {
				accessionAliasRepository.delete(aa);
			}
		}
		// Add or update
		for (final AccessionAlias aa : aliases) {
			final AccessionAlias accessionAlias = CollectionUtils.find(existingAliases, new Predicate<AccessionAlias>() {
				@Override
				public boolean evaluate(AccessionAlias alias) {
					return StringUtils.equals(alias.getName(), aa.getName());
				}
			});

			if (accessionAlias == null) {
				accessionAliasRepository.save(aa);
			}
		}
	}

	@Override
	@Transactional
	public Set<Long> removeAliases(Set<Long> toRemove) {
		for (final Long id : toRemove) {
			this.accessionAliasRepository.delete(id);
		}
		return toRemove;
	}

	@Override
	public List<Long> listAccessionsIds(Taxonomy2 taxonomy) {
		return accessionRepository.listAccessionsIds(taxonomy);
	}

}
