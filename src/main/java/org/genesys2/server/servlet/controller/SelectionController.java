/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.AccessionGeo;
import org.genesys2.server.service.FilterConstants;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.impl.FilterHandler;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilter;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@Scope("request")
@RequestMapping("/sel")
public class SelectionController extends BaseController {

	private final ObjectMapper mapper = new ObjectMapper();

	@Autowired
	private SelectionBean selectionBean;

	@Autowired
	private GenesysService genesysService;

	@RequestMapping("/")
	public String view(ModelMap model, @RequestParam(value = "page", required = false, defaultValue = "1") int page) {

		model.addAttribute("pagedData", genesysService.listAccessions(selectionBean.copy(), new PageRequest(page - 1, 50, new Sort("accessionName"))));

		model.addAttribute("selection", selectionBean);

		return "/selection/index";
	}

	@RequestMapping(value = "/map")
	public String map(ModelMap model) {
		model.addAttribute("selection", selectionBean);
		return "/selection/map";
	}

	@RequestMapping(value = "/order")
	public String order(ModelMap model) {
		return "redirect:/request";
	}

	@RequestMapping("/add/{id}")
	public String add(ModelMap model, @PathVariable("id") long accessionId) {
		selectionBean.add(accessionId);
		return "redirect:/sel/#a" + accessionId;
	}

	@RequestMapping(method = RequestMethod.POST, value = "add-many")
	public String add(ModelMap model, @RequestParam(required = true, value = "accessionIds") String accessionIds) {
		final String[] splits = accessionIds.split("\\s");
		for (final String s : splits) {
			try {
				final long accessionId = Long.parseLong(s);
				final Accession accession = genesysService.getAccession(accessionId);
				if (accession != null) {
					selectionBean.add(accessionId);
				}
			} catch (final NumberFormatException e) {

			}
		}
		return "redirect:/sel/";
	}

	@RequestMapping("/remove/{id}")
	public String remove(ModelMap model, @PathVariable("id") long accessionId) {
		selectionBean.remove(accessionId);
		return "redirect:/sel/";
	}

	@RequestMapping("/clear")
	public String remove(ModelMap model) {
		selectionBean.clear();
		return "redirect:/sel/";
	}

	/**
	 * Download DwCA of selected accessions
	 * 
	 * @param model
	 * @param cropName
	 * @param jsonFilter
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/dwca", method = RequestMethod.POST)
	public void dwca(ModelMap model, HttpServletResponse response) throws IOException {
		// Create JSON filter
		final AppliedFilters appliedFilters = new AppliedFilters();
		AppliedFilter arr = new FilterHandler.AppliedFilter().setFilterName(FilterConstants.ID);
		for (final long id : selectionBean.copy()) {
			arr.addFilterValue(new FilterHandler.LiteralValueFilter(id));
		}
		appliedFilters.add(arr);

		final int countFiltered = selectionBean.size();
		_logger.info("Attempting to download DwCA for " + countFiltered + " accessions");
		if (countFiltered > 100000) {
			throw new RuntimeException("Refusing to export more than 100,000 entries");
		}

		response.setContentType("application/zip");
		response.addHeader("Content-Disposition", String.format("attachment; filename=\"genesys-accessions-selected.zip\""));

		// Write Darwin Core Archive to the stream.
		final OutputStream outputStream = response.getOutputStream();

		genesysService.writeAccessions(appliedFilters, outputStream);
		response.flushBuffer();
	}

	// TODO REMOVE
	@RequestMapping(value = "/json/count", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public Long selectionCount() {
		return (long) selectionBean.size();
	}

	@RequestMapping(value = "/json/selection", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public Set<Long> selection() {
		return selectionBean.copy();
	}

	@RequestMapping(value = "/json/geo", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public List<AccessionGeoJson> selectionGeo() {
		final List<AccessionGeoJson> geo = new ArrayList<AccessionGeoJson>();
		if (selectionBean.size() > 0) {
			for (final AccessionGeo acnGeo : genesysService.listAccessionsGeo(selectionBean.copy())) {
				final AccessionGeoJson g = new AccessionGeoJson();
				g.id = acnGeo.getAccession().getId();
				g.lat = acnGeo.getLatitude();
				g.lng = acnGeo.getLongitude();
				g.accessionName = acnGeo.getAccession().getAccessionName();
				g.instCode = acnGeo.getAccession().getInstituteCode();
				geo.add(g);
			}
		}
		return geo;
	}

	@RequestMapping(value = "/json", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public JsonResponse jsonOp(@RequestBody JsonAction action) {
		_logger.info("Selection action " + action.action + ": " + action.id);
		final JsonResponse resp = new JsonResponse();
		if ("add".equals(action.action)) {
			selectionBean.add(action.id);
			resp.included = true;
		} else {
			selectionBean.remove(action.id);
			resp.included = false;
		}
		resp.count = selectionBean.size();
		return resp;
	}

	public static class JsonAction {
		public long id;
		public String action;
	}

	public static class JsonResponse {
		public int count;
		public boolean included;
	}

	public static class AccessionGeoJson {
		public long id;
		public Double lat;
		public Double lng;
		public String accessionName;
		public String instCode;
	}
}
