package org.genesys2.server.service.impl;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class ListToStringTest {

	@Test
	public void testLongListToString() {
		List<Long> list = null;
		assertTrue(null == BatchRESTServiceImpl.listToString(list));
		list = new ArrayList<Long>();
		assertTrue(null == BatchRESTServiceImpl.listToString(list));
		list.add(1l);
		assertTrue("1".equals(BatchRESTServiceImpl.listToString(list)));
		list.add(2l);
		list.add(3l);
		assertTrue("1;2;3".equals(BatchRESTServiceImpl.listToString(list)));
		list.add(3l);
		assertTrue("1;2;3;3".equals(BatchRESTServiceImpl.listToString(list)));
	}
	
	@Test
	public void testIntListToString() {
		List<Integer> list = null;
		assertTrue(null == BatchRESTServiceImpl.listToString(list));
		list = new ArrayList<Integer>();
		assertTrue(null == BatchRESTServiceImpl.listToString(list));
		list.add(1);
		assertTrue("1".equals(BatchRESTServiceImpl.listToString(list)));
		list.add(2);
		list.add(3);
		assertTrue("1;2;3".equals(BatchRESTServiceImpl.listToString(list)));
		list.add(3);
		assertTrue("1;2;3;3".equals(BatchRESTServiceImpl.listToString(list)));
	}

	@Test
	public void testStringListToString() {
		List<String> list = null;
		assertTrue(null == BatchRESTServiceImpl.listToString(list));
		list = new ArrayList<String>();
		assertTrue(null == BatchRESTServiceImpl.listToString(list));
		list.add("1");
		assertTrue("1".equals(BatchRESTServiceImpl.listToString(list)));
		list.add("2");
		list.add("3");
		assertTrue("1;2;3".equals(BatchRESTServiceImpl.listToString(list)));
		list.add("3");
		assertTrue("1;2;3;3".equals(BatchRESTServiceImpl.listToString(list)));
	}
}
