package org.genesys2.server.model.filters;

public class NoSuchFilterValueException extends Exception {

	private Object filterValue;

	public NoSuchFilterValueException(Object filterValue) {
		this.filterValue = filterValue;
	}

	public Object getFilterValue() {
		return filterValue;
	}
	
	@Override
	public String toString() {
		return super.toString() + " Invalid value: " + filterValue;
	}
}
