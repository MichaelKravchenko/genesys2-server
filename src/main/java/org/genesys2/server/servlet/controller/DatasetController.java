/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.Metadata;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.service.DatasetService;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.InstituteService;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller which simply handles *.html requests
 */
@Controller
@RequestMapping("/data")
public class DatasetController extends BaseController {

	@Autowired
	private GenesysService genesysService;

	@Autowired
	private DatasetService datasetService;

	@Autowired
	private Validator validator;

	@Autowired
	private InstituteService instituteService;

	@RequestMapping("/")
	public String index(ModelMap model, @RequestParam(value = "page", required = false, defaultValue = "1") int page) {
		model.addAttribute("pagedData", genesysService.listMetadata(new PageRequest(page - 1, 50)));
		return "/metadata/index";
	}

	@RequestMapping("/view/{id}")
	public String view(ModelMap model, @PathVariable(value = "id") long metadataId, @RequestParam(value = "page", required = false, defaultValue = "1") int page) {
		_logger.debug("Viewing data for " + metadataId);
		final Metadata metadata = genesysService.getMetadata(metadataId);
		if (metadata == null) {
			throw new ResourceNotFoundException();
		}
		model.addAttribute("page", page);
		model.addAttribute("metadata", metadata);
		model.addAttribute("methods", genesysService.listMethods(metadata));

		final FaoInstitute faoInstitute = instituteService.getInstitute(metadata.getInstituteCode());
		model.addAttribute("faoInstitute", faoInstitute);

		final Page<Accession> accessions = genesysService.listMetadataAccessions(metadata.getId(), new PageRequest(page - 1, 50));
		model.addAttribute("pagedData", accessions);

		// Map[accession.id][method.id]
		model.addAttribute("accessionMethods", genesysService.getMetadataTraitValues(metadata, accessions.getContent()));

		return "/metadata/view";
	}

	@RequestMapping(value = "/view/{id}/dwca", method = RequestMethod.POST, produces = "application/zip")
	public void downloadDwca(@PathVariable(value = "id") long metadataId, HttpServletResponse response) throws IOException {
		_logger.debug("Downloading data for " + metadataId);

		final Metadata metadata = genesysService.getMetadata(metadataId);
		if (metadata == null) {
			throw new ResourceNotFoundException();
		}
		response.setContentType("application/zip");
		response.addHeader("Content-Disposition", String.format("attachment; filename=\"genesys-dataset-%1$s.zip\"", metadata.getUuid()));
		response.flushBuffer();

		// Write Darwin Core Archive to the stream.
		final OutputStream outputStream = response.getOutputStream();

		datasetService.writeDataset(metadata, outputStream);
		response.flushBuffer();
	}
}
