/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.genesys;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.genesys2.server.model.AclAwareModel;
import org.genesys2.server.model.GlobalVersionedAuditedModel;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Entity
@Table(name = "method")
public class Method extends GlobalVersionedAuditedModel implements AclAwareModel {

	/**
	 *
	 */
	private static final long serialVersionUID = -1084675112110837714L;

	// private Language language;
	@Column(length = 36, unique = true, nullable = false)
	private String uuid;

	@Column(nullable = false)
	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String method;

	@Lob
	private String i18n;

	@Transient
	private JsonNode methodJ;

	@Column(length = 32, nullable = true)
	private String unit;

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String options;

	@Column(nullable = false, length = 32)
	private String fieldName;

	@Column(nullable = false)
	private int fieldType;

	@Column(nullable = true, length = 10)
	private String fieldSize;

	@Column(name = "\"range\"", length = 32)
	private String range;

	@ManyToOne(cascade = {}, optional = true)
	@JoinColumn(name = "parameterId", nullable = true)
	private Parameter parameter;

	@PrePersist
	void ensureUUID() {
		if (this.uuid == null) {
			this.uuid = UUID.nameUUIDFromBytes(method.getBytes()).toString();
		}
	}

	public String getMethod() {
		return this.method;
	}

	private synchronized void parseMethodLocal() {
		if (this.methodJ == null && this.i18n != null) {
			final ObjectMapper mapper = new ObjectMapper();
			try {
				this.methodJ = mapper.readTree(i18n);
			} catch (final IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Transient
	private final Map<String, String> methodMap = new HashMap<String, String>();

	/**
	 * Method to return the map of available languages keys
	 *
	 * @return Map<String,String> with language code as the key and the
	 *         vernacular string as the value.
	 */
	public Map<String, String> getLocalMethodMap() {
		return buildLocalMethodMap();
	}

	private synchronized Map<String, String> buildLocalMethodMap() {

		if (this.methodMap.isEmpty() && this.i18n != null) {
			parseMethodLocal();
			final Iterator<String> languages = this.methodJ.fieldNames();
			while (languages.hasNext()) {
				final String language = languages.next();
				methodMap.put(language, this.methodJ.get(language).textValue());
			}
		}
		return methodMap;
	}

	public String getMethod(Locale locale) {
		return getMethodLocal(locale);
	}

	private synchronized String getMethodLocal(Locale locale) {
		parseMethodLocal();
		return this.methodJ != null && this.methodJ.has(locale.getLanguage()) ? this.methodJ.get(locale.getLanguage()).textValue() : this.method;
	}

	public void setMethod(final String method) {
		this.method = method;
	}

	public void setI18n(String i18n) {
		this.i18n = i18n;
	}

	public String getUnit() {
		return this.unit;
	}

	public void setUnit(final String unit) {
		this.unit = unit;
	}

	public String getOptions() {
		return this.options;
	}

	public void setOptions(final String options) {
		this.options = options;
	}

	public String getFieldName() {
		return this.fieldName;
	}

	public void setFieldName(final String fieldName) {
		this.fieldName = fieldName;
	}

	public int getFieldType() {
		return this.fieldType;
	}

	public void setFieldType(final int fieldType) {
		this.fieldType = fieldType;
	}

	public String getFieldSize() {
		return this.fieldSize;
	}

	public void setFieldSize(final String fieldSize) {
		this.fieldSize = fieldSize;
	}

	@Transient
	public Class<?> getDataType() {
		switch (fieldType) {
		case 0:
			// Coded
			return String.class;
		case 1:
			// Double
			return Double.class;
		case 2:
			// Long
			return Long.class;
		default:
			throw new RuntimeException("Unknown data type " + fieldType + " for " + this);
		}
	}

	@Transient
	public boolean isCoded() {
		// TODO Check database!
		return this.options != null;
	}

	// FIXME String?
	public Object decode(Object value) {
		return isCoded() && value != null ? TraitCode.decode(this.options, value.toString()) : value;
	}

	/**
	 * Get list of codes for a coded descriptor.
	 *
	 * @return <code>null</code> if not coded, list of {@link TraitCode} for
	 *         coded traits
	 */
	// @Transient
	// public List<TraitCode> getCodes() {
	// if (!isCoded()) {
	// return null;
	// }
	//
	// return TraitCode.parseOptions(this.options);
	// }

	/**
	 * Get map of codes for a coded descriptor.
	 *
	 * @return <code>null</code> if not coded, list of {@link TraitCode} for
	 *         coded traits
	 */
	// @Transient
	// public Map<String, TraitCode> getCodeMap() {
	// if (!isCoded()) {
	// return null;
	// }
	//
	// return TraitCode.parseOptionsMap(this.options);
	// }

	public String getRange() {
		return this.range;
	}

	public void setRange(final String range) {
		this.range = range;
	}

	// @Override
	// public String toString() {
	// return MessageFormat.format(
	// "Method id={0,number,#} lang={1} param={2} fieldName={3}[{4}]",
	// id, language.getId(), parameter.getTitle(), fieldName,
	// fieldSize);
	// }

	public Method() {
	}

	public Parameter getParameter() {
		return this.parameter;
	}

	public void setParameter(final Parameter parameter) {
		this.parameter = parameter;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
