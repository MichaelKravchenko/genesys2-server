/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.impl.Article;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.model.impl.Organization;
import org.genesys2.server.persistence.domain.FaoInstituteRepository;
import org.genesys2.server.persistence.domain.OrganizationRepository;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.OrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class OrganizationServiceImpl implements OrganizationService {
	public static final Log LOG = LogFactory.getLog(OrganizationServiceImpl.class);

	@Autowired
	private FaoInstituteRepository instituteRepository;

	@Autowired
	private OrganizationRepository organizationRepository;

	@Autowired
	private ContentService contentService;

	@Override
	public Organization getOrganization(String slug) {
		return organizationRepository.findBySlug(slug);
	}

	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@Override
	@Transactional
	public Organization deleteOrganization(Organization organization) {
		organization = organizationRepository.findOne(organization.getId());
		organizationRepository.delete(organization);
		organization.setId(null);
		return organization;
	}

	@Override
	public Page<Organization> list(Pageable pageable) {
		return organizationRepository.findAll(pageable);
	}

	@Override
	public Article getBlurp(Organization organization, Locale locale) {
		return contentService.getArticle(organization, "blurp", locale);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@Transactional(readOnly = false)
	public Article updateBlurp(Organization organization, String blurp, Locale locale) {
		return contentService.updateArticle(organization, "blurp", null, blurp, locale);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@Transactional(readOnly = false)
	public Organization update(long id, String newSlug, String title) {
		final Organization organization = organizationRepository.findOne(id);
		organization.setSlug(newSlug);
		organization.setTitle(title);

		organizationRepository.save(organization);
		return organization;
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@Transactional(readOnly = false)
	public Organization create(String slug, String title) {
		final Organization organization = new Organization();
		organization.setSlug(slug);
		organization.setTitle(title);

		organizationRepository.save(organization);

		return organization;
	}

	@Override
	public List<FaoInstitute> getMembers(Organization organization) {
		if (organization == null) {
			throw new NullPointerException("organization");
		}
		return organizationRepository.findInstitutesByOrganization(organization);
	}

	@Override
	@Transactional
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public boolean setOrganizationInstitutes(Organization org, List<String> instituteList) {
		LOG.info("Setting institutes for organization " + org);

		final Organization organization = organizationRepository.findOne(org.getId());
		final List<FaoInstitute> toRemove = new ArrayList<FaoInstitute>();

		// Make set of INSTCODEs
		final Set<String> existingMembers = new HashSet<String>();
		for (final FaoInstitute member : organization.getMembers()) {
			if (instituteList.contains(member.getCode())) {
				existingMembers.add(member.getCode());
			} else {
				if (LOG.isDebugEnabled()) {
					LOG.debug("Will remove " + member);
				}
				toRemove.add(member);
			}
		}

		boolean updated = false;

		for (final FaoInstitute institute : toRemove) {
			LOG.info("Removing " + institute);
			organization.getMembers().remove(institute);
			updated = true;
		}

		for (final String wiewsCode : instituteList) {
			if (!existingMembers.contains(wiewsCode)) {
				final FaoInstitute newMemberInstitute = instituteRepository.findByCode(wiewsCode);
				if (newMemberInstitute == null) {
					LOG.warn("No such institute " + wiewsCode);
				} else {
					LOG.info("Adding " + newMemberInstitute);
					organization.getMembers().add(newMemberInstitute);
					updated = true;
				}
			}
		}

		if (updated) {
			LOG.info("Saving " + organization);
			organizationRepository.save(organization);
		}

		return updated;
	}

	@Override
	@Transactional
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public boolean addOrganizationInstitutes(final Organization organization, final List<String> instituteList) {
		LOG.info("Adding institutes to organization " + organization);

		// Make set of INSTCODEs
		final Set<String> existingMembers = new HashSet<String>();
		for (final FaoInstitute member : organization.getMembers()) {
			existingMembers.add(member.getCode());
		}

		boolean updated = false;

		for (final String wiewsCode : instituteList) {
			if (!existingMembers.contains(wiewsCode)) {
				final FaoInstitute newMemberInstitute = instituteRepository.findByCode(wiewsCode);
				if (newMemberInstitute == null) {
					LOG.warn("No such institute " + wiewsCode);
				} else {
					organization.getMembers().add(newMemberInstitute);
					updated = true;
				}
			}
		}

		if (updated) {
			organizationRepository.save(organization);
		}

		return updated;
	}
}
