package org.genesys2.server.model.elastic;

import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;

public class Country {

	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String iso3;
	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String iso2;
	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String name;

	public static Country from(org.genesys2.server.model.impl.Country country) {
		Country c = new Country();
		if (country != null) {
			c.iso3 = country.getCode3();
			c.iso2 = country.getCode2();
			c.name = country.getName();
		}
		return c;
	}

	public String getIso3() {
		return iso3;
	}

	public void setIso3(String iso3) {
		this.iso3 = iso3;
	}

	public String getIso2() {
		return iso2;
	}

	public void setIso2(String iso2) {
		this.iso2 = iso2;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}