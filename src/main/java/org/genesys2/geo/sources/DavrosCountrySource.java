/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.geo.sources;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 * Fetch and parse country information from
 * http://www.davros.org/misc/iso3166.txt
 *
 * @author mobreza
 */
public class DavrosCountrySource {
	private static final String DAVROS_ISO3166_URL = "http://www.davros.org/misc/iso3166.txt";

	public static final Log LOG = LogFactory.getLog(DavrosCountrySource.class);

	/**
	 * Retrieve data from davros.org
	 *
	 * @throws IOException
	 */
	public static List<CountryInfo> fetchCountryData() throws IOException {

		final HttpClient httpclient = new DefaultHttpClient();
		final HttpGet httpget = new HttpGet(DAVROS_ISO3166_URL);

		HttpResponse response = null;
		try {
			response = httpclient.execute(httpget);
		} catch (final ClientProtocolException e) {
			LOG.error(e.getMessage(), e);
			throw new IOException("Could not execute HTTP request: " + e.getMessage(), e);
		}

		LOG.debug(response.getStatusLine());

		// Get hold of the response entity
		final HttpEntity entity = response.getEntity();
		LOG.debug(entity.getContentType() + " " + entity.getContentLength());

		InputStream instream = null;

		try {
			instream = entity.getContent();
			final BufferedReader inreader = new BufferedReader(new InputStreamReader(instream));

			final List<CountryInfo> countries = new ArrayList<CountryInfo>();

			boolean active = true;
			String line;
			while ((line = inreader.readLine()) != null) {
				if (LOG.isTraceEnabled()) {
					LOG.trace(line);
				}
				if (line.startsWith("# Table 1: current codes")) {
					active = true;
					continue;
				} else if (line.startsWith("# Table 2: codes withdrawn from use")) {
					active = false;
					continue;
				} else if (line.length() == 0) {
					continue;
				} else if (line.startsWith("# ")) {
					continue;
				} else {
					final CountryInfo countryInfo = parseDavros(line, active);
					if (!countries.contains(countryInfo)) {
						countries.add(countryInfo);
					}
				}
			}

			inreader.close();
			LOG.info("Got " + countries.size() + " countries!");
			return countries;

		} catch (final RuntimeException ex) {
			LOG.error(ex.getMessage(), ex);
			httpget.abort();
			throw new IOException(ex);
		} finally {
			LOG.info("Done fetching country info from davros.org");
			IOUtils.closeQuietly(instream);
		}
	}

	/**
	 *
	 * BQ ATB British Antarctic Territory
	 *
	 * GB GBR 826 United Kingdom of Great Britain & N. Ireland
	 *
	 * @param line
	 * @return
	 */
	public static CountryInfo parseDavros(final String line, final boolean active) {
		final String a = line.substring(0, 10);
		final String b = line.substring(11);
		if (LOG.isTraceEnabled()) {
			LOG.trace("Davros a=" + a + " b=" + b);
		}
		final String[] codes = a.split(" +", 3);
		return new CountryInfo(codes[0], codes[1], codes[2], b, active);
	}
}
