/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.json;

public class AccessionJson {
	private long version = 0;
	private Long genesysId;

	private String instCode;
	private String acceNumb;
	private String genus;
	private String species;
	private String spauthor;
	private String subtaxa;
	private String subtauthor;
	private String uuid;
	private String orgCty;
	private String acqDate;
	private Boolean mlsStat;
	private Boolean inTrust;
	private Boolean available;
	private int[] storage;
	private Integer sampStat;
	private String[] duplSite;
	private String bredCode;
	private String ancest;
	private String donorCode;
	private String donorNumb;
	private String donorName;
	private CollectingJson coll;
	private GeoJson geo;
	private Remark[] remarks;

	public long getVersion() {
		return version;
	}

	public void setVersion(long l) {
		this.version = l;
	}

	public Long getGenesysId() {
		return genesysId;
	}

	public void setGenesysId(Long genesysId) {
		this.genesysId = genesysId;
	}

	public String getInstCode() {
		return instCode;
	}

	public void setInstCode(String instCode) {
		this.instCode = instCode;
	}

	public String getAcceNumb() {
		return acceNumb;
	}

	public void setAcceNumb(String acceNumb) {
		this.acceNumb = acceNumb;
	}

	public String getGenus() {
		return genus;
	}

	public void setGenus(String genus) {
		this.genus = genus;
	}

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public String getSpauthor() {
		return spauthor;
	}

	public void setSpauthor(String spauthor) {
		this.spauthor = spauthor;
	}

	public String getSubtaxa() {
		return subtaxa;
	}

	public void setSubtaxa(String subtaxa) {
		this.subtaxa = subtaxa;
	}

	public String getSubtauthor() {
		return subtauthor;
	}

	public void setSubtauthor(String subtauthor) {
		this.subtauthor = subtauthor;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getOrgCty() {
		return orgCty;
	}

	public void setOrgCty(String orgCty) {
		this.orgCty = orgCty;
	}

	public String getAcqDate() {
		return acqDate;
	}

	public void setAcqDate(String acqDate) {
		this.acqDate = acqDate;
	}

	public Boolean getMlsStat() {
		return mlsStat;
	}

	public void setMlsStat(Boolean mlsStat) {
		this.mlsStat = mlsStat;
	}

	public Boolean getInTrust() {
		return inTrust;
	}

	public void setInTrust(Boolean inTrust) {
		this.inTrust = inTrust;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}

	public int[] getStorage() {
		return storage;
	}

	public void setStorage(int[] storage) {
		this.storage = storage;
	}

	public Integer getSampStat() {
		return sampStat;
	}

	public void setSampStat(Integer sampStat) {
		this.sampStat = sampStat;
	}

	public String[] getDuplSite() {
		return duplSite;
	}

	public void setDuplSite(String[] duplSite) {
		this.duplSite = duplSite;
	}

	public String getBredCode() {
		return bredCode;
	}

	public void setBredCode(String bredCode) {
		this.bredCode = bredCode;
	}

	public String getAncest() {
		return ancest;
	}

	public void setAncest(String ancest) {
		this.ancest = ancest;
	}

	public String getDonorCode() {
		return donorCode;
	}

	public void setDonorCode(String donorCode) {
		this.donorCode = donorCode;
	}

	public String getDonorNumb() {
		return donorNumb;
	}

	public void setDonorNumb(String donorNumb) {
		this.donorNumb = donorNumb;
	}

	public String getDonorName() {
		return donorName;
	}

	public void setDonorName(String donorName) {
		this.donorName = donorName;
	}

	public CollectingJson getColl() {
		return coll;
	}

	public void setColl(CollectingJson coll) {
		this.coll = coll;
	}

	public GeoJson getGeo() {
		return geo;
	}

	public void setGeo(GeoJson geo) {
		this.geo = geo;
	}

	public Remark[] getRemarks() {
		return remarks;
	}
	
	public void setRemarks(Remark[] remarks) {
		this.remarks = remarks;
	}
}
