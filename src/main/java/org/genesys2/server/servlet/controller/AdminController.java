/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.persistence.domain.GenesysLowlevelRepository;
import org.genesys2.server.service.CountryNamesUpdater;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.GeoService;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.MappingService;
import org.genesys2.server.service.impl.ContentSanitizer;
import org.genesys2.server.service.worker.AccessionStorageScanner;
import org.genesys2.server.service.worker.ElasticUpdater;
import org.genesys2.server.service.worker.ITPGRFAStatusUpdater;
import org.genesys2.server.service.worker.InstituteUpdater;
import org.genesys2.server.service.worker.SGSVUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.hazelcast.core.IMap;

@Controller
@RequestMapping("/admin")
public class AdminController {
	public static final Log LOG = LogFactory.getLog(AdminController.class);

	@Autowired
	InstituteUpdater instituteUpdater;

	@Autowired
	CountryNamesUpdater alternateNamesUpdater;

	@Autowired
	ElasticUpdater elasticUpdater;

	@Autowired
	GenesysService genesysService;

	@Autowired
	GeoService geoService;

	@Autowired
	SGSVUpdate sgsvUpdater;

	@Autowired
	ContentSanitizer contentSanitizer;

	@Autowired
	ITPGRFAStatusUpdater itpgrfaUpdater;

	@Autowired
	private InstituteService instituteService;

	@Autowired
	private GenesysLowlevelRepository genesysLowlevelRepository;

	@Autowired
	private MappingService mappingService;

	@Autowired
	private CacheManager cacheManager;

	@Autowired
	private TaskExecutor taskExecutor;

	@Autowired
	protected AccessionStorageScanner accessionStorageScanner;

	@RequestMapping("/")
	public String root(Model model) {
		return "/admin/index";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/refreshWiews")
	public String refreshWiews() {
		try {
			instituteUpdater.updateFaoInstitutes();
		} catch (final IOException e) {
			LOG.error(e);
		}
		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/refreshCountries")
	public String refreshCountries() {
		try {
			geoService.updateCountryData();
		} catch (final IOException e) {
			LOG.error(e.getMessage(), e);
		}
		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/reindex-elastic")
	public String reindexElastic(@RequestParam(value="startAt", required=false) Long startAt, @RequestParam(value="slow", required=true, defaultValue="true") boolean slow) {
//		LOG.info("Json filter: " + jsonFilter);
		elasticUpdater.fullReindex();
		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/clear-queues")
	public String clearElasticQueues() {
		elasticUpdater.clearQueues();
		return "redirect:/admin/";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/updateAccessionCountryRefs")
	public String updateAccessionCountryRefs() {
		genesysService.updateAccessionCountryRefs();
		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/refreshMetadataMethods")
	public String refreshMetadataMethods() {
		genesysService.refreshMetadataMethods();
		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/updateInstituteCountryRefs")
	public String updateInstituteCountryRefs() {
		instituteService.updateCountryRefs();
		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/updateAccessionInstituteRefs")
	public String updateAccessionInstituteRefs() {
		genesysService.updateAccessionInstitueRefs();
		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/updateSGSV")
	public String updateSGSV() {
		sgsvUpdater.updateSGSV();
		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/convertNames")
	public String convertNames() {
		// Convert {@link AllAccenames} to Aliases
		final List<Object[]> list = new ArrayList<Object[]>(100000);
		genesysLowlevelRepository.listAccessionsAccenames(new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				final long accessionId = rs.getLong(1);
				final String acceNames = rs.getString(2);
				final String otherIds = rs.getString(3);
				list.add(new Object[] { accessionId, acceNames, otherIds });
				if (list.size() % 10000 == 0) {
					LOG.info("Loaded names: " + list.size());
				}
			}
		});

		int i = 0;
		for (final Object[] o : list) {
			if (++i % 1000 == 0) {
				LOG.info("Conversion progress " + i + " of " + list.size());
			}
			genesysService.upsertAliases((long) o[0], (String) o[1], (String) o[2]);
		}

		list.clear();

		System.err.println("FOOBAR!");

		final Set<Long> toRemove = new HashSet<Long>();

		// Remove stupid stuff
		// List<Long> aliasesToRemove = new ArrayList<Long>();
		genesysLowlevelRepository.listAccessionsAlias(new RowCallbackHandler() {
			private long prevAccnId = -1;
			private final List<Object[]> aliases = new ArrayList<Object[]>(10);

			@Override
			public void processRow(ResultSet rs) throws SQLException {
				System.err.println("..");
				// n.accessionId, n.instCode, n.name, n.aliasType, n.lang,
				// n.version
				if (prevAccnId == rs.getLong(1) || prevAccnId == -1) {
					prevAccnId = rs.getLong(1);
					System.err.println("Add... " + prevAccnId + " " + rs.getLong(1));
				} else {
					cleanup(prevAccnId, aliases);
					aliases.clear();
					prevAccnId = rs.getLong(1);
				}
				aliases.add(new Object[] { rs.getLong(7), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getString(5) });
			}

			private void cleanup(long accessionId, List<Object[]> existingAliases) {
				System.err.println("CLEANUP:");
				for (final Object[] alias : existingAliases) {
					System.err.println("" + accessionId + " = " + ArrayUtils.toString(alias, "NULL"));
				}

				for (int i = 0; i < existingAliases.size() - 1; i++) {
					final Object[] name1 = existingAliases.get(i);
					if (toRemove.contains(name1[0])) {
						continue;
					}
					System.err.println("Base " + i + " " + ArrayUtils.toString(name1));
					for (int j = i + 1; j < existingAliases.size(); j++) {
						System.err.println("Inspecting " + j);
						final Object[] name2 = existingAliases.get(j);
						if (toRemove.contains(name2[0])) {
							continue;
						}
						final int res = whatToKeep(name1, name2);
						if (res == -1) {
							System.err.println("Would remove " + i + " " + ArrayUtils.toString(name1));
							toRemove.add((long) name1[0]);
						} else if (res == 1) {
							System.err.println("Would remove " + j + " " + ArrayUtils.toString(name2));
							toRemove.add((long) name2[0]);
						}
					}
				}
			}

			private int whatToKeep(Object[] name1, Object[] name2) {
				if (StringUtils.equals((String) name1[2], (String) name2[2])) {
					final float score1 = score(name1), score2 = score(name2);
					if (score1 < score2) {
						return -1;
					} else {
						return 1;
					}
				} else {
					return 0;
				}
			}

			private float score(Object[] name1) {
				float score = 1.0f;
				if (name1[1] != null) {
					score += 2;
					if ((int) name1[3] == 5) {
						score *= 2;
					}
				} else {
					if ((int) name1[3] == 0) {
						score += 1;
					}
				}
				return score;
			}
		});

		this.genesysService.removeAliases(toRemove);

		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/sanitize")
	public String sanitize() {
		LOG.info("Sanitizing content");
		contentSanitizer.sanitizeAll();
		LOG.info("Sanitizing content.. Done");
		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/updateAlternateNames")
	public String updateAlternateNames() {
		LOG.info("Updating alternate GEO names");
		try {
			alternateNamesUpdater.updateAlternateNames();
		} catch (final IOException e) {
			LOG.error(e.getMessage(), e);
		}
		LOG.info("Updating alternate GEO names: done");
		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/updateITPGRFA")
	public String updateITPGRFA() {
		LOG.info("Updating country ITPGRFA status");
		try {
			itpgrfaUpdater.downloadAndUpdate();
		} catch (final IOException e) {
			LOG.error(e.getMessage(), e);
		}
		LOG.info("Updating done");
		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/scanStorage")
	public String scanStorage() {
		LOG.info("Updating accession.stoRage");
		taskExecutor.execute(new Runnable() {
			@Override
			public void run() {
				Thread t = Thread.currentThread();
				int prevPrio = t.getPriority();
				String prevName = t.getName();
				t.setPriority(Thread.MIN_PRIORITY);
				t.setName("background.storageScanner");
				try {
					accessionStorageScanner.scanStorage();
					LOG.info("Updating done");
				} catch (InterruptedException e) {
					LOG.warn("Storage scanner thread interrupted!");
				} finally {
					LOG.debug("Resetting thread priority");
					t.setPriority(prevPrio);
					t.setName(prevName);
				}
			}
		});
		LOG.info("Job scheduled");
		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/clearTilesCache")
	public String clearTilesCache() {
		final Cache tileServerCache = cacheManager.getCache("tileserver");
		System.err.println("tileServerCache=" + tileServerCache.getNativeCache());

		@SuppressWarnings("rawtypes")
		final IMap hazelCache = (IMap) tileServerCache.getNativeCache();

		LOG.info("Tiles cache size=" + hazelCache.size());
		int count = 0;
		for (final Object key : hazelCache.keySet()) {
			LOG.info("\tkey=" + key);
			if (++count > 20) {
				break;
			}
		}
		mappingService.clearCache();
		LOG.info("Tiles cache size=" + hazelCache.size());

		return "redirect:/admin/";
	}
}
