<%@include file="/WEB-INF/jsp/init.jsp"%>

<div class="navbar" role="navigation" id="nav-main">
  <div class="container">
    <div class="navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="notimportant"><a href="<c:url value="/" />"><spring:message code="menu.home" /></a></li>
		<li><a href="<c:url value="/explore" />"><spring:message code="menu.browse" /></a></li>
		<li><a href="<c:url value="/data/" />"><spring:message code="menu.datasets" /></a></li>
		<%-- <li><a href="<c:url value="/descriptors/" />"><spring:message code="menu.descriptors" /></a></li> --%>
		<li><a href="<c:url value="/geo/" />"><spring:message code="menu.countries" /></a></li>
		<li><a href="<c:url value="/wiews/active" />"><spring:message code="menu.institutes" /></a></li>
		<li><a href="<c:url value="/sel/" />"><spring:message code="menu.my-list" />
				<span class="badge" x-size="${selection.size() gt 0 ? selection.size() : '0'}" id="selcounter">${selection.size() gt 0 ? selection.size() : '0'}</span></a></li>
      </ul>
    </div>
  </div>
</div>
