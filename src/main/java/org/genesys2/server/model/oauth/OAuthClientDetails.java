/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.oauth;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.genesys2.server.model.AclAwareModel;
import org.genesys2.server.model.VersionedAuditedModel;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.provider.ClientDetails;

@Entity
@Table(name = "oauthclient")
public class OAuthClientDetails extends VersionedAuditedModel implements ClientDetails, AclAwareModel {
	private static final long serialVersionUID = 5328458631619687041L;

	@Column(length = 200)
	private String title;

	@Lob
	@Column
	private String description;

	@Column(unique = true, nullable = false)
	private String clientId;

	@Column(length = 100)
	private String clientSecret;

	@Column(length = 400)
	private String resourceIds;

	@Column(length = 400)
	private String scope;

	@Column(length = 400)
	private String authorities;

	@Column(length = 400)
	private String authorizedGrantTypes;

	@Column(length = 400)
	private String redirectUris;

	@Column
	private Integer accessTokenValiditySeconds;

	@Column
	private Integer refreshTokenValiditySeconds;

	@Column(name = "additional_information")
	private String additionalInformation;

	// @Enumerated(EnumType.STRING)
	// @Column(name = "clientType", length=50)
	// private OAuthClientType clientType;

	public OAuthClientDetails() {
	}

	public OAuthClientDetails(String clientId, String resourceIds, String scopes, String grantTypes, String authorities) {
		this(clientId, resourceIds, scopes, grantTypes, authorities, null);
	}

	public OAuthClientDetails(String clientId, String resourceIds, String scopes, String grantTypes, String authorities, String redirectUris) {

		this.clientId = clientId;

		if (StringUtils.isNotBlank(resourceIds)) {
			this.resourceIds = resourceIds;
		}

		if (StringUtils.isNotBlank(scopes)) {
			this.scope = scopes;
		}

		if (StringUtils.isNotBlank(grantTypes)) {
			this.authorizedGrantTypes = grantTypes;
		} else {
			this.authorizedGrantTypes = "authorization_code,refresh_token";
		}

		if (StringUtils.isNotBlank(authorities)) {
			this.authorities = authorities;
		}

		if (StringUtils.isNotBlank(redirectUris)) {
			this.redirectUris = redirectUris;
		}
	}

	// public OAuthClientType getClientType() {
	// return clientType;
	// }
	//
	// public void setClientType(OAuthClientType clientType) {
	// this.clientType = clientType;
	// }

	@Override
	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	@Override
	public Set<String> getResourceIds() {
		if (resourceIds != null && !resourceIds.isEmpty()) {
			final String[] split = resourceIds.split(",");
			final List<String> strings = Arrays.asList(split);
			return new LinkedHashSet<String>(strings);
		} else {
			return Collections.<String> emptySet();
		}
	}

	public void setResourceIds(String resourceIds) {
		if (!resourceIds.isEmpty()) {
			this.resourceIds = resourceIds;
		}
	}

	@Override
	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	@Override
	public Set<String> getScope() {
		if (scope != null && !scope.isEmpty()) {
			final String[] split = scope.split(",");
			final List<String> strings = Arrays.asList(split);
			return new LinkedHashSet<String>(strings);
		} else {
			return Collections.<String> emptySet();
		}
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	@Override
	public Set<String> getAuthorizedGrantTypes() {
		if (authorizedGrantTypes != null && !authorizedGrantTypes.isEmpty()) {
			final String[] split = authorizedGrantTypes.split(",");
			final List<String> strings = Arrays.asList(split);
			return new HashSet<String>(strings);
		} else {
			return Collections.<String> emptySet();
		}
	}

	public void setAuthorizedGrantTypes(String authorizedGrantTypes) {
		this.authorizedGrantTypes = authorizedGrantTypes;
	}

	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		if (authorities != null && !authorities.isEmpty()) {
			return new ArrayList<GrantedAuthority>(AuthorityUtils.createAuthorityList(authorities));
		} else {
			return Collections.emptyList();
		}
	}

	public void setAuthorities(String authorities) {
		this.authorities = authorities;
	}

	@Override
	public Integer getAccessTokenValiditySeconds() {
		return accessTokenValiditySeconds;
	}

	public void setAccessTokenValiditySeconds(Integer accessTokenValiditySeconds) {
		this.accessTokenValiditySeconds = accessTokenValiditySeconds;
	}

	@Override
	public Integer getRefreshTokenValiditySeconds() {
		return refreshTokenValiditySeconds;
	}

	public void setRefreshTokenValiditySeconds(Integer refreshTokenValiditySeconds) {
		this.refreshTokenValiditySeconds = refreshTokenValiditySeconds;
	}

	@Override
	public Set<String> getRegisteredRedirectUri() {
		if (redirectUris != null && !redirectUris.isEmpty()) {
			final String[] split = redirectUris.split(",");
			final List<String> strings = Arrays.asList(split);
			return new LinkedHashSet<String>(strings);
		} else {
			return Collections.<String> emptySet();
		}
	}

	public void setRedirectUris(String redirectUris) {
		this.redirectUris = redirectUris;
	}
	
	public String getRedirectUris() {
		return redirectUris;
	}

	@Override
	public Map<String, Object> getAdditionalInformation() {
		if (StringUtils.isNotBlank(additionalInformation)) {
			final Map<String, Object> myMap = new HashMap<String, Object>();
			final String[] pairs = additionalInformation.split(",");
			for (final String pair : pairs) {
				final String[] keyValue = pair.split(":");
				myMap.put(keyValue[0], Integer.valueOf(keyValue[1]));
			}
			return myMap;
		} else {
			return Collections.<String, Object> emptyMap();
		}

	}

	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

	@Override
	@Transient
	public boolean isSecretRequired() {
		return this.clientSecret != null;
	}

	@Override
	@Transient
	public boolean isScoped() {
		return this.scope != null && !this.scope.isEmpty();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
