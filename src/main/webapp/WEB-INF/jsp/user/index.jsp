<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="user.page.list.title" /></title>
</head>
<body>
	<h1>
		<spring:message code="user.page.list.title" />
	</h1>

	<div class="main-col-header clearfix">
	<div class="nav-header pull-left">
		<div class="results"><spring:message code="paged.totalElements" arguments="${pagedData.totalElements}" /></div>
		<div class="pagination">
			<spring:message code="paged.pageOfPages" arguments="${pagedData.number+1},${pagedData.totalPages}" />
			<a class="${pagedData.number eq 0 ? 'disabled' :''}" href="?page=${pagedData.number eq 0 ? 1 : pagedData.number}"><spring:message code="pagination.previous-page" /></a> <a href="?page=${pagedData.number + 2}"><spring:message code="pagination.next-page" /></a>
		</div>
	</div>
	</div>
	
	<table class="funny-list">
		<c:forEach items="${pagedData.content}" var="user" varStatus="status">
			<tr class="clearfix ${status.count % 2 == 0 ? 'even' : 'odd'}">
				<td><c:if test="${not user.systemAccount}"><a href="<c:url value="/profile/${user.uuid}" />"><c:out value="${user.name}" /></a></c:if></td>
				<td>${user.uuid}</td>
				<td>${user.email}</td>
				<td>
					<c:if test="${user.systemAccount}">SYSTEM</c:if>
					<c:if test="${not user.enabled}">DISABLED</c:if>
					<c:if test="${user.accountLocked}">LOCKED</c:if>
				</td>
			</tr>
		</c:forEach>
	</table>


</body>
</html>