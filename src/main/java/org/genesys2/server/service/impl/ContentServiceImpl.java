/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.io.StringWriter;
import java.io.Writer;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.genesys2.server.model.EntityId;
import org.genesys2.server.model.impl.ActivityPost;
import org.genesys2.server.model.impl.Article;
import org.genesys2.server.model.impl.ClassPK;
import org.genesys2.server.persistence.domain.ActivityPostRepository;
import org.genesys2.server.persistence.domain.ArticleRepository;
import org.genesys2.server.persistence.domain.ClassPKRepository;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.HtmlSanitizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class ContentServiceImpl implements ContentService {
	public static final Log LOG = LogFactory.getLog(ContentServiceImpl.class);

	@Autowired
	private ActivityPostRepository postRepository;

	@Autowired
	private ArticleRepository articleRepository;

	@Autowired
	private ClassPKRepository classPkRepository;

	@Autowired
	private HtmlSanitizer htmlSanitizer;

	@Autowired
	private VelocityEngine velocityEngine;

	@Override
	public Locale getDefaultLocale() {
		return Locale.getDefault();
	}

	@Override
	@Cacheable(value = "contentcache", key = "#root.methodName")
	public List<ActivityPost> lastNews() {
		final PageRequest page = new PageRequest(0, 6, Direction.DESC, "postDate");
		return postRepository.findAll(page).getContent();
	}

	@Override
	public Page<Article> listArticles(Pageable pageable) {
		return articleRepository.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	@CacheEvict(value = "contentcache", allEntries = true)
	public void save(Iterable<Article> articles) {
		articleRepository.save(articles);
	}

	@Override
	@Cacheable(value = "contentcache", key = "'globalarticle-' + #slug + '-' + #locale.language + '-' + #useDefault")
	public Article getGlobalArticle(String slug, Locale locale, boolean useDefault) {
		return getArticle(Article.class, null, slug, locale, useDefault);
	}

	/**
	 * Get article, use default locale if required
	 */
	@Override
	@Cacheable(value = "contentcache", key = "'globalarticle-' + #slug + '-' + #locale.language")
	public Article getGlobalArticle(String slug, Locale locale) {
		return getGlobalArticle(slug, locale, true);
	}

	@Override
	@Cacheable(value = "contentcache", key = "'article-' + #entity.class.name + '-' + #entity.id + '-' + #slug + '-' + #locale.language")
	public Article getArticle(EntityId entity, String slug, Locale locale) {
		return getArticle(entity.getClass(), entity.getId(), slug, locale, true);
	}

	@Override
	@Cacheable(value = "contentcache", key = "'article-' + #clazz.name + '-' + #id + '-' + #slug + '-' + #locale.language + '-' + #useDefault")
	public Article getArticle(Class<?> clazz, Long id, String slug, Locale locale, boolean useDefault) {
		Article article = articleRepository.findByClassPkAndTargetIdAndSlugAndLang(getClassPk(clazz), id, slug, locale.getLanguage());
		if (article == null && useDefault && !locale.getLanguage().equals(getDefaultLocale().getLanguage())) {
			article = articleRepository.findByClassPkAndTargetIdAndSlugAndLang(getClassPk(clazz), id, slug, getDefaultLocale().getLanguage());
		}
		return article;
	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	@CacheEvict(value = "contentcache", allEntries = true)
	public Article updateArticle(long id, String slug, String title, String body) {
		final Article article = articleRepository.findOne(id);

		article.setSlug(slug);
		article.setTitle(htmlSanitizer.sanitize(title));

		// FIXME TODO Find a better way to distinguish between Velocity
		// templates and general articles
		if (StringUtils.contains(body, "VELOCITY")) {
			article.setBody(body);
		} else {
			article.setBody(htmlSanitizer.sanitize(body));
		}
		articleRepository.save(article);

		return article;
	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	@CacheEvict(value = "contentcache", allEntries = true)
	public Article createGlobalArticle(String slug, Locale locale, String title, String body) {
		Article article = getGlobalArticle(slug, locale, false);
		if (article != null) {
			throw new RuntimeException("Article exists");
		}

		article = new Article();
		article.setClassPk(ensureClassPK(Article.class));
		article.setLang(locale.getLanguage());
		article.setSlug(slug);
		article.setTitle(htmlSanitizer.sanitize(title));
		article.setBody(htmlSanitizer.sanitize(body));
		article.setPostDate(Calendar.getInstance());

		articleRepository.save(article);
		return article;
	}

	/**
	 * Creates or updates an article
	 * 
	 * @param entity
	 * @param body
	 * @param locale
	 * @return
	 */
	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER') or hasPermission(#entity, 'ADMINISTRATION')")
	@CacheEvict(value = "contentcache", allEntries = true)
	public Article updateArticle(EntityId entity, String slug, String title, String body, Locale locale) {
		// return
		// articleRepository.findByClassPkAndTargetIdAndSlugAndLang(getClassPk(clazz),
		// id, slug, locale.getLanguage());
		return updateArticle(entity.getClass(), entity.getId(), slug, title, body, locale);
	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	@CacheEvict(value = "contentcache", allEntries = true)
	public Article updateArticle(Class<?> clazz, Long id, String slug, String title, String body, Locale locale) {
		Article article = getArticle(clazz, id, slug, locale, false);
		if (article == null || !article.getLang().equals(locale.getLanguage())) {
			article = new Article();
			article.setClassPk(ensureClassPK(clazz));
			article.setTargetId(id);
			article.setLang(locale.getLanguage());
			article.setPostDate(Calendar.getInstance());
			article.setSlug(slug);
		}
		article.setBody(htmlSanitizer.sanitize(body));
		article.setTitle(htmlSanitizer.sanitize(title));

		articleRepository.save(article);
		return article;
	}

	public ClassPK getClassPk(Class<?> clazz) {
		return classPkRepository.findByClassName(clazz.getName());
	}

	@Override
	@Transactional(readOnly = false)
	public ClassPK ensureClassPK(Class<?> clazz) {
		ClassPK classPk = classPkRepository.findByClassName(clazz.getName());
		if (classPk == null) {
			classPk = ensureClassPKInternal(clazz);
		}
		return classPk;
	}

	private synchronized ClassPK ensureClassPKInternal(Class<?> clazz) {
		final String className = clazz.getName();
		ClassPK classPk = classPkRepository.findByClassName(className);
		if (classPk == null) {
			LOG.info("Registering new ClassPK for " + className);
			classPk = new ClassPK();
			classPk.setClassName(className);
			classPkRepository.save(classPk);
		}
		return classPk;
	}

	@Override
	@Cacheable(value = "contentcache", key = "'activityPost-' + #id")
	public ActivityPost getActivityPost(long id) {
		return postRepository.findOne(id);
	}

	/**
	 * Create and persist a new {@link ActivityPost}
	 */
	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	@CacheEvict(value = "contentcache", allEntries = true)
	public ActivityPost createActivityPost(String title, String body) {
		final ActivityPost newPost = new ActivityPost();
		newPost.setPostDate(Calendar.getInstance());
		return updatePostData(newPost, title, body);
	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	@CacheEvict(value = "contentcache", allEntries = true)
	public ActivityPost updateActivityPost(long id, String title, String body) {
		final ActivityPost post = postRepository.findOne(id);
		return updatePostData(post, title, body);
	}

	private ActivityPost updatePostData(ActivityPost post, String title, String body) {
		post.setTitle(htmlSanitizer.sanitize(title));
		post.setBody(htmlSanitizer.sanitize(body));

		return postRepository.save(post);
	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	@CacheEvict(value = "contentcache", allEntries = true)
	public void deleteActivityPost(long id) {
		postRepository.delete(id);
	}

	@Override
	public String processTemplate(String templateStr, Map<String, Object> root) {
		final VelocityContext context = new VelocityContext();
		for (final String key : root.keySet()) {
			context.put(key, root.get(key));
		}

		final StringWriter swOut = new StringWriter();

		/**
		 * Merge data and template
		 */
		velocityEngine.evaluate(context, swOut, "log tag name", templateStr);
		if (LOG.isTraceEnabled()) {
			LOG.trace(swOut);
		}
		return swOut.toString();
	}

	@Override
	public void processTemplate(String templateStr, Map<String, Object> root, Writer writer) {
		final VelocityContext context = new VelocityContext();
		for (final String key : root.keySet()) {
			context.put(key, root.get(key));
		}

		/**
		 * Merge data and template
		 */
		velocityEngine.evaluate(context, writer, "log tag name", templateStr);
	}
}
