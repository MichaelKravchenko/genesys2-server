/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.genesys;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.genesys2.server.model.BusinessModel;

@Entity
@Table(name = "metadataaccession", uniqueConstraints = { @UniqueConstraint(name = "UI_metadataaccession_all", columnNames = { "metadataId", "accessionId" }) })
public class MetadataAccession extends BusinessModel {
	private static final long serialVersionUID = -240056837800843686L;

	@ManyToOne(cascade = {}, optional = false)
	@JoinColumn(name = "metadataId")
	private Metadata metadata;

	private long accessionId;

	public Metadata getMetadata() {
		return metadata;
	}

	public void setMetadata(Metadata metadata) {
		this.metadata = metadata;
	}

	public long getAccessionId() {
		return accessionId;
	}

	public void setAccessionId(long accessionId) {
		this.accessionId = accessionId;
	}
}
