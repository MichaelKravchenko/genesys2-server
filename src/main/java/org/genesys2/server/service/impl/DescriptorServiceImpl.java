/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.impl.Dataset;
import org.genesys2.server.model.impl.DatasetDescriptor;
import org.genesys2.server.model.impl.Descriptor;
import org.genesys2.server.persistence.domain.DatasetDescriptorRepository;
import org.genesys2.server.persistence.domain.DescriptorRepository;
import org.genesys2.server.service.DescriptorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class DescriptorServiceImpl implements DescriptorService {
	public static final Log LOG = LogFactory.getLog(DescriptorServiceImpl.class);

	@Autowired
	private DescriptorRepository descriptorRepository;

	@Autowired
	private DatasetDescriptorRepository datasetDescriptorRepository;

	@Override
	public List<Descriptor> list() {
		return descriptorRepository.findAll();
	}

	/**
	 * Fetch {@link DatasetDescriptor}s.
	 *
	 * @param dataset
	 * @param descriptors
	 * @return
	 */
	@Override
	@Transactional
	public DatasetDescriptor[] getDatasetDescriptors(final Dataset dataset, final Descriptor[] descriptors) {
		final DatasetDescriptor[] datasetDescriptors = new DatasetDescriptor[descriptors.length];

		for (int i = 0; i < descriptors.length; i++) {
			if (descriptors[i] == null) {
				// Skip null descriptor
				continue;
			}

			datasetDescriptors[i] = datasetDescriptorRepository.findByDatasetAndDescriptor(dataset, descriptors[i]);
		}
		return datasetDescriptors;
	}

	/**
	 * Fetch and ensure {@link DatasetDescriptor}s exist.
	 *
	 * @param dataset
	 * @param descriptors
	 * @return
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
	public DatasetDescriptor[] ensureDatasetDescriptors(final Dataset dataset, final Descriptor[] descriptors) {
		final DatasetDescriptor[] datasetDescriptors = new DatasetDescriptor[descriptors.length];

		for (int i = 0; i < descriptors.length; i++) {
			if (descriptors[i] == null) {
				// Skip null descriptor
				continue;
			}

			datasetDescriptors[i] = datasetDescriptorRepository.findByDatasetAndDescriptor(dataset, descriptors[i]);
			if (datasetDescriptors[i] == null) {
				LOG.info("Adding new dataset descriptor for " + descriptors[i]);
				final DatasetDescriptor dsd = new DatasetDescriptor();
				dsd.setDataset(dataset);
				dsd.setDescriptor(descriptors[i]);
				dsd.setOrderIndex(i);
				datasetDescriptors[i] = datasetDescriptorRepository.save(dsd);
			}
		}
		LOG.info("Done with dataset descriptors");
		return datasetDescriptors;
	}
}
