/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain;

import java.util.List;

import org.genesys2.server.model.impl.Country;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CountryRepository extends JpaRepository<Country, Long> {
	Country findByName(String name);

	// @Query("from Country c where c.code3= ?1 and c.current=true")
	Country findByCode3(String code3);

	Country findByCode2(String code2);

	// List<Country> findByRegion(Region region);

	@Query("from Country c where c.current=false")
	List<Country> findInactive();

	@Query("select count(c) from Country c where c.current = ?1")
	long countByCurrent(boolean current);

	List<Country> findByCurrent(boolean current);

	Page<Country> findByCurrent(boolean current, Pageable pageable);

	@Modifying
	@Query("update Country c set c.current=false")
	void deactivateAll();

	Country findByRefnameId(Long refnameId);

	@Query("select distinct c.refnameId from Country c where c.refnameId is not null")
	List<Long> listRefnameIds();

	@Query("select distinct itpgrfa.country from ITPGRFAStatus itpgrfa where itpgrfa.contractingParty = 'Yes'")
	List<Country> findITPGRFA();

	@Query("select distinct c from Country c where c.nameL like :pattern")
	List<Country> findWithI18N(@Param("pattern") String pattern);

	@Query("select distinct c from Country c where c.code3 like ?1 or c.code2 like ?1 or c.name like ?1 or c.nameL like ?1")
	List<Country> autocomplete(String ac, Pageable pageable);

	// @Query("select distinct c from Country c where c.region in ( ?1 )")
	// List<Country> findByRegions(List<Region> regions);
}
