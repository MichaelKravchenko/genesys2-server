/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain;

import java.util.List;

import org.genesys2.server.model.genesys.Taxonomy2;
import org.genesys2.server.model.impl.Crop;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface Taxonomy2Repository extends JpaRepository<Taxonomy2, Long> {

	@Query("select distinct t.genus from Taxonomy2 t where t.genus like ?1")
	List<String> autocompleteGenus(String term, Pageable page);

	@Query("select distinct t.genus from Taxonomy2 t join t.cropTaxonomies ct where t.genus like ?1 and ct.crop = ?2")
	List<String> autocompleteGenusByCrop(String term, Crop crop, Pageable page);

	@Query("select distinct t.species from Taxonomy2 t where t.species like ?1")
	List<String> autocompleteSpecies(String term, Pageable page);

	@Query("select distinct t.species from Taxonomy2 t join t.cropTaxonomies ct where t.species like ?1 and ct.crop = ?2")
	List<String> autocompleteSpeciesByCrop(String term, Crop crop, Pageable page);

	@Query("select distinct t.species from Taxonomy2 t where t.species like ?1 and t.genus in (?2)")
	List<String> autocompleteSpeciesByGenus(String term, List<String> genus, Pageable page);

	@Query("select distinct t.taxonName from Taxonomy2 t where t.taxonName like ?1")
	List<String> autocompleteTaxonomy(String term, Pageable page);

	Taxonomy2 findByGenusAndSpeciesAndSpAuthorAndSubtaxaAndSubtAuthor(String genus, String species, String spAuthor, String subtaxa, String subtAuthor);

	List<Taxonomy2> findByGenus(String genus);

	List<Taxonomy2> findByGenusAndSpecies(String genus, String species);

	List<Taxonomy2> findByGenusAndSpeciesAndSubtaxa(String genus, String species, String subtaxa);

}
