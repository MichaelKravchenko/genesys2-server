package org.genesys2.server.model.filters;

public class UnsupportedFilterOperation extends Exception {

	public UnsupportedFilterOperation(String message) {
		super(message);
	}

}
