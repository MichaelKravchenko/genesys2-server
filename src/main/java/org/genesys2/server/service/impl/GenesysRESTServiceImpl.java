/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.AccessionBreeding;
import org.genesys2.server.model.genesys.AccessionCollect;
import org.genesys2.server.model.genesys.AccessionGeo;
import org.genesys2.server.model.json.AccessionJson;
import org.genesys2.server.model.json.GenesysJsonFactory;
import org.genesys2.server.service.GenesysRESTService;
import org.genesys2.server.service.GenesysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class GenesysRESTServiceImpl implements GenesysRESTService {

	@Autowired
	private GenesysService genesysService;

	@Override
	public AccessionJson getAccessionJSON(String instCode, long accessionId) {
		final Accession accession = genesysService.getAccession(accessionId);
		if (accession == null) {
			return null;
		}
		if (!StringUtils.equalsIgnoreCase(accession.getInstituteCode(), instCode)) {
			throw new RuntimeException("Accession " + accessionId + " does not belong to " + instCode);
		}

		final AccessionJson aj = GenesysJsonFactory.from(accession);

		final AccessionBreeding breeding = genesysService.listAccessionBreeding(accession);
		if (breeding != null) {
			aj.setBredCode(breeding.getBreederCode());
			aj.setAncest(breeding.getPedigree());
		}
		final AccessionCollect collect = genesysService.listAccessionCollect(accession);
		aj.setColl(GenesysJsonFactory.from(collect));

		final AccessionGeo geo = genesysService.listAccessionGeo(accession);
		aj.setGeo(GenesysJsonFactory.from(geo));
		
		aj.setRemarks(GenesysJsonFactory.from(genesysService.listAccessionRemarks(accession)));
		
		return aj;
	}

	@Override
	public List<AccessionJson> getAccessionJSON(Collection<Long> accessionIds) {
		final List<AccessionJson> list = new ArrayList<AccessionJson>();

		for (final Accession accession : genesysService.listAccessions(accessionIds, new PageRequest(0, Integer.MAX_VALUE))) {
			list.add(GenesysJsonFactory.from(accession));
		}

		return list;
	}
}
