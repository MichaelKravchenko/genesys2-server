<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<html>
<head>
<title><spring:message code="admin.page.title" /></title>
</head>
<body>
	<h1>
		<spring:message code="admin.page.title" />
	</h1>
	
	<h3>Full-text Search</h3>
	<form method="post" action="<c:url value="/admin/clearTilesCache" />">
		<input type="submit" class="btn btn-default" value="Clear Tiles cache" />
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>
	
	<form method="post" action="<c:url value="/admin/reindex-elastic" />">
		<input type="text" name="startAt" disabled="true" />
		<label>
			<input type="checkbox" name="slow" value="false" /> No sleep
		</label>
		<input type="submit" class="btn btn-default" value="ES reindex" />
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>
	
	<form method="post" action="<c:url value="/admin/clear-queues" />">
		<input type="submit" class="btn btn-default" value="Clear ES update queues" />
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>
	
	<h3>Country data</h3>
	<form method="post" action="<c:url value="/admin/refreshCountries" />">
		<input type="submit" class="btn btn-default" value="Refresh country data" />
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>
	<form method="post" action="<c:url value="/admin/updateAlternateNames" />">
		<input type="submit" class="btn btn-default" value="Update alternate GEO names" />
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>
	<form method="post" action="<c:url value="/admin/updateITPGRFA" />">
		<input type="submit" class="btn btn-default" class="btn btn-default" value="Update country ITPGRFA status" />
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>

	
	
	<h3>WIEWS</h3>
	<form method="post" action="<c:url value="/admin/refreshWiews" />">
		<input type="submit" class="btn btn-default" value="Refresh WIEWS data" />
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>
	
	<h3>Svalbard Global Seed Vault</h3>
	<form method="post" action="<c:url value="/admin/updateSGSV" />">
		<input type="submit" class="btn btn-default" value="Update SGSV" />
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>
	
	<h3>Accession</h3>
	<form method="post" action="<c:url value="/admin/updateAccessionCountryRefs" />">
		<input type="submit" class="btn btn-default" class="btn btn-default" value="Update accession country info" />
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>
	<form method="post" action="<c:url value="/admin/updateInstituteCountryRefs" />">
		<input type="submit" class="btn btn-default" class="btn btn-default" value="Update WIEWS country info" />
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>
	<form method="post" action="<c:url value="/admin/updateAccessionInstituteRefs" />">
		<input type="submit" class="btn btn-default" value="Update accession institute info" />
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>
	<form method="post" action="<c:url value="/admin/convertNames" />">
		<input type="submit" class="btn btn-default" value="Convert old names to aliases" />
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>
	<form method="post" action="<c:url value="/admin/scanStorage" />">
		<input type="submit" class="btn btn-default" class="btn btn-default" value="Scan and convert STORAGE" />
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>
	<h3>C&E</h3>
	<form method="post" action="<c:url value="/admin/refreshMetadataMethods" />">
		<input type="submit" class="btn btn-default" class="btn btn-default" value="Recalculate metadata methods" />
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>
	
	<h3>Content</h3>
	<form method="post" action="<c:url value="/admin/sanitize" />">
		<input type="submit" class="btn btn-default" value="Sanitize HTML content" />
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>
	
	<h3>Full-text Search</h3>
	<form method="post" action="<c:url value="/admin/reindexEverything" />">
		<input type="submit" class="btn btn-default" class="btn btn-default" value="Reindex search indexes" />
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>

	<form method="post" action="<c:url value="/admin/reindexEntity" />">
		<select name="entity">
			<option value="org.genesys2.server.model.impl.Country">Countries</option>
			<option value="org.genesys2.server.model.impl.FaoInstitute">WIEWS Institutes</option>
			<option value="org.genesys2.server.model.impl.ActivityPost">Posts</option>
			<option value="org.genesys2.server.model.impl.Article">Articles</option>
			<option value="org.genesys2.server.model.impl.Organization">Organizations</option>
			<option value="org.genesys2.server.model.genesys.Accession">Accessions</option>
			<option value="org.genesys2.server.model.genesys.AccessionAlias">Accession alias</option>
			<option value="org.genesys2.server.model.genesys.Taxonomy2">Taxonomy2</option>
		</select> <input type="submit" class="btn btn-default" value="Reindex search indexes" />
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>

</body>
</html>