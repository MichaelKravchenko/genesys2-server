<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title>${title}</title>
</head>
<body>
	<h1>
		<spring:message code="article.edit-article" />
	</h1>

	<form role="form" class="" action="<c:url value="/content/save-article" />" method="post">
		<c:if test="${article.id ne null}">
			<input type="hidden" name="id" value="${article.id}" />
		</c:if>
		<div class="form-group">
			<label for="article-slug" class="control-label"><spring:message code="article.slug" /></label>
			<div class="controls">
				<input type="text" id="article-slug" name="slug" value="<c:out value="${article.slug}" />" class="span9 form-control required" />
			</div>
		</div>
		<div class="form-group">
			<label for="article-title" class="control-label"><spring:message code="article.title" /></label>
			<div class="controls">
				<input type="text" id="article-title" name="title" value="<c:out value="${article.title}" />" class="span9 form-control required" />
			</div>
		</div>
		<div class="form-group">
			<label for="article-body" class="control-label"><spring:message code="article.body" /></label>
			<div class="controls">
				<textarea id="article-body" name="body" class="span9 required form-control html-editor"><c:out value="${article.body}" escapeXml="false" /></textarea>
			</div>
		</div>

		<input type="submit" value="<spring:message code="save"/>" class="btn btn-primary" />
		<a href="<c:url value="${article.id ne null ? '/content/'.concat(article.slug) : '/' }" />" class="btn btn-default">Cancel</a>
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>

<content tag="javascript">	
	<script type="text/javascript" src="/html/js/tinymce/tinymce.min.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			if (window.location.hash=='#raw') {
			} else {
				tinyMCE.init({
					selector : ".html-editor",
					menubar : false,
					statusbar : false,
					height : 200,
					plugins: "link autolink code",
					directionality : document.dir
				});
			}
		});
	</script>
</content>
</body>
</html>