<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="traits.page.title" /></title>
</head>
<body>
	<h1>
		<spring:message code="trait-list" />
	</h1>

	<div class="main-col-header clearfix">
	<div class="nav-header pull-left">
		<div class="results"><spring:message code="paged.totalElements" arguments="${pagedData.totalElements}" /></div>
		<form method="get" action="">
			<div class="pagination">
				<spring:message code="paged.pageOfPages" arguments="${pagedData.number+1},${pagedData.totalPages}" />
				<a href="<spring:url value=""><spring:param name="page" value="${pagedData.number eq 0 ? 1 : pagedData.number}" /></spring:url>"><spring:message code="pagination.previous-page" /></a>
				<input class="form-control" style="display: inline; max-width: 5em; text-align: center" type="text" name="page" placeholder="${pagedData.number + 1}" />
				<a href="<spring:url value=""><spring:param name="page" value="${pagedData.number+2}" /></spring:url>"><spring:message code="pagination.next-page" /></a>
			</div>
		</form>
	</div>
	</div>
	
	<c:if test="${crop ne null}">
		<div class="applied-filters">
			<spring:message code="filter.crop" />: <b><a href="<c:url value="/c/${crop.shortName}" />">${crop.getName(pageContext.response.locale)}</a></b>
		</div>
	</c:if>

	<table class="accessions">
		<thead>
			<tr>
				<td class="idx-col" />
				<td>Trait</td>
				<td>Crop</td>
				<td><spring:message code="descriptor.category" /></td>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${pagedData.content}" var="trait" varStatus="status">
				<tr class="${status.count % 2 == 0 ? 'even' : 'odd'}">
					<td class="idx-col">${status.count + pagedData.size * pagedData.number}</td>
					<td><a href="<c:url value="/descriptors/${trait.id}" />">${trait.getTitle(pageContext.response.locale)}</a></td>
					<td><a href="<c:url value="/c/${trait.crop.shortName}" />">${trait.crop.getName(pageContext.response.locale)}</a></td>
					<td><c:out value="${trait.category.getName(pageContext.response.locale)}" /></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>