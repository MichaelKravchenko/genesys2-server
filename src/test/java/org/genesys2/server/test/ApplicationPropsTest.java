/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.test;

import static org.junit.Assert.assertTrue;

import org.genesys2.server.config.ApplicationProps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ApplicationPropsTest.Config.class, initializers = PropertyPlacholderInitializer.class)
public class ApplicationPropsTest {

	@PropertySource({ "classpath:application.properties", "classpath:/spring/spring.properties" })
	public static class Config {

		@Bean
		public ApplicationProps applicationProps() {
			return new ApplicationProps();
		}

	}

	@Autowired
	private ApplicationProps applicationProps;

	@Test
	public void testCdn() {
		assertTrue(applicationProps.getCdnServer() != null);
	}

	@Test
	public void testTileserverCdn() {
		assertTrue(applicationProps.getTileserverCdn() != null);
		assertTrue(applicationProps.getTileserverCdn().startsWith("'"));
		assertTrue(applicationProps.getTileserverCdn().endsWith("'"));
	}
}
