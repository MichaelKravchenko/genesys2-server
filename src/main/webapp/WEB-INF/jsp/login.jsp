<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
    <title><spring:message code="page.login"/></title>
</head>
<body>
	<h1><spring:message code="page.login" /></h1>
    <c:if test="${param['error'] ne null}">
        <div class="alert alert-danger"><spring:message code="login.invalid-credentials"/></div>
    </c:if>
    <c:if test="${error ne null}">
        <div class="alert alert-danger"><spring:message code="login.invalid-token"/></div>
    </c:if>

    <form role="form" method="POST" action="/login-attempt" class="form-horizontal">
        <div class="form-group">
            <label for="j_username" class="col-lg-2 control-label"><spring:message code="login.username"/></label>
            <div class="col-lg-3">
                <input type="text" id="j_username" name="j_username" class="form-control grabfocus" autofocus="autofocus" />
            </div>
        </div>

        <div class="form-group">
            <label for="j_password"  class="col-lg-2 control-label"><spring:message code="login.password"/></label>
            <div class="col-lg-3">
                <input type="password" id="j_password" name="j_password" class="form-control" />
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-3">
            <label class="" for='_spring_security_remember_me'>
                <input type='checkbox' name='_spring_security_remember_me'  id='_spring_security_remember_me'/>
                <spring:message code="login.remember-me"/>
            </label>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
	            <input type="submit" value="<spring:message code="login.login-button" />" class="btn btn-primary" />
	            <a href="<c:url value="/google/login" />" class="btn btn-default google-signin"><spring:message code="login.with-google-plus"/></a>
	            <a href="registration" id="registration" class="btn btn-default"><spring:message code="login.register-now"/></a>
	           	<a href="forgot-password" id="forgot-password" class="btn"><spring:message code="login.forgot-password"/></a>
        	</div>
        </div>
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>
</body>
</html>