/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.rdf;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.bioversityinternational.model.germplasm.CCO;
import org.bioversityinternational.model.rdf.skos.SKOS;
import org.genesys2.server.model.genesys.Method;
import org.genesys2.server.model.genesys.Parameter;
import org.genesys2.server.model.genesys.ParameterCategory;
import org.genesys2.server.model.genesys.TraitCode;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.service.TraitService;
import org.genesys2.spring.ResourceNotFoundException;
//import org.genesys2.server.model.genesys.Metadata;  // not yet integrated...
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.RDFS;

/**
 * Controller which simply handles RDF Turtle meta-data requests
 */
@Controller
@RequestMapping(value = "/descriptors", method = RequestMethod.GET, headers = "accept=text/turtle", produces = RdfBaseController.RDF_MEDIATYPE_TURTLE)
public class DescriptorControllerRdf extends RdfBaseController {

	public final String DESCRIPTOR_NS = "/descriptors/";;
	public final String CATEGORY_NS = DESCRIPTOR_NS + "categories/";
	public final String TRAIT_NS = DESCRIPTOR_NS + "trait/";
	public final String METHOD_NS = DESCRIPTOR_NS + "method/";
	public final String SCALE_NS = DESCRIPTOR_NS + "scale/";

	@Autowired
	private TraitService traitService;

	/**
	 * Method to retrieve list of all descriptor meta-data recorded in the
	 * database, formatted as Turtle RDF.
	 *
	 * @return List of RDF Turtle formatted trait properties
	 */
	@RequestMapping
	public @ResponseBody
	Object dumpAll() {

		final Model model = startModel();

		wrapCategories(model);

		for (final Parameter trait : traitService.listTraits()) {
			wrapTrait(model, trait);
		}

		for (final Method method : traitService.listMethods()) {
			final Parameter trait = method.getParameter();
			wrapMethod(model, trait, method);
		}
		return endModel(model);
	}

	/*
	 * Creates and annotates a Method subject
	 */
	private Resource categorySubject(Model model, ParameterCategory category) {
		return createSubject(model, category, CATEGORY_NS + category.getId(), category.getName());
	}

	/*
	 * Method generates RDF for one trait category record as RDF.
	 */
	private void wrapCategories(Model model) {
		final List<ParameterCategory> categories = traitService.listCategories();
		for (final ParameterCategory category : categories) {
			// Insert category record into RDF model

			final Resource subject = categorySubject(model, category);

			CCO.Vocabulary.set(model, subject, CCO.Vocabulary.CATEGORY, "Plant");
			subject.addProperty(SKOS.broader, CCO.TraitCategory);

			final Map<String, String> vernacularNameMap = category.getLocalNameMap();
			for (final String language : vernacularNameMap.keySet()) {
				final Literal vernacularNameLiteral = model.createLiteral(vernacularNameMap.get(language), language);
				subject.addLiteral(SKOS.prefLabel, vernacularNameLiteral);
			}
		}
	}

	/**
	 * Method to publish ParameterCategory meta-data as RDF
	 *
	 * @param model
	 * @return RDF formatted list of categories
	 *
	 *         Exemplar:
	 *
	 *         cco:MorphologyTrait rdfs:subClassOf cco:TraitCategory ;
	 *         skos:broader cco:TraitCategory ; skos:prefLabel "Morphology" @en;
	 *         skos:definition
	 *         "Category of trait pertaining to the anatomy and form of a plant"
	 *         @en; skos:prefLabel "Morfología" @es; skos:prefLabel "Mорфология"
	 *         @ru; skos:prefLabel "形态学" @zh; skos:inScheme
	 *         cco:PlantTraitCategoryVocabulary ; dc:hasVersion "Revision: 0.1"
	 *         ; dc:creator "Global Crop Diversity Trust";
	 */
	@RequestMapping(value = "/categories")
	public @ResponseBody
	String dumpCategories() {

		final Model model = startModel();

		wrapCategories(model);

		return endModel(model);
	}

	/**
	 * Trait Turtle RDF format Exemplar:
	 *
	 * cco:MusaTrait a skos:Concept; cco:category cco:GeneralTrait ;
	 * skos:prefLabel "Musa taxonomic traits"@en; skos:definition
	 * "Nomenclature for Characterization traits for species in the Musa taxa."
	 * @en; skos:prefLabel "Rasgos taxonómicos de Musa."@es; skos:definition
	 * "Nomenclatura de los rasgos de caracterización de las especies en el taxón Musa."
	 * @es; skos:prefLabel "Муса таксономические черты"@ru; skos:definition
	 * "Номенклатура для характеристики признаков для видов в таксонов Муса."
	 * @ru; skos:prefLabel "穆萨分类学特征"@zh; skos:definition "命名为表征特性的物种的穆萨类群"@zh;
	 * skos:inScheme cco:MusaTraitVocabulary; dc:creator
	 * "Bioversity International";
	 */

	/*
	 * Creates and annotates a Method subject
	 */
	private Resource traitSubject(Model model, Parameter trait) {
		return createProperty(model, trait, DESCRIPTOR_NS + trait.getId(), trait.getTitle());
	}

	/*
	 * Method generates RDF for one trait record as RDF.
	 */
	private void wrapTrait(Model model, Parameter trait) {

		final Resource traitSubject = traitSubject(model, trait);

		final String seeAlso = trait.getRdfUri();
		if (seeAlso != null) {
			traitSubject.addProperty(RDFS.seeAlso, model.createResource(seeAlso));
		}

		final Crop crop = trait.getCrop();
		CCO.Vocabulary.set(model, traitSubject, CCO.Vocabulary.TRAIT, crop.getShortName());

		final ParameterCategory category = trait.getCategory();
		if (category != null) {
			final Resource categorySubject = categorySubject(model, category);
			traitSubject.addProperty(CCO.category, categorySubject);
		}

		final Map<String, String> vernacularTitleMap = trait.getLocalTitleMap();

		for (final String language : vernacularTitleMap.keySet()) {
			final Literal vernacularNameLiteral = model.createLiteral(vernacularTitleMap.get(language), language);
			traitSubject.addLiteral(SKOS.prefLabel, vernacularNameLiteral);
		}

		/*
		 * TODO - The Parameter table doesn't yet appear to track trait
		 * descriptions...
		 *
		 * Map<String,String> vernacularTitleMap = trait.getLocalTitleMap() ;
		 *
		 * for( String language : vernacularDefinitionMap.keySet() ) { Literal
		 * vernacularNameLiteral =
		 * model.createLiteral(vernacularTitleMap.get(language), language) ;
		 * subject.addLiteral(SKOS.prefLabel,vernacularNameLiteral) ; }
		 */

	}

	/**
	 * Method to retrieve list of all trait properties in the database,
	 * formatted as Turtle RDF
	 *
	 * @return List of RDF Turtle formatted trait properties
	 */
	@RequestMapping("/traits")
	public @ResponseBody
	Object dumpTraits() {

		final Model model = startModel();

		for (final Parameter trait : traitService.listTraits()) {
			wrapTrait(model, trait);
		}

		return endModel(model);
	}

	/**
	 * Method to publish crop trait property ("Parameter") meta-data as
	 * multi-lingual RDF
	 *
	 * @param PathVariable
	 *            long traitId
	 * @return RDF formatted list of categories
	 */
	@RequestMapping("/{traitId}")
	public @ResponseBody
	Object dumpTrait(@PathVariable long traitId, HttpServletResponse response) {

		final Parameter trait = traitService.getTrait(traitId);

		if (trait == null) {
			throw new ResourceNotFoundException();
		}

		final Model model = startModel();

		wrapTrait(model, trait);

		if (trait.getLastModifiedDate() != null) {
			response.setDateHeader("Last-Modified", trait.getLastModifiedDate().getTime());
		}

		return endModel(model);
	}

	/**
	 * Trait Method RDF Exemplar (from the Bioversity model... some properties
	 * may not be generated from the GENESYS database)
	 *
	 * cco:fruitLengthMethod a skos:Concept; skos:broader cco:MusaTraitMethod;
	 * skos:related cco:fruitLength ; cco:trait cco:fruitLength ;
	 * dcterms:created "2014/01/14 12:00:00"; dcterms:modified
	 * "2014/01/14 12:00:00"; skos:prefLabel "Fruit length method"@en;
	 * skos:definition "Measured length of the fruit, without pedicel."@en;
	 * skos:prefLabel "Método para Longitud de los frutos  [cm]"@es;
	 * skos:definition "Medir el longitud del fruto, sin el pedicelo."@es;
	 * skos:prefLabel "Фрукты метод длина"@ru; skos:definition
	 * "Измеряется длина плода, без плодоножки."@ru; skos:prefLabel "果长法"@zh;
	 * skos:definition "测得的果实长度，无花梗"@zh; skos:inScheme
	 * cco:MusaTraitMethodVocabulary .
	 *
	 * cco:fruitLengthMethod2 a skos:Concept; skos:broader cco:MusaTraitMethod;
	 * skos:related cco:fruitLength ; cco:trait cco:fruitLength ;
	 * dcterms:created "2014/01/14 12:00:00"; dcterms:modified
	 * "2014/01/14 12:00:00"; skos:prefLabel
	 * "Alternative fruit length method"@en; skos:definition
	 * "Measured length of the fruit, with the pedicel."@en; skos:prefLabel
	 * "Método alternativo para longitud de los frutos  [cm]"@es;
	 * skos:definition "Medir el longitud del fruto, con el pedicelo."@es;
	 * skos:prefLabel "Альтернативный метод длина фрукты"@ru; skos:definition
	 * "Измеряется длина плода, с плодоножки."@ru; skos:prefLabel
	 * "另类水果长度的方法"@zh; skos:definition "测得的果实长度，与花梗"@zh; skos:inScheme
	 * cco:MusaTraitMethodVocabulary .
	 *
	 * cco:fruitLengthScale a skos:Concept; skos:broader cco:MusaTraitScale ;
	 * skos:related cco:fruitLengthMethod, cco:fruitLengthMethod2 ; cco:method
	 * cco:fruitLengthMethod, cco:fruitLengthMethod2 ; skos:prefLabel
	 * "Fruit length scale"@en; skos:prefLabel
	 * "Escala Longitud de los frutos  [cm] "@es; dcterms:created
	 * "2014/01/14 12:00:00"; dcterms:modified "2014/01/14 12:00:00";
	 * skos:inScheme cco:MusaTraitScaleVocabulary ; skos:definition
	 * "1 =<15 cm, 2 16- 20 cm, 3 21- 25 cm, 4 26- 30 cm, 5 >=31 cm"@en;
	 * skos:definition
	 * "1 =< 15 cm, 2 16-20 cm, 3 21-25 cm, 4 26-30 cm, 5 > =31 cm"@es;
	 * cco:scaleType "Nominal"; cco:scaleUnit cco:fruitLengthScaleValues .
	 *
	 * cco:fruitLengthScaleValues a skos:Collection; skos:prefLabel
	 * "Fruit length scale values"@en ; skos:prefLabel
	 * "Valores para Longitud de los frutos  [cm]"@es ; dcterms:created
	 * "2014/01/14 12:00:00"; dcterms:modified "2014/01/14 12:00:00";
	 * skos:definition "Controlled list of values for Fruit length"@en;
	 * skos:definition
	 * "Lista controlada de valores para Longitud de los frutos  [cm]"@es;
	 * skos:member cco:fruitLengthScaleValue1, cco:fruitLengthScaleValue2,
	 * cco:fruitLengthScaleValue3, cco:fruitLengthScaleValue4,
	 * cco:fruitLengthScaleValue5 .
	 *
	 * cco:fruitLengthScaleValue1 a cco:IntegerValueRange ; cco:integerMinValue
	 * "0"^^xsd:integer ; cco:integerMaxValue "15"^^xsd:integer ; cco:scaleUnit
	 * "cm" ; skos:broader cco:fruitLengthScale; dcterms:created
	 * "2014/01/14 12:00:00"; dcterms:modified "2014/01/14 12:00:00";
	 * skos:prefLabel "=<15 cm"@en ; skos:prefLabel "=<15 cm"@es ; skos:altLabel
	 * "1" ; skos:definition "=<15 cm"@en ; skos:definition "=<15 cm"@es ;
	 * skos:inScheme cco:MusaTraitScaleVocabulary .
	 *
	 */

	/*
	 * Creates and annotates a Method subject
	 */
	private Resource[] traitAndMethodSubjects(Model model, Parameter trait, Method method) {
		final Resource[] subjects = new Resource[3];
		subjects[0] = traitSubject(model, trait);
		subjects[1] = createProperty(model, method, DESCRIPTOR_NS + trait.getId() + "/" + method.getId(), trait.getTitle() + " Method");
		subjects[2] = createProperty(model, method,
		/*
		 * not sure if this is a complete enough specification for a Scale
		 * URI...
		 */
		SCALE_NS + trait.getId() + "/" + method.getId(), trait.getTitle() + " Scale");
		return subjects;
	}

	/*
	 * Method generates RDF for one trait method.
	 */
	private void wrapMethod(Model model, Parameter trait, Method method) {

		final Resource[] subjects = traitAndMethodSubjects(model, trait, method);
		final Resource traitSubj = subjects[0];
		final Resource methodSubj = subjects[1];
		final Resource scaleSubj = subjects[2];

		final String seeAlso = method.getRdfUri();
		if (seeAlso != null) {
			methodSubj.addProperty(RDFS.seeAlso, model.createResource(seeAlso));
		}

		final Crop crop = trait.getCrop();
		CCO.Vocabulary.set(model, methodSubj, CCO.Vocabulary.METHOD, crop.getShortName());

		methodSubj.addProperty(SKOS.related, traitSubj);
		methodSubj.addProperty(CCO.trait, traitSubj);

		/*
		 *
		 * TODO - The Parameter table doesn't yet appear to track method
		 * names...
		 *
		 * Map<String,String> vernacularTitleMap = trait.getLocalTitleMap() ;
		 *
		 * for( String language : vernacularDefinitionMap.keySet() ) { Literal
		 * vernacularNameLiteral =
		 * model.createLiteral(vernacularTitleMap.get(language), language) ;
		 * subject.addLiteral(SKOS.prefLabel,vernacularNameLiteral) ; }
		 */

		// Method "definitions" are in the "method" field...in the vernacular
		final Map<String, String> vernacularMethodMap = method.getLocalMethodMap();
		if (!(vernacularMethodMap == null || vernacularMethodMap.isEmpty())) {
			for (final String language : vernacularMethodMap.keySet()) {
				final String vernacularMethod = vernacularMethodMap.get(language);
				if (vernacularMethod == null) {
					continue;
				}
				final Literal vernacularNameLiteral = model.createLiteral(vernacularMethod, language);
				methodSubj.addLiteral(SKOS.prefLabel, vernacularNameLiteral);
			}
		}

		/*
		 * List<Metadata> metadata = traitService.listMetadataByMethod(method);
		 */

		final Resource scaleVocabulary = CCO.Vocabulary.set(model, scaleSubj, CCO.Vocabulary.SCALE, crop.getShortName());

		scaleSubj.addProperty(SKOS.related, methodSubj);
		scaleSubj.addProperty(CCO.method, methodSubj);

		final String units = method.getUnit();

		// Options associated with scale...
		if (method.isCoded()) {
			wrapScaleValues(model, trait, method, scaleSubj, scaleVocabulary, units);

			// Not sure what to do with Method statistics for now...
			// model.addAttribute("codeStatistics",
			// traitService.getMethodStatistics(method));
		} else {
			if (!(units == null || units.isEmpty())) {
				scaleSubj.addProperty(CCO.scaleUnit, units);
			}
		}

	}

	/*
	 * RDF of a Nominal Scale associated with a given method
	 *
	 * cco:fruitLengthScale a skos:Concept; skos:broader cco:MusaTraitScale ;
	 * skos:related cco:fruitLengthMethod, cco:fruitLengthMethod2 ; cco:method
	 * cco:fruitLengthMethod, cco:fruitLengthMethod2 ; skos:prefLabel
	 * "Fruit length scale"@en; skos:prefLabel
	 * "Escala Longitud de los frutos  [cm] "@es; dcterms:created
	 * "2014/01/14 12:00:00"; dcterms:modified "2014/01/14 12:00:00";
	 * skos:inScheme cco:MusaTraitScaleVocabulary ; skos:definition
	 * "1 =<15 cm, 2 16- 20 cm, 3 21- 25 cm, 4 26- 30 cm, 5 >=31 cm"@en;
	 * skos:definition
	 * "1 =< 15 cm, 2 16-20 cm, 3 21-25 cm, 4 26-30 cm, 5 > =31 cm"@es;
	 * cco:scaleType "Nominal"; cco:scaleUnit cco:fruitLengthScaleValues .
	 *
	 * cco:fruitLengthScaleValues a skos:Collection; skos:prefLabel
	 * "Fruit length scale values"@en ; skos:prefLabel
	 * "Valores para Longitud de los frutos  [cm]"@es ; dcterms:created
	 * "2014/01/14 12:00:00"; dcterms:modified "2014/01/14 12:00:00";
	 * skos:definition "Controlled list of values for Fruit length"@en;
	 * skos:definition
	 * "Lista controlada de valores para Longitud de los frutos  [cm]"@es;
	 * skos:member cco:fruitLengthScaleValue1, cco:fruitLengthScaleValue2,
	 * cco:fruitLengthScaleValue3, cco:fruitLengthScaleValue4,
	 * cco:fruitLengthScaleValue5 .
	 *
	 *
	 * cco:fruitLengthScaleValue1 a cco:IntegerValueRange ; cco:integerMinValue
	 * "0"^^xsd:integer ; cco:integerMaxValue "15"^^xsd:integer ; cco:scaleUnit
	 * "cm" ; skos:broader cco:fruitLengthScale; dcterms:created
	 * "2014/01/14 12:00:00"; dcterms:modified "2014/01/14 12:00:00";
	 * skos:prefLabel "=<15 cm"@en ; skos:prefLabel "=<15 cm"@es ; skos:altLabel
	 * "1" ; skos:definition "=<15 cm"@en ; skos:definition "=<15 cm"@es ;
	 * skos:inScheme cco:MusaTraitScaleVocabulary .
	 */
	private void wrapScaleValues(Model model, Parameter trait, Method method, Resource scaleSubj, Resource scaleVocabulary, String units) {

		final Resource scaleValuesSubj = createProperty(model, method, SCALE_NS + trait.getId() + "/" + method.getId(), trait.getTitle() + " Scale Values", true // is
																																							// a
																																							// SKOS
																																							// Collection...
		);
		scaleSubj.addProperty(CCO.scaleType, "Nominal");
		scaleSubj.addProperty(CCO.scaleUnit, scaleValuesSubj);
		scaleValuesSubj.addProperty(SKOS.inScheme, scaleVocabulary);

		// Enumerate da Options....
		final String options = method.getOptions();
		scaleSubj.addProperty(SKOS.definition, options);
		final Map<String, String> codeMap = TraitCode.parseCodeMap(options);
		int n = 1;
		for (final String code : codeMap.keySet()) {
			final Resource valueSubj = createProperty(model, method, SCALE_NS + trait.getId() + "/" + method.getId() + "/" + n, trait.getTitle() + " Scale Value" + n);
			scaleValuesSubj.addProperty(SKOS.member, valueSubj);
			valueSubj.addProperty(SKOS.broader, scaleSubj);
			valueSubj.addProperty(SKOS.inScheme, scaleVocabulary);
			valueSubj.addProperty(SKOS.altLabel, code);

			// TODO: do I need to use skos:definition and
			// have vernacular options? Nah! Overkill for now!
			valueSubj.addProperty(SKOS.prefLabel, codeMap.get(code));

			if (!(units == null || units.isEmpty())) {
				valueSubj.addProperty(CCO.scaleUnit, units);
			}
			n += 1;
		}
	}

	/**
	 * Method to retrieve list of all trait properties in the database,
	 * formatted as Turtle RDF
	 *
	 * @return List of RDF Turtle formatted trait properties
	 */
	@RequestMapping("/methods")
	public @ResponseBody
	Object dumpMethods() {

		final Model model = startModel();

		for (final Method method : traitService.listMethods()) {
			final Parameter trait = method.getParameter();
			wrapMethod(model, trait, method);
		}
		return endModel(model);
	}

	/**
	 * Method to publish a single crop trait method and associated scale
	 * meta-data, as multi-lingual RDF
	 *
	 * @param traitId
	 * @param methodId
	 * @return RDF formatted list of categories
	 */
	@RequestMapping("/{traitId}/{methodId}")
	public @ResponseBody
	Object dumpMethod(@PathVariable long traitId, @PathVariable long methodId, HttpServletResponse response) {

		final Parameter trait = traitService.getTrait(traitId);

		if (trait == null) {
			throw new ResourceNotFoundException();
		}

		final Method method = traitService.getMethod(methodId);
		if (method == null) {
			throw new ResourceNotFoundException();
		}

		if (!method.getParameter().getId().equals(trait.getId())) {
			_logger.warn("Method does not belong to Param");
			throw new ResourceNotFoundException();
		}

		final Model model = startModel();

		wrapTrait(model, trait);
		wrapMethod(model, trait, method);

		if (method.getLastModifiedDate() != null) {
			response.setDateHeader("Last-Modified", method.getLastModifiedDate().getTime());
		}

		return endModel(model);

	}
}
