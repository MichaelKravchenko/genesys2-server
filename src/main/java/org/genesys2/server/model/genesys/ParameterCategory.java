/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.genesys;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Transient;

import org.genesys2.server.model.GlobalVersionedAuditedModel;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Entity(name = "parametercategory")
public class ParameterCategory extends GlobalVersionedAuditedModel {
	/**
	 *
	 */
	private static final long serialVersionUID = -1240268191254360894L;

	@Column(length = 100)
	private String name;

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String nameL;

	@Transient
	private JsonNode nameJ;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameL() {
		return nameL;
	}

	public void setNameL(String nameL) {
		this.nameL = nameL;
	}

	public String getName(Locale locale) {
		return getNameLocal(locale);
	}

	private synchronized void parseNameLocal() {
		if (this.nameJ == null && this.nameL != null) {
			final ObjectMapper mapper = new ObjectMapper();
			try {
				this.nameJ = mapper.readTree(nameL);
			} catch (final IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Transient
	private final Map<String, String> nameMap = new HashMap<String, String>();

	/**
	 * Method to return the map of available languages keys
	 *
	 * @return Map<String,String> with language code as the key and the
	 *         vernacular string as the value.
	 */
	public Map<String, String> getLocalNameMap() {
		return buildLocalNameMap();
	}

	private synchronized Map<String, String> buildLocalNameMap() {

		if (this.nameMap.isEmpty() && this.nameL != null) {
			parseNameLocal();
			final Iterator<String> languages = this.nameJ.fieldNames();
			while (languages.hasNext()) {
				final String language = languages.next();
				nameMap.put(language, this.nameJ.get(language).textValue());
			}
		}
		return nameMap;
	}

	private synchronized String getNameLocal(Locale locale) {
		parseNameLocal();

		return this.nameJ != null && this.nameJ.has(locale.getLanguage()) ? this.nameJ.get(locale.getLanguage()).textValue() : this.name;
	}
}
