package org.genesys2.server.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.OrFilterBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.SortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.genesys2.server.model.elastic.AccessionDetails;
import org.genesys2.server.model.filters.GenesysFilter;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.service.ElasticService;
import org.genesys2.server.service.FilterConstants;
import org.genesys2.server.service.GenesysFilterService;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.OrganizationService;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilter;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.genesys2.server.service.impl.FilterHandler.FilterValue;
import org.genesys2.server.service.impl.FilterHandler.LiteralValueFilter;
import org.genesys2.server.service.impl.FilterHandler.MaxValueFilter;
import org.genesys2.server.service.impl.FilterHandler.MinValueFilter;
import org.genesys2.server.service.impl.FilterHandler.StartsWithFilter;
import org.genesys2.server.service.impl.FilterHandler.ValueRangeFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.FacetedPage;
import org.springframework.data.elasticsearch.core.facet.FacetRequest;
import org.springframework.data.elasticsearch.core.facet.request.TermFacetRequestBuilder;
import org.springframework.data.elasticsearch.core.facet.result.TermResult;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class ElasticsearchSearchServiceImpl implements ElasticService, InitializingBean {
	private static final Logger LOG = LoggerFactory.getLogger(ElasticsearchSearchServiceImpl.class);

	@Autowired
	private ElasticsearchTemplate elasticsearchTemplate;

	@Autowired
	private GenesysService genesysService;

	@Autowired
	private OrganizationService organizationService;

	@Autowired
	private GenesysFilterService filterService;

	@Autowired
	private FilterHandler filterHandler;

	@Autowired
	private ObjectMapper objectMapper;

	private final Map<String, Class<?>> clazzMap;

	public ElasticsearchSearchServiceImpl() {
		clazzMap = new HashMap<String, Class<?>>();
		clazzMap.put(Accession.class.getName(), AccessionDetails.class);
	}

	@Override
	public Page<AccessionDetails> search(String query, Pageable pageable) throws SearchException {
		SearchQuery searchQuery = new NativeSearchQueryBuilder().withQuery(org.elasticsearch.index.query.QueryBuilders.queryString(query))
				.withPageable(pageable).build();

		try {
			Page<AccessionDetails> sampleEntities = elasticsearchTemplate.queryForPage(searchQuery, AccessionDetails.class);
			return sampleEntities;
		} catch (Throwable e) {
			throw new SearchException(e.getMessage(), e);
		}
	}

	@Override
	public Page<AccessionDetails> filter(AppliedFilters appliedFilters, Pageable pageable) throws SearchException {

		AndFilterBuilder filterBuilder = getFilterBuilder(appliedFilters);

		SortBuilder sortBuilder = SortBuilders.fieldSort("acceNumb").order(SortOrder.ASC);
		SearchQuery searchQuery = new NativeSearchQueryBuilder().withFilter(filterBuilder).withSort(sortBuilder).withPageable(pageable).build();

		try {
			Page<AccessionDetails> sampleEntities = elasticsearchTemplate.queryForPage(searchQuery, AccessionDetails.class);
			return sampleEntities;
		} catch (Throwable e) {
			throw new SearchException(e.getMessage(), e);
		}
	}

	@Override
	public TermResult termStatistics(AppliedFilters appliedFilters, String term, int size) throws SearchException {

		AndFilterBuilder filterBuilder = getFilterBuilder(appliedFilters);

		FacetRequest termFacetRequest;
		if (FilterConstants.DUPLSITE.equals(term)) {
			termFacetRequest = new TermFacetRequestBuilder("f").applyQueryFilter().fields(term).excludeTerms("NOR051").size(size).build();
		} else if (FilterConstants.SGSV.equals(term)) {
			termFacetRequest = new TermFacetRequestBuilder("f").applyQueryFilter().fields(FilterConstants.IN_SGSV).size(size).build();
		} else {
			termFacetRequest = new TermFacetRequestBuilder("f").applyQueryFilter().fields(term).size(size).build();
		}
		SearchQuery searchQuery = new NativeSearchQueryBuilder().withFilter(filterBuilder).withFacet(termFacetRequest).build();

		try {
			FacetedPage<AccessionDetails> page = elasticsearchTemplate.queryForPage(searchQuery, AccessionDetails.class);
			return (TermResult) page.getFacet("f");

		} catch (Throwable e) {
			throw new SearchException(e.getMessage(), e);
		}
	}

	/**
	 * Runs TermFacet, but will automatically increase size if #otherCount is
	 * more than 10%
	 */
	@Override
	public TermResult termStatisticsAuto(AppliedFilters appliedFilters, String term, int size) throws SearchException {
		TermResult termResult = null;
		int newSize = size;
		do {
			termResult = termStatistics(appliedFilters, term, newSize);

			// Avoid div/0
			if (termResult.getTotalCount() == 0)
				break;

			double otherPerc = (double) termResult.getOtherCount() / (termResult.getMissingCount() + termResult.getTotalCount());
			if (otherPerc < 0.1)
				break;
			newSize += size + Math.max(1, (size / 3));
		} while (newSize < 2 * size);
		return termResult;
	}

	private AndFilterBuilder getFilterBuilder(AppliedFilters appliedFilters) {
		AndFilterBuilder filterBuilder = FilterBuilders.andFilter();
		for (AppliedFilter appliedFilter : appliedFilters) {

			String key = appliedFilter.getFilterName();

			GenesysFilter genesysFilter = filterHandler.getFilter(key);

			if (genesysFilter == null) {
				LOG.warn("No such filter " + key);
				continue;
			}

			// Filter-level OR
			OrFilterBuilder orFilter = FilterBuilders.orFilter();

			// null
			if (appliedFilter.getWithNull()) {
				orFilter.add(FilterBuilders.missingFilter(key));
			}

			Set<FilterValue> filterValues = appliedFilter.getValues();
			if (filterValues != null && !filterValues.isEmpty()) {

				{
					// Handle literals
					Set<Object> literals = new HashSet<Object>();
					for (FilterValue filterValue : filterValues) {
						if (filterValue instanceof FilterHandler.LiteralValueFilter) {
							FilterHandler.LiteralValueFilter literal = (LiteralValueFilter) filterValue;
							literals.add(literal.getValue());
						}
					}

					if (!literals.isEmpty()) {

						if (genesysFilter.isAnalyzed()) {
							// query
							StringBuilder sb = new StringBuilder();
							for (Object val : literals) {
								if (sb.length() > 0)
									sb.append(",");
								if (val instanceof String)
									sb.append("\"" + val + "\"");
								else
									sb.append(val);
							}

							if (FilterConstants.ALIAS.equals(key)) {
								// Nested
								orFilter.add(FilterBuilders.nestedFilter("aliases", QueryBuilders.queryString("aliases.name" + ":(" + sb.toString() + ")")));
							} else {
								orFilter.add(FilterBuilders.queryFilter(QueryBuilders.queryString(key + ":(" + sb.toString() + ")")));
							}
						} else {
							// terms

							if (FilterConstants.SGSV.equals(key)) {
								orFilter.add(FilterBuilders.termsFilter(FilterConstants.IN_SGSV, literals).execution("or"));
							} else {
								orFilter.add(FilterBuilders.termsFilter(key, literals).execution("or"));
							}
						}
					}
				}

				{
					// Handle operations
					for (FilterValue filterValue : filterValues) {
						if (filterValue instanceof ValueRangeFilter) {
							ValueRangeFilter range = (ValueRangeFilter) filterValue;
							LOG.debug("Range " + range.getClass() + " " + range);
							orFilter.add(FilterBuilders.rangeFilter(key).from(range.getFrom()).to(range.getTo()));
						} else if (filterValue instanceof MaxValueFilter) {
							MaxValueFilter max = (MaxValueFilter) filterValue;
							LOG.debug("Max " + max);
							orFilter.add(FilterBuilders.rangeFilter(key).to(max.getTo()));
						} else if (filterValue instanceof MinValueFilter) {
							MinValueFilter min = (MinValueFilter) filterValue;
							LOG.debug("Min " + min);
							orFilter.add(FilterBuilders.rangeFilter(key).from(min.getFrom()));
						} else if (filterValue instanceof StartsWithFilter) {
							StartsWithFilter startsWith = (StartsWithFilter) filterValue;
							LOG.debug("startsWith " + startsWith);
							if (genesysFilter.isAnalyzed()) {
								if (FilterConstants.ALIAS.equals(key)) {
									orFilter.add(FilterBuilders.nestedFilter("aliases",
											QueryBuilders.queryString("aliases.name" + ":" + startsWith.getStartsWith() + "*")));
								} else {
									orFilter.add(FilterBuilders.queryFilter(QueryBuilders.queryString(key + ":" + startsWith.getStartsWith() + "*")));
								}
							} else {
								orFilter.add(FilterBuilders.prefixFilter(key, startsWith.getStartsWith()));
							}
						}
					}
				}
			}

			filterBuilder.add(orFilter);

		}
		return filterBuilder;
	}

	@Override
	public void update(String className, long id) {
		if (!clazzMap.containsKey(className)) {
			return;
		}

		Object eo = toElasticObject(className, id);
		if (eo == null)
			return;

		IndexQuery iq = new IndexQuery();
		iq.setId(String.valueOf(id));
		iq.setObject(eo);

		if (LOG.isDebugEnabled()) {
			LOG.debug("Indexing " + className + " id=" + id);
		}
		elasticsearchTemplate.index(iq);
	}

	@Override
	public void updateAll(String className, Collection<Long> ids) {
		if (!clazzMap.containsKey(className)) {
			return;
		}
		LOG.info("Updating " + className + " bulk_size=" + ids.size());
		List<IndexQuery> queries = new ArrayList<IndexQuery>();

		for (Long id : ids) {
			if (id == null)
				continue; // Skip null id

			Object eo = toElasticObject(className, id);
			if (eo == null)
				continue; // Skip null results, TODO perhaps delete?

			IndexQuery iq = new IndexQuery();
			iq.setId(String.valueOf(id));
			iq.setObject(eo);

			queries.add(iq);
		}

		if (LOG.isInfoEnabled() && !queries.isEmpty()) {
			LOG.info("Indexing " + className + " count=" + queries.size());
			elasticsearchTemplate.bulkIndex(queries);
		}
	}

	@Override
	public void remove(String className, long id) {
		Class<?> clazz2 = clazzMap.get(className);
		if (clazz2 == null) {

			return;
		}
		LOG.info("Removing from index " + clazz2 + " id=" + id);
		elasticsearchTemplate.delete(clazz2, String.valueOf(id));
	}

	private Object toElasticObject(String className, long id) {
		if (Accession.class.getName().equals(className)) {
			return genesysService.getAccessionDetails(id);
		}

		LOG.warn("Unsupported class " + className);
		return null;
	}

	@Override
	public void refreshIndex(String className) {
		Class<?> clazz2 = clazzMap.get(className);
		if (clazz2 == null) {

			return;
		}
		LOG.info("Refreshing index " + clazz2);
		elasticsearchTemplate.refresh(clazz2, true);
	}

	@Override
	public void reindexByFilter(AppliedFilters filters, boolean slow) {

		LOG.info("Creating index");
		elasticsearchTemplate.createIndex(AccessionDetails.class);
		LOG.info("Putting mapping");
		elasticsearchTemplate.putMapping(AccessionDetails.class);
		LOG.info("Refreshing");
		elasticsearchTemplate.refresh(AccessionDetails.class, true);

		StopWatch stopWatch = new StopWatch();
		stopWatch.setKeepTaskList(false);

		int page = 0, size = 100;
		List<IndexQuery> queries = new ArrayList<IndexQuery>();
		do {
			LOG.info("Reindexing progress " + (page * size));
			queries.clear();

			stopWatch.start("Get data by filter");
			Page<Accession> accessions = filterService.listAccessions(filters, new PageRequest(page, size));
			stopWatch.stop();
			LOG.info(stopWatch.getLastTaskName() + " took " + stopWatch.getLastTaskTimeMillis());

			if (!accessions.hasContent()) {
				LOG.info("No more content");
				break;
			}

			page++;

			stopWatch.start("Query generation");
			for (Accession accn : accessions) {
				queries.add(reindexAccession(accn.getId()));
			}
			stopWatch.stop();
			LOG.info(stopWatch.getLastTaskName() + " took " + stopWatch.getLastTaskTimeMillis());

			stopWatch.start("Bulk index");
			elasticsearchTemplate.bulkIndex(queries);
			stopWatch.stop();
			LOG.info(stopWatch.getLastTaskName() + " took " + stopWatch.getLastTaskTimeMillis());

			if (page % 10 == 0) {
				stopWatch.start("Refresh");
				elasticsearchTemplate.refresh(AccessionDetails.class, true);
				stopWatch.stop();
				LOG.info(stopWatch.getLastTaskName() + " took " + stopWatch.getLastTaskTimeMillis());
			}

			if (slow) {
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					break;
				}
			}

		} while (true);

		elasticsearchTemplate.refresh(AccessionDetails.class, true);
		LOG.info("Done.");
	}

	private IndexQuery reindexAccession(long accessionId) {
		AccessionDetails ad = genesysService.getAccessionDetails(accessionId);

		IndexQuery iq = new IndexQuery();
		iq.setId(String.valueOf(ad.getId()));
		iq.setObject(ad);
		return iq;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		LOG.info("Initializing index");
		elasticsearchTemplate.createIndex(AccessionDetails.class);
		LOG.info("Putting mapping");
		try {
			elasticsearchTemplate.putMapping(AccessionDetails.class);
		} catch (Throwable e) {
			LOG.error("Mapping mismatch. Need to reindex.");
			reindexExistingData(AccessionDetails.class);
		}
		LOG.info("Refreshing");
		elasticsearchTemplate.refresh(AccessionDetails.class, true);

		Map<?, ?> indexMapping = elasticsearchTemplate.getMapping(AccessionDetails.class);

		// Ensure ES index 'genesysarchive'
		if (!elasticsearchTemplate.indexExists("genesysarchive")) {
			LOG.info("Initializing genesysarchive");
			elasticsearchTemplate.createIndex("genesysarchive");
			LOG.info("Copying mapping to genesysarchive");
			elasticsearchTemplate.putMapping("genesysarchive", "mcpd", indexMapping);
		}
	}

	// TODO FIXME Don't delete index; use aliases!
	private void reindexExistingData(Class<?> clazz) {
		LOG.warn("Deleting index for class=" + clazz);
		elasticsearchTemplate.deleteIndex(clazz);
		// Make new index
		LOG.warn("Creating new index for class=" + clazz);
		elasticsearchTemplate.createIndex(clazz);
		elasticsearchTemplate.putMapping(clazz);

		// TODO Scan & Scroll
		// TODO Re-alias

		// But... For now, don't do anything
	}

}
