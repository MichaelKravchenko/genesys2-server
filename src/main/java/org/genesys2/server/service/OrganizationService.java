/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service;

import java.util.List;
import java.util.Locale;

import org.genesys2.server.model.impl.Article;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.model.impl.Organization;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface OrganizationService {

	Page<Organization> list(Pageable pageable);

	Organization getOrganization(String slug);

	Article updateBlurp(Organization organization, String blurp, Locale locale);

	Organization update(long id, String newSlug, String title);

	/**
	 * Create a new organization
	 *
	 * @param newSlug
	 * @param title
	 * @return
	 */
	Organization create(String slug, String title);

	List<FaoInstitute> getMembers(Organization organization);

	boolean addOrganizationInstitutes(Organization organization, List<String> instituteList);

	boolean setOrganizationInstitutes(Organization organization, List<String> instituteList);

	Organization deleteOrganization(Organization organization);

	Article getBlurp(Organization organization, Locale locale);
}
