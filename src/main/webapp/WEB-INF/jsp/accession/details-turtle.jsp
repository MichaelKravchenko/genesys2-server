<%@ page contentType="text/turtle;charset=UTF-8" pageEncoding="UTF-8" language="java" session="false"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib
	prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix wgs84_pos: <http://www.w3.org/2003/01/geo/wgs84_pos#> .
# using DarwinCore
@prefix dwc: <http://rs.tdwg.org/dwc/terms/> .
# using https://code.google.com/p/darwincore-germplasm/
# as vocabulary for germplasm stuff
@prefix germplasm: <http://purl.org/germplasm/germplasmTerm#> .
@prefix germplasmType: <http://purl.org/germplasm/germplasmType#> .

<http://www.genesys-pgr.org/acn/id/${accession.id}>
  a germplasm:GermplasmAccession;
  germplasm:germplasmID "<c:out value="${accession.id}" />";
  dwc:catalogNumber "<c:out value="${accession.accessionName}" />";
  dwc:institutionCode "<c:out value="${accession.instituteCode}" />";
  dwc:instituteId "<c:out value="${accession.institute.id}" />";
  dwc:countryCode "<c:out value="${accession.institute.country.code3}" />";
  dwc:genus "<c:out value="${accession.taxonomy.genus}" />";  
  # using http://rs.tdwg.org/dwc/terms/specificEpithet but could also use "subgenus"
  dwc:specificEpithet "<c:out value="${accession.taxonomy.species}" />";
  germplasm:biologicalStatus "<c:out value="${accession.sampleStatus}" />";
  
  <c:if test="${accessionGeo ne null}">
      # found here: http://www.geonames.org/ontology/documentation.html
    <c:if test="${accessionGeo.latitude ne null}">      
      wgs84_pos:lat "<c:out value="${accessionGeo.latitude}" />";
    </c:if>
    <c:if test="${accessionGeo.longitude ne null}">      
      wgs84_pos:long "<c:out value="${accessionGeo.longitude}" />";
    </c:if>  
  </c:if>.
