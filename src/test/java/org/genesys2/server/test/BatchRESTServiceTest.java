/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.genesys2.server.aspect.AsAdminAspect;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.Taxonomy2;
import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.persistence.domain.CountryRepository;
import org.genesys2.server.service.AclService;
import org.genesys2.server.service.BatchRESTService;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.GeoService;
import org.genesys2.server.service.HtmlSanitizer;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.OrganizationService;
import org.genesys2.server.service.TaxonomyService;
import org.genesys2.server.service.UserService;
import org.genesys2.server.service.impl.AclServiceImpl;
import org.genesys2.server.service.impl.BatchRESTServiceImpl;
import org.genesys2.server.service.impl.ContentServiceImpl;
import org.genesys2.server.service.impl.GenesysServiceImpl;
import org.genesys2.server.service.impl.GeoServiceImpl;
import org.genesys2.server.service.impl.InstituteServiceImpl;
import org.genesys2.server.service.impl.NonUniqueAccessionException;
import org.genesys2.server.service.impl.OWASPSanitizer;
import org.genesys2.server.service.impl.OrganizationServiceImpl;
import org.genesys2.server.service.impl.RESTApiException;
import org.genesys2.server.service.impl.TaxonomyServiceImpl;
import org.genesys2.server.service.impl.UserServiceImpl;
import org.genesys2.server.servlet.controller.rest.model.AccessionHeaderJson;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.support.NoOpCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ui.velocity.VelocityEngineFactoryBean;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = BatchRESTServiceTest.Config.class, initializers = PropertyPlacholderInitializer.class)
public class BatchRESTServiceTest {
	private final ObjectMapper mapper = new ObjectMapper();

	@Import(JpaDataConfig.class)
	@ComponentScan(basePackages = { "org.genesys2.server.persistence.domain" })
	public static class Config {

		@Bean
		public AsAdminAspect asAdminAspect() {
			return new AsAdminAspect();
		}

		@Bean
		public UserService userService() {
			return new UserServiceImpl();
		}

		@Bean
		public AclService aclService() {
			return new AclServiceImpl();
		}

		@Bean
		public TaxonomyService taxonomyService() {
			return new TaxonomyServiceImpl();
		}

		@Bean
		public BatchRESTService batchRESTService() {
			return new BatchRESTServiceImpl();
		}

		@Bean
		public GenesysService genesysService() {
			return new GenesysServiceImpl();
		}

		@Bean
		public CacheManager cacheManager() {
			return new NoOpCacheManager();
		}

		@Bean
		public HtmlSanitizer htmlSanitizer() {
			return new OWASPSanitizer();
		}

		@Bean
		public GeoService geoService() {
			return new GeoServiceImpl();
		}

		@Bean
		public ContentService contentService() {
			return new ContentServiceImpl();
		}

		@Bean
		public VelocityEngine velocityEngine() throws VelocityException, IOException {
			final VelocityEngineFactoryBean vf = new VelocityEngineFactoryBean();
			return vf.createVelocityEngine();
		}

		@Bean
		public OrganizationService organizationService() {
			return new OrganizationServiceImpl();
		}

		@Bean
		public InstituteService instituteService() {
			return new InstituteServiceImpl();
		}
	}

	@Autowired
	private BatchRESTService batchRESTService;

	@Autowired
	private GenesysService genesysService;

	@Autowired
	private TaxonomyService taxonomyService;

	@Autowired
	private InstituteService instituteService;

	@Autowired
	private CountryRepository countryRepository;

	@Before
	public void setup() {
		System.err.println("Setting up");
		final Collection<FaoInstitute> institutes = new ArrayList<FaoInstitute>();
		for (final String instCode : new String[] { "INS002", "INS001" }) {
			final FaoInstitute institute = new FaoInstitute();
			institute.setFullName(instCode + " institute");
			institute.setCode(instCode);
			institute.setUniqueAcceNumbs(true);
			institutes.add(institute);
		}
		instituteService.update(institutes);

		final Country country = new Country();
		country.setCode2("CT");
		country.setCode3("CTY");
		country.setName("Country");
		country.setCurrent(true);
		countryRepository.save(country);
	}

	@After
	public void teardown() {
		System.err.println("Tearing down");
		for (final String instCode : new String[] { "INS002", "INS001" }) {
			final FaoInstitute institute = instituteService.getInstitute(instCode);
			System.err.println("Deleting accessions for " + institute);
			genesysService.removeAccessions(institute, genesysService.listAccessionsByInstitute(institute, new PageRequest(0, Integer.MAX_VALUE)).getContent());
			System.err.println("Deleting " + institute);
			instituteService.delete(institute.getCode());
		}

		countryRepository.delete(countryRepository.findAll());
	}

	@Test
	public void testNewAccession() throws NonUniqueAccessionException {
		final String instCode = "INS002";
		final FaoInstitute institute = instituteService.getInstitute(instCode);
		assertTrue("institute is null", institute != null);
		System.err.println(institute);

		final Map<AccessionHeaderJson, ObjectNode> batch = new HashMap<AccessionHeaderJson, ObjectNode>();

		final AccessionHeaderJson dataJson = new AccessionHeaderJson();
		dataJson.acceNumb = "AC 1";
		dataJson.instCode = instCode;

		final ObjectNode json = mapper.createObjectNode();
		json.put("genus", "Hordeum");
		batch.put(dataJson, json);

		try {
			batchRESTService.upsertAccessionData(institute, batch);
		} catch (final RESTApiException e) {
			fail(e.getMessage());
		}

		Accession accession = genesysService.getAccession(instCode, "AC 1");
		assertTrue(accession.getId() != null);
		assertTrue(accession.getInstituteCode().equals(instCode));
		assertTrue(accession.getInstitute().getId().equals(institute.getId()));
		assertTrue(accession.getTaxonomy() != null);
		final Taxonomy2 tax = accession.getTaxonomy();
		System.err.println(tax);
		System.err.println(accession.getTaxGenus());
		System.err.println(accession.getTaxSpecies());

		// Modify taxonomy
		json.put("genus", "Hordeum");
		json.put("species", "vulgare");
		json.put("spauthor", "L.");
		json.put("subtaxa", "some subtaxa");
		json.put("subtauthor", "Subtauthor");
		try {
			batchRESTService.upsertAccessionData(institute, batch);
		} catch (final RESTApiException e) {
			fail(e.getMessage());
		}
		// reload
		accession = genesysService.getAccession(instCode, "AC 1");
		final Taxonomy2 tax2 = accession.getTaxonomy();
		System.err.println(tax2);
		assertFalse(tax2.getId().equals(tax.getId()));
		System.err.println(accession.getTaxGenus());
		System.err.println(accession.getTaxSpecies());
		assertFalse(accession.getTaxGenus() == accession.getTaxSpecies());

		// test nothing
		try {
			System.err.println("NO UPDATE!");
			batchRESTService.upsertAccessionData(institute, batch);
		} catch (final RESTApiException e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void testClearOrgCty() throws NonUniqueAccessionException {
		final String instCode = "INS002";
		final FaoInstitute institute = instituteService.getInstitute(instCode);
		assertTrue("institute is null", institute != null);
		System.err.println(institute);

		final Map<AccessionHeaderJson, ObjectNode> batch = new HashMap<AccessionHeaderJson, ObjectNode>();

		final AccessionHeaderJson dataJson = new AccessionHeaderJson();
		dataJson.acceNumb = "AC 100";
		dataJson.instCode = instCode;

		final ObjectNode json = mapper.createObjectNode();
		json.put("genus", "Hordeum");
		json.put("orgCty", "CTY");
		batch.put(dataJson, json);
		System.err.println(json);

		try {
			batchRESTService.upsertAccessionData(institute, batch);
		} catch (final RESTApiException e) {
			fail(e.getMessage());
		}

		Accession accession = genesysService.getAccession(instCode, "AC 100");
		assertTrue(accession.getId() != null);
		assertTrue(accession.getInstituteCode().equals(instCode));
		assertTrue(accession.getInstitute().getId().equals(institute.getId()));
		assertTrue(accession.getOrigin() != null);
		assertTrue("CTY".equals(accession.getOrigin()));
		assertTrue(accession.getCountryOfOrigin() != null);
		assertTrue("Country".equals(accession.getCountryOfOrigin().getName()));

		// Modify taxonomy
		json.putNull("orgCty");
		System.err.println(json);
		try {
			batchRESTService.upsertAccessionData(institute, batch);
		} catch (final RESTApiException e) {
			fail(e.getMessage());
		}
		// reload
		accession = genesysService.getAccession(instCode, "AC 100");
		assertTrue(accession.getOrigin() == null);
		assertTrue(accession.getCountryOfOrigin() == null);
	}

}
