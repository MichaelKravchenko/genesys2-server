/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.genesys2.server.model.elastic.AccessionDetails;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.AccessionAlias;
import org.genesys2.server.model.genesys.AccessionBreeding;
import org.genesys2.server.model.genesys.AccessionCollect;
import org.genesys2.server.model.genesys.AccessionExchange;
import org.genesys2.server.model.genesys.AccessionGeo;
import org.genesys2.server.model.genesys.AccessionRemark;
import org.genesys2.server.model.genesys.AllAccnames;
import org.genesys2.server.model.genesys.ExperimentTrait;
import org.genesys2.server.model.genesys.Metadata;
import org.genesys2.server.model.genesys.Method;
import org.genesys2.server.model.genesys.SvalbardData;
import org.genesys2.server.model.genesys.Taxonomy2;
import org.genesys2.server.model.impl.AccessionIdentifier3;
import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.model.impl.Organization;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.genesys2.server.service.impl.NonUniqueAccessionException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface GenesysService {

	long countByInstitute(FaoInstitute institute);

	long countByOrigin(String isoCode3);

	long countByLocation(String isoCode3);

	Page<Accession> listAccessionsByOrigin(Country country, Pageable pageable);

	Page<Accession> listAccessionsByInstitute(FaoInstitute faoInstitute, Pageable pageable);

	Accession getAccession(long accessionId);

	List<Accession> listAccessions(FaoInstitute faoInstitute, String accessionName);

	AllAccnames listAccessionNames(Accession accession);

	List<AccessionAlias> listAccessionAliases(Accession accession);

	AccessionExchange listAccessionExchange(Accession accession);

	AccessionCollect listAccessionCollect(Accession accession);

	AccessionBreeding listAccessionBreeding(Accession accession);

	AccessionGeo listAccessionGeo(Accession accession);

	List<AccessionRemark> listAccessionRemarks(Accession accession);

	List<Metadata> listMetadata(Accession accession);

	Page<Metadata> listMetadata(Pageable pageable);

	Page<Metadata> listDatasets(FaoInstitute faoInstitute, Pageable pageable);

	Metadata getMetadata(long metadataId);

	List<Method> listMethods(Metadata metadata);

	Page<Accession> listMetadataAccessions(long metadataId, Pageable pageable);

	Map<Long, List<ExperimentTrait>> getAccessionTraitValues(Accession accession);

	Map<Long, Map<Long, List<Object>>> getMetadataTraitValues(Metadata metadata, List<Accession> content);

	Page<Accession> listAccessions(Collection<Long> accessionIds, Pageable pageable);

	List<Accession> listAccessionsSGSV(List<? extends AccessionIdentifier3> accns);

	Accession getAccession(AccessionIdentifier3 aid3) throws NonUniqueAccessionException;

	Accession getAccession(String instCode, String acceNumb, String genus) throws NonUniqueAccessionException;

	Page<Object[]> statisticsGenusByInstitute(FaoInstitute faoInstitute, Pageable pageable);

    Page<Object[]> statisticsSpeciesByInstitute(FaoInstitute faoInstitute, Pageable pageable);

	// Page<Object[]> statisticsCropByInstitute(FaoInstitute faoInstitute,
	// Pageable pageable);

	Page<Accession> listAccessionsByInstituteAndSpecies(FaoInstitute institute, long taxSpecies, Pageable pageable);

	Page<Accession> listAccessionsByTaxGenus(long taxGenusId, Pageable pageable);

	Page<Accession> listAccessionsByTaxSpecies(long taxSpeciesId, Pageable pageable);

	List<Long> listAccessionsIds(Pageable pageable);

	Page<Accession> listAccessionsByCrop(Crop crop, Pageable pageable);

	Page<Accession> listAccessionsByOrganization(Organization organization, Pageable pageable);

	void updateAccessionCountryRefs();

	void updateAccessionInstitueRefs();

	List<Accession> saveAccessions(FaoInstitute institute, List<Accession> matching);

	List<SvalbardData> saveSvalbards(List<SvalbardData> svalbards);

	long countAvailableForDistribution(Set<Long> accessionIds);

	Set<Long> filterAvailableForDistribution(Set<Long> accessionIds);

	List<Accession> saveAccession(Accession... accession);

	void updateAccessionCount(FaoInstitute institute);

	SvalbardData getSvalbardData(Accession accession);

	List<AccessionCollect> saveCollecting(List<AccessionCollect> all);

	List<AccessionGeo> saveGeo(List<AccessionGeo> all);

	List<AccessionBreeding> saveBreeding(List<AccessionBreeding> all);

	List<AccessionExchange> saveExchange(List<AccessionExchange> all);

	void refreshMetadataMethods();

	long countDatasets(FaoInstitute faoInstitute);

	void writeAccessions(AppliedFilters filters, OutputStream outputStream) throws IOException;

	List<AccessionAlias> saveAliases(List<AccessionAlias> aliases);

	List<AccessionAlias> removeAliases(List<AccessionAlias> aliases);

	void upsertAliases(long accessionId, String acceNames, String otherIds);

	Set<Long> removeAliases(Set<Long> toRemove);

	List<AccessionGeo> listAccessionsGeo(Set<Long> copy);

	List<Accession> removeAccessions(FaoInstitute institute, List<Accession> toDelete);

	void setInSvalbard(List<Accession> matching);

	void addAccessions(List<Accession> accessions);

	long countAll();

	/**
	 * For institutes with {@link FaoInstitute#uniqueAcceNumbs}
	 * 
	 * @param instCode
	 * @param acceNumb
	 * @return the 1 accession
	 * @throws NonUniqueAccessionException
	 */
	Accession getAccession(String instCode, String acceNumb) throws NonUniqueAccessionException;

	List<FaoInstitute> findHoldingInstitutes(Set<Long> accessionIds);

	Set<Long> listAccessions(FaoInstitute holdingInstitute, Set<Long> accessionIds);

	int countAccessions(AppliedFilters filters);

	List<AccessionRemark> saveRemarks(List<AccessionRemark> toSaveRemarks);

	List<AccessionRemark> removeRemarks(List<AccessionRemark> toRemoveRemarks);

	AccessionDetails getAccessionDetails(long accessionId);

	List<AccessionCollect> removeCollecting(List<AccessionCollect> toRemove);

	List<AccessionGeo> removeGeo(List<AccessionGeo> toRemove);

	List<AccessionExchange> removeExchange(List<AccessionExchange> toRemove);

	List<AccessionBreeding> removeBreeding(List<AccessionBreeding> toRemove);

	Set<AccessionDetails> getAccessionDetails(Collection<Accession> accessions);

	List<Long> listAccessionsIds(Taxonomy2 taxonomy);

}
