/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.worker;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.aspect.AsAdmin;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.impl.BatchRESTServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

/**
 * Scans Accession#storage records and updates Accession#stoRage
 */
@Component
public class AccessionStorageScanner {
	public static final Log LOG = LogFactory.getLog(AccessionStorageScanner.class);

	@Autowired
	private GenesysService genesysService;

	private static final int BATCH_SIZE = 50;

	@AsAdmin
	public void scanStorage() throws InterruptedException {
		int page = 0;

		do {
			page++;
			if (page % 100 == 0) {
				LOG.info("Scanning accessions page " + page);
			} else {
				LOG.debug("Scanning accessions page " + page);
			}
			List<Long> accns = genesysService.listAccessionsIds(new PageRequest(page, BATCH_SIZE));
			List<Accession> toSave = new ArrayList<Accession>(accns.size());

			if (accns.size() == 0)
				break;

			for (long accnId : accns) {
				Accession accession = genesysService.getAccession(accnId);
				boolean updated = BatchRESTServiceImpl.updateAccessionStorage(accession, accession.getStorage());
				if (updated) {
					toSave.add(accession);
				}
			}

			try {
				if (toSave.size() > 0) {
					LOG.info("Updating accession#stoRage for size=" + toSave.size());
					genesysService.saveAccession(toSave.toArray(new Accession[] {}));
				}
			} catch (Throwable e) {
				LOG.warn(e.getMessage(), e);
			}

			Thread.sleep(5);
		} while (true);

		LOG.info("Done scanning accession#storage");
	}
}
