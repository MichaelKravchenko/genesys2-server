/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.genesys2.server.model.AuditedModel;

/**
 *
 */
@Entity
@Table(name = "verificationtoken")
public class VerificationToken extends AuditedModel {
	/**
	 *
	 */
	private static final long serialVersionUID = 5031164157223780739L;

	@Column(length = 36, nullable = false, unique = true)
	private String uuid;

	@Column(name = "secret", length = 4, nullable = false)
	private String key;

	@Column(length = 36, nullable = false)
	private String purpose;

	@Column(length = 120, nullable = true)
	private String data;

	@PrePersist
	void generateUuid() {
		if (this.uuid == null) {
			this.uuid = UUID.randomUUID().toString();
		}
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setPurpose(String tokenPurpose) {
		this.purpose = tokenPurpose;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getData() {
		return data;
	}
}
