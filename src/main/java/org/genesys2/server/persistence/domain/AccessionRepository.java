/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.Taxonomy2;
import org.genesys2.server.model.impl.FaoInstitute;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AccessionRepository extends JpaRepository<Accession, Long> {

	@Query("select a.id from Accession a")
	public List<Long> listAccessionsIds(Pageable pageable);

	@Query(countQuery = "select count(*) from accession a where a.institute = ?1")
	Page<Accession> findByInstitute(FaoInstitute institute, Pageable pageable);

	@Query(countQuery = "select count(*) from accession a where a.taxGenus = ?1")
	Page<Accession> findByTaxGenus(long taxonomyId, Pageable pageable);

	@Query(countQuery = "select count(*) from accession a where a.taxSpecies = ?1")
	Page<Accession> findByTaxSpecies(long taxSpecies, Pageable pageable);

	List<Accession> findByInstitute(FaoInstitute institute);

	@Query("select count(a) from Accession a where a.institute = ?1")
	long countByInstitute(FaoInstitute institute);

	@Query(value = "select t.genus, count(a.id) from Accession a inner join a.taxonomy t where a.institute = ?1 group by t.genus order by count(a) desc", countQuery = "select count(distinct a.taxGenus) from Accession a where a.institute = ?1")
	Page<Object[]> statisticsGenusInInstitute(FaoInstitute institute, Pageable pageable);

	@Query(value = "select a.taxSpecies, count(a) from Accession a where a.institute = ?1 group by a.taxSpecies order by count(a) desc", countQuery = "select count(distinct a.taxSpecies) from Accession a where a.institute = ?1")
	Page<Object[]> statisticsSpeciesInInstitute(FaoInstitute institute, Pageable pageable);

	// FIXME Slow query
	// FIXME TOOD taxonomy1!
	// @Query(value =
	// "select ct.crop, count(a) from Accession a inner join a.taxonomy1 t join t.cropTaxonomies ct where a.institute = ?1 group by ct.crop order by count(a) desc",
	// countQuery =
	// "select count(distinct ct.crop) from Accession a inner join a.taxonomy t join t.cropTaxonomies ct where a.institute = ?1")
	// Page<Object[]> statisticsCropInInstitute(FaoInstitute institute, Pageable
	// pageable);

	@Query("select count(a) from Accession a where a.origin = ?1")
	long countByOrigin(String isoCode3);

	@Query("select count(a) from Accession a inner join a.institute i inner join i.country c where c.code3 = ?1")
	long countByLocation(String isoCode3);

	@Query("select a from Accession a where a.institute in ( ?1 )")
	Page<Accession> findByInstitute(List<FaoInstitute> institutes, Pageable pageable);

	// @Query("select a from Accession a where a.genus in ( ?1 )")
	// Page<Accession> findByGenus(List<String> genus, Pageable pageable);

	// @Query("select a.institute, count(a.id) as total from Accession a where a.genus in ( ?1 ) group by a.institute order by total desc")
	// List<Object[]> statisticsInstitute(List<String> genus);

	// @Query("select a.origin, count(a.id) as total from Accession a where a.genus in ( ?1 ) group by a.origin order by total desc")
	// List<Object[]> statisticsOrigin(List<String> genus);

	// @Query("select a.taxonomy, count(a.id) as total from Accession a where a.genus in ( ?1 ) group by a.taxonomy order by total desc")
	// List<Object[]> statisticsTaxonomy(List<String> genus);

	// List<Accession> findByOrigin(String origin);

	Page<Accession> findByOrigin(String isoCode3, Pageable pageable);

	List<Accession> findByInstituteAndAccessionName(FaoInstitute faoInstitute, String accessionName);

	@Query("from Accession a where a.id in ( ?1 )")
	Page<Accession> findById(Collection<Long> accessionIds, Pageable pageable);

	@Query("from Accession a where a.id in ( ?1 )")
	List<Accession> listById(Collection<Long> accessionIds, Sort sort);

	Page<Accession> findByInstituteAndTaxonomy(FaoInstitute institute, Taxonomy2 taxonomy, Pageable pageable);

	Page<Accession> findByTaxonomy(Taxonomy2 taxonomy, Pageable pageable);

	@Query("select a from Accession a where a.taxonomy in ( ?1 )")
	Page<Accession> findByTaxonomy(Collection<Taxonomy2> taxonomies, Pageable pageable);

	Accession findByInstituteCodeAndAccessionName(String instCode, String accessionName);

    @Query("select a from Accession a where a.accessionName=?2 and (a.institute.code=?1 or a.institute.codeSGSV=?1) and a.taxonomy.genus=?3")
    Accession findOneSGSV(String instCode, String acceNumb, String genus);

	@Query("select a from Accession a where a.institute.code=:instCode and a.accessionName=:acceNumb and a.taxonomy.genus=:genus")
	Accession findOne(@Param("instCode") String holdingInstitute, @Param("acceNumb") String accessionName, @Param("genus") String genus);

	@Query("select count(a.id) from Accession a where a.id in ( ?1 ) and a.availability = true and a.institute.allowMaterialRequests = true")
	long countAvailableForDistribution(Set<Long> accessionIds);

	@Query("select a.id from Accession a where a.id in ( ?1 ) and a.availability = true and a.institute.allowMaterialRequests = true")
	Set<Long> filterAvailableForDistribution(Set<Long> accessionIds);

	@Modifying
	@Query("update Accession a set a.inSvalbard = true where a in ?1")
	void setInSvalbard(List<Accession> matching);

	Page<Accession> findByInstituteAndTaxSpecies(FaoInstitute institute, long taxSpecies, Pageable pageable);

	@Query("select distinct a.institute from Accession a where a.id in ( ?1 )")
	List<FaoInstitute> findDistinctInstitutesFor(Set<Long> accessionIds);

	@Query("select distinct a.id from Accession a where a.institute = :institute and a.id in ( :ids )")
	Set<Long> findByInstituteAndIds(@Param("institute") FaoInstitute institute, @Param("ids") Set<Long> accessionIds);

	@Query("select a.id from Accession a where a.taxonomy = ?1")
	public List<Long> listAccessionsIds(Taxonomy2 taxonomy);

}
