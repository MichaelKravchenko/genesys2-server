package org.genesys2.server.model.elastic;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.genesys2.server.model.genesys.AccessionCollect;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;

public class Collect {

	public Set<String> collCode;
	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	public String collDate;
	public String collInstAddr;
	public String collMissId;
	public String collName;
	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	public String collNumb;
	public String collSite;
	public Integer collSrc;
	public Date collectingDate;

	public static Collect from(AccessionCollect collect) {
		if (collect == null)
			return null;

		Collect c = new Collect();
		if (StringUtils.isNotBlank(collect.getCollCode()))
			c.collCode = new HashSet<String>(Arrays.asList(AccessionDetails.mcpdSplit.split(collect.getCollCode())));
		c.collDate = StringUtils.defaultIfBlank(collect.getCollDate(), null);
		c.collInstAddr = StringUtils.defaultIfBlank(collect.getCollInstAddress(), null);
		c.collMissId = StringUtils.defaultIfBlank(collect.getCollMissId(), null);
		c.collName = StringUtils.defaultIfBlank(collect.getCollName(), null);
		c.collNumb = StringUtils.defaultIfBlank(collect.getCollNumb(), null);
		c.collSite = StringUtils.defaultIfBlank(collect.getCollSite(), null);
		c.collSrc = collect.getCollSrc();
		return c;
	}

}