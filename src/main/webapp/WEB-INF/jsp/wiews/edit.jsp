<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="faoInstitutes.page.profile.title" arguments="${faoInstitute.fullName}" argumentSeparator="|" /></title>
</head>
<body>
	<h1>
		<img class="country-flag bigger" src="<c:url value="${cdnFlagsUrl}" />/${faoInstitute.country.code3.toUpperCase()}.png" />
		<c:out value="${faoInstitute.fullName}" />
		<small><c:out value="${faoInstitute.code}" /></small>
	</h1>

	<form role="form" class="form-horizontal" action="<c:url value="/wiews/${faoInstitute.code}/update" />" method="post">
		<div class="form-group">
			<label for="blurp-body" class="col-lg-12 control-label"><spring:message code="blurp.blurp-body" /></label>
			<div class="controls col-lg-12">
				<textarea id="blurp-body" name="blurp" class="span9 required html-editor">
					<c:out value="${blurp.body}" />
				</textarea>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-lg-3 control-label"><spring:message code="ga.tracker-code" /></label>
			<div class="controls col-lg-9">
				<input type="text" name="gaTracker" class="form-control required" value="${faoInstitute.settings['googleAnalytics.tracker'].value}" />
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-lg-3 control-label"><spring:message code="faoInstitute.requests.mailto" /></label>
			<div class="controls col-lg-9">
				<input type="text" name="mailto" class="form-control required" value="${faoInstitute.settings['requests.mailto'].value}" />
			</div>
		</div>

        <div class="form-group">
            <label class="col-lg-3 control-label"><spring:message code="faoInstitute.sgsv" /></label>
            <div class="controls col-lg-9">
                <input type="text" name="codeSVGS" class="form-control required" value="${faoInstitute.codeSGSV}" />
            </div>
        </div>

		<div class="form-group">
			<div class="controls col-lg-offset-3 col-lg-9">
				<label><input type="radio" name="uniqueAcceNumbs" class="" value="true" ${faoInstitute.uniqueAcceNumbs==true ? 'checked' : ''} /> <spring:message code="faoInstitute.uniqueAcceNumbs.true" /></label>
				<label><input type="radio" name="uniqueAcceNumbs" class="" value="false" ${faoInstitute.uniqueAcceNumbs==false ? 'checked' : ''} /> <spring:message code="faoInstitute.uniqueAcceNumbs.false" /></label>
			</div>
            <div class="controls col-lg-offset-3 col-lg-9">
            <label><input type="checkbox" name="allowMaterialRequests" class=""  ${faoInstitute.allowMaterialRequests==true ? 'checked' : ''} /> <spring:message code="faoInstitute.allow.requests" /></label>
            </div>
        </div>

		<input type="submit" value="<spring:message code="save"/>" class="btn btn-primary" /> <a href="<c:url value="/wiews/${faoInstitute.code}" />" class="btn btn-default"> <spring:message code="cancel" />
		</a>
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>


<content tag="javascript">
	<script type="text/javascript" src="/html/js/tinymce/tinymce.min.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			tinyMCE.init({
				selector : "#blurp-body.html-editor",
				menubar : false,
				statusbar : false,
				height : 200,
				plugins: "link autolink",
				directionality: document.dir
			});
		});
	</script>
</content>

</body>
</html>