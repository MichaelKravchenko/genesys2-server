<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="data-overview" /></title>
</head>
<body>
	<h1>
		<spring:message code="data-overview" />
	</h1>

	<div class="main-col-header clearfix">
		<div class="row">
			<div class="col-xs-9 pull-right text-right">
				<a class="btn btn-default" href="<c:url value="/explore"><c:param name="filter" value="${jsonFilter}" /></c:url>"><span class="glyphicon glyphicon-eye-open"></span><span style="margin-left: 0.5em;"><spring:message code="view.accessions" /></span></a>
				<a class="btn btn-default" href="<c:url value="/explore/map"><c:param name="filter" value="${jsonFilter}" /></c:url>"><span class="glyphicon glyphicon-globe"></span><span style="margin-left: 0.5em;"><spring:message code="maps.view-map" /></span></a>
			</div>
			<div class="col-xs-3 results">
				<a class="btn btn-default" href="javascript: window.history.go(-1);"><spring:message code="navigate.back" /></a>
			</div>
		</div>
	</div>
	
	<c:if test="${fn:length(currentFilters) gt 0}">
	<div id="allfilters" class="disabled applied-filters">
		<%-- Only render currently present filters --%>
        <c:forEach items="${currentFilters}" var="filter">

            <c:set var="normalizedKey" value="${filter.key.replace('.', '-').replace(':', '_')}"/>

            <div class="clearfix filter-block" id="<c:out value="${normalizedKey}" />_filter" norm-key="<c:out value="${normalizedKey}" />" i-key="<c:out value="${filter.key}" />">
                <div class="col-lg-3 edit-fil">
                    <c:if test="${not filter.core}">
                        <c:out value="${filter.title}" />
                        <%-- <a href="<c:url value="/descriptors/${filter.key}" />"> --%>
                    </c:if>

                    <c:if test="${filter.core}">
						<spring:message code="filter.${filter.key}" />
                    </c:if>
                </div>
                <div class="col-lg-9">
                	<div class="filter-values" id="<c:out value="${normalizedKey}" />_value">
	                    <c:forEach items="${filters[filter.key]}" var="value">
	                        <c:set var="string" value="${value}"/>
	                        <c:if test="${fn:contains(value, 'range')}">
	                            <c:set var="string" value="${fn:replace(value,'{range=[','Between ')}"/>
	                            <c:set var="string" value="${fn:replace(string,',',' and ')}"/>
	                            <c:set var="string" value="${fn:replace(string,']}','')}"/>
                                <c:set var="value" value="${fn:replace(value,'{','{\"')}"/>
                                <c:set var="value" value="${fn:replace(value,'=','\":')}"/>
	                        </c:if>
	                        <c:if test="${fn:contains(value, 'min')}">
	                            <c:set var="string" value="${fn:replace(value,'{min=','More than ')}"/>
	                            <c:set var="string" value="${fn:replace(string,'}','')}"/>
                                <c:set var="value" value="${fn:replace(value,'{','{\"')}"/>
                                <c:set var="value" value="${fn:replace(value,'=','\":')}"/>
	                        </c:if>
	                        <c:if test="${fn:contains(value, 'max')}">
	                            <c:set var="string" value="${fn:replace(value,'{max=','Less than ')}"/>
	                            <c:set var="string" value="${fn:replace(string,'}','')}"/>
                                <c:set var="value" value="${fn:replace(value,'{','{\"')}"/>
                                <c:set var="value" value="${fn:replace(value,'=','\":')}"/>
	                        </c:if>
                            <c:if test="${fn:contains(value, 'like')}">
                                <c:set var="string" value="${fn:replace(value,'{like=','Like ')}"/>
                                <c:set var="string" value="${fn:replace(string,'}','')}"/>
                                <c:set var="value" value="${fn:replace(value,'{','{\"')}"/>
                                <c:set var="value" value="${fn:replace(value,'=','\":\"')}"/>
                                <c:set var="value" value="${fn:replace(value,'}','\"}')}"/>
                            </c:if>

	                        <c:if test="${string==null}">
	                        	<c:set var="string" value="null" />
	                        	<c:set var="value" value="null" />
	                        </c:if>
	                        <div class="filtval complex" x-key="<c:out value="${normalizedKey}" /><c:out value="${value}"/>" i-key="<c:out value="${filter.key}" />"><c:out value="${string}" /></div>
	                        <c:remove var="string" />
	                    </c:forEach>
	                </div>
                </div>
            </div>
        </c:forEach>
    </div>
    </c:if>
	
	<h3><spring:message code="data-overview.institutes" /></h3>	
    <div class="row">
    	<div class="col-xs-12 col-sm-6 row-section">
			<h4 id="stats-instcode"><spring:message code="filter.institute.code" /></h4>
			<c:set var="type" value="instCode" />
			<c:set var="termResult" value="${statsInstCode}" />
				<%@include file="termresult.jspf"%>
			<c:remove var="termResult" />
			<c:remove var="type" />
		</div>
		<div class="col-xs-12 col-sm-6 row-section">
			<h4 id="stats-instcty"><spring:message code="filter.institute.country.iso3" /></h4>
			<c:set var="type" value="country" />
			<c:set var="termResult" value="${statsInstCountry}" />
				<%@include file="termresult.jspf"%>
			<c:remove var="termResult" />
			<c:remove var="type" />
		</div>
	</div>

	<h3><spring:message code="data-overview.composition" /></h3>	
    <div class="row">
    	<div class="col-xs-12 col-sm-6 row-section">
			<h4 id="stats-crops"><spring:message code="filter.crops" /></h4>
			<c:set var="type" value="crop" />
			<c:set var="termResult" value="${statsCrops}" />
				<%@include file="termresult.jspf"%>
			<c:remove var="termResult" />
			<c:remove var="type" />
		</div>
		<div class="col-xs-12 col-sm-6 row-section">
			<h4 id="stats-sampstat"><spring:message code="filter.sampStat" /></h4>
			<c:set var="type" value="i18n.accession.sampleStatus" />
			<c:set var="termResult" value="${statsSampStat}" />
				<%@include file="termresult.jspf"%>
			<c:remove var="termResult" />
			<c:remove var="type" />
		</div>
	</div>
	<div class="row">
    	<div class="col-xs-12 col-sm-6 row-section">
			<h4 id="stats-genus"><spring:message code="filter.taxonomy.genus" /></h4>
			<c:set var="termResult" value="${statsGenus}" />
				<%@include file="termresult.jspf"%>
			<c:remove var="termResult" />
		</div>
		<div class="col-xs-12 col-sm-6 row-section">
			<h4 id="stats-species"><spring:message code="filter.taxonomy.species" /></h4>
			<c:set var="termResult" value="${statsSpecies}" />
				<%@include file="termresult.jspf"%>
			<c:remove var="termResult" />
		</div>
	</div>
	
	<h3><spring:message code="data-overview.sources" /></h3>	
    <div class="row">
    	<div class="col-xs-12 col-sm-6 row-section">
			<h4 id="stats-orgcty"><spring:message code="filter.orgCty.iso3" /></h4>
			<c:set var="type" value="country" />
			<c:set var="termResult" value="${statsOrgCty}" />
				<%@include file="termresult.jspf"%>
			<c:remove var="termResult" />
			<c:remove var="type" />
		</div>
		<div class="col-xs-12 col-sm-6 row-section">
			<h4 id="stats-donorcode"><spring:message code="data-overview.donorCode" /></h4>
			<c:set var="type" value="instCode" />
			<c:set var="termResult" value="${statsDonorCode}" />
				<%@include file="termresult.jspf"%>
			<c:remove var="termResult" />
			<c:remove var="type" />
		</div>
	</div>
	
	<h3><spring:message code="data-overview.availability" /></h3>	
    <div class="row">
    	<div class="col-xs-12 col-sm-6 row-section">
			<h4 id="stats-sampstat"><spring:message code="data-overview.mlsStatus" /></h4>
			<c:set var="type" value="bool" />
			<c:set var="termResult" value="${statsMLS}" />
				<%@include file="termresult.jspf"%>
			<c:remove var="termResult" />
			<c:remove var="type" />
		</div>
		<div class="col-xs-12 col-sm-6 row-section">
			<h4 id="stats-available"><spring:message code="filter.available" /></h4>
			<c:set var="type" value="bool" />
			<c:set var="termResult" value="${statsAvailable}" />
				<%@include file="termresult.jspf"%>
			<c:remove var="termResult" />
			<c:remove var="type" />
		</div>
	</div>
	
	<h3><spring:message code="data-overview.management" /></h3>	
	<div class="row">
		<div class="col-xs-12 col-sm-6 row-section">
			<h4 id="stats-duplsite"><spring:message code="filter.duplSite" /></h4>
			<c:set var="type" value="instCode" />
			<c:set var="count" value="${accessionCount}" />
			<c:set var="termResult" value="${statsDuplSite}" />
				<%@include file="termresult.jspf"%>
			<c:remove var="termResult" />
			<c:remove var="count" />
			<c:remove var="type" />
		</div>
		<div class="col-xs-12 col-sm-6 row-section">
			<h4 id="stats-duplsite"><spring:message code="filter.sgsv" /></h4>
			<c:set var="type" value="bool" />
			<c:set var="termResult" value="${statsSGSV}" />
				<%@include file="termresult.jspf"%>
			<c:remove var="termResult" />
			<c:remove var="type" />
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-6 row-section">
			<h4 id="stats-storage"><spring:message code="filter.storage" /></h4>
			<c:set var="type" value="i18n.accession.storage" />
			<c:set var="count" value="${accessionCount}" />
			<c:set var="termResult" value="${statsStorage}" />
				<%@include file="termresult.jspf"%>
			<c:remove var="termResult" />
			<c:remove var="count" />
			<c:remove var="type" />
		</div>
	</div>

<content tag="javascript">
	<script type="text/javascript">
	
	</script>
</content>
</body>
</html>