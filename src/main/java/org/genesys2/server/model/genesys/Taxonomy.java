/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.genesys;

import java.text.MessageFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.genesys2.server.model.BusinessModel;

@Entity
// Add index on genus, genus+species
@Table(name = "taxonomy", uniqueConstraints = { @UniqueConstraint(columnNames = { "genus", "species" }) })
public class Taxonomy extends BusinessModel {

	private static final long serialVersionUID = 8881324404490162933L;

	@Column(nullable = false, length = 64)
	private String genus;

	@Column(nullable = false, length = 64)
	private String species;

	@Column(name = "taxonName", nullable = false, length = 200)
	private String taxonName;

	public String getGenus() {
		return this.genus;
	}

	public void setGenus(final String genus) {
		this.genus = genus;
	}

	public String getSpecies() {
		return this.species;
	}

	public void setSpecies(final String species) {
		this.species = species;
	}

	public String getTaxonName() {
		return this.taxonName;
	}

	public void setTaxonName(final String taxonName) {
		this.taxonName = taxonName;
	}

	@Override
	public String toString() {
		return MessageFormat.format("Tax id={0} genus={1} sp={2}, full={3}", id, genus, species, taxonName);
	}

	public boolean sameAs(Taxonomy taxonomy) {
		return taxonomy == null ? false : taxonomy.getId().equals(id);
	}

}
