/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.genesys.Taxonomy2;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.persistence.domain.Taxonomy2Repository;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.TaxonomyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.hazelcast.core.ILock;

@Service
@Transactional(readOnly = true)
public class TaxonomyServiceImpl implements TaxonomyService {
	public static final Log LOG = LogFactory.getLog(TaxonomyServiceImpl.class);

	@Autowired
	private Taxonomy2Repository taxonomy2Repository;
	
	@Autowired
	private CropService cropService;

	/**
	 * This lock ensures only one member of the cluster can enter taxonomy
	 * update logic
	 */
	@Resource
	private ILock taxonomyUpdateLock;

	@Override
	public Taxonomy2 get(Long id) {
		return taxonomy2Repository.findOne(id);
	}

	@Override
	public List<String> autocompleteGenus(String term, Crop crop) {
		List<String> strings;

		if (crop != null) {
			strings = taxonomy2Repository.autocompleteGenusByCrop(term + "%", crop, new PageRequest(0, 10));
		} else {
			strings = taxonomy2Repository.autocompleteGenus(term + "%", new PageRequest(0, 10));
		}

		return strings;
	}

	@Override
	public List<String> autocompleteSpecies(String term, Crop crop, List<String> genus) {
		List<String> strings;

		if (!genus.isEmpty()) {
			strings = taxonomy2Repository.autocompleteSpeciesByGenus(term + "%", genus, new PageRequest(0, 10));
		} else if (crop != null) {
			strings = taxonomy2Repository.autocompleteSpeciesByCrop(term + "%", crop, new PageRequest(0, 10));
		} else {
			strings = taxonomy2Repository.autocompleteSpecies(term + "%", new PageRequest(0, 10));
		}

		return strings;
	}

	@Override
	public List<String> autocompleteTaxonomy(String term) {
		return taxonomy2Repository.autocompleteTaxonomy("%" + term + "%", new PageRequest(0, 10));
	}

	@Override
	// @Transactional(noRollbackFor = AssertionFailure.class)
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Taxonomy2 ensureTaxonomy2(String genus, String species, String spAuthor, String subtaxa, String subtAuthor) {
		if (StringUtils.isBlank(genus)) {
			throw new RuntimeException("Genus can't be blank");
		}

		// Capitalize
		genus = WordUtils.capitalizeFully(genus).trim();

		species = StringUtils.defaultIfBlank(species, "sp.");
		species = species.trim().toLowerCase();

		spAuthor = StringUtils.defaultIfBlank(spAuthor, "").trim();
		subtaxa = StringUtils.defaultIfBlank(subtaxa, "").trim();

		if (subtaxa != null) {
			subtaxa = subtaxa.toLowerCase();
		}

		subtAuthor = StringUtils.defaultIfBlank(subtAuthor, "").trim();

		if (StringUtils.equalsIgnoreCase(species.trim(), "sp")) {
			species = "sp.";
		}

		final Taxonomy2 existing = taxonomy2Repository.findByGenusAndSpeciesAndSpAuthorAndSubtaxaAndSubtAuthor(genus, species, spAuthor, subtaxa, subtAuthor);
		if (existing == null) {
			try {
				Taxonomy2 taxonomy = internalEnsure(genus, species, spAuthor, subtaxa, subtAuthor);
				return taxonomy;
			} catch (InterruptedException e) {
				LOG.warn("Thread interrupted while waiting for taxonomy lock.", e);
				throw new RuntimeException(e);
			}
		}
		return existing;
	}

	private synchronized Taxonomy2 internalEnsure(String genus, String species, String spAuthor, String subtaxa, String subtAuthor) throws InterruptedException {
		Long taxSpeciesId = null, taxGenusId = null;

		// LOCK
		if (taxonomyUpdateLock.tryLock(10, TimeUnit.SECONDS)) {
			try {

				if (!StringUtils.equals(species, "sp.") && !(subtaxa.equals("") && spAuthor.equals("") && subtAuthor.equals(""))) {
					final Taxonomy2 speciesTaxa = internalEnsure(genus, species, "", "", "");
					taxSpeciesId = speciesTaxa.getId();
					taxGenusId = speciesTaxa.getTaxGenus();
				} else if (!StringUtils.equals(species, "sp.") && subtaxa.equals("") && spAuthor.equals("") && subtAuthor.equals("")) {
					final Taxonomy2 genusTaxa = internalEnsure(genus, "sp.", "", "", "");
					taxGenusId = genusTaxa.getId();
					taxSpeciesId = genusTaxa.getId();
				}

				Taxonomy2 taxonomy = null;
				try {
					taxonomy = taxonomy2Repository.findByGenusAndSpeciesAndSpAuthorAndSubtaxaAndSubtAuthor(genus, species, spAuthor, subtaxa, subtAuthor);
				} catch (final Throwable e) {
					LOG.info("Taxonomy not found: " + e.getMessage());
				}

				if (taxonomy != null) {
					return taxonomy;
				} else {
					LOG.info("Adding new taxonomic name: " + genus + " " + species + " " + spAuthor + " " + subtaxa + " " + subtAuthor);
					taxonomy = new Taxonomy2();
					taxonomy.setGenus(genus);
					taxonomy.setSpecies(species);
					taxonomy.setSpAuthor(spAuthor);
					taxonomy.setSubtaxa(subtaxa);
					taxonomy.setSubtAuthor(subtAuthor);
					taxonomy.setTaxGenus(taxGenusId);
					taxonomy.setTaxSpecies(taxSpeciesId);

					try {
						taxonomy = taxonomy2Repository.save(taxonomy);

						if (taxGenusId == null) {
							taxonomy.setTaxGenus(taxonomy.getId());
							taxonomy = taxonomy2Repository.save(taxonomy);
						}
						if (taxSpeciesId == null) {
							taxonomy.setTaxSpecies(taxonomy.getId());
							taxonomy = taxonomy2Repository.save(taxonomy);
						}

						// Update crop taxonomy lists
						cropService.updateCropTaxonomyLists(taxonomy);
						
						return taxonomy;

					} catch (final Throwable e) {
						LOG.warn("Error " + e.getMessage() + " :" + taxonomy);
						throw new RuntimeException(e.getMessage());
					}
				}

			} finally {
				taxonomyUpdateLock.unlock();
			}
		} else {
			throw new RuntimeException("Could not persist Taxonomy2, lock wait timeout occured.");
		}
	}

	@Override
	public long getTaxonomy2Id(String genus) {
		return taxonomy2Repository.findByGenusAndSpeciesAndSpAuthorAndSubtaxaAndSubtAuthor(genus, "sp.", "", "", "").getId();
	}

	@Override
	public long getTaxonomy2Id(String genus, String species) {
		final Taxonomy2 tax = taxonomy2Repository.findByGenusAndSpeciesAndSpAuthorAndSubtaxaAndSubtAuthor(genus, species, "", "", "");
		return tax.getId();
	}

	@Override
	public Taxonomy2 get(String genus, String species) {
		return taxonomy2Repository.findByGenusAndSpeciesAndSpAuthorAndSubtaxaAndSubtAuthor(genus, species, "", "", "");
	}

	@Override
	public long countTaxonomy2() {
		return taxonomy2Repository.count();
	}

	@Override
	public Taxonomy2 get(String genus) {
		return taxonomy2Repository.findByGenusAndSpeciesAndSpAuthorAndSubtaxaAndSubtAuthor(genus, "sp.", "", "", "");
	}
}
