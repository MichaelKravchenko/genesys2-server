/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.oauth;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.genesys2.server.model.BusinessModel;

/**
 * Verification code model
 * 
 * @author matijaobreza
 */
@Entity
@Table(name = "oauthcode")
public class OAuthCode extends BusinessModel {
	private static final long serialVersionUID = 8018089692223912764L;

	@Column(name = "code", length = 32, unique = true, nullable = false)
	private String code;

	@Lob
	@Column(name = "authentication")
	private byte[] authentication;

	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@Column(length = 36, nullable = false)
	private String userUuid;

	@Column(length = 100, nullable = false)
	private String clientId;

	@Column(length = 200)
	private String redirectUri;

	@Column(length = 100)
	private String scopes;

	public OAuthCode() {
		createdDate = new Date();
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setAuthentication(byte[] authentication) {
		this.authentication = authentication;
	}

	public byte[] getAuthentication() {
		return authentication;
	}

	public void setUserUuid(String uuid) {
		this.userUuid = uuid;
	}

	public String getUserUuid() {
		return userUuid;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientId() {
		return clientId;
	}

	public void setRedirectUri(String redirectUri) {
		this.redirectUri = redirectUri;
	}

	public String getRedirectUri() {
		return redirectUri;
	}

	public String getScopes() {
		return this.scopes;
	}

	public void setScopes(String scopes) {
		this.scopes = scopes;
	}
}
