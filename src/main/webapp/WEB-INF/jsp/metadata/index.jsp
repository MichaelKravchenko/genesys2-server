<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="metadata.page.title" /></title>
</head>
<body>
	<h1>
		<spring:message code="metadata.page.title" />
	</h1>

	<div class="main-col-header clearfix">
	<div class="nav-header pull-left">
		<div class="results"><spring:message code="paged.totalElements" arguments="${pagedData.totalElements}" /></div>
		<form method="get" action="">
			<div class="pagination">
				<spring:message code="paged.pageOfPages" arguments="${pagedData.number+1},${pagedData.totalPages}" />
				<a href="<spring:url value=""><spring:param name="page" value="${pagedData.number eq 0 ? 1 : pagedData.number}" /></spring:url>"><spring:message code="pagination.previous-page" /></a>
				<input class="form-control" style="display: inline; max-width: 5em; text-align: center" type="text" name="page" placeholder="${pagedData.number + 1}" />
				<a href="<spring:url value=""><spring:param name="page" value="${pagedData.number+2}" /></spring:url>"><spring:message code="pagination.next-page" /></a>
			</div>
		</form>
	</div>
	</div>
	
	<ul class="funny-list">
		<c:forEach items="${pagedData.content}" var="metadata" varStatus="status">
			<li class="clearfix ${status.count % 2 == 0 ? 'even' : 'odd'}"><a class="show pull-left" href="/data/view/${metadata.id}"><c:out value="${metadata.title}" /></a>
<div class="pull-right"><c:out value="${metadata.instituteCode}" /></div></li>
		</c:forEach>
	</ul>

</body>
</html>