/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.genesys;

import java.text.MessageFormat;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.genesys2.server.model.AclAwareModel;
import org.genesys2.server.model.VersionedAuditedModel;
import org.genesys2.server.model.impl.GeoReferencedEntity;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "metadata")
public class Metadata extends VersionedAuditedModel implements AclAwareModel, GeoReferencedEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = -7551263239168816406L;

	@Column(length = 7)
	private String instituteCode;

	@Column(nullable = false, length = 128)
	private String title;

	@Column(name = "startDate", length = 10)
	private String SDate;

	@Column(name = "endDate", length = 10)
	private String EDate;

	@Column(length = 128)
	private String location;

	private Double longitude;
	private Double latitude;
	private Double elevation;

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String citation;

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String description;

	@Column(length = 36, unique = true, nullable = false)
	private String uuid;

	public Metadata() {
	}

	@PrePersist
	void ensureUUID() {
		if (this.uuid == null) {
			this.uuid = UUID.nameUUIDFromBytes(title.getBytes()).toString();
		}
	}

	public String getInstituteCode() {
		return this.instituteCode;
	}

	public void setInstituteCode(final String instituteCode) {
		this.instituteCode = instituteCode;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getSDate() {
		return this.SDate;
	}

	public void setSDate(final String SDate) {
		this.SDate = SDate;
	}

	public String getEDate() {
		return this.EDate;
	}

	public void setEDate(final String EDate) {
		this.EDate = EDate;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(final String location) {
		this.location = location;
	}

	@Override
	public Double getLongitude() {
		return this.longitude;
	}

	public void setLongitude(final Double longitude) {
		this.longitude = longitude;
	}

	@Override
	public Double getLatitude() {
		return this.latitude;
	}

	public void setLatitude(final Double latitude) {
		this.latitude = latitude;
	}

	@Override
	public Double getElevation() {
		return this.elevation;
	}

	public void setElevation(Double elevation) {
		this.elevation = elevation;
	}

	public String getCitation() {
		return this.citation;
	}

	public void setCitation(final String citation) {
		this.citation = citation;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	@Override
	public String toString() {
		return MessageFormat.format("Metadata id={0,number,#} inst={2} title={1}", id, title, instituteCode);
	}

}
