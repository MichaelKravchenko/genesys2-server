/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain;

import java.util.Collection;
import java.util.List;

import javax.persistence.OrderBy;

import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.AccessionRemark;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AccessionRemarkRepository extends JpaRepository<AccessionRemark, Long> {

	@OrderBy("fieldName")
	List<AccessionRemark> findByAccession(Accession accession);

	@Modifying
	@Query("delete from AccessionRemark ar where ar.accession in (from Accession a where a.id in ( :ids ))")
	void deleteForAccessions(@Param("ids") Collection<Long> accessionIds);
}
