/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * http://www.sitemaps.org/protocol.html
 *
 * @author Matija Obreza, matija.obreza@croptrust.org
 */
@Controller
@RequestMapping("/sitemap.xml")
public class SitemapXMLController {

	@Value("${base.url}")
	private final String baseUrl = "http://localhost:8080";

	private static class SitemapPage {
		String url;
		String freq = "daily";
		Double priority;

		public SitemapPage(String url) {
			this.url = url;
		}

		public SitemapPage(String url, double priority) {
			this.url = url;
			this.priority = priority;
		}

		public SitemapPage(String url, String changeFrequency, double priority) {
			this.url = url;
			this.freq = changeFrequency;
			this.priority = priority;
		}

	}

	private final SitemapPage[] sitemapPages = new SitemapPage[] { new SitemapPage("/welcome", 1.0), new SitemapPage("/content/about", "monthly", 0.2),
			new SitemapPage("/wiews/active", 1.0), new SitemapPage("/geo/", 1.0), new SitemapPage("/explore", 1.0), new SitemapPage("/org/") };

	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody
	String sitemapXml(HttpServletResponse response) {
		response.setContentType("text/xml");

		final StringBuffer sb = new StringBuffer();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<urlset xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\" xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">");
		for (final SitemapPage page : sitemapPages) {
			sb.append(" <url>");
			sb.append(" <loc>").append(baseUrl).append(page.url).append("</loc>");
			if (page.freq != null) {
				sb.append(" <changefreq>").append(page.freq).append("</changefreq>");
			}
			if (page.priority != null) {
				sb.append(" <priority>").append(page.priority).append("</priority>");
			}
			sb.append(" </url>");
		}
		sb.append("</urlset>");
		return sb.toString();

	}
}
