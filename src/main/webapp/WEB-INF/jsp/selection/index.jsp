<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="selection.page.title" /></title>
</head>
<body>
	<h1>
		<spring:message code="selection.page.title" />
	</h1>

	<c:if test="${pagedData == null}">
		<div class="alert alert-info">
			<spring:message code="selection.empty-list-warning" />
		</div>
	</c:if>

	<c:if test="${pagedData != null}">
		
		<div class="main-col-header clearfix">
		<div class="nav-header">
			<form class="pull-right form-horizontal" method="post" action="/sel/dwca">
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				<div class="row" style="margin-top: 2em;">
					<div class="col-sm-4">
						<button class="btn btn-default" type="submit"><spring:message code="filter.download-dwca" /></button>
					</div>
				</div>
			</form>

			<div class="results"><spring:message code="accessions.number" arguments="${pagedData.totalElements}" /></div>
			
			<div class="pagination">
				<spring:message code="paged.pageOfPages" arguments="${pagedData.number+1},${pagedData.totalPages}" />
				<a href="<spring:url value=""><spring:param name="page" value="${pagedData.number eq 0 ? 1 : pagedData.number}" /><spring:param name="filter" value="${jsonFilter}" /></spring:url>"><spring:message code="pagination.previous-page" /></a>
				<a href="<spring:url value=""><spring:param name="page" value="${pagedData.number+2}" /><spring:param name="filter" value="${jsonFilter}" /></spring:url>"><spring:message code="pagination.next-page" /></a>
			</div>
		</div>
		</div>

		<table class="accessions">
			<thead>
				<tr>
					<td class="idx-col"></td>
					<td />
					<td><spring:message code="accession.accessionName" /></td>
					<td><spring:message code="accession.taxonomy" /></td>
					<td class="notimportant"><spring:message code="accession.origin" /></td>
					<td class="notimportant"><spring:message code="accession.holdingInstitute" /></td>
					<%-- <td class="notimportant"><spring:message code="accession.holdingCountry" /></td> --%>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${pagedData.content}" var="accession" varStatus="status">
					<tr id="a${accession.id}" class="acn targeted ${status.count % 2 == 0 ? 'even' : 'odd'}">
						<td class="idx-col">${status.count + pagedData.size * pagedData.number}</td>
						<td class="sel ${selection.containsId(accession.id) ? 'picked' : ''}" x-aid="${accession.id}"></td>
						<td><a href="<c:url value="/acn/id/${accession.id}" />"><b><c:out value="${accession.accessionName}" /></b></a></td>
						<td><c:out value="${accession.taxonomy.taxonName}" /></td>
						<td class="notimportant"><c:out value="${accession.countryOfOrigin.getName(pageContext.response.locale)}" /></td>
						<td class="notimportant"><a href="<c:url value="/wiews/${accession.institute.code}" />"><c:out value="${accession.institute.code}" /></a></td>
						<%-- <td class="notimportant"><a href="<c:url value="/geo/${accession.institute.country.code3}" />"><c:out value="${accession.institute.country.getName(pageContext.response.locale)}" /></a></td> --%>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		
		<form method="post" action="<c:url value="/sel/order" />" class="form-vertical">
			<div class="form-actions">
				<a href="<c:url value="/sel/" />"><button class="btn" type="button">Refresh</button></a>
				<button class="btn btn-primary" type="submit">Send request for germplasm</button>
				<a href="<c:url value="/sel/clear" />"><button class="btn" type="button">Clear list</button></a>
				<a href="<c:url value="/sel/map" />"><button class="btn" type="button">Display on map</button></a>
			</div>
            <!-- CSRF protection -->
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		</form>
		
	</c:if>

<%--
	<c:if test="${pagedData eq null or pagedData.number eq 0}">
		<h4 style="margin-top: 3em">Add multiple accessions</h4>
		<form method="post" action="<c:url value="/sel/add-many" />" class="form-horizontal">
			<div class="control-group">
				<label for="accessionIds" class="control-label"><spring:message code="selection.add-many.accessionIds" /></label>
				<div class="controls">
					<textarea class="form-control" placeholder="12345 123545 423231" name="accessionIds"></textarea>
				</div>
			</div>
			<div class="form-actions clearfix">
				<input type="submit" class="btn" value="<spring:message code="selection.add-many" />" />
			</div>
            <!-- CSRF protection -->
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		</form>
	</c:if>
--%>
</body>
</html>